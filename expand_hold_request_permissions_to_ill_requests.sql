begin tran

insert into polaris.Polaris.PermissionGroups
select cr2.ControlRecordID, pd2.PermissionID, pg.GroupID
from polaris.polaris.PermissionGroups pg
join polaris.polaris.ControlRecords cr
	on cr.ControlRecordID = pg.ControlRecordID
join polaris.polaris.ControlRecordDefs crd
	on crd.ControlRecordDefID = cr.ControlRecordDefID
join polaris.Polaris.PermissionDefs pd
	on pd.ControlRecordDefID = cr.ControlRecordDefID and pg.PermissionID = pd.PermissionID and pd.PermissionNameID = 3
join polaris.polaris.PermissionNames pn
	on pn.PermissionNameID = pd.PermissionNameID
join Polaris.Polaris.ControlRecords cr2
	on cr2.OrganizationID = cr.OrganizationID
join polaris.Polaris.PermissionDefs pd2
	on pd2.ControlRecordDefID = cr2.ControlRecordDefID
where crd.ControlRecordDefID = 20 -- Hold Request
	and cr2.ControlRecordDefID = 21 -- ILL Request
	and pg.GroupID > 1 -- Not Administrator group
	and not exists ( select 1 from polaris.polaris.permissiongroups pg2 where pg2.ControlRecordID = cr2.ControlRecordID and pg2.PermissionID = pd2.PermissionID and pg2.GroupID = pg.GroupID ) -- Group doesn't already have permission
	--and pg.GroupID = 103 and cr.OrganizationID = 40


--rollback
--commit