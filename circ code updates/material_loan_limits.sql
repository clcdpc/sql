-- =CONCATENATE("insert into @newmll values (", A2, ", ", C2, ", ", E2, ", ", G2, ", ", H2, ")")

declare @newmll table ( orgid int, pcid int, mtid int, MaxItems int, maxrequests int )

-- PASTE EXCEL DATA BELOW
insert into @newmll values (29, 17, 10, 1, 0)
insert into @newmll values (29, 1, 10, 1, 0)
insert into @newmll values (29, 6, 10, 1, 0)
insert into @newmll values (29, 10, 10, 1, 0)
insert into @newmll values (29, 14, 10, 1, 0)
insert into @newmll values (29, 15, 10, 1, 0)
insert into @newmll values (30, 17, 10, 1, 0)
insert into @newmll values (30, 1, 10, 1, 0)
insert into @newmll values (30, 6, 10, 1, 0)
insert into @newmll values (30, 10, 10, 1, 0)
insert into @newmll values (30, 14, 10, 1, 0)
insert into @newmll values (30, 15, 10, 1, 0)
insert into @newmll values (76, 17, 10, 1, 0)
insert into @newmll values (76, 1, 10, 1, 0)
insert into @newmll values (76, 6, 10, 1, 0)
insert into @newmll values (76, 10, 10, 1, 0)
insert into @newmll values (76, 14, 10, 1, 0)
insert into @newmll values (76, 15, 10, 1, 0)
insert into @newmll values (31, 17, 10, 1, 0)
insert into @newmll values (31, 1, 10, 1, 0)
insert into @newmll values (31, 6, 10, 1, 0)
insert into @newmll values (31, 10, 10, 1, 0)
insert into @newmll values (31, 14, 10, 1, 0)
insert into @newmll values (31, 15, 10, 1, 0)

-- PASTE EXCEL DATA ABOVE

begin tran

update mll
set mll.MaxItems = n.MaxItems
	,mll.MaxRequestItems = n.maxrequests
from Polaris.Polaris.MaterialLoanLimits mll
join @newmll n
	on n.orgid = mll.OrganizationID and n.pcid = mll.PatronCodeID and n.mtid = mll.MaterialTypeID

--rollback
--commit