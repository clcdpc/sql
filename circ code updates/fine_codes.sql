-- =CONCATENATE("insert into @newf values (", A2, ", ", C2, ", ", E2, ", ", G2, ", ", H2, ", ", I2, ")")

declare @newf table (orgid int, pcid int, fcid int, amount decimal(18,2), maxfine decimal(18,2), graceunits int)

-- PASTE EXCEL DATA BELOW

-- PASTE EXCEL DATA ABOVE

begin tran

update f
set f.amount = s.amount
	,f.MaximumFine = s.maxfine
	,f.graceunits = s.graceunits
from Polaris.Polaris.Fines f
join @newf s
	on s.orgid = f.OrganizationID and s.pcid = f.PatronCodeID and s.fcid = f.FineCodeID

--rollback
--commit