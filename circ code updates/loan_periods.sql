-- =CONCATENATE("insert into @newlp values (",A2, ", ", C2, ", ", E2, ", ", G2, ")")

declare @newlp table (orgid int, pcid int, lpid int, units int)

-- PASTE EXCEL DATA BELOW

-- PASTE EXCEL DATA ABOVE

begin tran

update lp
set lp.units = s.units
from Polaris.Polaris.LoanPeriods lp
join @newlp s
	on s.orgid = lp.OrganizationID and s.pcid = lp.PatronCodeID and s.lpid = lp.LoanPeriodCodeID

--rollback
--commit

