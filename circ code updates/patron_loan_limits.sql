-- =CONCATENATE("insert into @newpll values (", A2, ", ", C2, ", ", E2, ", ", F2, ", ", G2, ", ", H2, ", ", I2, ", ", J2, ")")

declare @newpll table (orgid int, pcid int, maxfine decimal(18,2), minfine decimal(18,2), totalitems int, totaloverdue int, totalholds int, totalill int)

-- PASTE EXCEL DATA BELOW

-- PASTE EXCEL DATA ABOVE

begin tran

update pll
set pll.MaxFine = n.MaxFine
	,pll.MinFine = n.MinFine
	,pll.TotalItems = n.TotalItems
	,pll.TotalOverDue = n.TotalOverDue
	,pll.TotalHolds = n.TotalHolds
	,pll.TotalILL = n.totalill
from Polaris.Polaris.PatronLoanLimits pll
join @newpll n
	on n.OrgID = pll.OrganizationID and n.pcid = pll.PatronCodeID

--rollback
--commit