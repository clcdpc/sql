begin tran

update pr
set pr.ExpirationDate = dateadd(year, 99, pr.AddrCheckDate)
from polaris.polaris.Patrons p
join Polaris.Polaris.PatronRegistration pr
	on pr.PatronID = p.PatronID
where p.OrganizationID = 115

--rollback
--commit