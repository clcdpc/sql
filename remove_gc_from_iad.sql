begin tran

delete from Polaris.Polaris.ItemAvailabilityDisplay where BranchLocationOrgID = 29 and OrganizationID != 76

;with iad2
as
(
select *, RANK() over(partition by OrganizationID, ItemAvailabilityLevelID order by SortOrder, BranchLocationOrgID) [neworder]
from Polaris.Polaris.ItemAvailabilityDisplay 
)

update iad
set iad.SortOrder = iad2.neworder
from Polaris.Polaris.ItemAvailabilityDisplay iad
join iad2 on 
	iad.OrganizationID = iad2.OrganizationID and iad.BranchLocationOrgID = iad2.BranchLocationOrgID and iad.ItemAvailabilityLevelID = iad2.ItemAvailabilityLevelID

select * from Polaris.Polaris.ItemAvailabilityDisplay iad order by iad.OrganizationID, iad.ItemAvailabilityLevelID, iad.SortOrder

--rollback
--commit