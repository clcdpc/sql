--select * from Polaris.polaris.Organizations order by Name

declare @orgs table (orgid int)
insert into @orgs values (51),(73)

declare @closedDateRangeStart date = '3/14/2021'
declare @newHoldTillDate datetime = '3/27/2021' 

set @newHoldTillDate = dateadd(second, 86399, @newHoldTillDate)

begin tran

update shr
set shr.HoldTillDate = @newHoldTillDate
	,shr.SysHoldStatusID = 6
from Polaris.polaris.SysHoldRequests shr
join Polaris.Polaris.Organizations o
	on isnull(shr.NewPickupBranchID, shr.PickupBranchID) = o.OrganizationID
where shr.HoldTillDate between @closedDateRangeStart and @newHoldTillDate
	and shr.TrappingItemRecordID is not null
	and shr.SysHoldStatusID in (6,8)
	and o.OrganizationID in (select * from @orgs)

--rollback
--commit