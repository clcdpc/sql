USE [JitbitHelpDesk]
GO

/****** Object:  UserDefinedFunction [dbo].[StatsForPRTG]    Script Date: 11/2/2015 1:23:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[StatsForPRTG]
(
)
RETURNS 
@dt TABLE 
(
	field varchar(max),
	val decimal(18,1)
)
AS
BEGIN
	declare @firstOfLastMonth datetime = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0)
	declare @lastOfLastMonth datetime = DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) + .999999

	--Number of tickets addressed in the past month
	;with data
	as
	(
		SELECT cast(count(distinct(hdi.IssueID)) as DECIMAL(18,1)) [TicketsAddressed]
		FROM [JitbitHelpDesk].[dbo].[hdIssues] hdi
		join hdCategories hdc on
			hdi.CategoryID = hdc.CategoryID
		join hdComments hc on
			hc.IssueID = hdi.IssueID
		WHERE hdi.StartDate between @firstOfLastMonth and @lastOfLastMonth
		and 
		hdc.SectionID in ( 5,6 )
		and
		hc.IsSystem = 0
	)

	insert into @dt
	select field, val
	from data
	unpivot
	(
		val
		for field in ( [TicketsAddressed] )
	) u;

	--Find information on issues resolved within the past month
	;with data
	as
	(
		SELECT cast(COUNT(distinct hdi.IssueID) as DECIMAL(18,1)) [TicketsResolvedLastMonth]
		FROM [JitbitHelpDesk].[dbo].[hdIssues] hdi
		join hdCategories hdc on
			hdi.CategoryID = hdc.CategoryID
		join hdComments hc on
			hc.IssueID = hdi.IssueID
		WHERE hdi.ResolvedDate between @firstOfLastMonth and @lastOfLastMonth
		and 
		hdc.SectionID in ( 5,6 )
		and
		hc.IsSystem = 0
	)

	insert into @dt
	select field, val
	from data
	unpivot
	(
		val
		for field in ( [TicketsResolvedLastMonth] )
	) u;

	--Num Critical Alerts Addressed
	;with data
	as
	(
		select cast(COUNT(distinct hdi.IssueID) as DECIMAL(18,1)) [CriticalAlertsAddressed]
		from hdIssues hdi
		where (hdi.CategoryID = 29 or hdi.Priority = 2) and hdi.StartDate between @firstOfLastMonth and @lastOfLastMonth
	)

	insert into @dt
	select field, val
	from data
	unpivot
	(
		val
		for field in ( [CriticalAlertsAddressed] )
	) u;

	--Average response and open times
	;with data
	as
	(
	select	ISNULL(cast(avg(case when c.CategoryID = 29 or i.Priority = 2 then DATEDIFF(minute, i.IssueDate, i.StartDate) end) as DECIMAL(18,1)), 0) [AvgCriticalResponseMinutes],		
			ISNULL(cast(avg(case when c.CategoryID = 29 or i.Priority = 2 then DATEDIFF(minute, i.IssueDate, i.ResolvedDate) end) as DECIMAL(18,1)), 0) [AvgCriticalOpenMinutes],
			cast(avg(case when c.CategoryID != 29 or i.Priority != 2 then DATEDIFF(second, i.IssueDate, i.StartDate) / 3600.0 end) as DECIMAL(18,1)) [AvgResponseHours],
			cast(avg(case when c.CategoryID != 29 or i.Priority != 2 then DATEDIFF(second, i.IssueDate, i.ResolvedDate) / 3600.0 end) as DECIMAL(18,1)) [AvgOpenHours]	
	from hdIssues i
	join hdCategories c on
		i.CategoryID = c.CategoryID
	where c.SectionID in ( 5,6 )
	and i.IssueDate between @firstOfLastMonth and @lastOfLastMonth
	)

	insert into @dt
	select u.field, u.val
	from data
	unpivot
	(
		val
		for field in ( [AvgCriticalResponseMinutes], [AvgCriticalOpenMinutes], [AvgResponseHours], [AvgOpenHours] )
	) u;

	--Finding number of tickets that were opened before the 1st of last month
	;with data
	as
	(
		SELECT	cast(COUNT(distinct hdi.IssueID) as DECIMAL(18,1)) [TicketsOpenedBeforeFirstOfMonth]
		FROM [JitbitHelpDesk].[dbo].[hdIssues] hdi
		join hdCategories hdc on
			hdi.CategoryID = hdc.CategoryID
		WHERE issuedate < @firstOfLastMonth
		and hdc.SectionID in ( 5,6 )
		and hdi.StatusID != 3
	)

	insert into @dt
	select field, val
	from data
	unpivot
	(
		val
		for field in ( [TicketsOpenedBeforeFirstOfMonth] )
	) u;

	--Open tickets before 1st of Previous Month NUM AT VENDOR
	;with data
	as
	(
		SELECT	cast(COUNT(distinct hdi.IssueID) as DECIMAL(18,1)) [TicketsOpenedBeforeFirstOfMonthAtVendor]
		FROM [JitbitHelpDesk].[dbo].[hdIssues] hdi
		join hdCategories hdc on
			hdi.CategoryID = hdc.CategoryID
		left join hdCustomFieldValues fv on
			hdi.IssueID = fv.IssueID
		WHERE issuedate < @firstOfLastMonth
		and hdc.SectionID in ( 5,6 )
		and hdi.StatusID != 3
		and (fv.FieldID = 5 and fv.Value = 1)
	)

	insert into @dt
	select field, val
	from data
	unpivot
	(
		val
		for field in ( [TicketsOpenedBeforeFirstOfMonthAtVendor] )
	) u;


	--Open tickets before 1st of Previous Month Known Bug or Resolved w/Upgrade
	;with data
	as
	(
		SELECT	cast(COUNT(distinct hdi.IssueID) as DECIMAL(18,1)) [TicketsOpenedBeforeFirstOfMonthBugOrPendingUpgrade]
		FROM [JitbitHelpDesk].[dbo].[hdIssues] hdi
		join hdCategories hdc on
			hdi.CategoryID = hdc.CategoryID
		WHERE issuedate < @firstOfLastMonth
		and hdc.SectionID in ( 5,6 )
		and hdi.StatusID in ( 4,6 )
	)

	insert into @dt
	select field, val
	from data
	unpivot
	(
		val
		for field in ( [TicketsOpenedBeforeFirstOfMonthBugOrPendingUpgrade] )
	) u;

	--;with data
	--as
	--(
	--	select COUNT(*) as [PACLoginsLastMonthLastYear]
	--	from PolarisTransactions.polaris.TransactionHeaders th 
	--	where th.TransactionTypeID = 2200 and th.TransactionDate between DATEADD(year, -1, @firstOfLastMonth) and DATEADD(year, -1, @lastOfLastMonth)
	--)

	--insert into @dt
	--select field, val
	--from data
	--unpivot
	--(
	--	val
	--	for field in ( [PACLoginsLastMonthLastYear] )
	--) u;

	--;with data
	--as
	--(
	--	select COUNT(*) [PACLoginsLastMonthThisYear]
	--	from PolarisTransactions.polaris.TransactionHeaders th 
	--	where th.TransactionTypeID = 2200 and th.TransactionDate between @firstOfLastMonth and @lastOfLastMonth
	--)

	--insert into @dt
	--select field, val
	--from data
	--unpivot
	--(
	--	val
	--	for field in ( [PACLoginsLastMonthThisYear] )
	--) u;

	
	RETURN 
END

GO


