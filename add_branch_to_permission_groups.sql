begin tran

declare @oldBranch int = 9
declare @newBranch int = 126

insert into Polaris.Polaris.PermissionGroups
select cr.ControlRecordID, data.PermissionID, data.GroupID
from 
(
	select pg.GroupID, pd.PermissionID, cr.ControlRecordDefID
	from Polaris.Polaris.PermissionGroups pg
	join Polaris.Polaris.PermissionDefs pd on
		pg.PermissionID = pd.PermissionID
	join Polaris.Polaris.ControlRecords cr on
		cr.ControlRecordID = pg.ControlRecordID and pd.ControlRecordDefID = cr.ControlRecordDefID
	where cr.OrganizationID = @oldBranch and pg.GroupID > 12
) data 
join Polaris.Polaris.ControlRecords cr on
	data.ControlRecordDefID = cr.ControlRecordDefID
where cr.OrganizationID = @newBranch and not exists ( select 1 from Polaris.Polaris.PermissionGroups pg where pg.GroupID = data.GroupID and pg.ControlRecordID = cr.ControlRecordID and pg.PermissionID = data.PermissionID )

--rollback
--commit