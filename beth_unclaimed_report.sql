-- add last activity date

declare @libraryId int = 78
declare @beginDate date = '1/1/2023'
declare @endDate date = '1/1/2024'
declare @minUnclaimeds int = 5
declare @percentCutoff int = 70

create table #data ( TransactionID int, PatronID int, TransactionTypeID int, SysHoldRequestID int, PickupBranchID int, TranClientDate datetime )

declare @endDate2 date = case when @endDate > getdate() then getdate() else @endDate end

insert into #data (TransactionID, TransactionTypeID, TranClientDate, PickupBranchID)
select th.TransactionID, th.TransactionTypeID, th.TranClientDate, th.OrganizationID
from PolarisTransactions.polaris.TransactionHeaders (nolock) th
join polaris.polaris.organizations (nolock) o
	on o.OrganizationID = th.OrganizationID
where th.TransactionTypeID in (6001,6008)
	and th.TranClientDate between @beginDate and @endDate2
	and (th.TransactionTypeID = 6008 or o.ParentOrganizationID = @libraryId)

update th
set  th.PatronID = td_p.numValue 
	,th.SysHoldRequestID = td_shr.numValue
	,th.PickupBranchID = isnull(td_pub.numValue, th.PickupBranchID)
from #data th
join PolarisTransactions.polaris.TransactionDetails (nolock) td_p
	on td_p.TransactionID = th.TransactionID and td_p.TransactionSubTypeID = 6
join PolarisTransactions.polaris.TransactionDetails (nolock) td_shr
	on td_shr.TransactionID = th.TransactionID and td_shr.TransactionSubTypeID = 233 and td_shr.numValue is not null
left join PolarisTransactions.polaris.TransactionDetails (nolock) td_pub
	on td_pub.TransactionID = th.TransactionID and td_pub.TransactionSubTypeID = 130
left join PolarisTransactions.polaris.TransactionDetails (nolock) td_r
	on td_r.TransactionID = th.TransactionID and td_r.TransactionSubTypeID = 124 and td_r.numValue = 1
where td_r.TransactionID is null

select *
from (
	select d.PatronID
			,o.Name [PickupBranch]
			,count(distinct d.TransactionID) [TotalHeld]
			,count(case d.TransactionTypeID when 6001 then 1 end) [CheckedOut]
			,count(case d.TransactionTypeID when 6008 then 1 end) [Unclaimed]
			,count(distinct case when d.TransactionTypeID = 6008 then datepart(week, d.TranClientDate) + '' + datepart(year, d.tranclientdate) end) [WeeksWithUnclaimed]
			,count(distinct case when d.TransactionTypeID = 6001 then datepart(week, d.TranClientDate) + '' + datepart(year, d.tranclientdate) end) [WeeksWithCKO]
			,cast(count(distinct case when d.TransactionTypeID = 6008 then datepart(week, d.TranClientDate) + '' + datepart(year, d.tranclientdate) end) / 1.0 / count(distinct datepart(week, d.TranClientDate) + '' + datepart(year, d.tranclientdate)) * 100 as int) [PercentBadWeeks]
			,cast(count(distinct case when d.TransactionTypeID = 6008 then datepart(week, d.TranClientDate) + '' + datepart(year, d.tranclientdate) end) / 1.0 / datediff(week, @beginDate, @endDate2) * 100 as int) [PercentBadWeeksAbsolute]
			,cast(cast(count(case d.TransactionTypeID when 6008 then 1 end) / 1.0 / count(*) as decimal(18,2)) * 100 as int) [PercentUnclaimed]
			,min(p.LastActivityDate) [LastActivityDate]

	from #data d
	join polaris.polaris.organizations (nolock) o
		on o.OrganizationID = d.PickupBranchID
	left join polaris.polaris.patrons (nolock) p
		on p.PatronID = d.PatronID
	where o.ParentOrganizationID = @libraryId
	group by d.PatronID, o.Name
) d
where d.Unclaimed >= @minUnclaimeds and d.PercentUnclaimed >= @percentCutoff
order by d.Unclaimed desc

drop table #data