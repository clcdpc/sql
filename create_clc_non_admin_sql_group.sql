CREATE LOGIN [CLCDPC\CLCStaffMembersNonAdmin] FROM WINDOWS WITH DEFAULT_DATABASE=[Polaris], DEFAULT_LANGUAGE=[us_english]

use Polaris
go
CREATE USER [CLCDPC\CLCStaffMembersNonAdmin] FOR LOGIN [CLCDPC\CLCStaffMembersNonAdmin]
CREATE ROLE [CLC_Regular_Staff_Accounts_READONLY]
ALTER ROLE [CLC_Regular_Staff_Accounts_READONLY] ADD MEMBER [CLCDPC\CLCStaffMembersNonAdmin]

DENY ALTER, INSERT, DELETE, UPDATE ON SCHEMA :: dbo TO [CLC_Regular_Staff_Accounts_READONLY]
DENY ALTER, INSERT, DELETE, UPDATE ON SCHEMA :: polaris TO [CLC_Regular_Staff_Accounts_READONLY]
GRANT SELECT, EXECUTE, view definition ON SCHEMA :: dbo TO [CLC_Regular_Staff_Accounts_READONLY]
GRANT SELECT, EXECUTE, view definition ON SCHEMA :: polaris TO [CLC_Regular_Staff_Accounts_READONLY]

use PolarisTransactions
go
CREATE USER [CLCDPC\CLCStaffMembersNonAdmin] FOR LOGIN [CLCDPC\CLCStaffMembersNonAdmin]
CREATE ROLE [CLC_Regular_Staff_Accounts_READONLY]
ALTER ROLE [CLC_Regular_Staff_Accounts_READONLY] ADD MEMBER [CLCDPC\CLCStaffMembersNonAdmin]

DENY ALTER, INSERT, DELETE, UPDATE ON SCHEMA :: dbo TO [CLC_Regular_Staff_Accounts_READONLY]
DENY ALTER, INSERT, DELETE, UPDATE ON SCHEMA :: polaris TO [CLC_Regular_Staff_Accounts_READONLY]
GRANT SELECT, EXECUTE, view definition ON SCHEMA :: dbo TO [CLC_Regular_Staff_Accounts_READONLY]
GRANT SELECT, EXECUTE, view definition ON SCHEMA :: polaris TO [CLC_Regular_Staff_Accounts_READONLY]

use Results
go
CREATE USER [CLCDPC\CLCStaffMembersNonAdmin] FOR LOGIN [CLCDPC\CLCStaffMembersNonAdmin]
CREATE ROLE [CLC_Regular_Staff_Accounts_READONLY]
ALTER ROLE [CLC_Regular_Staff_Accounts_READONLY] ADD MEMBER [CLCDPC\CLCStaffMembersNonAdmin]

DENY ALTER, INSERT, DELETE, UPDATE ON SCHEMA :: dbo TO [CLC_Regular_Staff_Accounts_READONLY]
DENY ALTER, INSERT, DELETE, UPDATE ON SCHEMA :: polaris TO [CLC_Regular_Staff_Accounts_READONLY]
GRANT SELECT, EXECUTE, view definition ON SCHEMA :: dbo TO [CLC_Regular_Staff_Accounts_READONLY]
GRANT SELECT, EXECUTE, view definition ON SCHEMA :: polaris TO [CLC_Regular_Staff_Accounts_READONLY]