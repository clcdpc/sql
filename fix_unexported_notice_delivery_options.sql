begin tran

update nq
set nq.DeliveryOptionID = pr.DeliveryOptionID
from results.polaris.notificationqueue nq
join polaris.polaris.PatronRegistration pr
	on pr.PatronID= nq.PatronID
where nq.Processed != 1
	and nq.DeliveryOptionID = 1
	and nq.DeliveryOptionID != pr.DeliveryOptionID
	and nq.NotificationTypeID not in (3,8,11,20)

--rollback
--commit