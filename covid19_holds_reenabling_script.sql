
begin tran

-- double time to go unclaimed
update op
set op.Value = op.Value * 2
from (
	select *
	from polaris.Polaris.OrganizationsPPPP op
	where op.AttrID = 793
) d
join polaris.polaris.Organizationspppp op
	on op.OrganizationID = d.OrganizationID and op.AttrID = d.AttrID

-- double material limits
update mll
set mll.MaxItems = mll.MaxItems * 2
	,mll.MaxRequestItems = mll.MaxRequestItems * 2
from polaris.polaris.MaterialLoanLimits mll
where mll.MaterialTypeID not in (43,44,33,40)
	and mll.MaxItems < 999

-- double patron limits
update pll
set pll.TotalItems = pll.TotalItems * 2
	,pll.TotalOverDue = pll.TotalOverDue * 2
	,pll.TotalHolds = pll.TotalHolds * 2
from Polaris.polaris.PatronLoanLimits pll
where pll.TotalItems < 999

-- double material group limits
update l
set l.Limit = l.Limit * 2
from Polaris.polaris.SA_MaterialTypeGroups_Limits l
where l.limit < 999

-- disable rtf
update polaris.polaris.OrganizationsPPPP
set Value = 'Yes'
where AttrID = 3101 

-- set 'prefer checkin location'
update polaris.polaris.OrganizationsPPPP
set Value = '3'
where AttrID = 2652 

-- disable hold notices
update polaris.polaris.OrganizationsPPPP
set Value = 'No'
where AttrID = 1614

-- disable 'if no items transfer to secondary queue'
update polaris.polaris.OrganizationsPPPP
set value = 'No'
where AttrID = 2199

-- remove all but the local branch from primary queue
delete shrs
from polaris.Polaris.SysHoldRoutingSequences shrs
where shrs.PrimarySecondaryFlag = 1 
	and shrs.RequesterBranchID != shrs.ResponderBranchID

-- reactivate all holds as of today
update shr
set shr.ActivationDate = cast(getdate() as date)
from Polaris.Polaris.SysHoldRequests shr
where shr.ActivationDate = '6/1/2020'

--rollback
--commit



-- fix 'requested' holds
begin tran

update shr
set shr.PrimaryRTFBeginDate = cast(getdate() as date)
	,shr.PrimaryRTFEndDate = cast(dateadd(day, 90, getdate()) as date)
	,shr.RTFCyclesPrimary = 0
	,shr.PrimarySecondaryFlag = 1
from polaris.Polaris.SysHoldRequests shr
where shr.PrimaryRTFBeginDate is not null
	and shr.PrimarySecondaryFlag = 2
	and shr.SysHoldStatusID = 3

--rollback
--commit


--32389196

begin tran

update shr
set shr.PrimaryRTFBeginDate = cast(getdate() as date)
	,shr.PrimaryRTFEndDate = cast(dateadd(day, 90, getdate()) as date)
	,shr.RTFCyclesPrimary = 0
	,shr.PrimarySecondaryFlag = 1
from polaris.Polaris.SysHoldRequests shr
where shr.SysHoldRequestID = 32389196


/*
-- change duedates to 6/1, not ale or mpc
-- unclaimed dates to 6/1, not ale or mpc
update polaris.dbo.CLC_Custom_CV_Closed_Date_Ranges set ItemDueDate = '6/1/2020', HoldUnclaimedDate='6/1/2020', EndDate='6/1/2020' where LibraryId not in (3,14)
*/

--rollback
--commit