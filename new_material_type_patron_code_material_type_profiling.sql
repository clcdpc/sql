declare @dummy int = 0 -- dummy variable to fix code folding


begin -- insert values
	if (OBJECT_ID('tempdb..#values') is not null) 
	begin
		drop table #values
	end

	create table #values ( libraryid int, materialtypeid int, materialtypedesc varchar(255), patroncodeid int, patroncodedesc varchar(255), ckolimit int, holdlimit int, finecodeid int, finerate decimal(18,2), maxfine decimal(18,2), graceunits int, loanperiodid int, loanperiod int )

	BULK INSERT #values
	FROM 'c:\temp\new_material_type_material_loan_limits.csv'
	WITH
	(
		FIRSTROW = 2,
		FIELDTERMINATOR = ',',
		ROWTERMINATOR = '\n',
		TABLOCK,
		KEEPNULLS -- Treat empty fields as NULLs.
	)
end

begin -- insert mappings
	declare @mapping table ( orgid int, source_code varchar(255), new_code varchar(255) )
	-- Alexandria
	insert into @mapping values (2, 'Kit', 'zzNEW Book Club Kits')
	insert into @mapping values (2, 'Juvenile Kit', 'zzNEW Games and Toys')
	insert into @mapping values (2, 'Equipment and Misc', 'zzNEW Hotspots')
	insert into @mapping values (2, 'Equipment and Misc', 'zzNEW In House Equipment')
	insert into @mapping values (2, 'Juvenile Video Non-Fiction', 'zzNEW Juvenile Video Un-Rated')
	insert into @mapping values (2, 'Equipment and Misc', 'zzNEW Loanable Equipment 1')
	insert into @mapping values (2, 'Equipment and Misc', 'zzNEW Loanable Equipment 2')
	insert into @mapping values (2, 'Periodical', 'zzNEW Maps & Pamphlets')
	insert into @mapping values (2, 'Equipment and Misc', 'zzNEW Office')
	insert into @mapping values (2, 'Equipment and Misc', 'zzNEW Traditional ILL')
	insert into @mapping values (2, 'Video G', 'zzNEW Video G / TV-G / TV-Y / TV-Y7 / TV-Y7 FV')
	insert into @mapping values (2, 'Video Non-Fiction and Other Restricted', 'zzNEW Video Non-Fiction')
	insert into @mapping values (2, 'Video PG-13', 'zzNEW Video PG-13 / TV-14')
	insert into @mapping values (2, 'Video R', 'zzNEW Video R / TV-MA')
	insert into @mapping values (2, 'Video Non-Fiction and Other Restricted', 'zzNEW Video Un-Rated')

	-- Bexley
	insert into @mapping values (84, 'Puppet, Poster or Toy', 'zzNEW Games and Toys')
	insert into @mapping values (84, 'Equipment and Misc', 'zzNEW In House Equipment')
	insert into @mapping values (84, 'Equipment and Misc', 'zzNEW Hotspots')
	insert into @mapping values (84, 'Video PG-13', 'zzNEW Video PG-13 / TV-14')
	insert into @mapping values (84, 'Video R', 'zzNEW Video R / TV-MA')

	-- Fairfield
	insert into @mapping values (8, 'Kit', 'zzNEW Book Club Kits')
	insert into @mapping values (8, 'Puppet, Poster or Toy', 'zzNEW Games and Toys')
	insert into @mapping values (8, 'Equipment and Misc', 'zzNEW Hotspots')
	insert into @mapping values (8, 'Equipment and Misc', 'zzNEW In House Equipment')
	insert into @mapping values (8, 'Juvenile Video Non-Fiction', 'zzNEW Juvenile Video Un-Rated')
	insert into @mapping values (8, 'Equipment and Misc', 'zzNEW Loanable Equipment 1')
	insert into @mapping values (8, 'Equipment and Misc', 'zzNEW Loanable Equipment 2')
	insert into @mapping values (8, 'Vertical File', 'zzNEW Maps & Pamphlets')
	insert into @mapping values (8, 'Local Short Term Loan', 'zzNEW Office')
	insert into @mapping values (8, 'Equipment and Misc', 'zzNEW Traditional ILL')
	insert into @mapping values (8, 'Video G', 'zzNEW Video G / TV-G / TV-Y / TV-Y7 / TV-Y7 FV')
	insert into @mapping values (8, 'Video Non-Fiction & Other', 'zzNEW Video Non-Fiction')
	insert into @mapping values (8, 'Video PG-13', 'zzNEW Video PG-13 / TV-14')
	insert into @mapping values (8, 'Video R', 'zzNEW Video R / TV-MA')
	insert into @mapping values (8, 'Video R', 'zzNEW Video Un-Rated')

	-- London
	insert into @mapping values (86, 'zzNEW Juvenile Video Un-Rated', 'zzNEW Video G / TV-G / TV-Y / TV-Y7 / TV-Y7 FV')
	insert into @mapping values (86, 'zzNEW Juvenile Video Un-Rated', 'zzNEW Video Non-Fiction')
	insert into @mapping values (86, 'zzNEW Juvenile Video Un-Rated', 'zzNEW Video PG-13 / TV-14')
	insert into @mapping values (86, 'zzNEW Juvenile Video Un-Rated', 'zzNEW Video R / TV-MA')
	insert into @mapping values (86, 'zzNEW Juvenile Video Un-Rated', 'zzNEW Video Un-Rated')

	-- Pickerington
	insert into @mapping values (23, 'Puppet, Poster or Toy', 'zzNEW Games and Toys')
	insert into @mapping values (23, 'Local A/V Material', 'zzNEW Hotspots')
	insert into @mapping values (23, 'Equipment and Misc', 'zzNEW In House Equipment')
	insert into @mapping values (23, 'Playaway', 'zzNEW Loanable Equipment 1')
	insert into @mapping values (23, 'Playaway', 'zzNEW Loanable Equipment 2')
	insert into @mapping values (23, 'Book', 'zzNEW Maps & Pamphlets')
	insert into @mapping values (23, 'Book', 'zzNEW Office')

	-- Upper Arlington
	insert into @mapping values (78, 'Video G', 'zzNEW Video G / TV-G / TV-Y / TV-Y7 / TV-Y7 FV')
	insert into @mapping values (78, 'Video Non-Fiction and Other', 'zzNEW Video Non-Fiction')
	insert into @mapping values (78, 'Video PG-13', 'zzNEW Video PG-13 / TV-14')
	insert into @mapping values (78, 'Video R', 'zzNEW Video R / TV-MA')
	insert into @mapping values (78, 'Video R', 'zzNEW Video Un-Rated')

	insert into @mapping values (78, 'Video R', 'zzNEW Video Un-Rated')
	insert into @mapping values (78, 'Video R', 'zzNEW Video Un-Rated')
	insert into @mapping values (78, 'Video R', 'zzNEW Video Un-Rated')

	-- Worthington
	insert into @mapping values (32, 'Puppet, Poster or Toy', 'zzNEW Games and Toys')
	insert into @mapping values (32, 'Kit', 'zzNEW Hotspots')
	insert into @mapping values (32, 'Juvenile Video Non-Fiction', 'zzNEW Juvenile Video Un-Rated')
	insert into @mapping values (32, 'Vertical File', 'zzNEW Maps & Pamphlets')

end

select * from polaris.polaris.MaterialTypes

begin -- insert new patron loan limits 
	if (OBJECT_ID('tempdb..#newpll') is not null) 
	begin
		drop table #newpll
	end

	create table #newpll ( patroncode varchar(255), libraryid int, maxfine decimal(18,2), minfine decimal(18,2), totalitems int, totaloverdue int, totalholds int )

	BULK INSERT #newpll
	FROM 'c:\temp\new_patron_code_patron_loan_limits.csv'
	WITH
	(
		FIRSTROW = 2,
		FIELDTERMINATOR = ',',
		ROWTERMINATOR = '\n',
		TABLOCK,
		KEEPNULLS -- Treat empty fields as NULLs.
	)
end

begin tran

-- Update Material Loan Limits
update mll
set MaxItems = v.ckolimit, MaxRequestItems = v.holdlimit
from polaris.polaris.MaterialLoanLimits mll
join polaris.polaris.organizations o
	on o.OrganizationID = mll.OrganizationID
join #values v
	on v.libraryid = o.ParentOrganizationID and v.materialtypeid = mll.MaterialTypeID and v.patroncodeid = mll.PatronCodeID

update mll
set MaxItems = mll_source.MaxItems, MaxRequestItems = mll_source.MaxRequestItems
from Polaris.Polaris.MaterialLoanLimits mll
join polaris.polaris.MaterialTypes mt
	on mt.MaterialTypeID = mll.MaterialTypeID
join polaris.polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
join Polaris.polaris.MaterialLoanLimits mll_source
	on mll_source.OrganizationID = mll.OrganizationID and mll_source.PatronCodeID = mll.PatronCodeID
join polaris.polaris.MaterialTypes mt_source
	on mt_source.MaterialTypeID = mll_source.MaterialTypeID
join @mapping m
	on m.orgid = o.ParentOrganizationID and m.source_code = mt_source.Description and m.new_code = mt.Description

-- Update Fines
update f
set f.Amount = v.finerate, f.MaximumFine = v.maxfine, f.GraceUnits = v.graceunits
from polaris.polaris.Fines f
join polaris.polaris.Organizations o
	on o.OrganizationID = f.OrganizationID
join #values v
	on v.finecodeid = f.FineCodeID and v.patroncodeid = f.PatronCodeID and v.libraryid = o.ParentOrganizationID

update f
set f.Amount = f_source.Amount, f.MaximumFine = f_source.MaximumFine, f.GraceUnits = f_source.GraceUnits
from polaris.polaris.Fines f
join polaris.polaris.Organizations o
	on o.OrganizationID = f.OrganizationID
join polaris.polaris.FineCodes fc
	on fc.FineCodeID = f.FineCodeID
join polaris.polaris.Fines f_source
	on f_source.OrganizationID = f.OrganizationID and f_source.PatronCodeID = f.PatronCodeID
join polaris.polaris.FineCodes fc_source
	on fc_source.FineCodeID = f_source.FineCodeID
join @mapping m
	on m.orgid = o.ParentOrganizationID and m.source_code = fc_source.Description and m.new_code = fc.Description

-- Update Loan Periods
update lp
set lp.Units = v.loanperiod
from polaris.polaris.LoanPeriods lp
join polaris.polaris.Organizations o
	on o.OrganizationID = lp.OrganizationID
join #values v
	on v.libraryid = o.ParentOrganizationID and v.loanperiodid = lp.LoanPeriodCodeID and v.patroncodeid = lp.PatronCodeID

update lp
set lp.units = lp_source.Units
from polaris.polaris.LoanPeriods lp
join polaris.polaris.LoanPeriodCodes lpc
	on lpc.LoanPeriodCodeID = lp.LoanPeriodCodeID
join polaris.polaris.Organizations o
	on o.OrganizationID = lp.OrganizationID
join polaris.polaris.LoanPeriods lp_source
	on lp_source.OrganizationID = lp.OrganizationID and lp_source.PatronCodeID = lp.PatronCodeID
join polaris.polaris.LoanPeriodCodes lpc_source
	on lpc_source.LoanPeriodCodeID = lp_source.LoanPeriodCodeID
join @mapping m
	on m.orgid = o.ParentOrganizationID and m.source_code = lpc_source.Description and m.new_code = lpc.Description

-- Update Patron Loan Limits
update pll
set MaxFine = v.maxfine, MinFine = v.minfine, TotalItems = v.totalitems, TotalOverDue = v.totaloverdue, TotalHolds = v.totalholds
from Polaris.polaris.PatronLoanLimits pll
join Polaris.polaris.Organizations o
	on o.OrganizationID = pll.OrganizationID
join polaris.polaris.patroncodes pc
	on pc.PatronCodeID = pll.PatronCodeID
join #newpll v
	on v.libraryid = o.ParentOrganizationID and v.patroncode = pc.Description 


drop table #values
drop table #newpll

--rollback
--commit

--select * from polaris.polaris.materialtypes order by description



begin tran

update mll
set MaxItems = case when mll.PatronCodeID in (1,8,14) then 2 else 0 end, MaxRequestItems = 0
from polaris.Polaris.MaterialLoanLimits mll
join polaris.Polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
where o.ParentOrganizationID = 19 and mll.MaterialTypeID = 10

update lp
set Units = 7
from polaris.polaris.LoanPeriods lp
join polaris.polaris.organizations o
	on o.OrganizationID = lp.OrganizationID
where o.ParentOrganizationID = 19 and lp.LoanPeriodCodeID = 10

update f
set f.Amount = f_source.Amount, f.MaximumFine = f_source.MaximumFine, f.GraceUnits = f_source.GraceUnits
from polaris.polaris.Fines f
join polaris.polaris.Fines f_source
	on f_source.OrganizationID = f.OrganizationID and f_source.PatronCodeID = f.PatronCodeID
join polaris.polaris.organizations o
	on o.OrganizationID = f.OrganizationID
where f.FineCodeID = 10 and f_source.FineCodeID = 2 and o.ParentOrganizationID = 19

--rollback
--commit
