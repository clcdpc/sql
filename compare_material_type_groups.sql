declare @branch int = 115

;with branch as (
	select distinct o.Name [OrgName]
			,g.GroupID
			,g.GroupName
			,d.MaterialTypeID
	from polaris.Polaris.SA_MaterialTypeGroups g
	join polaris.polaris.SA_MaterialTypeGroups_Definitions d
		on d.GroupID = g.GroupID
	join polaris.polaris.Organizations o
		on o.OrganizationID = g.OrganizationID
	where g.OrganizationID in (@branch)
), library as (
	select distinct o.Name [OrgName]
			,g.GroupID
			,g.GroupName
			,d.MaterialTypeID
	from polaris.Polaris.SA_MaterialTypeGroups g
	join polaris.polaris.SA_MaterialTypeGroups_Definitions d
		on d.GroupID = g.GroupID
	join polaris.polaris.Organizations o
		on o.OrganizationID = ( select o.ParentOrganizationID from polaris.polaris.organizations o where o.OrganizationID = @branch )
	where g.OrganizationID in ( o.OrganizationID )
)

select 'branch' [branch]
		,b.GroupName
		,'' [ ]
		,'library' [library]
		,l.GroupName
		,mt.Description
from branch b
full outer join library l
	on l.GroupName = b.GroupName and l.MaterialTypeID = b.MaterialTypeID
left join polaris.Polaris.MaterialTypes mt
	on mt.MaterialTypeID in ( b.MaterialTypeID, l.MaterialTypeID )
order by b.GroupName