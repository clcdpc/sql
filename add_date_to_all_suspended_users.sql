begin tran

update pu
set pu.name = pu.Name + format(isnull(pu.ModificationDate, getdate()), 'yyyyMMdd')
from polaris.polaris.UsersPPPP up
join polaris.polaris.PolarisUsers pu
	on pu.PolarisUserID = up.PolarisUserID
where up.AttrID = 659
	and up.Value in ('suspended', 'closed')
	and pu.Name not like '%[0-9]'

--rollback
--commit


select distinct up.PolarisUserID 
from polaris.polaris.UsersPPPP up
join polaris.polaris.PolarisUsers pu
	on pu.PolarisUserID = up.PolarisUserID
where up.AttrID = 659
	and up.Value in ('suspended', 'closed')