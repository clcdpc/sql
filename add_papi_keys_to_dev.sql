insert into devdb.Polaris.polaris.PAPIUsers (AccessID, AccessKey)
select pu.AccessID, pu.AccessKey
from Polaris.Polaris.PAPIUsers pu
where not exists ( select 1 from devdb.Polaris.Polaris.PAPIUsers dpu where dpu.AccessID = pu.AccessID )