begin tran

declare @ratingRank table (mtid int, therank int identity(1,1))
insert into @ratingRank values (53), (52), (51), (3), (49)

;with fixed521 as (
	select bt.BibliographicRecordID
			,bs.Data [raw521]
			,case when bs.data like '%not rated%' or bs.data like '%unrated%' or bs.data like '%un rated%' or bs.data like '%un-rated%' then 'not rated' else replace(replace(bs.Data, 'rated', 'rating'), ':', '') end [fixed521]
			,rank() over(partition by bt.BibliographicRecordID order by bt.Sequence) [tagrank]
	from polaris.polaris.BibliographicTags bt
	join polaris.polaris.BibliographicSubfields bs
		on bs.BibliographicTagID = bt.BibliographicTagID
	where bt.TagNumber = 521
	and bs.Subfield = 'a'
	and (bs.data like '%rating%' or bs.data like '%rated%' or bs.Data like 'preschool%')
	and (bs.Data not like '%ratings%' and bs.Data not like '%CHV%' and bs.Data not like '%Canada%' and bs.Data not like '%Canadian%' and bs.data not like '%OFBR%' and bs.data not like '%BBFC%' and bs.data not like '%ofrb%' and bs.data not like '%cmpda%' and bs.Data not like '%chr%' and bs.data not like '%Recommended for ages%' and bs.data not like '%13up%' and bs.Data not like '%ratings vary%' and bs.data not like '%various ratings%' and bs.Data not like '%all others%' and bs.data not like '%(others)%')
	--and len(bs.Data) < 90
), ratings as (
	select *
		,case when f.fixed521 != 'not rated' and f.fixed521 like '%rating%' then replace(replace(replace(RIGHT(f.fixed521,CHARINDEX('gnitar',REVERSE(f.fixed521),0)-1), '-', ''), '.', ''), ' ', '') else f.fixed521 end [Rating]
	from fixed521 f
	where --f.tagrank = 1 and
		 len(f.fixed521) - len(replace(f.fixed521, 'rating', '12345')) < 2
), fixedratings as (
	select *,
		case
			-- order of these is important
			when r.Rating = 'not rated' or r.rating like 'ur%' or r.rating like 'nr%' or r.rating like 'unk%' then 53
			when (r.rating like 'r%' and r.rating not like 'recc%') or r.raw521 like '% r[.,; ]%' or r.rating like '1[68]%'  or r.Rating like '%tvma%' then 52
			when r.Rating like '%pg13%' or r.rating like '1[34]%' or r.Rating like 'tv14%' then 51
			when r.Rating like '%PG%' or r.rating like 'tvpg%' then 3
			when r.Rating like 'g%' or r.rating like 'tv[yg]%' or r.rating like 'preschool%' or r.Rating like '3up%' then 49
			else -1
		end [FixedRating]
	from ratings r
), rankedRatings as (
	select fr.*
			,rank() over(partition by fr.BibliographicRecordID order by rr.therank, fr.tagrank) [ratingrank]
	from fixedratings fr
	join @ratingRank rr
		on rr.mtid = fr.FixedRating
), excludedBibs as (
	select distinct bt.BibliographicRecordID
	from polaris.Polaris.BibliographicTags bt
	join polaris.Polaris.BibliographicSubfields bs
		on bs.BibliographicTagID = bt.BibliographicTagID
	where bt.TagNumber = 490
		and bs.Subfield = 'a'
		and bs.Data = 'binge box'
	union all
	select distinct bt.BibliographicRecordID
	from polaris.Polaris.BibliographicTags bt
	join polaris.Polaris.BibliographicSubfields bs
		on bs.BibliographicTagID = bt.BibliographicTagID
	where bt.TagNumber = 246
		and bs.Subfield = 'a'
		and bs.Data like '%binge box%'

)

--select cir.ItemRecordID	,cir.AssociatedBibRecordID,cir.MaterialTypeID,r.FixedRating,r.Rating,r.raw521,mt.Description [new_mt],mt2.Description [old_mt]
update cir set cir.MaterialTypeID = r.FixedRating
from rankedRatings r
join polaris.polaris.CircItemRecords cir
	on cir.AssociatedBibRecordID = r.BibliographicRecordID
join polaris.Polaris.BibliographicRecords br
	on br.BibliographicRecordID = cir.AssociatedBibRecordID
join polaris.polaris.MaterialTypes mt
	on mt.MaterialTypeID = r.FixedRating
join polaris.Polaris.MaterialTypes mt2
	on mt2.MaterialTypeID = cir.MaterialTypeID
where r.FixedRating not in (-1,53)
	and r.ratingrank = 1
	and br.PrimaryMARCTOMID != 1
	and cir.MaterialTypeID not in (r.fixedrating,10,16,20,33)
	and cir.ItemStatusID not in (13)
	and exists ( select 1 from polaris.polaris.CircItemRecords cir2 where cir2.AssociatedBibRecordID = cir.AssociatedBibRecordID and cir2.MaterialTypeID in (3,49,50,51,52,53) )
	and not exists ( select 1 from polaris.polaris.CircItemRecords cir2 where cir2.AssociatedBibRecordID = cir.AssociatedBibRecordID and cir2.MaterialTypeID in (18,50) )
	and not exists ( select 1 from excludedBibs eb where eb.BibliographicRecordID = cir.AssociatedBibRecordID )
	--and r.BibliographicRecordID = 470852
--order by r.raw521
--order by len(r.raw521) desc, r.BibliographicRecordID, cir.ItemRecordID, r.FixedRating


--rollback
--commit



--select case when 'DirectorscutR;theatricalversionPG13' like '% r[.,; ]%' then 'yes' else 'no' end
