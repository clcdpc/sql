declare @library int = 0


begin tran

update p
set p.PatronCodeID = m.NewPatronCodeID
from polaris.polaris.Patrons p
join polaris.polaris.organizations o
	on o.OrganizationID = p.OrganizationID
join polaris.dbo.CLC_Custom_Old_To_New_Patron_Code_Mapping m
	on m.LibraryID = o.ParentOrganizationID and m.OldPatronCodeID = p.PatronCodeID
where m.OldPatronCodeID != m.NewPatronCodeID and o.ParentOrganizationID = @library

--rollback
--commit