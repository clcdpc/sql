declare @rebuild_recordsets bit = 0


begin tran

if (@rebuild_recordsets = 1)
begin
	delete from Polaris.polaris.PatronRecordSets
	where RecordSetID in (
		select m.RecordSetID
		from polaris.dbo.CLC_Custom_Old_To_New_Patron_Code_Mapping m
		where m.RecordSetID > 0
	)

	delete from Polaris.polaris.RecordSets
	where RecordSetID in (
		select m.RecordSetID
		from polaris.dbo.CLC_Custom_Old_To_New_Patron_Code_Mapping m
		where m.RecordSetID > 0
	)

	update Polaris.dbo.CLC_Custom_Old_To_New_Patron_Code_Mapping
	set RecordSetID = 0
end


declare @libraryid int

declare valueCursor cursor for
select m.LibraryID, m.OldPatronCodeID, m.NewPatronCodeID
from Polaris.dbo.CLC_Custom_Old_To_New_Patron_Code_Mapping m
where m.OldPatronCodeID != m.NewPatronCodeID

declare @oldpcid int
declare @newpcid int

set nocount on

open valueCursor
fetch next from valueCursor into @libraryid, @oldpcid, @newpcid
while @@FETCH_STATUS = 0
begin
	
	declare @oldpc varchar(255) = ( select Description from polaris.Polaris.PatronCodes where PatronCodeID = @oldpcid )
	declare @newpc varchar(255) = ( select Description from polaris.Polaris.PatronCodes where PatronCodeID = @newpcid )

	insert into Polaris.polaris.RecordSets (Name, ObjectTypeID, CreatorID, OrganizationOwnerID, CreationDate, Note)
	select	@oldpc	 + ' -> ' + @newpc -- Name
			,27			-- Type
			,425		-- Creator
			,@libraryid	-- Owner
			,getdate()	-- Creation Date
			,@oldpc	 + ' -> ' + @newpc + ' | created automatically as part of the patron code cleanup project'
	
	update Polaris.dbo.CLC_Custom_Old_To_New_Patron_Code_Mapping
	set RecordSetID = @@identity
	where LibraryID = @libraryid and OldPatronCodeID = @oldpcid and NewPatronCodeID = @newpcid and LibraryID = @libraryid
	

	fetch next from valueCursor into @libraryid, @oldpcid, @newpcid
end

close valueCursor
deallocate valueCursor

set nocount off 

insert into Polaris.Polaris.PatronRecordSets
select m.RecordSetID, p.PatronID
from Polaris.polaris.Patrons p
join polaris.polaris.organizations o
	on o.OrganizationID = p.OrganizationID
join Polaris.dbo.CLC_Custom_Old_To_New_Patron_Code_Mapping m
	on m.LibraryID = o.ParentOrganizationID and m.OldPatronCodeID = p.PatronCodeID
where m.OldPatronCodeID != m.NewPatronCodeID


--rollback
--commit
