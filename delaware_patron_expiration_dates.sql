begin tran

update pr
set pr.ExpirationDate = dateadd(year, 99, pr.AddrCheckDate)
from polaris.polaris.Patrons p
join Polaris.Polaris.PatronRegistration pr
	on pr.PatronID = p.PatronID
join polaris.polaris.Organizations o
	on o.OrganizationID = p.OrganizationID
where o.ParentOrganizationID = 105

--rollback
--commit