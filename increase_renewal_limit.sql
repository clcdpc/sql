declare  @libraryId int = 23
		,@oldRenewals int = 3
		,@newRenewals int = 5

begin tran

update cir
set RenewalLimit = @newRenewals
from polaris.polaris.CircItemRecords cir
join polaris.polaris.organizations o
	on o.OrganizationID = cir.AssignedBranchID
where cir.RecordStatusID = 1
	and o.ParentOrganizationID = @libraryId
	and cir.RenewalLimit = @oldRenewals

update it
set RenewalLimit = @newRenewals
from polaris.polaris.ItemTemplates it
join polaris.polaris.organizations o
	on o.OrganizationID = it.AssignedBranchID
where o.ParentOrganizationID = @libraryId
	and it.RenewalLimit = @oldRenewals

--rollback
--commit