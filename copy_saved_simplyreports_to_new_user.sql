declare @sourceUserId int = 707
declare @destUserId int = 447

begin tran

insert into Polaris.polaris.RWRITERSavedReports (ReportName, ReportDescription, ReportTypeID, OutputTypeID, PolarisUserID, ReportSQL, ReportMetaColumns, SharedFlag, CreationDate, LastRunDate, Enabled)
select r.ReportName
	,r.ReportDescription
	,r.ReportTypeID
	,r.OutputTypeID
	,@destUserId
	,r.ReportSQL
	,r.ReportMetaColumns
	,r.SharedFlag
	,getdate()
	,r.LastRunDate
	,r.Enabled
from Polaris.Polaris.RWRITERSavedReports r 
where r.PolarisUserID = @sourceUserId

--rollback
--commit

--select * from Polaris.Polaris.PolarisUsers pu where pu.Name like '%mccoy%'

/*
select *
from Polaris.Polaris.RWRITERSavedReports r 
where r.PolarisUserID = 447
*/

/*
begin tran
delete from polaris.Polaris.RWRITERSavedReports where PolarisUserID = 425 and CreationDate > cast(getdate() as date)
*/