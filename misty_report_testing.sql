select irh.ItemRecordHistoryID
		,irh.ItemRecordID
		,irh.TransactionDate
		,irh.OrganizationID
		,o.Name [Organization]
		,irh.PolarisUserID
		,pu.Name [PolarisUser]
		,irh.WorkstationID
		,w.ComputerName [Workstation]
		,irh.ActionTakenID
		,irha.ActionTakenDesc [ActionTaken]
		,irh.OldItemStatusID
		,iss_old.Description [OldItemStatus]
		,irh.NewItemStatusID
		,iss_new.Description [NewStatus]
		,irh.PatronID
		,irh.AssignedBranchID
		,o_ab.Name [AssignedBranch]
		,irh.InTransitRecvdBranchID
		,o_rb.Name [InTransitRecvdBranch]
		,rank() over(partition by irh.ItemRecordID order by irh.ItemRecordHistoryID) [seq]
from Polaris.polaris.ItemRecordHistory irh 
join Polaris.polaris.ItemRecordHistoryActions irha
	on irha.ActionTakenID = irh.ActionTakenID
join Polaris.Polaris.ItemStatuses iss_old
	on iss_old.ItemStatusID = irh.OldItemStatusID
join Polaris.Polaris.ItemStatuses iss_new
	on iss_new.ItemStatusID = irh.NewItemStatusID
join Polaris.Polaris.Organizations o_ab
	on o_ab.OrganizationID = irh.AssignedBranchID
join Polaris.Polaris.Organizations o
	on o.OrganizationID = irh.OrganizationID
join Polaris.Polaris.PolarisUsers pu
	on pu.PolarisUserID = irh.PolarisUserID
join Polaris.polaris.Workstations w
	on w.WorkstationID = irh.WorkstationID
left join Polaris.polaris.Organizations o_rb
	on o_rb.OrganizationID = irh.InTransitRecvdBranchID
where irh.ItemRecordID = 25369496


with foo as (
	select irh.*
			,rank() over(partition by irh.ItemRecordID order by irh.ItemRecordHistoryID) [seq]
	from polaris.polaris.ItemRecordHistory irh
	join polaris.polaris.CircItemRecords cir
		on cir.ItemRecordID = irh.ItemRecordID
	join polaris.polaris.organizations o
		on o.OrganizationID = cir.AssignedBranchID
	where o.ParentOrganizationID = 39
		and irh.TransactionDate > '10/1/2022'
	--where irh.ItemRecordID = 25369496
)

select *
from foo f
join foo f2
	on f2.ItemRecordID = f.ItemRecordID and f2.seq = f.seq + 1 and f2.ActionTakenID = 8
where f.ActionTakenID = 3



create table #foo ( TransactionDate datetime, ItemRecordID int, ActionTakenID int, UserID int, seq int )

insert into #foo
select irh.TransactionDate
		,irh.ItemRecordID
		,irh.ActionTakenID
		,irh.PolarisUserID
		,rank() over(partition by irh.ItemRecordID order by irh.ItemRecordHistoryID) [seq]
from polaris.polaris.ItemRecordHistory irh
join polaris.polaris.CircItemRecords cir
	on cir.ItemRecordID = irh.ItemRecordID
join polaris.polaris.organizations o
	on o.OrganizationID = cir.AssignedBranchID
where o.ParentOrganizationID = 39
	and irh.TransactionDate > '12/1/2022'

select pu.Name
		,cir.Barcode
		,'' [ ]
		,f.*
		,'' [  ]
		,f2.*
from #foo f
join polaris.polaris.CircItemRecords cir
	on cir.ItemRecordID = f.ItemRecordID
join #foo f2
	on f2.ItemRecordID = f.ItemRecordID and f2.seq > f.seq and f2.TransactionDate < cir.FirstAvailableDate
join polaris.polaris.PolarisUsers pu
	on pu.PolarisUserID = f2.UserID
where f.ActionTakenID = 3
	and cir.Barcode is not null
order by f.TransactionDate, f.ItemRecordID, f2.seq

--drop table #foo


select * from polaris.polaris.ItemRecordHistoryActions