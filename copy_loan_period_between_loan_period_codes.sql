declare @libraryid int = 0

declare @map table ( destlp int, sourcelp int )
insert into @map values ( 0, 0 )

begin tran

update lp_dest
set lp_dest.Units = lp_source.Units
from Polaris.Polaris.LoanPeriods lp_dest
join Polaris.polaris.Organizations o
	on o.OrganizationID = lp_dest.OrganizationID
join Polaris.polaris.LoanPeriods lp_source
	on lp_source.OrganizationID = lp_dest.OrganizationID and lp_source.PatronCodeID = lp_dest.PatronCodeID
join @map m
	on m.sourcelp = lp_source.LoanPeriodCodeID and m.destlp = lp_dest.LoanPeriodCodeID
where o.ParentOrganizationID = @libraryid

--rollback
--commit

--select * from polaris.polaris.materialtypes order by description