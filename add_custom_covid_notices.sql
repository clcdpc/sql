select *
from CLC_Notices.dbo.PolarisNotifications pn
where pn.NotificationTypeID = 999
	and pn.PatronLibrary = 39

insert into CLC_Notices.dbo.ClcCustomNotificationQueue(PatronId,NotifictationTypeId,ItemRecordId,DeliveryOptionId,ReportingBranchId,Amount,AutoRenewal,BibId)
select shr.PatronID
		,999
		,null
		,2
		,isnull(shr.NewPickupBranchID, shr.PickupBranchID)
		,null
		,0
		,shr.BibliographicRecordID
from Polaris.Polaris.SysHoldRequests shr
join polaris.polaris.Organizations o_pickup
	on o_pickup.OrganizationID = isnull(shr.NewPickupBranchID, shr.PickupBranchID)
join polaris.polaris.Patrons p
	on p.PatronID = shr.PatronID
join Polaris.polaris.Organizations o_patron
	on o_patron.OrganizationID = p.OrganizationID
where o_pickup.ParentOrganizationID = 105
	and o_patron.ParentOrganizationID = 105
	and p.PatronCodeID != 20


select shr.*
from Polaris.Polaris.SysHoldRequests shr
join polaris.polaris.Organizations o_pickup
	on o_pickup.OrganizationID = isnull(shr.NewPickupBranchID, shr.PickupBranchID)
join polaris.polaris.Patrons p
	on p.PatronID = shr.PatronID
join Polaris.polaris.Organizations o_patron
	on o_patron.OrganizationID = p.OrganizationID
where o_pickup.ParentOrganizationID = 105
	and o_patron.ParentOrganizationID = 105
	and p.PatronCodeID != 20

--truncate table CLC_Notices.dbo.ClcCustomNotificationQueue





select distinct shr.PatronID
from Polaris.Polaris.SysHoldRequests shr
join polaris.polaris.Organizations o_pickup
	on o_pickup.OrganizationID = isnull(shr.NewPickupBranchID, shr.PickupBranchID)
join polaris.polaris.Patrons p
	on p.PatronID = shr.PatronID
join Polaris.polaris.Organizations o_patron
	on o_patron.OrganizationID = p.OrganizationID
where o_pickup.ParentOrganizationID = 39
	and o_patron.ParentOrganizationID = 39


select pn.PatronID
from CLC_Notices.dbo.PolarisNotifications pn 
where pn.NotificationTypeID = 999
group by pn.PatronID
having count(distinct pn.ReportingBranchID) > 1



select *
from CLC_Notices.dbo.PolarisNotifications pn
where pn.NotificationTypeID = 999
	and pn.PatronID = ( select top 1 q.PatronId from CLC_Notices.dbo.ClcCustomNotificationQueue q where q.NotifictationTypeId = 999 and q.ReportingBranchId = 85 group by q.PatronId having count(*) > 5 )



