USE [Polaris]
GO

/****** Object:  Table [dbo].[CLC_Custom_Resolved_Collections]    Script Date: 12/5/2016 11:17:41 AM ******/
DROP TABLE [dbo].[CLC_Custom_Resolved_Collections]
GO

/****** Object:  Table [dbo].[CLC_Custom_Resolved_Collections]    Script Date: 12/5/2016 11:17:41 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CLC_Custom_Resolved_Collections](
	[CollectionID] [int] NOT NULL
) ON [PRIMARY]

GO


/****** Object:  StoredProcedure [dbo].[CLC_Custom_SA_ResolveCollectionOrgConflicts]    Script Date: 12/5/2016 11:19:35 AM ******/
DROP PROCEDURE [dbo].[CLC_Custom_SA_ResolveCollectionOrgConflicts]
GO

/****** Object:  StoredProcedure [dbo].[CLC_Custom_SA_ResolveCollectionOrgConflicts]    Script Date: 12/5/2016 11:19:35 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[CLC_Custom_SA_ResolveCollectionOrgConflicts]
/*************************************************************************
	Version 3.0
	Stored Procedure: SA_ResolveCollectionOrgConflicts

	This procedure updates the collection columns of the various tables
	that have foreign key constraints on the OrganizationsCollections
	table.  The passed in Org and Collection ID lists are traversed in
	parallel.  Wherever a table has the passed-in collection as its collection
	and the organization at a certain position in the Org ID list, it's collection
	field is changed to the collection at the corresponding position in the
	Collection ID list.

	Inputs:	@oldCollectionID	(Collection ID)
			@userID
			@strOrgs
			@strCollections
**************************************************************************
Revision History
	23-Apr-2009		Tom Lafave
		Include the ReserveAssignedCollectionID/ReserveAssignedBranchID
		foreign key on the ItemTemplates table in the resolution
		(Issue #44188)
	21-Apr-2009		Tom Lafave
		Include the PrimaryAssignedCollectionID/PrimaryAssignedBranchID
		foreign key on the ReserveItemRecords table in the resolution
		(Issue #44188)
	29-Oct-2002		Tom Lafave
		Updated the ModifierID and ModificationDate columns of the
		records whose Collection link is being changed
	30-Sep-2002		Tom Lafave
		Created
**************************************************************************/
	@oldCollectionID integer,
	@newCollectionID integer,
	@userID integer,
	@strOrgs varchar(8000)
/************************************************************************/
AS
	SET NOCOUNT ON

	-- This Table Will Hold The Rowset To Be Returned
	declare @tOrgColls table (	OrgID int not null,
						CollectionID int null )

	-- Parse The Passed-In List Of Branch IDs
	declare @oPos integer
	declare @oStart integer
	declare @intOrgID integer
	declare @strOrgID varchar(10)

	declare @currentDate datetime
 	select @currentDate = getdate()

	if (DATALENGTH(@strOrgs) > 0)
	begin
		set @oStart = 1
		set @oPos = CHARINDEX(',', @strOrgs, @oStart)

		-- Insert Each Parsed ID Pair Into The Table
		while (@oPos > 0)
		begin
			set @strOrgID = SUBSTRING(@strOrgs, @oStart, @oPos - @oStart)
			set @intOrgID = CAST (@strOrgID AS integer)

			if (@intOrgID != 5) insert into @tOrgColls Select @intOrgID, @newCollectionID

			set @oStart = @oPos + 1
			set @oPos = CHARINDEX(',', @strOrgs, @oStart)
		end

		-- Handle The Last ID Pair
		set @strOrgID = SUBSTRING(@strOrgs, @oStart, (DATALENGTH(@strOrgs) - @oStart) + 1)
		set @intOrgID = CAST(@strOrgID AS integer)

		if (@intOrgID != 5) insert into @tOrgColls Select @intOrgID, @newCollectionID

		if (not exists(select 1 from Polaris.dbo.CLC_Custom_Resolved_Collections where CollectionID = @oldCollectionID))
		begin
			insert into Polaris.dbo.CLC_Custom_Resolved_Collections values (@oldCollectionID)
		end

		-- Now Update All Object Tables With The New Collections
		UPDATE Polaris.POLineItemSegments 
		SET DestinationCollectionID = OC.CollectionID,
							ModifierID = @userID,
							ModificationDate = @currentDate
		FROM
		@tOrgColls AS OC
		WHERE DestinationCollectionID = @oldCollectionID AND
			DestinationOrgID = OC.OrgID

		UPDATE Polaris.InvLineItemSegments 
		SET DestinationCollectionID = OC.CollectionID,
							 ModifierID = @userID,
							 ModificationDate = @currentDate
		FROM
		@tOrgColls AS OC
		WHERE DestinationCollectionID = @oldCollectionID AND
			DestinationOrgID = OC.OrgID

		UPDATE Polaris.SelListLineSegments 
		SET DestinationCollectionID = OC.CollectionID,
							 ModifierID = @userID,
							 ModificationDate = @currentDate
		FROM
		@tOrgColls AS OC
		WHERE DestinationCollectionID = @oldCollectionID AND
			DestinationOrgID = OC.OrgID

		UPDATE Polaris.ItemRecordDetails 
		SET ModifierID = @userID, ModificationDate = @currentDate
		FROM
			@tOrgColls AS OC
		WHERE
			ItemRecordDetails.ItemRecordID IN
			(SELECT ItemRecordID FROM Polaris.CircItemRecords (NOLOCK) WHERE
			 AssignedCollectionID = @oldCollectionID AND AssignedBranchID = OC.OrgID)

		UPDATE Polaris.CircItemRecords 
		SET AssignedCollectionID = OC.CollectionID
		FROM
		@tOrgColls AS OC
		WHERE AssignedCollectionID = @oldCollectionID AND
			AssignedBranchID = OC.OrgID

		UPDATE Polaris.ReserveItemRecords 
		SET ReserveAssignedCollectionID = OC.CollectionID
		FROM
		@tOrgColls AS OC
		WHERE ReserveAssignedCollectionID = @oldCollectionID AND
			ReserveAssignedBranchID = OC.OrgID

		-- Resolve the Primary Assigned Branch / Collection Foreign
		-- Key Dependency On ReserveItemRecords (Issue #44188)
		UPDATE Polaris.ReserveItemRecords 
		SET PrimaryAssignedCollectionID = OC.CollectionID
		FROM
		@tOrgColls AS OC
		WHERE PrimaryAssignedCollectionID = @oldCollectionID AND
			PrimaryAssignedBranchID = OC.OrgID

		UPDATE Polaris.ItemTemplates 
		SET AssignedCollectionID = OC.CollectionID,
						 ModifierID = @userID,
						 ModificationDate = @currentDate
		FROM
		@tOrgColls AS OC
		WHERE AssignedCollectionID = @oldCollectionID AND
			AssignedBranchID = OC.OrgID

		-- Resolve the Reserved Assigned Branch / Collection Foreign
		-- Key Dependency On ItemTemplates (Issue #44188)
		UPDATE Polaris.ItemTemplates 
		SET ReserveAssignedCollectionID = OC.CollectionID,
						 ModifierID = @userID,
						 ModificationDate = @currentDate
		FROM
		@tOrgColls AS OC
		WHERE ReserveAssignedCollectionID = @oldCollectionID AND
			ReserveAssignedBranchID = OC.OrgID
	
		UPDATE Polaris.SHRCopies SET DestinationCollectionID = OC.CollectionID,
					ModifierID = @userID
		FROM
		@tOrgColls AS OC
		WHERE DestinationCollectionID = @oldCollectionID AND
			DestinationOrgID = OC.OrgID

		UPDATE Polaris.EphemeralItemRecords SET AssignedCollectionID = OC.CollectionID,
						 ModifierID = @userID,
						 ModificationDate = @currentDate
		FROM
		@tOrgColls AS OC
		WHERE AssignedCollectionID = @oldCollectionID AND
			AssignedBranchID = OC.OrgID
	end



GO