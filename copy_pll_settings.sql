--can you copy the total holds/checkout/fine blocks limits (the top of the patron/material type loan limit blocks table) from MPL Main to the new MPL school branches?
begin tran

update pll_dest
set pll_dest.MaxFine = pll_source.MaxFine
	,pll_dest.MinFine = pll_source.MinFine
	,pll_dest.TotalItems = pll_source.TotalItems
	,pll_dest.TotalOverDue = pll_source.TotalOverDue
	,pll_dest.TotalHolds = pll_source.TotalHolds
	,pll_dest.TotalILL = pll_source.TotalILL
	,pll_dest.TotalReserveItems = pll_source.TotalReserveItems
from polaris.polaris.PatronLoanLimits pll_source
join Polaris.Polaris.PatronLoanLimits pll_dest
	on pll_source.PatronCodeID = pll_dest.PatronCodeID
where pll_source.OrganizationID = 17 and pll_dest.OrganizationID in (117,118,119,120,121,122,123,124,125)

select pll_source.*, '' [ ], pll_dest.*
from polaris.polaris.PatronLoanLimits pll_source
join Polaris.Polaris.PatronLoanLimits pll_dest
	on pll_source.PatronCodeID = pll_dest.PatronCodeID
where pll_source.OrganizationID = 17 and pll_dest.OrganizationID in (117,118,119,120,121,122,123,124,125)


--rollback
--commit
