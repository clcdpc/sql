begin tran

declare @library int = 84

update p
set p.PatronCodeID = m.NewPatronCodeID
from Polaris.Polaris.Patrons p
join Polaris.polaris.Organizations o
	on o.OrganizationID = p.OrganizationID
join Polaris.polaris.Organizations o2
	on o2.OrganizationID = o.ParentOrganizationID
join Polaris.dbo.CLC_Custom_Old_To_New_Patron_Code_Mapping m
	on m.LibraryID = o.ParentOrganizationID and m.OldPatronCodeID = p.PatronCodeID
join Polaris.Polaris.PatronCodes oldpc
	on oldpc.PatronCodeID = p.PatronCodeID
join Polaris.polaris.PatronCodes newpc
	on newpc.PatronCodeID = m.NewPatronCodeID
where m.OldPatronCodeID != m.NewPatronCodeID and m.LibraryID = @library


select o2.Name, oldpc.Description [Old PatronCode], newpc.Description [New PatronCode], count(*) [Patrons]
from Polaris.Polaris.Patrons p
join Polaris.polaris.Organizations o
	on o.OrganizationID = p.OrganizationID
join Polaris.polaris.Organizations o2
	on o2.OrganizationID = o.ParentOrganizationID
join Polaris.dbo.CLC_Custom_Old_To_New_Patron_Code_Mapping m
	on m.LibraryID = o.ParentOrganizationID and m.OldPatronCodeID = p.PatronCodeID
join Polaris.Polaris.PatronCodes oldpc
	on oldpc.PatronCodeID = p.PatronCodeID
join Polaris.polaris.PatronCodes newpc
	on newpc.PatronCodeID = m.NewPatronCodeID
where m.OldPatronCodeID != m.NewPatronCodeID and m.LibraryID = @library
group by o2.Name, oldpc.Description, newpc.Description


--rollback
--commit

