select * from polaris.Polaris.Organizations

select shr.SysHoldRequestID
		,shr.PatronID
		,p.Barcode [PatronBarcode]
		,shr.BibliographicRecordID
		,shr.TrappingItemRecordID
		,cir.Barcode [ItemBarcode]
		,cir.ItemStatusID
		,shr.SysHoldStatusID
		,iss.Description [ItemStatus]
from Polaris.polaris.SysHoldRequests shr
join Polaris.polaris.Patrons p
	on p.PatronID = shr.PatronID
join Polaris.Polaris.CircItemRecords cir
	on cir.ItemRecordID = shr.TrappingItemRecordID
join polaris.polaris.ItemStatuses iss
	on iss.ItemStatusID = cir.ItemStatusID
where shr.SysHoldStatusID in (6,8)
	and isnull(shr.NewPickupBranchID, shr.PickupBranchID) = 33
order by p.PatronID

select * from Polaris.Polaris.SysHoldStatuses

select	shr.PatronID
		,p.Barcode [PatronBarcode]
		,cir.Barcode [ItemBarcode]
		,br.BrowseTitle [Title]
		,shr.StatusId [StatusId]
		,isnull(shs.Description, ils.Description) [StatusDescription]
		,shr.PickupBranchId
		,shr.IsIllRequest
from (
	select shr.PatronID, shr.TrappingItemRecordID [ItemRecordId], shr.SysHoldStatusID [StatusId], isnull(shr.NewPickupBranchID, shr.PickupBranchID) [PickupBranchId], 0 [IsIllRequest]
	from Polaris.polaris.SysHoldRequests shr
	where isnull(shr.NewPickupBranchID, shr.PickupBranchID) = 37
	union all
	select ir.PatronID, ir.ItemRecordID, ir.ILLStatusID, ir.PickupBranchID, 1
	from Polaris.Polaris.ILLRequests ir
	where ir.PickupBranchID = 37
) shr
join Polaris.Polaris.Patrons p
	on p.PatronID = shr.PatronID
join Polaris.Polaris.CircItemRecords cir
	on cir.ItemRecordID = shr.ItemRecordId
join Polaris.Polaris.BibliographicRecords br
	on br.BibliographicRecordID = cir.AssociatedBibRecordID
left join Polaris.Polaris.SysHoldStatuses shs
	on shs.SysHoldStatusID = shr.StatusId and shr.IsIllRequest = 0
left join Polaris.Polaris.ILLStatuses ils
	on ils.ILLStatusID = shr.StatusId and shr.IsIllRequest = 1
where shr.StatusId in (6,10)
	--and exists ( select 1 from Polaris.Polaris.ILLRequests ir where ir.PatronID = shr.PatronID and ir.ILLStatusID = 10 )
	--and exists ( select 1 from Polaris.Polaris.SysHoldRequests shr2 where shr2.PatronID = shr.PatronID and shr2.SysHoldStatusID = 6 )
order by shr.PatronID



select * from Polaris.polaris.CircItemRecords cir where cir.InTransitSentBranchID = 37



select * from Polaris.dbo.CLC_Custom_SIP_Port_Info sip where sip.OrganizationID = 152
select * from Polaris.polaris.SysHoldStatuses
select * from Polaris.polaris.Organizations o where o.ParentOrganizationID = 16

select * from Polaris.polaris.organizations

select * from clc_web_membership.dbo.ApiKeys