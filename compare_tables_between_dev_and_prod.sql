declare @tables table ( tablename varchar(255) )

insert into @tables values ('RecordSets')
						  ,('PatronRecordSets')
						  ,('Patrons')
						  ,('PatronRegistration')
						  ,('Organizations')
						  ,('NotificationQueue')
						  ,('NotificationHistory')
						  ,('PatronNotes')
						  ,('ItemCheckouts')
						  ,('CircItemRecords')
						  ,('SysHoldRequests')
						  ,('BibliographicRecords')
						  ,('DatesClosed')

;with dev as (
	select  t.name	 [tablename]
			,c.name  [columnname]
			,t2.name [datatype]
	from devdb.Polaris.sys.tables t
	join devdb.Polaris.sys.columns c
		on c.object_id = t.object_id
	join sys.types t2
		on t2.system_type_id = c.system_type_id
	where t.name in ( select tablename from @tables )
	union all
	select  t.name	 [tablename]
			,c.name  [columnname]
			,t2.name [datatype]
	from devdb.Results.sys.tables t
	join devdb.Results.sys.columns c
		on c.object_id = t.object_id
	join sys.types t2
		on t2.system_type_id = c.system_type_id
	where t.name in ( select tablename from @tables )
), prod as (
	select	t.name	 [tablename]
			,c.name  [columnname]
			,t2.name [datatype]
	from Polaris.sys.tables t
	join Polaris.sys.columns c
		on c.object_id = t.object_id
	join sys.types t2
		on t2.system_type_id = c.system_type_id
	where t.name in ( select tablename from @tables )
	union all
	select	t.name	 [tablename]
			,c.name  [columnname]
			,t2.name [datatype]
	from Results.sys.tables t
	join Results.sys.columns c
		on c.object_id = t.object_id
	join sys.types t2
		on t2.system_type_id = c.system_type_id
	where t.name in ( select tablename from @tables )
)

select	d.tablename		[dev TableName]
		,d.columnname	[dev ColumnName]
		,d.datatype		[dev DataType]
		,'' [ ]
		,p.tablename	[prod TableName]
		,p.columnname	[prod ColumnName]
		,p.datatype		[prod DataType]
from prod p
full outer join dev d
	on d.tablename = p.tablename and d.columnname = p.columnname and d.datatype = p.datatype
where p.tablename is null or d.tablename is null
