begin tran

update c
set c.Name = replace(c.Name, 'prod', 'dev')
	,c.ConnectionString = replace(c.ConnectionString, 'prod', 'dev')
	,c.ConfigurationString = replace(c.ConfigurationString, 'prod', 'dev')
from DBSyncConfig.Polaris.Connections c
where c.Name like 'prod%'


update s
set s.AppServerName = replace(s.AppServerName, 'prod', 'dev')
	,s.Z3950NameSpace = replace(s.Z3950NameSpace, 'prod', 'dev')
	,s.WebServerName = replace(s.WebServerName, 'prod', 'dev')
from DBSyncConfig.Polaris.DWIAppServers s


update op
set op.Value = replace(replace(op.Value, 'prod', 'dev'), 'catalog.clcohio.org', 'devpac.clcohio.org')
from DBSyncConfig.Polaris.OrganizationsPPPP op
where op.Value like '%prod%' 
	or op.Value like '%catalog.clcohio.org%'


update v
set v.AppServer = replace(v.AppServer, 'prod', 'dev')
from DBSyncConfig.Polaris.PolarisDBVersion v


update o
set o.OutputDirectory = replace(o.OutputDirectory, 'prod', 'dev')
	,o.ReportingServicesServerName = replace(o.ReportingServicesServerName, 'reports.clcohio.org', 'devreports.clcohio.org')
	,o.ERMSTarget = replace(o.ERMSTarget, 'prod', 'dev')
	,o.SRWebServiceURL = replace(o.SRWebServiceURL, 'simplyreports.clcohio.org', 'devsr.clcohio.org')
	,o.SRMARCWebServiceURL = replace(o.SRMARCWebServiceURL, 'simplyreports.clcohio.org', 'devreports.clcohio.org')
from DBSyncConfig.Polaris.RWRITERexecOptions o


update s
set s.ComputerName = replace(s.ComputerName, 'prod', 'dev')
	,s.DisplayName = replace(s.ComputerName, 'prod', 'dev')
from DBSyncConfig.Polaris.Servers s
where s.ComputerName like 'prod%'


update sp
set sp.Value = replace(replace(sp.Value, 'prod', 'dev'), 'catalog.clcohio.org', 'devpac.clcohio.org')
from DBSyncConfig.Polaris.ServersPPPP sp


select * from DBSyncConfig.Polaris.Connections
select * from DBSyncConfig.Polaris.DWIAppServers
select * from DBSyncConfig.Polaris.OrganizationsPPPP
select * from DBSyncConfig.Polaris.PolarisDBVersion
select * from DBSyncConfig.Polaris.RWRITERexecOptions
select * from DBSyncConfig.Polaris.Servers
select * from DBSyncConfig.Polaris.ServersPPPP

--rollback
--commit