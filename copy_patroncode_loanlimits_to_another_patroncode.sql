begin tran

declare @library int = 39
declare @source_patroncode int = 1
declare @destination_patroncode int = 10

update pll2
set pll2.MaxFine = pll1.MaxFine, 
	pll2.MinFine = pll1.MinFine, 
	pll2.TotalItems = pll1.TotalItems, 
	pll2.TotalOverDue = pll1.TotalOverDue,
	pll2.TotalHolds = pll1.TotalHolds,
	pll2.TotalILL = pll1.TotalILL,
	pll2.TotalReserveItems = pll1.TotalReserveItems
from Polaris.Polaris.PatronLoanLimits pll1
join Polaris.Polaris.PatronLoanLimits pll2 on
	pll1.OrganizationID = pll2.OrganizationID
join Polaris.Polaris.organizations o on
	pll1.OrganizationID = o.OrganizationID and pll2.OrganizationID = o.OrganizationID
where o.ParentOrganizationID = @library and pll1.PatronCodeID = @source_patroncode and pll2.PatronCodeID = @destination_patroncode

select pll.*
from Polaris.Polaris.PatronLoanLimits pll
join Polaris.Polaris.organizations o on
	pll.OrganizationID = o.OrganizationID
where o.ParentOrganizationID = @library and pll.PatronCodeID in (@source_patroncode,@destination_patroncode)

update mll2
set mll2.MaxItems = mll1.MaxItems,
	mll2.MaxRequestItems = mll1.MaxRequestItems
from Polaris.Polaris.MaterialLoanLimits mll1
join Polaris.Polaris.MaterialLoanLimits mll2 on
	mll1.OrganizationID = mll2.OrganizationID and mll1.MaterialTypeID = mll2.MaterialTypeID
join Polaris.Polaris.organizations o on
	o.OrganizationID = mll1.OrganizationID and o.OrganizationID = mll2.OrganizationID
where o.ParentOrganizationID = @library and mll1.PatronCodeID = @source_patroncode and mll2.PatronCodeID = @destination_patroncode

select mll.*
from Polaris.Polaris.MaterialLoanLimits mll
join Polaris.Polaris.Organizations o on
	mll.OrganizationID = o.OrganizationID
where o.ParentOrganizationID = @library and mll.PatronCodeID in (@source_patroncode,@destination_patroncode)

--rollback
--commit