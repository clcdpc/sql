declare @branch int = 115

declare @output table (mnemonic varchar(255), description varchar(255), value varchar(max))

declare @settings table ( mnemonic varchar(100), descriptionOverride varchar(255) default(null), sortorder int identity ) 
declare @valueOverrides table ( mnemonic varchar(100), oldValue varchar(255), newValue varchar(255) )

insert into @settings (mnemonic) values('NSPARMRMCOMBINE')
insert into @settings (mnemonic) values('NSPARMRMEMAILREPLYTO')
insert into @settings (mnemonic) values('NSPARMRMDAYSBETWEENMAIL')

insert into @settings (mnemonic) values('NSPARMRMBFROVD')
insert into @settings (mnemonic) values('NSPARMRMDAYSBFROVD')
insert into @settings (mnemonic) values('NSPARMRMPATEXP')
insert into @settings (mnemonic) values('NSPARMRMDAYSPATEXP')
insert into @settings (mnemonic) values('NSPARMRMINACTV')
insert into @settings (mnemonic) values('NSPARMRMDAYSINACTV')
insert into @settings (mnemonic) values('NSPARMRM_ADDTXT')

insert into @settings (mnemonic) values('NSPARM_EMN_FINES_ENABLE')
insert into @settings (mnemonic) values('NSPARM_EMN_FINE_FINEAGE')
insert into @settings (mnemonic) values('NSPARM_EMN_FINE_MINBALANCE')
insert into @settings (mnemonic) values('NSPARM_FINE_METHOD')
insert into @settings (mnemonic) values('NSPARM_FINE_ADDTXT')

insert into @settings (mnemonic) values('NSPARMRMOVRDUE')
insert into @settings (mnemonic) values('PSPARMOVRDUEINTRVL1')
insert into @settings (mnemonic) values('NSPARM_OVD1_METHOD')
insert into @settings (mnemonic) values('NSPARM_OVD1_ADDTXT')
insert into @settings (mnemonic) values('PSPARMOVRDUEINTRVL2')
insert into @settings (mnemonic) values('NSPARM_OVD2_METHOD')
insert into @settings (mnemonic) values('NSPARM_OVD2_ADDTXT')
insert into @settings (mnemonic) values('PSPARMOVRDUEINTRVL3')
insert into @settings (mnemonic) values('NSPARM_OVD3_METHOD')
insert into @settings (mnemonic) values('NSPARM_OVD3_ADDTXT')
insert into @settings (mnemonic) values('NSPARMRMBILLING')
insert into @settings (mnemonic, descriptionOverride) values('PSPARMBILLINGINTRVL', 'Bill notice interval')
insert into @settings (mnemonic) values('NSPARM_BILL_ADDTXT')
insert into @settings (mnemonic) values('NSPARM_BILL_METHOD')

insert into @settings (mnemonic) values('NSPARMRMREQUEST')
insert into @settings (mnemonic) values('NSPARMRMREQUEST2')
insert into @settings (mnemonic) values('NSPARM_REQUEST2_INTERVAL')
insert into @settings (mnemonic) values('NSPARM_REQUEST_METHOD')
insert into @settings (mnemonic) values('NSPARM_REQUEST_ADDTXT')

insert into @settings (mnemonic) values('NSPARMRMREQUESTCANCELED')
insert into @settings (mnemonic) values('NSPARM_REQUEST_CANCEL_ADDTXT')
insert into @settings (mnemonic) values('NSPARM_REQUEST_CANCEL_METHOD')

insert into @settings (mnemonic) values('NSPARMRMROUTING')
insert into @settings (mnemonic) values('NSPARM_ROUTING_METHOD')

declare @wording table (mnemonic varchar(255), description varchar(1024))

insert into @wording values ('NT_CANCEL_M_HEADER', 'Mail Cancel Notice: Header')
insert into @wording values ('NT_CANCEL_M_TEXT', 'Mail Cancel Notice: Text')
insert into @wording values ('NT_COMBINE_M_HEADER', 'Mail Combined Notice: Header')
insert into @wording values ('NT_COMBINE_M_TEXT', 'Mail Combined Notice: Text')
insert into @wording values ('NT_COMM_M_HEADER', 'Mail Community Notice: Header')
insert into @wording values ('NT_COMM_M_TEXT', 'Mail Community Notice: Text')
insert into @wording values ('NT_FINE_M_HEADER', 'Mail Fine Notice: Header')
insert into @wording values ('NT_FINE_M_TEXT', 'Mail Fine Notice: Text')
insert into @wording values ('NT_HOLD_M_HEADER', 'Mail Hold Notice: Header')
insert into @wording values ('NT_HOLD_M_TEXT', 'Mail Hold Notice: Text')
insert into @wording values ('NT_MISSINGPART_M_HEADER', 'Mail Missing Part Notice: Header')
insert into @wording values ('NT_MISSINGPART_M_TEXT', 'Mail Missing Part Notice: Text')
insert into @wording values ('NT_OVD_M_HEADER', 'Mail 1st Overdue Notice: Header')
insert into @wording values ('NT_OVD_M_TEXT', 'Mail 1st Overdue Notice: Text')
insert into @wording values ('NT_OVD2ND_M_HEADER', 'Mail 2nd Overdue Notice: Header')
insert into @wording values ('NT_OVD2ND_M_TEXT', 'Mail 2nd Overdue Notice: Text')
insert into @wording values ('NT_OVD3RD_M_HEADER', 'Mail 3rd Overdue Notice: Header')
insert into @wording values ('NT_OVD3RD_M_TEXT', 'Mail 3rd Overdue Notice: Text')
insert into @wording values ('NT_BILL_M_HEADER', 'Mail Bill Notice: Header')
insert into @wording values ('NT_BILL_M_TEXT', 'Mail Bill Notice: Text')
insert into @wording values ('NT_ROUTE_M_HEADER', 'Mail Routing Notice: Header')
insert into @wording values ('NT_ROUTE_M_TEXT', 'Mail Routing Notice: Text')

-- change all 'request notice' settings to say 'hold notice' instead
update s
set s.descriptionOverride = replace(aa.AttrDesc, 'Request Notice', 'Hold Notice')
from @settings s
join Polaris.polaris.AdminAttributes aa
	on aa.Mnemonic = s.mnemonic
where aa.AttrDesc like '%request notice%'

-- add value overrides for all delivery method settings to make them say the description instead of id
insert into @valueOverrides select mnemonic, '0', 'Patron Preference' from @settings where mnemonic like '%_method'
insert into @valueOverrides select mnemonic, '1', 'Print Only' from @settings where mnemonic like '%_method'
insert into @valueOverrides values('NSPARM_REQUEST_CANCEL_METHOD', '2', 'Patron Preference')

-- insert settings into output table
insert into @output
select	 aa.Mnemonic
		,isnull(s.descriptionOverride, aa.AttrDesc) [Description]
		,isnull(vo.newValue, Polaris.Polaris.fn_SA_GetValue(aa.Mnemonic, @branch)) [Value]
from Polaris.Polaris.AdminAttributes aa
join @settings s
	on s.mnemonic = aa.Mnemonic
left join @valueOverrides vo
	on vo.mnemonic = aa.Mnemonic and vo.oldValue = Polaris.Polaris.fn_SA_GetValue(aa.Mnemonic, @branch)
order by s.sortorder

-- insert wording into output table
insert into @output
select s.Mnemonic, s.description, s.Value
from (
	select *, rank() over(partition by s.Mnemonic order by s.OrganizationID desc) [therank]
	from (
		select 1 [OrganizationID], ds.Mnemonic, w.description, ds.Value
		from polaris.polaris.SA_DefaultMultiLingualStrings ds
		join @wording w on w.mnemonic = ds.Mnemonic
		where ds.LanguageID = 1033
		union all
		select s.OrganizationID, s.Mnemonic, w.description, s.Value
		from polaris.polaris.SA_CustomMultiLingualStrings s
		join @wording w on w.mnemonic = s.Mnemonic
		where s.LanguageID = 1033
			and s.OrganizationID = @branch
	) s
) s
where s.therank = 1

-- insert clc dialout settings into output table
insert into @output
select '', 'CLC Dialout: ' + dst.Description, s.Value
from CLC_Notices.dbo.Dialout_String_Types dst
join (
	select ds.*, rank() over(partition by ds.StringTypeID order by ds.OrganizationID desc) [therank]
	from CLC_Notices.dbo.Dialout_Strings ds
	where ds.OrganizationID in (@branch, (select _o.ParentOrganizationID from polaris.polaris.Organizations _o where _o.OrganizationID = @branch), 1)
) s
	on s.StringTypeID = dst.StringTypeID
where s.therank = 1

select * from @output