/*
change item templates (names = *ohiol*) to replacement cost of $125
change current item records replacement cost to $125
change fine rates to $0
*/

begin tran

update it
set it.Price = 125.00
from polaris.polaris.ItemTemplates it
where it.Name like '%ohiol%'

update ird
set ird.Price = 125
from polaris.polaris.CircItemRecords cir
join polaris.polaris.ItemRecordDetails ird
	on ird.ItemRecordID = cir.ItemRecordID
where cir.FineCodeID = 44

update f
set f.Amount = 0
	,f.MaximumFine = 0
from polaris.polaris.Fines f
where f.FineCodeID = 44

--rollback
--commit