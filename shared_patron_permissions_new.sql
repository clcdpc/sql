declare @branches table (orgid int)

insert into @branches values (157)

;with permissionBranchCounts as (
	select pg.GroupID, cr.ControlRecordDefID, pd.PermissionNameID, count(distinct o.ParentOrganizationID) [BranchCount]
	from Polaris.Polaris.PermissionGroups pg
	join Polaris.Polaris.ControlRecords cr
		on cr.ControlRecordID = pg.ControlRecordID
	join Polaris.Polaris.ControlRecordDefs crd
		on crd.ControlRecordDefID = cr.ControlRecordDefID
	join Polaris.polaris.PermissionDefs pd
		on pd.PermissionID = pg.PermissionID and pd.ControlRecordDefID = crd.ControlRecordDefID
	join Polaris.polaris.organizations o
		on o.OrganizationID = cr.OrganizationID
	group by pg.GroupID, cr.ControlRecordDefID, pd.PermissionNameID
), allPerms as (
	select distinct
			 g.GroupID
			,g.GroupName
			,pp_new.PermissionID
			,pp_new.ControlRecordID
			,pp_new.ControlRecordName
			,pp_new.PermissionName
			,pp_new.OrganizationID
			,o_new.Name
			,pp_new.ControlRecordDefID
			,pp_new.PermissionNameID
			,pp_source.SubSystem
			,pbc.BranchCount
	from Polaris.Polaris.Groups g
	join polaris.Polaris.PermissionGroups pg
		on pg.GroupID = g.GroupID
	join Polaris.dbo.CLC_Custom_PolarisPermissions pp_source
		on pp_source.ControlRecordID = pg.ControlRecordID and pp_source.PermissionID = pg.PermissionID
	join Polaris.Polaris.Organizations o_source
		on o_source.OrganizationID = pp_source.OrganizationID
	join Polaris.dbo.CLC_Custom_PolarisPermissions pp_new
		on pp_new.ControlRecordName not like '%patron record sets%' and pp_new.ControlRecordDefID = pp_source.ControlRecordDefID and pp_new.PermissionNameID = pp_source.PermissionNameID and pp_new.OrganizationID != pp_source.OrganizationID
	join polaris.Polaris.Organizations o_new
		on o_new.OrganizationID = pp_new.OrganizationID
	join permissionBranchCounts pbc
		on pbc.GroupID = pg.GroupID and pbc.ControlRecordDefID = pp_source.ControlRecordDefID and pbc.PermissionNameID = pp_source.PermissionNameID
	where (pp_source.SubSystemID = 2 or o_new.ParentOrganizationID = o_source.ParentOrganizationID) -- allow other subsystems for same library
		-- excluded sources
		and case o_source.OrganizationCodeID when 2 then o_source.OrganizationID when 3 then o_source.ParentOrganizationID end not in (14)
		-- exclude non-shared patron libraries from new permissions, ALE in shared patron as of 4/13/2023
		and ( case o_new.OrganizationCodeID when 2 then o_new.OrganizationID when 3 then o_new.ParentOrganizationID end not in (4,8,86) )
		-- exclude non-shared patron library groups
		and g.GroupName not like 'FCD%' and g.GroupName not like 'LPL%' and g.GroupName not like 'Scoville%'
		-- only add permissions to valid groups
		and g.GroupID >= 22
		-- exclude archived branches
		and (o_new.Name not like 'z%')-- or o_new.OrganizationID = 92)
		-- exclude 'deny item request'
		and pp_new.PermissionNameID not in (35)
		-- new org is one of the ones specified
		and o_new.OrganizationID in (select * from @branches)
		-- group has permissions at at least 5 branches or shares the parent org
		and (pbc.BranchCount > 5 or o_new.ParentOrganizationID = o_source.ParentOrganizationID)
		--and (exists ( select 1 from Polaris.dbo.CLC_Custom_PermissionGroups _pg where _pg.GroupID = g.GroupID and _pg.ControlRecordDefID = pp_source.ControlRecordDefID and _pg.PermissionNameID = pp_source.PermissionNameID and _pg.LibraryId != o_source.ParentOrganizationID ) or o_new.ParentOrganizationID = o_source.ParentOrganizationID )
)





select ap.*
from allPerms ap
left join polaris.polaris.PermissionGroups pg
	on pg.GroupID = ap.GroupID and pg.ControlRecordID = ap.ControlRecordID and pg.PermissionID = ap.PermissionID
where pg.GroupID is null
order by ap.Subsystem, ap.GroupName, ap.PermissionName







--insert into polaris.polaris.PermissionGroups
--select ap.ControlRecordID, ap.PermissionID, ap.GroupID
--from allPerms ap
--left join polaris.polaris.PermissionGroups pg
--	on pg.GroupID = ap.GroupID and pg.ControlRecordID = ap.ControlRecordID and pg.PermissionID = ap.PermissionID
--where pg.GroupID is null






























-- OLD VERSION BELOW
-- **********************************

----begin tran

--declare @branches table (orgid int)

--insert into @branches values (157)

--;with permissionBranchCounts as (
--	select pg.GroupID, cr.ControlRecordDefID, pd.PermissionNameID, count(distinct o.ParentOrganizationID) [BranchCount]
--	from Polaris.Polaris.PermissionGroups pg
--	join Polaris.Polaris.ControlRecords cr
--		on cr.ControlRecordID = pg.ControlRecordID
--	join Polaris.Polaris.ControlRecordDefs crd
--		on crd.ControlRecordDefID = cr.ControlRecordDefID
--	join Polaris.polaris.PermissionDefs pd
--		on pd.PermissionID = pg.PermissionID and pd.ControlRecordDefID = crd.ControlRecordDefID
--	join Polaris.polaris.organizations o
--		on o.OrganizationID = cr.OrganizationID
--	group by pg.GroupID, cr.ControlRecordDefID, pd.PermissionNameID
--)

----select *
----from permissionBranchCounts pbc
----join polaris.dbo.CLC_Custom_PolarisPermissions pp
----	on pp.ControlRecordDefID = pbc.ControlRecordDefID and pp.PermissionNameID = pbc.PermissionNameID
----where pbc.GroupID = 153

----insert into polaris.polaris.permissiongroups select d.ControlRecordID, d.PermissionID, d.GroupID from (
--	select distinct g.GroupID, g.GroupName, pp_new.PermissionID, pp_new.ControlRecordID, pp_new.ControlRecordName, pp_new.PermissionName, pp_new.OrganizationID, o_new.Name, pp_new.ControlRecordDefID, pp_new.PermissionNameID, pp_source.SubSystem, pbc.BranchCount
--	from Polaris.Polaris.Groups g
--	join polaris.Polaris.PermissionGroups pg
--		on pg.GroupID = g.GroupID
--	join Polaris.dbo.CLC_Custom_PolarisPermissions pp_source
--		on pp_source.ControlRecordID = pg.ControlRecordID and pp_source.PermissionID = pg.PermissionID
--	join Polaris.Polaris.Organizations o_source
--		on o_source.OrganizationID = pp_source.OrganizationID
--	join Polaris.dbo.CLC_Custom_PolarisPermissions pp_new
--		on pp_new.ControlRecordName not like '%patron record sets%' and pp_new.ControlRecordDefID = pp_source.ControlRecordDefID and pp_new.PermissionNameID = pp_source.PermissionNameID and pp_new.OrganizationID != pp_source.OrganizationID
--	join polaris.Polaris.Organizations o_new
--		on o_new.OrganizationID = pp_new.OrganizationID
--	join permissionBranchCounts pbc
--		on pbc.GroupID = pg.GroupID and pbc.ControlRecordDefID = pp_source.ControlRecordDefID and pbc.PermissionNameID = pp_source.PermissionNameID
--	where (pp_source.SubSystemID = 2 or o_new.ParentOrganizationID = o_source.ParentOrganizationID) -- allow other subsystems for same library
--		-- excluded sources
--		and case o_source.OrganizationCodeID when 2 then o_source.OrganizationID when 3 then o_source.ParentOrganizationID end not in (14)
--		-- permission not already granted
--		--and not exists ( select 1 from Polaris.Polaris.PermissionGroups _pg where _pg.GroupID = g.GroupID and _pg.ControlRecordID = pp_new.ControlRecordID and _pg.PermissionID = pp_new.PermissionID )
--		-- exclude non-shared patron libraries from new permissions, ALE in shared patron as of 4/13/2023
--		and ( case o_new.OrganizationCodeID when 2 then o_new.OrganizationID when 3 then o_new.ParentOrganizationID end not in (4,8,86) )
--		-- ALE in shared patron as of 4/13/2023
--		--and g.GroupName not like 'ALE%' 
--		-- exclude non-shared patron library groups
--		and g.GroupName not like 'FCD%' and g.GroupName not like 'LPL%' and g.GroupName not like 'Scoville%'
--		-- only add permissions to valid groups
--		and g.GroupID >= 22
--		-- exclude archived branches
--		and (o_new.Name not like 'z%')-- or o_new.OrganizationID = 92)
--		-- exclude 'deny item request'
--		and pp_new.PermissionNameID not in (35)
--		-- new org is one of the ones specified
--		and o_new.OrganizationID in (select * from @branches)
--		-- group has permissions at at least 5 branches or shares the parent org
--		--and (pbc.BranchCount > 5 or o_new.ParentOrganizationID = o_source.ParentOrganizationID)
--		--and (exists ( select 1 from Polaris.dbo.CLC_Custom_PermissionGroups _pg where _pg.GroupID = g.GroupID and _pg.ControlRecordDefID = pp_source.ControlRecordDefID and _pg.PermissionNameID = pp_source.PermissionNameID and _pg.LibraryId != o_source.ParentOrganizationID ) or o_new.ParentOrganizationID = o_source.ParentOrganizationID )
--	order by pp_source.Subsystem, g.GroupName
----) d

----rollback
----commit

----select * from polaris.polaris.ControlRecords cr where cr.OrganizationID = 157

--/*
--select pg.GroupID, cr.ControlRecordDefID, pd.PermissionNameID, count(distinct o.ParentOrganizationID)
--from Polaris.Polaris.PermissionGroups pg
--join Polaris.Polaris.ControlRecords cr
--	on cr.ControlRecordID = pg.ControlRecordID
--join Polaris.Polaris.ControlRecordDefs crd
--	on crd.ControlRecordDefID = cr.ControlRecordDefID
--join Polaris.polaris.PermissionDefs pd
--	on pd.ControlRecordDefID = crd.ControlRecordDefID
--join Polaris.polaris.organizations o
--	on o.OrganizationID = cr.OrganizationID
--group by pg.GroupID, cr.ControlRecordDefID, pd.PermissionNameID
--order by pg.GroupID, cr.ControlRecordDefID, pd.PermissionNameID
--*/

----select * from polaris.polaris.Groups
