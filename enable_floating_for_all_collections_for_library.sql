declare @library int = 105

begin tran

insert into Polaris.Polaris.OrganizationsFloatingCollections
select oc.OrganizationID, oc.CollectionID
from Polaris.polaris.OrganizationsCollections oc
join Polaris.polaris.Organizations o
	on o.OrganizationID = oc.OrganizationID
where not exists ( select 1 from polaris.polaris.OrganizationsFloatingCollections ofc where ofc.HomeBranchID = oc.OrganizationID and ofc.CollectionID = oc.CollectionID )
	and o.ParentOrganizationID = @library

--rollback
--commit