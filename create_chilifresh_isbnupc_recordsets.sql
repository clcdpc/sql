
create table #isbnupcs ( isbnupc varchar(24), numtype varchar(4), hasimg int )
/*
BULK INSERT #isbnupcs
FROM 'c:\temp\clcohio_isbns_dbcheck.csv'
WITH
(
	FORMAT='CSV',
	FIRSTROW = 2,
	FIELDTERMINATOR = ';',
	ROWTERMINATOR = '\n',
	TABLOCK
)

--drop table #isbnupcs
*/


select top 1000 *
from #isbnupcs i
left join polaris.polaris.BibliographicISBNIndex bii
	on bii.ISBNString = i.isbnupc and i.numtype = 'isbn'
left join polaris.polaris.BibliographicUPCIndex bui
	on bui.UPCNumber = i.isbnupc and i.numtype = 'upc'
where i.hasimg = 1

begin tran

-- isbn with cover
insert into polaris.polaris.BibRecordSets
select distinct bii.BibliographicRecordID, 229724
from polaris.polaris.BibliographicISBNIndex bii
join #isbnupcs i
	on i.isbnupc = bii.ISBNString and i.hasimg = 1 and i.numtype = 'isbn'

-- isbn without cover
insert into polaris.polaris.BibRecordSets
select distinct bii.BibliographicRecordID, 229725
from #isbnupcs i
join polaris.polaris.BibliographicISBNIndex bii
	on bii.ISBNString = i.isbnupc and i.numtype = 'isbn'
where i.hasimg = 2

-- upc with cover
insert into polaris.polaris.BibRecordSets
select distinct bui.BibliographicRecordID, 229726
from polaris.polaris.BibliographicUPCIndex bui
join #isbnupcs i
	on i.isbnupc = bui.UPCNumber and i.hasimg = 1 and i.numtype = 'upc'

-- upc without cover
insert into polaris.polaris.BibRecordSets
select distinct bui.BibliographicRecordID, 229727
from #isbnupcs i
join polaris.polaris.BibliographicUPCIndex bui
	on bui.UPCNumber = i.isbnupc and i.numtype = 'upc'
where i.hasimg = 2


--rollback
--commit


select rs.RecordSetID from polaris.polaris.RecordSets rs where rs.Name like 'chili%cover%'


select *
from polaris.polaris.BibRecordSets brs
join polaris.polaris.BibliographicRecords br
	on br.BibliographicRecordID = brs.BibliographicRecordID
where brs.RecordSetID in (229724,229725,229726,229727)
	and br.RecordStatusID = 4