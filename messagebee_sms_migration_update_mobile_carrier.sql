--select * from polaris.polaris.organizations where OrganizationCodeID = 2

begin tran

update pr
set  pr.Phone1CarrierID = case when pr.Phone1CarrierID is not null then 26 end
	,pr.Phone2CarrierID = case when pr.Phone2CarrierID is not null then 26 end
	,pr.Phone3CarrierID = case when pr.Phone3CarrierID is not null then 26 end
from polaris.Polaris.Patrons p
join polaris.polaris.PatronRegistration pr
	on pr.PatronID = p.PatronID
join Polaris.polaris.organizations o
	on o.OrganizationID = p.OrganizationID
where o.ParentOrganizationID in 
		(2   -- Alexandria
		,6   -- Grandview
		,8   -- Fairfield
		,14  -- Plain City
		,16  -- Marysville
		,19  -- Pickaway
		,23  -- Pickerington
		,25  -- Wagnalls
		,28  -- Southwest
		,32  -- Worthington
		,78  -- UAPL
		,84  -- Bexley
		,86  -- London
		,105 -- Delaware
		,112 -- Granville
		,114)--Pataskala
	and (pr.Phone1CarrierID is not null or pr.Phone2CarrierID is not null or pr.Phone3CarrierID is not null)

--rollback
--commit