/**************************************

      MUST RUN AS POLARIS DB USER

**************************************/

begin tran

declare targetCursor cursor for
select edo.EntryID, edo.ParentEntryID
from Polaris.polaris.DWIEntryDisplayOrder edo
where edo.EntryID in (
	select e.EntryID
		from Polaris.Polaris.DWIEntries e
	join Polaris.Polaris.DWIAttributeTypes at on
		e.EntryID = at.EntryID
	join Polaris.Polaris.DWIAttributeValues av on
		at.AttributeTypeID = av.AttributeTypeID
	where at.AttributeType in ('Z39.50 Object', 'E-Source Object') and av.Value = 'ETarget'
)

declare @entryId int
declare @parentEntryId int

open targetCursor
fetch next from targetCursor into @entryId, @parentEntryId
while @@FETCH_STATUS = 0
begin	
	exec polaris.Polaris.DWIDeleteTargetFromSubject @entryId, @parentEntryId
	fetch next from targetCursor into @entryId, @parentEntryId
end

close targetCursor
deallocate targetCursor

declare subjectCursor cursor for
select edo.EntryID, edo.ParentEntryID
from Polaris.polaris.DWIEntryDisplayOrder edo
where edo.EntryID in (
	select e.EntryID
		from Polaris.Polaris.DWIEntries e
	join Polaris.Polaris.DWIAttributeTypes at on
		e.EntryID = at.EntryID
	join Polaris.Polaris.DWIAttributeValues av on
		at.AttributeTypeID = av.AttributeTypeID
	where at.AttributeType in ('Z39.50 Object', 'E-Source Object') and av.Value = 'ESubject'
)

open subjectCursor
fetch next from subjectCursor into @entryId, @parentEntryId
while @@FETCH_STATUS = 0
begin	
	exec polaris.Polaris.DWIDeleteSubjectFromSubject @entryId, @parentEntryId
	fetch next from subjectCursor into @entryId, @parentEntryId
end

close subjectCursor
deallocate subjectCursor

--rollback
--commit