declare @orgid int
declare @abbr varchar(50)
declare @rstid int
declare @rst varchar(50)

begin tran

delete nrs
from CLC_Notices.dbo.RecordSets nrs
left join Polaris.polaris.RecordSets rs
	on rs.RecordSetID = nrs.RecordSetID
where rs.RecordSetID is null

declare valueCursor cursor for
select a.organizationid, a.Abbreviation, a.RecordSetTypeID, a.RecordSetType
from (
	select  o.OrganizationID
			,o.Name
			,o.Abbreviation
			,rst.RecordSetTypeID
			,rst.Description [RecordSetType]
	from Polaris.Polaris.Organizations o
	cross join CLC_Notices.dbo.RecordSetTypes rst
	where o.OrganizationCodeID = 2
) a
left join CLC_Notices.dbo.RecordSets rs
	on rs.OrganizationID = a.OrganizationID and rs.RecordSetTypeID = a.RecordSetTypeID
where rs.RecordSetID is null

open valueCursor
fetch next from valueCursor into @orgid, @abbr, @rstid, @rst
while @@FETCH_STATUS = 0
begin
	declare @suffix varchar(255) = case @rstid
		when 1 then 'Bounced Email Patrons'
		when 2 then 'Email Spam Report Patrons'
		when 3 then 'SMS Opt-out Patrons'
	end
			
	declare @rsname varchar(255) = @abbr + ' ' + @suffix
	
	-- create record set
	insert into Polaris.polaris.RecordSets (Name, ObjectTypeID, CreatorID, OrganizationOwnerID, CreationDate, Note)
	values (@rsname, 27, 425, @orgid, getdate(), 'KEEP - Used by CLC notice application')

	-- insert clc_notices record set entry
	insert into CLC_Notices.dbo.RecordSets
	values ((select @@identity), @orgid, @rstid)
	

	--select @rsname

	fetch next from valueCursor into @orgid, @abbr, @rstid, @rst
end

close valueCursor
deallocate valueCursor

--rollback
--commit


select * 
from CLC_Notices.dbo.RecordSets nrs
join Polaris.polaris.RecordSets rs
	on rs.RecordSetID = nrs.RecordSetID