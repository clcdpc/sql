--Sample volume level hold request is 29469041. It is showing up on CML Hilliard's Daily Pull List (filtered by Periodical) to be filled with item 1333777736.  
--The issue value for the item is Vol. 325 No. 2  (Mar. 2020), but the request was for (July/Aug. 2019).


declare @org int = 62
declare @pickupBranches int = 44
declare @includeBranches int = 1
----------------------------------------------

declare @assignedBranchOnly int = 0
declare @includeLibraryBranches int = 1
declare @useDropdownBranches int = 2

select cir.Barcode		 [Barcode]
		,c.name			 [Collection]
		,ird.callnumber	 [CallNo]
		,sl.description	 [ShelfLocation]
		,case when items.Designation is not null then br.browsetitle + ' <' + items.Designation + '>' else br.BrowseTitle end	 [Title]
		,br.browseauthor [Author]
		,items.HoldCreationDate [HoldCreationDate]
		,o.Abbreviation [PickupBranch]
		,items.hold_rank
		,items.item_rank
		,cir.AssociatedBibRecordID
		,items.SysHoldRequestID
		,items.ItemLevelHoldItemRecordID
		,cir.ItemRecordID
		,pr.PatronFullName
		,cast(items.ItemLevelHold as int) [ItemLevelHold]
		,case when items.Designation is not null then 1 else 0 end [HasDesignation]
from (
	select cir.ItemRecordID
			,cir.AssociatedBibRecordID
			,cir.AssignedBranchID
			,shr.SysHoldRequestID
			,shr.PatronID
			,shr.ItemLevelHoldItemRecordID
			,shr.ItemLevelHold
			,shr.Designation
			,shr.VolumeNumber
			,cast(shr.CreationDate as date) [HoldCreationDate]
			,isnull(shr.NewPickupBranchID, shr.PickupBranchID) [PickupBranchID]
			,rank() over(partition by cir.ItemRecordID order by shr.Sequence) [hold_rank]
			,rank() over(partition by shr.SysHoldRequestID order by newid()) [item_rank]
	from Polaris.Polaris.CircItemRecords cir
	join polaris.polaris.ItemRecordDetails ird
		on ird.ItemRecordID = cir.ItemRecordID
	join Polaris.Polaris.SysHoldRequests shr
		on shr.BibliographicRecordID = cir.AssociatedBibRecordID
			and ( 
				   (@includeBranches = @assignedBranchOnly and isnull(shr.NewPickupBranchID, shr.PickupBranchID) = cir.AssignedBranchID)
				or (@includeBranches = @includeLibraryBranches and isnull(shr.NewPickupBranchID, shr.PickupBranchID) in ( select b.OrganizationID from Polaris.dbo.CLC_Custom_BranchAssociations b where b.LookupOrgId = cir.AssignedBranchID )) 
				or (@includeBranches = @useDropdownBranches and isnull(shr.NewPickupBranchID, shr.PickupBranchID) in ( @pickupBranches )) 
				)
			and shr.SysHoldStatusID in (3,4)
			and shr.Origin != 3
	where cir.ItemStatusID = 1
		--and cir.MaterialTypeID in ( @materialTypes )
		and cir.ElectronicItem = 0
		and cir.AssignedBranchID = @org
		and (shr.ItemLevelHold = 0 or shr.ItemLevelHoldItemRecordID = cir.ItemRecordID)
		and cir.Holdable = 1
		and (shr.Designation is null or exists ( select 1 from Polaris.Polaris.MfhdIssues i where (i.BibliographicRecordID = shr.BibliographicRecordID or i.ItemRecordID = cir.ItemRecordID) and i.Designation like '%' + shr.Designation + '%')) 
		and (shr.VolumeNumber is null or ird.VolumeNumber like '%' + shr.VolumeNumber + '%')
) items
left join polaris.polaris.CircItemRecords cir
	on items.ItemRecordID = cir.ItemRecordID
left join polaris.polaris.Collections c
	on cir.AssignedCollectionID = c.CollectionID
left join polaris.polaris.ShelfLocations sl
	on cir.ShelfLocationID = sl.ShelfLocationID and cir.AssignedBranchID = sl.OrganizationID
join polaris.polaris.ItemRecordDetails ird
	on cir.ItemRecordID = ird.ItemRecordID
join polaris.polaris.BibliographicRecords br
	on cir.AssociatedBibRecordID = br.BibliographicRecordID
join polaris.polaris.Organizations o
	on o.OrganizationID = items.PickupBranchID
join polaris.polaris.PatronRegistration pr
	on pr.PatronID = items.PatronID
left join Polaris.Polaris.MfhdIssues i
	on i.ItemRecordID = cir.ItemRecordID
where items.item_rank = 1
	and items.hold_rank = 1
order by c.CollectionID, ird.CallNumber





/*

select shr.SysHoldRequestID, shr.PickupBranchID, cir.AssignedBranchID
from polaris.polaris.SysHoldRequests shr 
join polaris.Polaris.CircItemRecords cir
	on cir.ItemRecordID = shr.ItemLevelHoldItemRecordID
where shr.ItemLevelHold = 1


select mt.MaterialTypeID
		,mt.Description
from Polaris.Polaris.MaterialTypes mt
where mt.Description not like 'zzz%'
order by mt.Description


select 'Assigned Branch Only' [Description], 0 [Value]
union all
select 'Include Branches From Dropdown', 1
union all
select 'Include My Library''s Branches', 2


select *
from (
select 0 [OrgId], 'Select Pickup Branches' [Name]
	union all
	select o.OrganizationID, o.Name
	from polaris.polaris.Organizations o
	where o.OrganizationCodeID = 3
) d
order by case when d.orgid = 0 then '___' else d.name end
*/

select * from polaris.polaris.CircItemRecords cir where cir.ItemRecordID = 21456353
select * from polaris.polaris.ItemRecordDetails ird where ird.ItemRecordID = 21456353
select * from polaris.polaris.BibliographicRecords br where br.BibliographicRecordID = 3222325
select * from polaris.polaris.SHRCopies c where c.BibRecordID = 3222325
select * from polaris.polaris.MfhdIssues i where i.ItemRecordID in ( select cir.ItemRecordID from polaris.Polaris.CircItemRecords cir where cir.AssociatedBibRecordID = 3222325 ) and i.Designation like '%July/Aug. 2019%'
select * from polaris.polaris.SysHoldRequests shr where shr.SysHoldRequestID = 29469041 

select * from Polaris.polaris.SysHoldRequests shr where shr.BibliographicRecordID = 3173297
select * from Polaris.polaris.Organizations where ParentOrganizationID = 39


select ird.*
from polaris.polaris.CircItemRecords cir
join polaris.polaris.ItemRecordDetails ird
	on ird.ItemRecordID = cir.ItemRecordID
where cir.AssociatedBibRecordID = 3222325



select * from polaris.Polaris.Organizations where ParentOrganizationID = 39 order by Name

select * from polaris.polaris.MfhdIssues i where i.ItemRecordID = 18979622