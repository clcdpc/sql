USE [CLC_Notices]
GO

/****** Object:  StoredProcedure [dbo].[GetCheckedOutItems]    Script Date: 6/16/2016 3:19:09 PM ******/
DROP PROCEDURE [dbo].[GetCheckedOutItems]
GO

/****** Object:  StoredProcedure [dbo].[GetCheckedOutItems]    Script Date: 6/16/2016 3:19:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetCheckedOutItems]
	@patronId int,
	@overdueOnly int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @dueDate datetime = case when @overdueOnly = 1 then current_timestamp else current_timestamp + 10000 end;

    select	ic.ItemRecordID,
			cir.Barcode,
			br.BrowseTitle,
			ic.OrganizationID,
			ic.CheckOutDate,
			ic.DueDate
	from Polaris.Polaris.ItemCheckouts ic
	join Polaris.Polaris.CircItemRecords cir on
		ic.ItemRecordID = cir.ItemRecordID
	join Polaris.Polaris.BibliographicRecords br on
		cir.AssociatedBibRecordID = br.BibliographicRecordID
	where ic.PatronID = @patronId and ic.DueDate <= @dueDate
	order by ic.DueDate
END


GO


