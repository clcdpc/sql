---Budget Spend

select
	Funds.ParentFundID
   ,Funds.FundID
   ,Organizations.Name as FundOwner
   ,FiscalYears.Name as FiscalYear
   ,(select f.Name
		from Polaris.Polaris.Funds as f with (nolock)
		where f.FundID = Funds.ParentFundID)
	as ParentFundName
   ,Funds.Name as FundName
   ,FundTypes.Type as FundType
   ,Funds.BegAllocation as BeginningAllocation
   ,Funds.TotAlloc as CurrentAllocation
   ,Funds.TotCurrEnc as Encumbered
   ,Funds.TotExp as Spent
   ,case
		when Funds.TypeID > 0 and Funds.TypeID < 3 then Funds.FreeBal
		when Funds.TypeID = 3 then Funds.FreeDepBal
		else 0
	end as FreeBalance
   ,Funds.PercentExpYTD as PercentExpended
from Polaris.Polaris.Funds as Funds
inner join Polaris.Polaris.FiscalYears as FiscalYears
	on Funds.FiscalYearID = FiscalYears.FiscalYearID
inner join Polaris.Polaris.FundTypes as FundTypes
	on Funds.TypeID = FundTypes.TypeID
inner join Polaris.Polaris.Organizations as Organizations
	on Funds.OrganizationID = Organizations.OrganizationID
where FiscalYears.FiscalYearStatusID = 1
	and Funds.ParentFundID is not null
	and Organizations.ParentOrganizationID = 23
order by Funds.OrganizationID, Funds.ParentFundID;


---Items

select
	cir.ItemRecordID
   ,cir.Barcode as ItemBarcode
   ,cir.AssociatedBibRecordID
   ,(select top 1 isbnString
		from polaris.polaris.BibliographicISBNIndex bi with (nolock)
		where bi.BibliographicRecordID = cir.AssociatedBibRecordID)
	as SortISBN
   ,(select top 1 UPCNumber
		from polaris.polaris.BibliographicUPCIndex bu with (nolock)
		where bu.BibliographicRecordID = cir.AssociatedBibRecordID)
	as SortUPC
   ,col.Abbreviation as CollectionAbbr
   ,isc.Description as ItemStatCodeDescr
   ,mt.Description as MaterialTypeDescription
   ,oi.Abbreviation as ItemBranchAbbr
   ,ird.CallNumber
   ,sl.Description as ShelfLocationDescription
   ,iob.Abbreviation as OwningOrgAbbr
   ,cir.FirstAvailableDate as ItemCreationDate
   ,ist.Description as ItemStatusDescr
   ,cir.LastCheckOutRenewDate
   ,cir.CheckInDate
   ,icko.DueDate
   ,cir.YTDCircCount as ItemYTDCircCount
   ,cir.LifetimeCircCount as ItemLifetimeCircCount
from polaris.Polaris.CircItemRecords cir with (nolock)
inner join polaris.Polaris.ItemRecordDetails ird with (nolock)
	on (cir.ItemRecordID = ird.ItemRecordID)
left join polaris.Polaris.Collections col with (nolock)
	on (cir.AssignedCollectionID = col.CollectionID)
inner join polaris.Polaris.ItemStatuses ist with (nolock)
	on (cir.ItemStatusID = ist.ItemStatusID)
inner join polaris.Polaris.MaterialTypes mt with (nolock)
	on (cir.MaterialTypeID = mt.MaterialTypeID)
inner join polaris.Polaris.Organizations oi with (nolock)
	on (cir.AssignedBranchID = oi.OrganizationID)
left join polaris.Polaris.Organizations iob with (nolock)
	on (ird.OwningBranchID = iob.OrganizationID)
left join polaris.Polaris.ShelfLocations sl with (nolock)
	on (cir.ShelfLocationID = sl.ShelfLocationID and cir.AssignedBranchID = sl.OrganizationID)
left join polaris.Polaris.StatisticalCodes isc with (nolock)
	on (cir.StatisticalCodeID = isc.StatisticalCodeID and cir.AssignedBranchID = isc.OrganizationID)
left join polaris.Polaris.ItemCheckouts icko with (nolock)
	on (cir.ItemRecordID = icko.ItemRecordID)
where oi.ParentOrganizationID = 23


---Bib Records

select
	br.BibliographicRecordID
   ,(select top 1 isbnString
		from polaris.polaris.BibliographicISBNIndex bi with (nolock)
		where bi.BibliographicRecordID = br.BibliographicRecordID)
	as sortISBN
   ,(select top 1 UPCNumber
		from polaris.polaris.BibliographicUPCIndex bu with (nolock)
		where bu.BibliographicRecordID = br.BibliographicRecordID)
	as sortUPC
   ,mtom.Description as MARCTOMDescription
   ,br.BrowseTitle
   ,br.BrowseAuthor
   ,br.MARCPubDateOne
from polaris.Polaris.BibliographicRecords br with (nolock)
left join polaris.Polaris.MARCTypeOfMaterial mtom with (nolock)
	on (br.PrimaryMARCTOMID = mtom.MARCTypeOfMaterialID)

---Circ Records

select
	cir.LastUsePatronID as PatronID
   ,cir.ItemRecordID
   ,cir.Barcode as ItemBarcode
   ,cir.AssociatedBibRecordID
   ,cir.LastCheckOutRenewDate
   ,ckobr.Abbreviation as CheckOutBranchAbbr
   ,icko.DueDate
   ,cir.CheckInDate
from polaris.Polaris.CircItemRecords cir with (nolock)
inner join polaris.Polaris.ItemRecordDetails ird with (nolock)
	on (cir.ItemRecordID = ird.ItemRecordID)
left join polaris.Polaris.ItemCheckouts icko with (nolock)
	on (cir.ItemRecordID = icko.ItemRecordID)
left join polaris.Polaris.Organizations ckobr with (nolock)
	on (cir.LoaningOrgID = ckobr.OrganizationID)
where cir.LastCheckOutRenewDate is not null
	and ckobr.ParentOrganizationID = 23

---Patron Records

select
	o.Abbreviation as branchcode
   ,P.patronid
   ,convert(varchar, P.LastActivityDate, 120) as LastActivityDate
   ,NameFirst
   ,NameLast
   ,StreetOne
   ,City
   ,State
   ,PostalCode
   ,convert(varchar, PR.ExpirationDate, 120) as ExpirationDate
   ,(select count(*)
		from Polaris.Polaris.ItemRecordHistory ih with (nolock)
		where ih.PatronID = P.PatronID
		and ActionTakenID in (13))
	as PreviousYearCount
   ,(select convert(varchar, max(TransactionDate), 120)
		from Polaris.Polaris.ItemRecordHistory ih with (nolock)
		where ih.PatronID = P.PatronID
		and ActionTakenID in (13))
	as LastCheckoutDate
   ,P.LifetimeCircCount as LifetimeCount
   ,P.YTDCircCount as YTDCount
   ,PR.RegistrationDate
from polaris.Polaris.Patrons P (nolock)
inner join polaris.Polaris.PatronCodes PC (nolock)
	on (P.PatronCodeID = PC.PatronCodeID)
inner join polaris.Polaris.PatronRegistration PR (nolock)
	on (P.PatronID = PR.PatronID)
join polaris.Polaris.PatronAddresses pad
	on pad.PatronID = P.PatronID
join polaris.Polaris.Addresses a
	on a.AddressID = pad.AddressID
join polaris.Polaris.PostalCodes pcc
	on pcc.PostalCodeID = a.PostalCodeID
join polaris.Polaris.Organizations o
	on P.OrganizationID = o.OrganizationID and o.ParentOrganizationID = 23
where AddressTypeID = 2
	and PR.ExpirationDate > getdate()

---Holds

select
	hr.BibliographicRecordID
   ,hpu.abbreviation as PickupBranchAbbr
   ,count(*) as NumberOfRequests
from polaris.Polaris.SysHoldRequests hr with (nolock)
inner join polaris.Polaris.Organizations hpu with (nolock)
	on (hr.PickupBranchID = hpu.OrganizationID)
where hr.SysHoldStatusID in (6)
	and hpu.ParentOrganizationID = 23
group by hr.BibliographicRecordID, hpu.abbreviation
order by NumberOfRequests desc

---InHouse Circ

select
	convert(varchar, th.TranClientDate, 120) as LastCheckOutRenewDate
   ,o.Abbreviation as CheckOutBranchAbbr
   ,td2.numValue as ItemRecordID
   ,th.PolarisUserID as PatronID
from PolarisTransactions.Polaris.TransactionHeaders th with (nolock)
inner join PolarisTransactions.Polaris.TransactionDetails td with (nolock)
	on td.TransactionID = th.TransactionID and td.TransactionSubTypeID = 128
inner join PolarisTransactions.Polaris.TransactionDetails td2 with (nolock)
	on td2.TransactionID = th.TransactionID and td2.TransactionSubTypeID = 38
join Polaris.polaris.Organizations o with (nolock)
	on o.OrganizationID = th.OrganizationID
where th.TransactionTypeID = 6002
	and o.ParentOrganizationID = 23
	and td.NumValue in (6, 56)
	and th.TranClientDate > dateadd(day, -14, getdate())
group by td2.numValue, convert(varchar, th.TranClientDate, 120), o.Abbreviation, th.PolarisUserID

      
    