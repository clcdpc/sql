begin tran

insert into Results.Polaris.HoldNotices
select v.ItemRecordID, v.AssignedBranchID, nq.ReportingOrgID, nq.PatronID, v.ItemBarcode, v.BrowseTitle, v.BrowseAuthor, v.ItemCallNumber, v.Price, v.Abbreviation, v.Name, v.PhoneNumberOne, nq.DeliveryOptionID, shr.HoldTillDate, cir.MaterialTypeID, 1033, nq.NotificationTypeID
from results.polaris.NotificationQueue nq 
join Polaris.Polaris.ViewHoldNoticesData v 
	on v.ItemRecordID = nq.ItemRecordID
join polaris.polaris.SysHoldRequests shr
	on shr.TrappingItemRecordID = nq.ItemRecordID
join Polaris.polaris.CircItemRecords cir
	on cir.ItemRecordID = shr.TrappingItemRecordID
where nq.PatronID = 1752752


select * from results.Polaris.HoldNotices hn where hn.PatronID = 1752752

--rollback
--commit