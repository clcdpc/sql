DECLARE @CircRemindersCreationDate DATETIME --Used as CreationDate for reminer notices
SELECT TOP(1)@CircRemindersCreationDate =
	CONVERT(DATETIME, RTRIM(run_date)) + ((run_time / 10000 * 3600) + ((run_time % 10000) / 100 * 60) + (run_time % 10000) % 100) / (86399.9964)
	FROM [msdb].[dbo].[sysjobhistory] h 
	JOIN msdb.dbo.sysjobs j ON h.job_id=j.job_id 
	WHERE run_status=1 
	AND (j.name = N'Notices Processing')
	AND (h.step_name = N'Process Almost Overdue, Expiration and Inactive Reminders');

with hoursofoperation(orgid, [hours]) as 
(
	select o.OrganizationID, isnull(opb.Value, opl.Value) [HoursOfOperation]
	from Polaris.Polaris.Organizations o
	left join Polaris.polaris.OrganizationsPPPP opb
		on opb.OrganizationID = o.OrganizationID and opb.AttrID = 96
	left join Polaris.polaris.OrganizationsPPPP opl
		on o.ParentOrganizationID = opl.OrganizationID and opl.AttrID = 96
	where o.OrganizationCodeID = 3
), notices as (
	select nq.PatronID, nq.NotificationTypeID, nq.ItemRecordID, nq.DeliveryOptionID, nq.ReportingOrgID, 0 [AutoRenewal], nq.IsAdditionalTxt, nq.CreationDate, nq.Amount
	from results.Polaris.NotificationQueue nq
	union all
	select cr.PatronID, cr.NotificationTypeID, cr.ItemRecordID, cr.DeliveryOptionID, cr.ReportingOrgID, cr.AutoRenewal, cr.IsAdditionalTxt, (SELECT @CircRemindersCreationDate) AS [CreationDate], null
	from Results.polaris.CircReminders cr
), pacctdata as (
	select   nq.PatronID
			,ird.CallNumber [BilledItemCallNo]
			,br.BrowseAuthor [BilledItemAuthor]
			,br.BrowseTitle [BilledItemTitle]
			,cast(pa.TxnDate as date) [BilledDate]
			,sum(pa.OutstandingAmount) [BilledItemOutstandingAmount]
	from Results.polaris.NotificationQueue nq
	join Polaris.Polaris.PatronAccount pa
		on pa.PatronID = nq.PatronID and pa.TxnCodeID = 1 and pa.OutstandingAmount > 0
	left join Polaris.polaris.CircItemRecords cir
		on cir.ItemRecordID = pa.ItemRecordID
	left join Polaris.polaris.ItemRecordDetails ird
		on ird.ItemRecordID = cir.ItemRecordID
	left join Polaris.polaris.BibliographicRecords br
		on br.BibliographicRecordID = cir.AssociatedBibRecordID
	where nq.NotificationTypeID = 8
		and nq.DeliveryOptionID = 2
	group by nq.PatronID, ird.CallNumber, br.BrowseAuthor, br.BrowseTitle, cast(pa.TxnDate as date)
)

Select n.NotificationTypeID
	,p.PatronID
	,p.Barcode [PatronBarcode]
	,case when pr.UseLegalNameOnNotices = 1 then pr.LegalNameFirst else pr.NameFirst end [NameFirst]
	,case when pr.UseLegalNameOnNotices = 1 then pr.LegalNameLast else pr.NameLast end [NameLast]
	,a.StreetOne [PatronStreetOne]
	,coalesce(a.StreetTwo, '') [PatronStreetTwo]
	,pc.City [PatronCity]
	,pc.State [PatronState]
	,pc.PostalCode [PatronZip]
	,isnull(n.ItemRecordID, 0) [ItemRecordID]
	,p.OrganizationID [PatronBranch]
	,o_patron.Abbreviation [PatronBranchAbbr]
	,isnull(o_patron.ParentOrganizationID, 0) [PatronLibrary]
	,o_patron2.Abbreviation [PatronLibraryAbbr]
	,n.DeliveryOptionID
	,pr.EmailAddress
	,coalesce(pr.AltEmailAddress, '') [AltEmailAddress]
	,pr.EmailFormatID
	,REPLACE(case n.DeliveryOptionID
		when 3 then pr.PhoneVoice1
		when 4 then pr.PhoneVoice2
		when 5 then pr.PhoneVoice3
		when 8 then
			case pr.TxtPhoneNumber
				when 1 then pr.PhoneVoice1
				when 2 then pr.PhoneVoice2
				when 3 then pr.PhoneVoice3
				else ''
			end
		else ''
	end,'-','') [DeliveryPhone]
	,REPLACE(coalesce(pr.PhoneVoice1, pr.PhoneVoice2, pr.PhoneVoice3),'-','') [PatronPhone]
	,isnull(n.ReportingOrgID,0) [ReportingBranchID]
	,o_notice.Abbreviation [ReportingBranchAbbr]	
	,o_notice.Name [ReportingBranchName]
	,saa_notice.StreetOne [ReportingBranchStreetOne]
	,saa_notice.StreetTwo [ReportingBranchStreetTwo]
	,saa_notice.City [ReportingBranchCity]
	,saa_notice.State [ReportingBranchState]
	,saa_notice.PostalCode [ReportingBranchZip]
	,isnull(o_notice.ParentOrganizationID,0) [ReportingLibraryID]
	,coalesce(shr.HoldTillDate, ir.HoldTillDate) [HoldTillDate]
	,ic.DueDate [DueDate]
	,cir.Barcode [ItemBarcode]
	,br.BrowseAuthor [Author]
	,coalesce(hcn.Title, br.BrowseTitle) [Title]
	,coalesce(isbn.ISBNString, ir.ISBN, upc.UPCNumber) [ISBNUPC]
	,isnull(mt.Description,'') [MaterialType]
	,ic.OriginalCheckOutDate [CkoDate]
	,o_cko.Name [CkoLocation]
	,ird.CallNumber [ItemCallNumber]
	,hoo.hours
	,isnull(l.AdminLanguageID, 1033) [PatronLanguage]
	,REPLACE(case pr.TxtPhoneNumber
		when 1 then REPLACE(pr.PhoneVoice1,' ','')
		when 2 then REPLACE(pr.PhoneVoice2,' ','')
		when 3 then REPLACE(pr.PhoneVoice3,' ','')
		else ''
	end,'-','') [TxtPhoneNumber]
	,pr.EnableSMS
	,hcn.Reason [HoldCancellationReason]
	,hcn.RequestDate [CancelledHoldRequestDate]
	,isnull(cast(isnull(n.AutoRenewal, 0) as bit),0) [AutoRenewal]
	,o_item.Name [ItemAssignedBranch]
	,hpu.name as [HoldPickupBranchName]
	,n.IsAdditionalTxt
	,n.CreationDate
	,op_branch_phone.value [ReportingBranchPhone]
	,REPLACE(case pr.TxtPhoneNumber
		when 1 then REPLACE(pr.PhoneVoice1,' ','') + (Select Email2SMSEmailAddress from polaris.polaris.SA_MobilePhoneCarriers where CarrierID = pr.Phone1CarrierID)
		when 2 then REPLACE(pr.PhoneVoice2,' ','') + (Select Email2SMSEmailAddress from polaris.polaris.SA_MobilePhoneCarriers where CarrierID = pr.Phone2CarrierID)
		when 3 then REPLACE(pr.PhoneVoice3,' ','') + (Select Email2SMSEmailAddress from polaris.polaris.SA_MobilePhoneCarriers where CarrierID = pr.Phone3CarrierID)
		else ''
	end,'-','') [Email2SMSEmailAddress]
	,pacct.BilledItemCallNo
	,pacct.BilledItemAuthor
	,pacct.BilledItemTitle
	,pacct.BilledDate
	,pacct.BilledItemOutstandingAmount
	,n.Amount [NoticeAmount]
	,p.ChargesAmount [PatronChargesAmount]
	,isnull(isbn.ISBNString,ir.ISBN) [ISBN]
	,upc.UPCNumber [UPC]
from notices n with (nolock)
join Polaris.Polaris.Patrons p with (nolock)
	on n.PatronID = p.PatronID
join Polaris.Polaris.PatronRegistration pr with (nolock)
	on p.PatronID = pr.PatronID
join Polaris.Polaris.Organizations o_patron with (nolock)
	on p.OrganizationID = o_patron.OrganizationID
join Polaris.Polaris.Organizations o_patron2 with (nolock)
	on o_patron.ParentOrganizationID = o_patron2.OrganizationID
join Polaris.Polaris.Organizations o_notice with (nolock)
	on n.ReportingOrgID = o_notice.OrganizationID
join polaris.polaris.OrganizationsPPPP op_notice_addr with (nolock)
	on op_notice_addr.OrganizationID = n.ReportingOrgID and op_notice_addr.AttrID = 212
join polaris.Polaris.SA_Addresses saa_notice with (nolock)
	on saa_notice.SA_AddressID = op_notice_addr.Value
left join polaris.polaris.CircItemRecords cir with (nolock)
	on cir.ItemRecordID = n.ItemRecordID
left join Polaris.polaris.Organizations o_item with (nolock)
	on o_item.OrganizationID = cir.AssignedBranchID
left join Polaris.Polaris.SysHoldRequests shr with (nolock)
	on shr.PatronID = p.PatronID and n.ItemRecordID = shr.TrappingItemRecordID
join polaris.polaris.OrganizationsPPPP op_branch_phone with (nolock)
	on op_branch_phone.OrganizationID = ISNULL(shr.PickupBranchID,p.OrganizationID) and op_branch_phone.AttrID = 6
left join Polaris.polaris.ILLRequests ir with (nolock)
	on ir.PatronID = n.PatronID and ir.ItemRecordID = n.ItemRecordID
left join polaris.Polaris.Organizations hpu with (nolock) 
	on (ISNULL(shr.PickupBranchID,ir.PickupBranchID) = hpu.OrganizationID)  
left join Polaris.polaris.BibliographicRecords br with (nolock)
	on br.BibliographicRecordID = cir.AssociatedBibRecordID
OUTER APPLY (SELECT TOP 1 * FROM polaris.Polaris.BibliographicISBNIndex ISBN WHERE ISBN.BibliographicRecordID = br.BibliographicRecordID ORDER BY BibliographicISBNIndexID DESC) isbn
OUTER APPLY (SELECT TOP 1 * FROM polaris.Polaris.BibliographicUPCIndex UPC WHERE UPC.BibliographicRecordID = br.BibliographicRecordID ORDER BY UPC.BibliographicUPCIndexID DESC) upc
left join hoursofoperation hoo with (nolock)
	on hoo.orgid = n.ReportingOrgID
left join polaris.polaris.PatronAddresses pa with (nolock)
	on pa.PatronID = p.PatronID and pa.AddressTypeID = 2
left join polaris.Polaris.Addresses a with (nolock)
	on a.AddressID = pa.AddressID
left join Polaris.polaris.PostalCodes pc with (nolock)
	on pc.PostalCodeID = a.PostalCodeID
left join Polaris.polaris.ItemCheckouts ic with (nolock)
	on ic.PatronID = n.PatronID and ic.ItemRecordID = n.ItemRecordID
left join polaris.Polaris.Languages l with (nolock)
	on l.LanguageID = pr.LanguageID
left join polaris.polaris.MaterialTypes mt with (nolock)
	on mt.MaterialTypeID = cir.MaterialTypeID
left join polaris.Polaris.Organizations o_cko with (nolock)
	on o_cko.OrganizationID = ic.OrganizationID
left join polaris.polaris.ItemRecordDetails ird with (nolock)
	on ird.ItemRecordID = cir.ItemRecordID
left join results.Polaris.HoldCancellationNotices hcn with (nolock)
	on hcn.RequestID = n.ItemRecordID and hcn.PatronID = n.PatronID
left join pacctdata pacct
	on pacct.PatronID = n.PatronID
where n.DeliveryOptionID in (2) --Email
and n.NotificationTypeID in (1, 2, 3, 7, 8, 10, 11, 12, 13, 18) -- 1stOD, Hold, Cancel, AlmostOD/AutoRenewReminder, Fine, Expiration, Bill, 2ndOD, 3rdOD, 2ndHold
and (n.NotificationTypeID != 3 or hcn.Reason is not null) -- Is a hold cancel notice
and (n.NotificationTypeID != 2 or shr.SysHoldRequestID is not null or ir.ILLRequestID is not null) -- Is a hold notice OR ILL Hold notice
and o_patron.ParentOrganizationID in (8,19,28,32,86,105,112,114)