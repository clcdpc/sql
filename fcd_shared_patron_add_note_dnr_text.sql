declare @patternMatch varchar(255) = '%---Only remove/change lines below if you are the registered library (DNR)---%'

declare @text varchar(255) = '
---Only remove/change lines below if you are the registered library (DNR)---
'




begin tran

update pn
	set pn.NonBlockingStatusNotes = case when pn.NonBlockingStatusNotes is not null and pn.NonBlockingStatusNotes not like @patternMatch and len(pn.NonBlockingStatusNotes) < 3900 then @text + pn.NonBlockingStatusNotes else pn.NonBlockingStatusNotes end
	,pn.BlockingStatusNotes = case when pn.BlockingStatusNotes is not null and pn.BlockingStatusNotes not like @patternMatch and len(pn.BlockingStatusNotes) < 3900 then @text + pn.BlockingStatusNotes else pn.BlockingStatusNotes end


/*
select 	case when pn.NonBlockingStatusNotes is not null and pn.NonBlockingStatusNotes not like @patternMatch and len(pn.NonBlockingStatusNotes) < 3900 then @text + pn.NonBlockingStatusNotes else pn.NonBlockingStatusNotes end
	,case when pn.BlockingStatusNotes is not null and pn.BlockingStatusNotes not like @patternMatch and len(pn.BlockingStatusNotes) < 3900 then @text + pn.BlockingStatusNotes else pn.BlockingStatusNotes end
	,pn.*
*/


from Polaris.polaris.PatronNotes pn
join Polaris.Polaris.Patrons p
	on p.PatronID = pn.PatronID
join Polaris.Polaris.Organizations o
	on o.OrganizationID = p.OrganizationID
where o.ParentOrganizationID = 8
	and (pn.NonBlockingStatusNotes not like @patternMatch or pn.BlockingStatusNotes not like @patternMatch )


--rollback
--commit


/*


select case when d.updateNonBlockingNote = 1 then @text + pn.NonBlockingStatusNotes else pn.NonBlockingStatusNotes end [NewNonBlockingNote]
		,case when d.updateNonBlockingNote = 1 then getdate() else pn.NonBlockingStatusNoteDate end
		,case when d.updateNonBlockingNote = 1 then 425 else pn.NonBlockingUserID end
		,case when d.updateNonBlockingNote = 1 then 5 else pn.NonBlockingBranchID end
		,case when d.updateNonBlockingNote = 1 then 2563 else pn.NonBlockingWorkstationID end

		,case when d.updateBlockingNote = 1 then @text + pn.BlockingStatusNotes else pn.BlockingStatusNotes end [NewBlockingNote]
		,case when d.updateBlockingNote = 1 then getdate() else pn.BlockingStatusNoteDate end
		,case when d.updateBlockingNote = 1 then 425 else pn.BlockingUserID end
		,case when d.updateBlockingNote = 1 then 5 else pn.BlockingBranchID end
		,case when d.updateBlockingNote = 1 then 2563 else pn.BlockingWorkstationID end	
		,'' [ ]
		,pn.*	
from (
	select case when pn.NonBlockingStatusNotes is not null and pn.NonBlockingStatusNotes not like @patternMatch and len(pn.NonBlockingStatusNotes) < 3900 then 1 else 0 end [updateNonBlockingNote]
		,case when pn.BlockingStatusNotes is not null and pn.BlockingStatusNotes not like @patternMatch and len(pn.BlockingStatusNotes) < 3900 then 1 else 0 end [updateBlockingNote]
		,pn.PatronID
	from Polaris.polaris.PatronNotes pn
	join Polaris.Polaris.Patrons p
		on p.PatronID = pn.PatronID
	join Polaris.Polaris.Organizations o
		on o.OrganizationID = p.OrganizationID
	where o.ParentOrganizationID = 8
		and (pn.NonBlockingStatusNotes not like @patternMatch or pn.BlockingStatusNotes not like @patternMatch )
) d
join Polaris.Polaris.PatronNotes pn
	on pn.PatronID = d.PatronID

*/