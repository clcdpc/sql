begin tran

update lp
set lp.Units = 1
from polaris.polaris.LoanPeriods lp
join polaris.polaris.organizations o
	on o.OrganizationID = lp.OrganizationID
where lp.LoanPeriodCodeID = 10 and o.ParentOrganizationID = 32 and lp.PatronCodeID in (1,6,14,15)

--rollback
--commit