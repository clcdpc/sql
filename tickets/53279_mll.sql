-- =CONCATENATE("insert into @newmll values (", A2, ", ", C2, ", ", E2, ", ", G2, ", ", H2, ")")

declare @newmll table ( orgid int, pcid int, mtid int, MaxItems int, maxrequests int )

-- PASTE EXCEL DATA BELOW
insert into @newmll values (8, 17, 33, 2, 2)
insert into @newmll values (8, 18, 33, 2, 2)
insert into @newmll values (8, 1, 33, 2, 2)
insert into @newmll values (8, 19, 33, 0, 0)
insert into @newmll values (8, 3, 33, 0, 0)
insert into @newmll values (8, 6, 33, 2, 2)
insert into @newmll values (8, 21, 33, 0, 0)
insert into @newmll values (8, 22, 33, 2, 2)
insert into @newmll values (8, 10, 33, 2, 2)
insert into @newmll values (8, 11, 33, 2, 2)
insert into @newmll values (8, 23, 33, 2, 2)
insert into @newmll values (8, 24, 33, 0, 0)
insert into @newmll values (8, 20, 33, 2, 2)
insert into @newmll values (8, 12, 33, 0, 2)
insert into @newmll values (8, 13, 33, 2, 2)
insert into @newmll values (8, 14, 33, 2, 2)
insert into @newmll values (8, 15, 33, 2, 2)
insert into @newmll values (8, 25, 33, 0, 0)
insert into @newmll values (8, 27, 33, 0, 0)
insert into @newmll values (8, 26, 33, 0, 0)
insert into @newmll values (8, 28, 33, 0, 0)
insert into @newmll values (8, 16, 33, 0, 0)

-- PASTE EXCEL DATA ABOVE

begin tran

update mll
set mll.MaxItems = n.MaxItems
	,mll.MaxRequestItems = n.maxrequests
from Polaris.Polaris.MaterialLoanLimits mll
join polaris.polaris.organizations o
	on o.OrganizationID = mll.OrganizationID
join @newmll n
	on n.orgid = o.ParentOrganizationID and n.pcid = mll.PatronCodeID and n.mtid = mll.MaterialTypeID

--rollback
--commit