--copy material type loan/hold limits for all FCD branches for the Branch Use Card patron code, using Full Access to copy from

begin tran

update mll_dest
set mll_dest.MaxItems = mll_source.MaxItems
	,mll_dest.MaxRequestItems = mll_source.MaxRequestItems
from Polaris.Polaris.MaterialLoanLimits mll_dest
join polaris.polaris.Organizations o
	on o.OrganizationID = mll_dest.OrganizationID
join polaris.polaris.MaterialLoanLimits mll_source
	on mll_source.MaterialTypeID = mll_dest.MaterialTypeID and mll_source.OrganizationID = 9 and mll_source.PatronCodeID = 1
where o.ParentOrganizationID = 8
	and mll_dest.PatronCodeID = 17

--rollback
--commit



begin tran

update lp_dest
set lp_dest.Units = lp_source.Units
from polaris.polaris.LoanPeriods lp_dest
join polaris.polaris.Organizations o
	on o.OrganizationID = lp_dest.OrganizationID
join polaris.polaris.LoanPeriods lp_source
	on lp_source.LoanPeriodCodeID = lp_dest.LoanPeriodCodeID and lp_source.OrganizationID = 9 and lp_source.PatronCodeID = 1
where o.ParentOrganizationID = 8
	and lp_dest.PatronCodeID = 17

--rollback
--commit