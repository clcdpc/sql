begin tran

update fmml
set fmml.MaterialTypeBibLimitInItems = 9999
	,fmml.MaterialTypeBibLimit = 9999
from polaris.polaris.SA_FloatingMatrixMaterialLimits fmml
join polaris.polaris.Organizations o
	on o.OrganizationID = fmml.ItemCheckInBranchID
where o.ParentOrganizationID = 39
	and o.Name not in ('CML New Albany Drive-up Window',
'CML Northern Lights Drive-up Window',
'CML Parsons Drive-up Window',
'CML Shepard Drive-up Window',
'CML Whitehall Drive-up Window',
'CML Dublin Curbside',
'CML Gahanna Curbside',
'CML Hilliard Curbside',
'CML Whetstone Curbside',
'CML School Delivery',
'CML ILL',
'CML R2R',
'CML R2R Bookmobile',
'CML Operations Center',
'CML OSU Thompson',
'CML-Check Out App',
'CML-Columbus City Schools')
	and fmml.MaterialTypeID in (40)

update fmcl
set fmcl.Limit = 9999
from polaris.polaris.SA_FloatingMatrixCollectionLimits fmcl
join polaris.polaris.Organizations o
	on o.OrganizationID = fmcl.ItemCheckInBranchID
where o.ParentOrganizationID = 39
	and o.Name not in ('CML New Albany Drive-up Window',
'CML Northern Lights Drive-up Window',
'CML Parsons Drive-up Window',
'CML Shepard Drive-up Window',
'CML Whitehall Drive-up Window',
'CML Dublin Curbside',
'CML Gahanna Curbside',
'CML Hilliard Curbside',
'CML Whetstone Curbside',
'CML School Delivery',
'CML ILL',
'CML R2R',
'CML R2R Bookmobile',
'CML Operations Center',
'CML OSU Thompson',
'CML-Check Out App',
'CML-Columbus City Schools')
	and fmcl.CollectionID in (493)

--rollback
--commit
