begin tran

update lp
set lp.Units = 1
from polaris.polaris.LoanPeriods lp
join polaris.polaris.organizations o
	on o.OrganizationID = lp.OrganizationID
join polaris.polaris.PatronCodes pc
	on lp.PatronCodeID = pc.PatronCodeID
where o.ParentOrganizationID = 28
	and lp.LoanPeriodCodeID = 38
	and pc.Description in ('Full Access','Key Customers','Newly Registered','Newly Registered Minor','Senior','Teachers/Educators','Video up to G/VG up to E','Video up to PG/VG up to E','Video up to PG-13/VG up to T','Video/VG Restricted','Business/Community Card')


--rollback
--commit


