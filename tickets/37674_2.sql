begin tran

update p
set p.PatronCodeID = 
	case p.PatronCodeID
		when 9 then 21
		when 4 then 17
		when 8 then 26
		when 2 then 19
		when 6 then 18
	end
from Polaris.polaris.Patrons p
join polaris.polaris.Organizations o
	on o.OrganizationID = p.OrganizationID
where o.ParentOrganizationID = 39 and p.PatronCodeID in (9,4,8,2,6)

delete f
from polaris.polaris.SA_PatronCodesFilterByRegisteredBranch f
where f.OrganizationID = 39

insert into Polaris.polaris.SA_PatronCodesFilterByRegisteredBranch
select 39, pc.PatronCodeID
from Polaris.polaris.PatronCodes pc
where pc.Description in (
	'Branch Card',
	'Business/Community Card',
	'Full Access',
	'Homebound by Mail',
	'Institutions',
	'ILL/MORE',
	'Kids Card',
	'Video up to PG-13/VG up to T',
	'Outreach',
	'School Delivery',
	'Self Registered',
	'Staff',
	'Teachers/Educators',
	'Web Only'
)

--rollback
--commit