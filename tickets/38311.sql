--Please make the following changes for loan and request limits for patron code School Delivery (24) for CML:

--Loan (checkout) limit = 60 and request limit = 25 for
--Audio Book 1 1
--Juvenile Audio Book 13 13
--Juvenile Music CD/Cassette 17 17
--Music CD/Cassette 21 21

--Loan (checkout) limit = 10 and request limit = 10 for
--Juvenile Video Non-Fiction 18 18
--Video G/TV-G/TV-Y/TV-Y7/TV-Y7 FV 49 46
--Video Non-Fiction 50 47

begin tran

update mll
set mll.MaxItems = 60
	,mll.MaxRequestItems = 25
from Polaris.polaris.MaterialLoanLimits mll
join polaris.polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
where o.ParentOrganizationID = 39 and mll.PatronCodeID = 24 and mll.MaterialTypeID in (1,13,17,21)

update mll
set mll.MaxItems = 10
	,mll.MaxRequestItems = 10
from Polaris.polaris.MaterialLoanLimits mll
join polaris.polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
where o.ParentOrganizationID = 39 and mll.PatronCodeID = 24 and mll.MaterialTypeID in (18,49,50)

update lp
set lp.Units = 56
from Polaris.polaris.LoanPeriods lp
where lp.OrganizationID = 88 and lp.PatronCodeID = 24 and lp.LoanPeriodCodeID in (1,13,17,21,18,46,47)

update lp
set lp.Units = lp2.Units
from Polaris.Polaris.LoanPeriods lp
join polaris.Polaris.LoanPeriods lp2
	on lp2.OrganizationID = lp.OrganizationID and lp2.LoanPeriodCodeID = lp.LoanPeriodCodeID and lp2.PatronCodeID = 1
join polaris.polaris.Organizations o
	on o.OrganizationID = lp.OrganizationID
where o.ParentOrganizationID = 39 and lp.OrganizationID != 88 and lp.PatronCodeID = 24 and lp.LoanPeriodCodeID in (1,13,17,21,18,46,47)

--rollback
--commit