declare @libraryId int = 8
declare @daysToAdd int = 3
declare @oldduedate date = '12/24/2021'

declare @excludedMaterialTypes table ( mtid int )
insert into @excludedMaterialTypes values (47) --Inn-Reach Books and Audio
										 ,(55) --Inn-Reach Video

if (@oldduedate < cast(getdate() as date)) begin select 'old due date cannot be in the past'; return; end

begin tran; update ic set DueDate = dateadd(day, @daysToAdd, duedate)
--select ic.ItemRecordID,cir.Barcode,ic.OrganizationID,ic.DueDate,dateadd(day, @daysToAdd, duedate) [NewDueDate]
from polaris.polaris.ItemCheckouts ic
join polaris.polaris.CircItemRecords cir
	on cir.ItemRecordID = ic.ItemRecordID
join polaris.polaris.organizations o on
	ic.organizationid = o.organizationid
where o.parentorganizationid = @libraryId 
	and @oldduedate > getdate()
	and cast(ic.DueDate as date) = @oldduedate
	and cir.ElectronicItem = 0
	and cir.MaterialTypeID not in ( select mtid from @excludedMaterialTypes )

--rollback
--commit