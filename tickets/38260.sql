--Group checkout/loan limits:
--Outreach - Audio 30, Print 50, Realia 10, Video 10, and Video Games 2
--Homebound by Mail - Video 10

--Material type loan limits (for all CML locations):
--Outreach - eBook 20
--Homebound - Juvenile Video Non-Fiction 10
--Homebound by Mail - Juvenile Video Non-Fiction 10

begin tran

-- outreach - ebook - 20
update mll
set mll.MaxItems = 20
from Polaris.Polaris.MaterialLoanLimits mll
join polaris.Polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
where o.ParentOrganizationID = 39 and mll.PatronCodeID = 23 and mll.MaterialTypeID = 34

-- homebound - juvenile video non-fiction - 10
update mll
set mll.MaxItems = 10
from Polaris.Polaris.MaterialLoanLimits mll
join polaris.Polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
where o.ParentOrganizationID = 39 and mll.PatronCodeID = 2 and mll.MaterialTypeID = 18

-- homebound by mail - juvenile video non-fiction - 10
update mll
set mll.MaxItems = 10
from Polaris.Polaris.MaterialLoanLimits mll
join polaris.Polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
where o.ParentOrganizationID = 39 and mll.PatronCodeID = 19 and mll.MaterialTypeID = 18


--rollback
--commit