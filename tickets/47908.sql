begin tran

update ic
set ic.OVDNoticeCount = 2
from Polaris.Polaris.ItemCheckouts ic
join polaris.polaris.Patrons p
	on p.PatronID = ic.PatronID
join Polaris.Polaris.PatronRegistration pr
	on pr.PatronID = p.PatronID
join polaris.polaris.Organizations o
	on o.OrganizationID = p.OrganizationID
where o.ParentOrganizationID = 78
	and p.PatronCodeID = 14
	and ic.OVDNoticeCount = 3

--rollback
--commit