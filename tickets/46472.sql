/*
Can you please update the loan periods for FCDL's 3 lockers by adding an additional 5 days to whatever they currently are?

Millersport 24 hour Kiosk 126
Baltimore 24 hour Lockers 138
FCDL Main Lockers 145
*/

begin tran

update lp
set lp.Units = lp.Units + 5
from polaris.Polaris.LoanPeriods lp
where lp.OrganizationID in (126,138,145)

--rollback
--commit



select *
from Polaris.polaris.LoanPeriods lp
where lp.OrganizationID in (9,126,138,145) and lp.PatronCodeID = 1
order by lp.LoanPeriodCodeID, lp.OrganizationID