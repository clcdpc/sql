begin tran

update lp
set lp.Units = 1
from polaris.polaris.LoanPeriods lp
join polaris.Polaris.organizations o
	on o.OrganizationID = lp.OrganizationID
where o.ParentOrganizationID = 39 and lp.LoanPeriodCodeID = 38

--rollback
--commit