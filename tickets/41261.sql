-- copy the hold/loan limits, fine amounts and loan periods from Video G to Juvenile Video Un-Rated for FCDL

begin tran

update mll_dest
set mll_dest.MaxItems = mll_source.MaxItems
	,mll_dest.MaxRequestItems = mll_source.MaxRequestItems
from Polaris.polaris.MaterialLoanLimits mll_dest
join polaris.Polaris.Organizations o
	on o.OrganizationID = mll_dest.OrganizationID
join polaris.Polaris.MaterialLoanLimits mll_source
	on mll_source.OrganizationID = mll_dest.OrganizationID and mll_source.PatronCodeID = mll_dest.PatronCodeID
where o.ParentOrganizationID = 8 and mll_dest.MaterialTypeID = 42 and mll_source.MaterialTypeID = 49


update f_dest
set f_dest.Amount = f_source.Amount
	,f_dest.MaximumFine = f_source.MaximumFine
	,f_dest.GraceUnits = f_source.GraceUnits
from polaris.polaris.Fines f_dest
join polaris.polaris.Organizations o
	on o.OrganizationID = f_dest.OrganizationID
join Polaris.polaris.Fines f_source
	on f_source.OrganizationID = f_dest.OrganizationID and f_source.PatronCodeID = f_dest.PatronCodeID
where o.ParentOrganizationID = 8 and f_dest.FineCodeID = 39 and f_source.FineCodeID = 46



update lp_dest
set lp_dest.Units = lp_source.Units
from polaris.polaris.LoanPeriods lp_dest
join polaris.polaris.Organizations o
	on o.OrganizationID = lp_dest.OrganizationID
join polaris.polaris.LoanPeriods lp_source
	on lp_source.OrganizationID = lp_dest.OrganizationID and lp_source.PatronCodeID = lp_dest.PatronCodeID
where o.ParentOrganizationID = 8 and lp_dest.LoanPeriodCodeID = 39 and lp_source.LoanPeriodCodeID = 46



--rollback
--commit