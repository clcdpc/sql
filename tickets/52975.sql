begin tran

update ird
set ird.ClassificationNumber = substring(ird.ClassificationNumber, 3, 99)
	,ird.CallNumberPrefix = 'J'
from polaris.polaris.ItemRecordSets irs
join polaris.polaris.ItemRecordDetails ird
	on ird.ItemRecordID = irs.ItemRecordID
where irs.RecordSetID in ( 229479, 229480 )

select ird.ItemRecordID
		,ird.CallNumberPrefix
		,ird.ClassificationNumber
		,ird.CallNumber
from polaris.polaris.ItemRecordSets irs
join polaris.polaris.ItemRecordDetails ird
	on ird.ItemRecordID = irs.ItemRecordID
where irs.RecordSetID in ( 229479, 229480 )

--rollback
--commit