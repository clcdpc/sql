begin tran

update pr
set pr.User4 = null
from polaris.polaris.PatronRegistration pr
join polaris.polaris.patrons p
	on p.PatronID = pr.PatronID 
join Polaris.Polaris.Organizations o
	on o.OrganizationID = p.OrganizationID
where o.ParentOrganizationID = 23
	and pr.user4 is not null

--rollback
--commit