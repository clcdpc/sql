/*
 In order to do some testing we'd like only for Patron Code Kids Card to change the total checkout limit for only zzzdonotuseWeinland location to 5 total checkouts
 ,  and to set all the other material type holds and checkout limits except for book to zero checkouts and holds.  
 For material type book we would like a checkout limit of 2 and a hold limit of 1.
*/

begin tran

update pll
set pll.TotalItems = 5
from polaris.polaris.PatronLoanLimits pll 
where pll.OrganizationID = 61 
	and pll.PatronCodeID = 21

update mll
set mll.MaxItems = case mll.MaterialTypeID when 8 then 2 else 0 end
	,mll.MaxRequestItems = case mll.MaterialTypeID when 8 then 1 else 0 end
from Polaris.polaris.MaterialLoanLimits mll
where mll.OrganizationID = 61
	and mll.PatronCodeID = 21

--rollback
--commit