begin tran

update lp
set lp.Units = 21
from polaris.polaris.LoanPeriods lp
join polaris.polaris.organizations o
	on o.OrganizationID = lp.OrganizationID
where lp.LoanPeriodCodeID = 45 and o.ParentOrganizationID = 32 and lp.PatronCodeID in (1,14,15)

--rollback
--commit

