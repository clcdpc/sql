begin tran

--1. Video PG (3) should have a loan period of 7 days for patron code Business/Community Card (18) for all CML locations except CML Outreach (66), which should remain at 56 days. 
update lp
set lp.Units = 7
from polaris.Polaris.LoanPeriods lp
join polaris.polaris.Organizations o
	on o.OrganizationID = lp.OrganizationID
where o.ParentOrganizationID = 39
	and lp.OrganizationID != 66
	and lp.LoanPeriodCodeID = 3
	and lp.PatronCodeID = 18

--2. Video PG (3) should have a loan period of 7 days for patron code Video up to PG/VG up to E (27) for all CML locations.
update lp
set lp.Units = 7
from polaris.Polaris.LoanPeriods lp
join polaris.polaris.Organizations o
	on o.OrganizationID = lp.OrganizationID
where o.ParentOrganizationID = 39
	and lp.LoanPeriodCodeID = 3
	and lp.PatronCodeID = 27

--3. Video PG (3) should have a loan period of 0 days for patron code Video up to G/VG up to E. (25) 
update lp
set lp.Units = 0
from polaris.Polaris.LoanPeriods lp
join polaris.polaris.Organizations o
	on o.OrganizationID = lp.OrganizationID
where o.ParentOrganizationID = 39
	and lp.LoanPeriodCodeID = 3
	and lp.PatronCodeID = 25

--rollback
--commit