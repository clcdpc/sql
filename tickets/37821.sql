declare @settings table (patroncode varchar(255), materialtypeid int, maxitems int, maxrequestitems int)

begin --insert settings
set nocount on
insert into @settings values ('Branch card', 43, 5, 5)
insert into @settings values ('Business/Community Card', 43, 5, 5)
insert into @settings values ('Full Access', 43, 5, 5)
insert into @settings values ('Video up to PG/VG up to E', 43, 5, 5)
insert into @settings values ('Video up to G/VG up to E', 43, 5, 5)
insert into @settings values ('Video up to PG-13/VG up to T', 43, 5, 5)
insert into @settings values ('Outreach', 43, 5, 5)
insert into @settings values ('Senior', 43, 5, 5)
insert into @settings values ('Staff', 43, 5, 5)
insert into @settings values ('Teachers/Educators', 43, 5, 5)
insert into @settings values ('Newly Registered', 43, 5, 5)
insert into @settings values ('Branch card', 44, 5, 5)
insert into @settings values ('Business/Community Card', 44, 5, 5)
insert into @settings values ('Full Access', 44, 5, 5)
insert into @settings values ('Video up to PG/VG up to E', 44, 5, 5)
insert into @settings values ('Video up to G/VG up to E', 44, 5, 5)
insert into @settings values ('Video up to PG-13/VG up to T', 44, 5, 5)
insert into @settings values ('Outreach', 44, 5, 5)
insert into @settings values ('Senior', 44, 5, 5)
insert into @settings values ('Staff', 44, 5, 5)
insert into @settings values ('Teachers/Educators', 44, 5, 5)
insert into @settings values ('Newly Registered', 44, 5, 5)
insert into @settings values ('Branch card', 3, 20, 75)
insert into @settings values ('Business/Community Card', 3, 20, 75)
insert into @settings values ('Full Access', 3, 20, 75)
insert into @settings values ('Video up to PG/VG up to E', 3, 20, 75)
insert into @settings values ('Video up to G/VG up to E', 3, 20, 75)
insert into @settings values ('Video up to PG-13/VG up to T', 3, 20, 75)
insert into @settings values ('Outreach', 3, 20, 75)
insert into @settings values ('Senior', 3, 20, 75)
insert into @settings values ('Staff', 3, 20, 75)
insert into @settings values ('Teachers/Educators', 3, 20, 75)
insert into @settings values ('Newly Registered', 3, 10, 75)
insert into @settings values ('Branch card', 29, 5, 5)
insert into @settings values ('Business/Community Card', 29, 5, 5)
insert into @settings values ('Full Access', 29, 5, 5)
insert into @settings values ('Video up to PG/VG up to E', 29, 5, 5)
insert into @settings values ('Video up to G/VG up to E', 29, 5, 5)
insert into @settings values ('Video up to PG-13/VG up to T', 29, 5, 5)
insert into @settings values ('Outreach', 29, 5, 5)
insert into @settings values ('Senior', 29, 5, 5)
insert into @settings values ('Staff', 29, 5, 5)
insert into @settings values ('Teachers/Educators', 29, 5, 5)
insert into @settings values ('Newly Registered', 29, 5, 5)
insert into @settings values ('Branch card', 34, 100, 75)
insert into @settings values ('Business/Community Card', 34, 100, 75)
insert into @settings values ('Full Access', 34, 100, 75)
insert into @settings values ('Video up to PG/VG up to E', 34, 100, 75)
insert into @settings values ('Video up to G/VG up to E', 34, 100, 75)
insert into @settings values ('Video up to PG-13/VG up to T', 34, 100, 75)
insert into @settings values ('Outreach', 34, 100, 75)
insert into @settings values ('Senior', 34, 100, 75)
insert into @settings values ('Staff', 34, 100, 75)
insert into @settings values ('Teachers/Educators', 34, 100, 75)
insert into @settings values ('Newly Registered', 34, 100, 75)
insert into @settings values ('Branch card', 35, 100, 75)
insert into @settings values ('Business/Community Card', 35, 100, 75)
insert into @settings values ('Full Access', 35, 100, 75)
insert into @settings values ('Video up to PG/VG up to E', 35, 100, 75)
insert into @settings values ('Video up to G/VG up to E', 35, 100, 75)
insert into @settings values ('Video up to PG-13/VG up to T', 35, 100, 75)
insert into @settings values ('Outreach', 35, 100, 75)
insert into @settings values ('Senior', 35, 100, 75)
insert into @settings values ('Staff', 35, 100, 75)
insert into @settings values ('Teachers/Educators', 35, 100, 75)
insert into @settings values ('Newly Registered', 35, 100, 75)
insert into @settings values ('Branch card', 36, 100, 75)
insert into @settings values ('Business/Community Card', 36, 100, 75)
insert into @settings values ('Full Access', 36, 100, 75)
insert into @settings values ('Video up to PG/VG up to E', 36, 100, 75)
insert into @settings values ('Video up to G/VG up to E', 36, 100, 75)
insert into @settings values ('Video up to PG-13/VG up to T', 36, 100, 75)
insert into @settings values ('Outreach', 36, 100, 75)
insert into @settings values ('Senior', 36, 100, 75)
insert into @settings values ('Staff', 36, 100, 75)
insert into @settings values ('Teachers/Educators', 36, 100, 75)
insert into @settings values ('Newly Registered', 36, 100, 75)
insert into @settings values ('Branch card', 37, 100, 75)
insert into @settings values ('Business/Community Card', 37, 100, 75)
insert into @settings values ('Full Access', 37, 100, 75)
insert into @settings values ('Video up to PG/VG up to E', 37, 100, 75)
insert into @settings values ('Video up to G/VG up to E', 37, 100, 75)
insert into @settings values ('Video up to PG-13/VG up to T', 37, 100, 75)
insert into @settings values ('Outreach', 37, 100, 75)
insert into @settings values ('Senior', 37, 100, 75)
insert into @settings values ('Staff', 37, 100, 75)
insert into @settings values ('Teachers/Educators', 37, 100, 75)
insert into @settings values ('Newly Registered', 37, 100, 75)
set nocount off
end


begin tran

update mll
set  mll.MaxItems = s.maxitems
	,mll.MaxRequestItems = s.maxrequestitems
from Polaris.polaris.MaterialLoanLimits mll
join polaris.polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
join polaris.polaris.PatronCodes pc
	on pc.PatronCodeID = mll.PatronCodeID
join @settings s
	on s.patroncode = pc.Description and s.materialtypeid = mll.MaterialTypeID
where o.ParentOrganizationID = 6

--rollback
--commit