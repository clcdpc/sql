begin tran

update mll
set mll.MaxItems = 999
from polaris.polaris.materialloanlimits mll
where mll.OrganizationID = 131
	and mll.MaxItems > 0

update pll
set pll.TotalOverDue = 999
	,pll.TotalItems = 999
from polaris.polaris.PatronLoanLimits pll
where pll.OrganizationID = 131
	and pll.TotalItems > 0

--rollback
--commit