/*
I was wondering if there was any way that you could update the 

Loanable Equipment 1 (traffic cones) material type, Loan period, and Fine code for all patron codes except Web Only, Kids Card and self-registered (16,21,12). These should be a 14 day check out, no fines. 5 max items, 5 max holds. The three patron codes not to change should be 0 max items and 0 holds.
*/

begin tran

update mll
set mll.MaxItems = case when mll.PatronCodeID in (16,21,12) then 0 else 5 end 
	,mll.MaxRequestItems = case when mll.PatronCodeID in (16,21,12) then 0 else 5 end
from Polaris.Polaris.MaterialLoanLimits mll
join polaris.polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
where o.ParentOrganizationID = 8
	and mll.MaterialTypeID = 43


update lp
set lp.Units = 14 -- case when lp.PatronCodeID in (16,21,12) then 0 else 14 end
from polaris.polaris.LoanPeriods lp
join Polaris.polaris.organizations o
	on o.OrganizationID = lp.OrganizationID
where o.ParentOrganizationID = 8
	and lp.LoanPeriodCodeID = 40

--rollback
--commit