--Patron codes that should be charged for all dvds: ($1 a day-$10 Max)
--Full Access
--Restricted 1, 2 and 3
--Newly Registered
--Newly Registered Minor
--Video/VG Restricted
--Video up to PG
--Video up to G
--Video up to PG-13

--1,7,8,9,10,1125,26,27,28
--46,47,3,48,49,50,18,39

begin tran

update f
set f.Amount = 1, f.MaximumFine = 10
from polaris.polaris.Fines f
join polaris.polaris.organizations o
	on o.OrganizationID = f.OrganizationID
where o.ParentOrganizationID = 16 and f.PatronCodeID in (1,7,8,9,10,11,25,26,27,28) and f.FineCodeID in (46,47,3,48,49,50,18,39)

--rollback
--commit