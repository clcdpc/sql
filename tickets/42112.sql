begin tran

update cir
set cir.LastUsePatronID = h.PatronID
from polaris.polaris.CircItemRecords cir 
join ( 
	select irh.ItemRecordID, irh.PatronID, rank() over(partition by irh.ItemRecordID order by irh.ItemRecordHistoryID desc) [hrank]
	from polaris.polaris.ItemRecordHistory irh
	where irh.NewItemStatusID = 7
		and irh.OldItemStatusID != 7
		and irh.PatronID is not null 
) h
	on h.ItemRecordID = cir.ItemRecordID and h.hrank = 1
where cir.ItemStatusID = 7 
	and cir.LastUsePatronID is null
	and not exists ( select 1 from polaris.polaris.PatronLostItems pli where pli.ItemRecordID = cir.ItemRecordID )

--rollback
--commit