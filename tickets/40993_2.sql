begin tran

--Office Fine Code (43) --> $.50 day/$5.00 max for all locations, and all patron codes except: staff (14), key customers (6), branch use (17)
update f
set  f.Amount = .50	
	,f.MaximumFine = 5.00
from Polaris.polaris.Fines	f
join Polaris.Polaris.Organizations o
	on o.OrganizationID = f.OrganizationID
where o.ParentOrganizationID = 8 and f.FineCodeID = 43 and f.PatronCodeID not in (14,6,17) and f.FineCodeID not in (44,52)

--Business/Community (18) patron code --> Mirror Full Access
--Outreach Patron Code (23) --> Mirror Full Access
update fd
set  fd.Amount = fs.Amount
	,fd.MaximumFine = fs.MaximumFine
	,fd.GraceUnits = fs.GraceUnits
from Polaris.polaris.Fines	fd
join Polaris.Polaris.Organizations o	
	on o.OrganizationID = fd.OrganizationID
join polaris.polaris.Fines fs
	on fs.FineCodeID = fd.FineCodeID and fs.OrganizationID = fd.OrganizationID and fs.PatronCodeID = 1
where o.ParentOrganizationID = 8 and fd.PatronCodeID in (18,23)


--Key Customers Patron Code (6) --> No Fines
--Homebound by Mail Patron Code (19) --> No Fines
update f
set  f.Amount = 0	
	,f.MaximumFine = 0
from Polaris.polaris.Fines	f
join Polaris.Polaris.Organizations o
	on o.OrganizationID = f.OrganizationID
where o.ParentOrganizationID = 8 and f.PatronCodeID in (6,19) and f.FineCodeID not in (44,52)


--rollback
--commit