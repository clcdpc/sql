begin tran

update nq
set nq.DeliveryOptionID = pr.DeliveryOptionID
from results.Polaris.NotificationQueue nq 
join polaris.Polaris.PatronRegistration pr
	on pr.PatronID = nq.PatronID
where nq.NotificationTypeID = 13 
	and nq.ReportingOrgID = 7
	and nq.DeliveryOptionID = 1

--rollback
--commit