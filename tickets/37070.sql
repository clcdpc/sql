--Material Type limits changes for Kids Card: id 21 // 33,
--Juvenile Board Book � 5
--eBook � 10
--Inn-Reach Books and Audio � 0
--Inn-Reach Video � 0
 
--Max Requests changes for Kids Card:
--Juvenile Board Book � 5
--Inn-Reach Books and Audio � 0
--Inn-Reach Video � 0


--select * from polaris.Polaris.PatronCodes
--select * from polaris.polaris.MaterialTypes
begin tran

update mll
set mll.MaxItems = case mll.MaterialTypeID when 14 then 5 when 34 then 10 when 47 then 0 when 55 then 0 end, 
	mll.MaxRequestItems = case mll.MaterialTypeID when 14 then 5 when 34 then mll.MaxRequestItems when 47 then 0 when 55 then 0 end
from Polaris.polaris.MaterialLoanLimits mll
join polaris.polaris.organizations o
	on o.OrganizationID = mll.OrganizationID
where o.ParentOrganizationID = 39 and mll.PatronCodeID = 21 and mll.MaterialTypeID in (14,34,47,55)


--Material Type limits changes for Restricted 3:
--Local A/V material � 1
--eBook � 10
--Inn-Reach Books and Audio � 0
--Inn-Reach Video � 0

--Max Requests changes for Restricted 3:
--Inn-Reach Books and Audio � 0
--Inn-Reach Video � 0
update mll
set mll.MaxItems = case mll.MaterialTypeID when 33 then 1 when 34 then 10 when 47 then 0 when 55 then 0 end, 
	mll.MaxRequestItems = case mll.MaterialTypeID when 47 then 0 when 55 then 0 else mll.MaxRequestItems end
from Polaris.polaris.MaterialLoanLimits mll
join polaris.polaris.organizations o
	on o.OrganizationID = mll.OrganizationID
where o.ParentOrganizationID = 39 and mll.PatronCodeID = 9 and mll.MaterialTypeID in (33,34,47,55)

--rollback
--commit