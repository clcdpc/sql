select br.BibliographicRecordID
		,count(distinct cir.ItemRecordID) [items]
		,count(distinct shr.SysHoldRequestID) [holds]
		,case when exists ( select 1 from Polaris.Polaris.CircItemRecords cir2 where cir2.AssociatedBibRecordID = br.BibliographicRecordID and cir2.MaterialTypeID in (2,3,4,5,6,7,29,30,31,42,49,51,52,53,54) ) then 'yes' else 'no' end [hasMixedTypes]
from Polaris.polaris.BibliographicRecords br
left join Polaris.Polaris.CircItemRecords cir
	on cir.AssociatedBibRecordID = br.BibliographicRecordID
left join Polaris.Polaris.SysHoldRequests shr
	on shr.BibliographicRecordID = br.BibliographicRecordID
where exists ( select 1 from Polaris.Polaris.CircItemRecords cir2 where cir2.AssociatedBibRecordID = br.BibliographicRecordID and cir2.MaterialTypeID = 18 ) -- Juvenile Video Non-Fiction
	and exists ( select 1 from Polaris.Polaris.CircItemRecords cir2 where cir2.AssociatedBibRecordID = br.BibliographicRecordID and cir2.MaterialTypeID = 50 ) --Video Non-Fiction
group by br.BibliographicRecordID
order by [holds] desc