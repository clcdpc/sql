begin tran

update pr
set pr.EmailAddress = case when pr.emailaddress is null then 'noreply@clcohio.org' else pr.emailaddress end
	,pr.DeliveryOptionID = 2
from Polaris.polaris.PatronNotes pn
join Polaris.Polaris.PatronRegistration pr
	on pr.PatronID = pn.PatronID
where pn.NonBlockingStatusNotes like '%noreply@clcohio.org%'


--rollback
--commit