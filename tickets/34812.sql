begin tran

update lp
set Units = lp_source.Units
from Polaris.Polaris.LoanPeriods lp
join Polaris.polaris.Organizations o
	on o.OrganizationID = lp.OrganizationID
join Polaris.polaris.LoanPeriods lp_source
	on lp_source.OrganizationID = lp.OrganizationID and lp_source.LoanPeriodCodeID = lp.LoanPeriodCodeID
where lp.PatronCodeID = 13 and lp_source.PatronCodeID = 1 and o.ParentOrganizationID = 32

update mll
set MaxItems = mll_source.MaxItems, MaxRequestItems = mll_source.MaxRequestItems
from polaris.polaris.MaterialLoanLimits mll
join polaris.Polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
join polaris.Polaris.MaterialLoanLimits mll_source
	on mll_source.OrganizationID = mll.OrganizationID and mll_source.MaterialTypeID = mll.MaterialTypeID
where o.ParentOrganizationID = 32 and mll.PatronCodeID = 13 and mll_source.PatronCodeID = 1


select lp.*, '' [ ], lp_source.Units [NewUnits]
from Polaris.Polaris.LoanPeriods lp
join Polaris.polaris.Organizations o
	on o.OrganizationID = lp.OrganizationID
join Polaris.polaris.LoanPeriods lp_source
	on lp_source.OrganizationID = lp.OrganizationID and lp_source.LoanPeriodCodeID = lp.LoanPeriodCodeID
where lp.PatronCodeID = 13 and lp_source.PatronCodeID = 1 and o.ParentOrganizationID = 32

select mll.*, '' [ ], mll_source.MaxItems [NewMaxItems], mll_source.MaxRequestItems [NewMaxRequestItems]
from polaris.polaris.MaterialLoanLimits mll
join polaris.Polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
join polaris.Polaris.MaterialLoanLimits mll_source
	on mll_source.OrganizationID = mll.OrganizationID and mll_source.MaterialTypeID = mll.MaterialTypeID
where o.ParentOrganizationID = 32 and mll.PatronCodeID = 13 and mll_source.PatronCodeID = 1


--rollback
--commit