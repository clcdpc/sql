-- =CONCATENATE("insert into @newlp values (",A2, ", ", C2, ", ", E2, ", ", G2, ")")

declare @newlp table (orgid int, pcid int, lpid int, units int)

-- PASTE EXCEL DATA BELOW
insert into @newlp values (153, 1, 40, 2)
insert into @newlp values (106, 1, 40, 2)
insert into @newlp values (107, 1, 40, 2)
insert into @newlp values (109, 1, 40, 2)
insert into @newlp values (110, 1, 40, 2)
insert into @newlp values (108, 1, 40, 2)
insert into @newlp values (116, 1, 40, 2)

-- PASTE EXCEL DATA ABOVE

begin tran

update lp
set lp.units = s.units
	,TimeUnit = 2
from Polaris.Polaris.LoanPeriods lp
join @newlp s
	on s.orgid = lp.OrganizationID and s.pcid = lp.PatronCodeID and s.lpid = lp.LoanPeriodCodeID

--rollback
--commit
