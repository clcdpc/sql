begin tran

update lirc
set lirc.ProcessingFee = 3.00
from polaris.polaris.SA_LostItemReplacementCosts lirc
where lirc.OrganizationID in (112,113)
	and lirc.MaterialTypeID not in (47,55)

--rollback
--commit
