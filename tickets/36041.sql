begin -- insert values
	if (OBJECT_ID('tempdb..#values') is not null) 
	begin
		drop table #values
	end

	create table #values ( finecodeid int, patroncodeid int, branchid int, amount decimal(18,2), maxfine decimal(18,2), graceunits int )

	BULK INSERT #values
	FROM 'c:\temp\pcl_fine_rates_20180430.csv'
	WITH
	(
		FIRSTROW = 2,
		FIELDTERMINATOR = ',',
		ROWTERMINATOR = '\n',
		TABLOCK,
		KEEPNULLS -- Treat empty fields as NULLs.
	)
end

begin tran

update f
set f.Amount = v.amount, f.MaximumFine = v.maxfine, f.GraceUnits = v.graceunits
from Polaris.Polaris.Fines f
join #values v
	on v.finecodeid = f.FineCodeID and v.patroncodeid = f.PatronCodeID and v.branchid = f.OrganizationID

--rollback
--commit


--drop table #values