declare @msg VARCHAR(50) = CONVERT(char(5), GETDATE(), 108) + ' - start'
RAISERROR (@msg , 0, 1) WITH NOWAIT

declare @entityid int
declare entityCursor CURSOR LOCAL for
		select re.ResourceEntityID
		from Polaris.Polaris.ResourceEntities re
		where re.URL like '%clc.lib%'
			and exists ( select 1 from polaris.polaris.ResourceEntities re2 
						 where re2.URL like '%api.overdrive%' 
							and re2.VendorObjectIdentifier = re.VendorObjectIdentifier 
							and re2.VendorAccountID = re.VendorAccountID )

open entityCursor
fetch next from entityCursor into @entityid

declare @numdeleted int = 0

while @@FETCH_STATUS = 0 
begin
	exec Polaris.Polaris.Cat_DeleteResourceEntity @entityid,5,425,2915,1,0 --@entityid,5,@userid,@workstationid,1,null,null,null
	fetch next from entityCursor into @entityid

	set @numdeleted = @numdeleted + 1
	if (@numdeleted % 100 = 0) 
	begin
		set @msg = CONVERT(char(5), GETDATE(), 108) + ' - deleted ' + cast(@numdeleted as VARCHAR) + ' entities'
		RAISERROR (@msg , 0, 1) WITH NOWAIT
	end
end

close entityCursor
deallocate entityCursor