begin tran

update f
set  f.Amount = 0
	,f.MaximumFine = 0
from Polaris.polaris.Fines f
join Polaris.Polaris.Organizations o
	on o.OrganizationID = f.OrganizationID
where o.ParentOrganizationID = 8 and f.PatronCodeID = 21

--rollback
--commit