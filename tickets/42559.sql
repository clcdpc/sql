declare @values table (collectionid int, collectiongroup varchar(255))
insert into @values values(629, 'Fiction')
insert into @values values(630, 'Other')
insert into @values values(631, 'Juvenile Fiction')
insert into @values values(632, 'Juvenile Fiction')
insert into @values values(633, 'YA Fiction')
insert into @values values(634, 'Juvenile Fiction')
insert into @values values(635, 'Special Collections')
insert into @values values(636, 'Fiction')
insert into @values values(637, 'Video')
insert into @values values(638, 'Juvenile Fiction')
insert into @values values(639, 'Juvenile Fiction')
insert into @values values(640, 'Special Collections')
insert into @values values(641, 'Special Collections')
insert into @values values(642, 'Juvenile Fiction')
insert into @values values(643, 'Other')
insert into @values values(644, 'Fiction')

begin tran

insert into Polaris.dbo.CLC_Custom_CollectionGroups
select * from @values

--rollback
--commit