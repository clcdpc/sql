-- 856 z Available for PCL via Gale Virtual Reference Library. Columbus Metropolitan Library Card required for access.

select bt.BibliographicRecordID
from Polaris.Polaris.BibliographicTags bt
join Polaris.polaris.BibliographicSubfields bs
	on bs.BibliographicTagID = bt.BibliographicTagID
where bt.TagNumber = 856
	and bs.Subfield = 'z'
	and bs.Data like '%via Gale Virtual Reference Library. Columbus Metropolitan Library Card required for access.%'