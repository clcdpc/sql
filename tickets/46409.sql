declare @window table ( orgId int )

declare @windowSubject varchar(255) = 'Requested items are ready for pick up at the drive up window'
declare @windowText varchar(1024) = 'The following items are being held for you at the library and are available for pick up at the drive up window. Your timely pickup of these materials is appreciated. Public access into our buildings is unavailable at this time. For hours and information about drive-up window pickup or walk-up services, go to https://www.columbuslibrary.org/covid-19-coronavirus-response or contact us by chat, e-mail, or phone if you have questions.'

insert into @window values (51) --CML New Albany Branch
insert into @window values (73) --CML New Albany Drive-up Window
insert into @window values (52) --CML Northern Lights Branch
insert into @window values (71) --CML Northern Lights Drive-up Window
insert into @window values (56) --CML Parsons Branch
insert into @window values (68) --CML Parsons Drive-up Window
insert into @window values (58) --CML Shepard Branch
insert into @window values (70) --CML Shepard Drive-up Window
insert into @window values (63) --CML Whitehall Branch
insert into @window values (69) --CML Whitehall Drive-up Window

begin tran

-- Library level defaults
update Polaris.Polaris.SA_CustomMultiLingualStrings
set Value = case when Mnemonic = 'NT_HOLD_EM_HEADER' then 'Requested items are ready for curbside pick up' else 'The following items are being held for you at the library and are available for curbside pickup. Your timely pickup of these materials is appreciated. Public access into our buildings is unavailable at this time. For hours and information about curbside pickup or walk-up services, go to https://www.columbuslibrary.org/covid-19-coronavirus-response or contact us by chat, e-mail, or phone if you have questions.' end
where OrganizationID = 39 and Mnemonic in ('NT_HOLD_EM_HEADER','NT_HOLD_EM_TEXT')

-- Override defaults for window branches and their regular branch

-- Update any branches with existing custom strings
update s
set s.Value = case when s.Mnemonic = 'NT_HOLD_EM_HEADER' then @windowSubject else @windowText end
from @window w
join Polaris.Polaris.SA_CustomMultiLingualStrings s
	on s.OrganizationID = w.orgId
where s.LanguageID = 1033 
	and s.Mnemonic in ('NT_HOLD_EM_HEADER','NT_HOLD_EM_TEXT')

-- Insert any new entries
insert into Polaris.Polaris.SA_CustomMultiLingualStrings (ProductID, LanguageID, OrganizationID, Mnemonic, Value)
select *
from (
	select 7 [ProductID], 1033 [LanguageID], w.orgId [OrganizationID], 'NT_HOLD_EM_HEADER' [Mnemonic], @windowSubject [Value] from @window w
	union all
	select 7,1033, w.orgId, 'NT_HOLD_EM_TEXT', @windowText from @window w
) d
where not exists ( select 1 from Polaris.Polaris.SA_CustomMultiLingualStrings _s where _s.OrganizationID = d.OrganizationID and _s.Mnemonic = d.Mnemonic )

-- Check results
select s.*
from @window w
join Polaris.Polaris.SA_CustomMultiLingualStrings s
	on s.OrganizationID = w.orgId
where s.Mnemonic in ('NT_HOLD_EM_HEADER','NT_HOLD_EM_TEXT')
union all
select * from Polaris.Polaris.SA_CustomMultiLingualStrings s where s.OrganizationID = 39 and s.LanguageID = 1033 and s.Mnemonic in ('NT_HOLD_EM_HEADER','NT_HOLD_EM_TEXT')
order by s.OrganizationID

--rollback
--commit