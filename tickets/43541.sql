--New Fiction  - Collection Code ('Fiction', 'Inspirational', 'Mystery', 'Romance', 'Science Fiction', 'Western')
--New Nonfiction  - Collection Code ('Nonfiction', 'Biography')
--New Large Print  - Collection Code ('Large Print Fiction', 'Large Print Inspirational', 'Large Print Mystery', 'Large Print Romance', 'Large Print Western', 'Large Print Nonfiction')
--New DVDs  - Collection Code ('DVD', 'DVD Action Adventure', 'DVD Classic', 'DVD Comedy', 'DVD Documentary', 'DVD Drama', 'DVD Family', 'DVD Fantasy', 'DVD Foreign', 'DVD Holiday', 'DVD Horror', 'DVD Music', 'DVD Musical', 'DVD Nonfiction', 'DVD Romance', 'DVD Science Fiction', 'DVD Suspense', 'DVD Television', 'Juvenile DVD', 'Juvenile DVD Nonfiction')
--New Music  - Collection Code ('Music')
--New Audio Books  - Collection Code ('Audio Book', 'Audio Book Nonfiction')
--New Graphic Novels  - Collection Code ('Graphic Novel')
--New Picture Books  - Collection Code ('Juvenile Picture Book')
--New Juvenile Fiction  - Collection Code ('Juvenile Fiction')
--New Juvenile Nonfiction  - Collection Code ('Juvenile Nonfiction', 'Juvenile Biography')
--New Juvenile Graphic Novels  - Collection Code ('Juvenile Graphic Novel')
--New Juvenile Audio Books  - Collection Code ('Juvenile Audio Book')
--New Teen Fiction  - Collection Code ('Teen Fiction')
--New Teen Graphic Novels  - Collection Code ('Teen Graphic Novel')




select collectionid from polaris.Polaris.collections where Name in ('Teen Graphic Novel')

/*******************************
On-order
*******************************/
--Fiction
use Polaris;
declare @fictionCollections dbo.clc_custom_int_list
insert into @fictionCollections values (5),(70),(100),(134),(168),(356),(415),(416),(417),(418),(419),(420),(629)
select * from dbo.CLC_Custom_FCDL_On_Order_Titles_RecordSets(@fictionCollections, default)

--Nonfiction
use Polaris;
declare @nonfictionCollections dbo.clc_custom_int_list
insert into @nonfictionCollections values (14),(34)
select * from dbo.CLC_Custom_FCDL_On_Order_Titles_RecordSets(@nonfictionCollections, default)

--Large Print
use Polaris;
declare @largePrintCollections dbo.clc_custom_int_list
insert into @largePrintCollections values (88),(89),(91),(93),(411),(412)
select * from dbo.CLC_Custom_FCDL_On_Order_Titles_RecordSets(@largePrintCollections, 27)

--DVD
use Polaris;
declare @dvdCollections dbo.clc_custom_int_list
insert into @dvdCollections values (55),(269),(365),(366),(369),(384),(429),(438),(439),(441),(442),(443),(446),(447),(448),(449),(450),(451),(452),(479)
select * from dbo.CLC_Custom_FCDL_On_Order_Titles_RecordSets(@dvdCollections, 33)

--Music
use Polaris;
declare @musicCollections dbo.clc_custom_int_list
insert into @musicCollections values (13)
select * from dbo.CLC_Custom_FCDL_On_Order_Titles_RecordSets(@musicCollections, 35)

--Audio books
use Polaris;
declare @audioBooksCollections dbo.clc_custom_int_list
insert into @audioBooksCollections values (1), (362)
select * from dbo.CLC_Custom_FCDL_On_Order_Titles_RecordSets(@audioBooksCollections, 52)

--Graphic Novels
use Polaris;
declare @graphicNovelCollections dbo.clc_custom_int_list
insert into @graphicNovelCollections values (66)
select * from dbo.CLC_Custom_FCDL_On_Order_Titles_RecordSets(@graphicNovelCollections, default)

--Picture Books
use Polaris;
declare @pictureBookCollections dbo.clc_custom_int_list
insert into @pictureBookCollections values (609)
select * from dbo.CLC_Custom_FCDL_On_Order_Titles_RecordSets(@pictureBookCollections, default)

--Juvenile Fiction
use Polaris;
declare @juvenileFictionCollections dbo.clc_custom_int_list
insert into @juvenileFictionCollections values (7)
select * from dbo.CLC_Custom_FCDL_On_Order_Titles_RecordSets(@juvenileFictionCollections, default)

--Juvenile Nonfiction
use Polaris;
declare @juvenileNonfictionCollections dbo.clc_custom_int_list
insert into @juvenileNonfictionCollections values (9),(72)
select * from dbo.CLC_Custom_FCDL_On_Order_Titles_RecordSets(@juvenileNonfictionCollections, default)

--Juvenile Graphic Novels
use Polaris;
declare @juvenileGraphicNovelCollections dbo.clc_custom_int_list
insert into @juvenileGraphicNovelCollections values (75)
select * from dbo.CLC_Custom_FCDL_On_Order_Titles_RecordSets(@juvenileGraphicNovelCollections, default)

--Juvenile Audio Books
use Polaris;
declare @juvenileAudioBookCollections dbo.clc_custom_int_list
insert into @juvenileAudioBookCollections values (6)
select * from dbo.CLC_Custom_FCDL_On_Order_Titles_RecordSets(@juvenileAudioBookCollections, default)

--Teen Fiction
use Polaris;
declare @teenFictionCollections dbo.clc_custom_int_list
insert into @teenFictionCollections values (144)
select * from dbo.CLC_Custom_FCDL_On_Order_Titles_RecordSets(@teenFictionCollections, default)

--Teen Graphic Novels
use Polaris;
declare @teenGraphicNovelCollections dbo.clc_custom_int_list
insert into @teenGraphicNovelCollections values (145)
select * from dbo.CLC_Custom_FCDL_On_Order_Titles_RecordSets(@teenGraphicNovelCollections, default)

-------------------------------------------------------------------------------------------------------


/*******************************
New
*******************************/

--Fiction
use Polaris;
declare @fictionCollections dbo.clc_custom_int_list
insert into @fictionCollections values (5),(70),(100),(134),(168),(356),(415),(416),(417),(418),(419),(420),(629)
select * from dbo.CLC_Custom_FCDL_New_Titles_RecordSets(@fictionCollections, default)

--Nonfiction
use Polaris;
declare @nonfictionCollections dbo.clc_custom_int_list
insert into @nonfictionCollections values (14),(34)
select * from dbo.CLC_Custom_FCDL_New_Titles_RecordSets(@nonfictionCollections, default)

--Large Print
use Polaris;
declare @largePrintCollections dbo.clc_custom_int_list
insert into @largePrintCollections values (88),(89),(91),(93),(411),(412)
select * from dbo.CLC_Custom_FCDL_New_Titles_RecordSets(@largePrintCollections, 27)

--DVD
use Polaris;
declare @dvdCollections dbo.clc_custom_int_list
insert into @dvdCollections values (55),(269),(365),(366),(369),(384),(429),(438),(439),(441),(442),(443),(446),(447),(448),(449),(450),(451),(452),(479)
select * from dbo.CLC_Custom_FCDL_New_Titles_RecordSets(@dvdCollections, 33)

--Music
use Polaris;
declare @musicCollections dbo.clc_custom_int_list
insert into @musicCollections values (13)
select * from dbo.CLC_Custom_FCDL_New_Titles_RecordSets(@musicCollections, 35)

--Audio books
use Polaris;
declare @audioBooksCollections dbo.clc_custom_int_list
insert into @audioBooksCollections values (1), (362)
select * from dbo.CLC_Custom_FCDL_New_Titles_RecordSets(@audioBooksCollections, 52)

--Graphic Novels
use Polaris;
declare @graphicNovelCollections dbo.clc_custom_int_list
insert into @graphicNovelCollections values (66)
select * from dbo.CLC_Custom_FCDL_New_Titles_RecordSets(@graphicNovelCollections, default)

--Picture Books
use Polaris;
declare @pictureBookCollections dbo.clc_custom_int_list
insert into @pictureBookCollections values (609)
select * from dbo.CLC_Custom_FCDL_New_Titles_RecordSets(@pictureBookCollections, default)

--Juvenile Fiction
use Polaris;
declare @juvenileFictionCollections dbo.clc_custom_int_list
insert into @juvenileFictionCollections values (7)
select * from dbo.CLC_Custom_FCDL_New_Titles_RecordSets(@juvenileFictionCollections, default)

--Juvenile Nonfiction
use Polaris;
declare @juvenileNonfictionCollections dbo.clc_custom_int_list
insert into @juvenileNonfictionCollections values (9),(72)
select * from dbo.CLC_Custom_FCDL_New_Titles_RecordSets(@juvenileNonfictionCollections, default)

--Juvenile Graphic Novels
use Polaris;
declare @juvenileGraphicNovelCollections dbo.clc_custom_int_list
insert into @juvenileGraphicNovelCollections values (75)
select * from dbo.CLC_Custom_FCDL_New_Titles_RecordSets(@juvenileGraphicNovelCollections, default)

--Juvenile Audio Books
use Polaris;
declare @juvenileAudioBookCollections dbo.clc_custom_int_list
insert into @juvenileAudioBookCollections values (6)
select * from dbo.CLC_Custom_FCDL_New_Titles_RecordSets(@juvenileAudioBookCollections, 52)

--Teen Fiction
use Polaris;
declare @teenFictionCollections dbo.clc_custom_int_list
insert into @teenFictionCollections values (144)
select * from dbo.CLC_Custom_FCDL_New_Titles_RecordSets(@teenFictionCollections, default)

--Teen Graphic Novels
use Polaris;
declare @teenGraphicNovelCollections dbo.clc_custom_int_list
insert into @teenGraphicNovelCollections values (145)
select * from dbo.CLC_Custom_FCDL_New_Titles_RecordSets(@teenGraphicNovelCollections, default)





select mtom.Description, mtom.MARCTypeOfMaterialID, count(*)
from polaris.Polaris.CircItemRecords cir
join polaris.polaris.MaterialTypes mt
	on mt.MaterialTypeID = cir.MaterialTypeID
join Polaris.polaris.BibliographicRecords br
	on br.BibliographicRecordID = cir.AssociatedBibRecordID
join polaris.polaris.MARCTypeOfMaterial mtom
	on mtom.MARCTypeOfMaterialID = br.PrimaryMARCTOMID
where cir.AssignedBranchID = 9
	and cir.AssignedCollectionID in (select collectionid from polaris.Polaris.collections where Name in ('DVD', 'DVD Action Adventure', 'DVD Classic', 'DVD Comedy', 'DVD Documentary', 'DVD Drama', 'DVD Family', 'DVD Fantasy', 'DVD Foreign', 'DVD Holiday', 'DVD Horror', 'DVD Music', 'DVD Musical', 'DVD Nonfiction', 'DVD Romance', 'DVD Science Fiction', 'DVD Suspense', 'DVD Television', 'Juvenile DVD', 'Juvenile DVD Nonfiction'))
group by mtom.Description, mtom.MARCTypeOfMaterialID

/*

declare @recordsets table (thetype varchar(255))
insert into @recordsets values ('Nonfiction'),('Large Print'),('DVDs'),('Music'),('Audio Books'),('Graphic Novels'),('Picture Books'),('Juvenile Fiction'),('Juvenile Nonfiction'),('Juvenile Graphic Novels'),('Juvenile Audio Books'),('Teen Fiction'),('Teen Graphic Novels ')

declare @type varchar(255)

declare typeCursor cursor for
select thetype from @recordsets

set nocount on

open typeCursor
fetch next from typeCursor into @type
while @@FETCH_STATUS = 0
begin	
	insert into polaris.polaris.RecordSets(Name, ObjectTypeID, CreatorID, OrganizationOwnerID, CreationDate, Note) values ('FCDL New ' + @type, 2, 1, 1, getdate(), 'KEEP - Used for PAC menu item')
	insert into polaris.polaris.RecordSets(Name, ObjectTypeID, CreatorID, OrganizationOwnerID, CreationDate, Note) values ('FCDL On-order ' + @type, 2, 1, 1, getdate(), 'KEEP - Used for PAC menu item')
	fetch next from typeCursor into @type
end

select * from polaris.polaris.RecordSets where Note = 'KEEP - Used for PAC menu item' order by name

*/

insert into polaris.dbo.CLC_Custom_Record_Set_SQL_Search_Mapping (RecordSetID, SearchID, RecordSetType, Note)
select rs.RecordSetID, 0, 'b', rs.Name
from polaris.polaris.RecordSets rs 
where rs.Note = 'KEEP - Used for PAC menu item'
	and rs.RecordSetID not in (170733,170734)

select m.RecordSetID from Polaris.dbo.CLC_Custom_Record_Set_SQL_Search_Mapping m where m.Note like 'fcdl%'