declare @illrequests table ( requestid int )
declare @items table ( itemid int )

insert into @illrequests
select ir.ILLRequestID
from Polaris.Polaris.ILLRequests ir
where (not exists ( select 1 from Polaris.Polaris.CircItemRecords cir where cir.ItemRecordID = ir.ItemRecordID ) 
	or not exists ( select 1 from Polaris.Polaris.ItemRecordDetails ird where ird.ItemRecordID = ir.ItemRecordID ))
	and ir.ILLStatusID not in (1,3,5,16)

insert into @items
select ItemRecordID from Polaris.Polaris.ILLRequests where ILLRequestID in ( select requestid from @illrequests ) 

begin tran

delete from Polaris.Polaris.InnReachRequests where ILLRequestID in ( select requestid from @illrequests )
delete from Polaris.Polaris.ILLRequests where ILLRequestID in ( select requestid from @illrequests )
delete from Polaris.Polaris.CircItemRecords where ItemRecordID in ( select itemid from @items )

select * from @items

--rollback
--commit