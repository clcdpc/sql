/*
Material type	Maximum items 	Maximum requests 
Periodicals 25			50	15
Acq Material Order 32	10	15
Juvenile Kit* 16		6	6
Local A/V Material* 33	6	6
Kit tabletop 20		5	5
Office 46				10	10
Ebook, evideos, emusic, eaudiobook (34,35,36,37)	10	10
Local short term loan 10	5	5
Book 8	50	30
*All patron codes except Kids Card (21), Newly Registered Minor (11), Limited Access (22), and Homebound by Mail (19).  

*/


--select * from Polaris.Polaris.MaterialTypes order by Description

declare @newmll table (mtid int, newmi int, newmr int)

insert into @newmll values(25,50,15)
insert into @newmll values(32,10,15)
insert into @newmll values(16,6,6)
insert into @newmll values(33,6,6)
insert into @newmll values(20,5,5)
insert into @newmll values(46,10,10)
insert into @newmll values(34,10,10)
insert into @newmll values(35,10,10)
insert into @newmll values(36,10,10)
insert into @newmll values(37,10,10)
insert into @newmll values(10,5,5)
insert into @newmll values(8,50,30)

begin tran

update mll
set  mll.MaxItems = case when mll.MaterialTypeID in (16,33) and mll.PatronCodeID in (21,11,22,19) then mll.MaxItems else newmll.newmi end
	,mll.MaxRequestItems = case when mll.MaterialTypeID in (16,33) and mll.PatronCodeID in (21,11,22,19) then mll.MaxRequestItems else newmll.newmr end
from polaris.polaris.MaterialLoanLimits mll
join Polaris.polaris.organizations o
	on o.OrganizationID = mll.OrganizationID
join @newmll newmll
	on newmll.mtid = mll.MaterialTypeID
where o.ParentOrganizationID = 23
	and mll.PatronCodeID not in (20)


--rollback
--commit




--select * from Polaris.dbo.CLC_Custom_MaterialLoanLimits mll where mll.OrganizationID = 24 and mll.MaterialTypeID = 1

