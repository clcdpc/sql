select * from Polaris.polaris.RecordSets rs where rs.RecordSetID = 205439

select irs.ItemRecordID
from polaris.polaris.ItemRecordSets irs
where irs.RecordSetID = 205439

select * from Polaris.polaris.CircItemRecords cir where cir.ItemRecordID = 262285

select h.ItemAgencyCode, count(distinct h.ItemRecordID)
from Polaris.polaris.InnReachRequestsHistory h
where h.ItemID in (
	select irs.ItemRecordID
	from polaris.polaris.ItemRecordSets irs
	where irs.RecordSetID = 205439
)
group by h.ItemAgencyCode


select *
from (
	select	 h.ItemRecordID
			,isnull(h.ItemBarcode, cir.Barcode) [ItemBarcode]
			,h.ItemAgencyCode
			,h.PatronAgencyCode
			,h.LastAPIMessageTime
			,h.Title
			,rank() over(partition by h.ItemRecordID order by h.LastAPIMessageTime desc) [therank]
	from Polaris.polaris.InnReachRequestsHistory h
	left join polaris.polaris.CircItemRecords cir
		on cir.ItemRecordID = h.ItemRecordID
	where h.ItemID in (
		select irs.ItemRecordID
		from polaris.polaris.ItemRecordSets irs
		where irs.RecordSetID = 205439
	)
) d
where d.therank = 1
order by d.ItemRecordID

select br.RecordOwnerID, count(*) from Polaris.polaris.BibliographicRecords br group by br.RecordOwnerID