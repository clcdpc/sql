begin tran

--Loan Periods
	--Loanable Equipment 1	- 40 - 28 days
	--Juvenile Kit			- 16 - 28 days
	--In House Equipment	- 38 -	2 days
	--Book Club Kit			- 35 - 56 days

update lp
	set lp.Units = case lp.LoanPeriodCodeID
		when 40 then 28 
		when 16 then 28 
		when 38 then 2 
		when 35 then 56 
	end
from polaris.polaris.LoanPeriods lp
join polaris.polaris.organizations o
	on o.OrganizationID = lp.OrganizationID
where o.ParentOrganizationID = 78 and lp.LoanPeriodCodeID in (40,16,38,35)

------------------------------------------------------

--Fine Rates - ALL EXCEPT In-House (4) 
	--Loanable Equipment 1	- 40 - $2/day | $10 max
	--Juvenile Kit			- 16 - $2/day | $10 max
	--In House Equipment	- 38 - $2/day | $10 max
	--Book Club Kit			- 35 - $1/day | $10 max

update f
	set f.Amount = case
		when f.FineCodeID in (40,16,38) then 2
		when f.FineCodeID in (35) then 1
	end
	,f.MaximumFine = 10
from polaris.polaris.Fines f
join polaris.polaris.Organizations o
	on o.OrganizationID = f.OrganizationID
where o.ParentOrganizationID = 78 and f.FineCodeID in (40,16,38,35) and f.PatronCodeID not in (4)

-----------------------

--Fine Rates - In-House (4) ONLY 
	--Loanable Equipment 1	- 40 - $0/day | $0 max
	--Juvenile Kit			- 16 - $0/day | $0 max
	--In House Equipment	- 38 - $0/day | $0 max
	--Book Club Kit			- 35 - $0/day | $0 max

update f
	set f.Amount = 0
	   ,f.MaximumFine = 0 
from polaris.polaris.Fines f
join polaris.polaris.Organizations o
	on o.OrganizationID = f.OrganizationID
where o.ParentOrganizationID = 78 and f.FineCodeID in (40,16,38,35) and f.PatronCodeID in (4)

------------------------------------------------------

--Limits - ALL EXCEPT In-House (4) 
	--Loanable Equipment 1	- 43 - 10 cko | 50 hold
	--Juvenile Kit			- 16 - 10 cko | 50 hold
	--In House Equipment	- 41 - 10 cko | 50 hold
	--Book Club Kit			- 38 - 10 cko | 50 hold

update mll
	set mll.MaxItems = 10
	   ,mll.MaxRequestItems = 50
from polaris.Polaris.MaterialLoanLimits mll
join Polaris.polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
where o.ParentOrganizationID = 78 and mll.MaterialTypeID in (43,16,41,38) and mll.PatronCodeID not in (4)

--Limits - In-House (4) ONLY 
	--Loanable Equipment 1	- 43 - 200 cko | 200 hold
	--Juvenile Kit			- 16 - 200 cko | 200 hold
	--In House Equipment	- 41 - 200 cko | 200 hold
	--Book Club Kit			- 38 - 200 cko | 200 hold

update mll
	set mll.MaxItems = 200
	,mll.MaxRequestItems = 200
from polaris.Polaris.MaterialLoanLimits mll
join Polaris.polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
where o.ParentOrganizationID = 78 and mll.MaterialTypeID in (43,16,41,38) and mll.PatronCodeID in (4)

--rollback
--commit









select * from polaris.polaris.LoanPeriodCodes order by Description
select * from polaris.polaris.patroncodes order by Description
select * from polaris.polaris.FineCodes order by Description
select * from polaris.polaris.materialtypes order by Description