--We would like the loan period to be 1 day, fine code $1/day, max items 1, max requests 0
-- Loan period id = 10 ; Fine code = 10 ; material type = 10 ; parent org id = 16 Key customers patron id = 6

begin tran

update lp
set lp.Units = 1
from polaris.Polaris.LoanPeriods lp
join polaris.polaris.Organizations o
	on o.OrganizationID = lp.OrganizationID
where o.ParentOrganizationID = 16 and lp.LoanPeriodCodeID = 10

update f
set f.Amount = 1, f.MaximumFine = 10
from Polaris.polaris.Fines f
join polaris.polaris.organizations o
	on o.OrganizationID = f.OrganizationID
where o.ParentOrganizationID = 16 and f.FineCodeID = 10 and f.PatronCodeID != 6

update mll
set mll.MaxItems = 1, mll.MaxRequestItems = 0
from polaris.polaris.MaterialLoanLimits mll
join Polaris.polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
where o.ParentOrganizationID = 16 and mll.MaterialTypeID = 10 and mll.PatronCodeID != 6

select lp.*
from polaris.Polaris.LoanPeriods lp
join polaris.polaris.Organizations o
	on o.OrganizationID = lp.OrganizationID
where o.ParentOrganizationID = 16 and lp.LoanPeriodCodeID = 10

select f.*
from Polaris.polaris.Fines f
join polaris.polaris.organizations o
	on o.OrganizationID = f.OrganizationID
where o.ParentOrganizationID = 16 and f.FineCodeID = 10 and f.PatronCodeID != 6

select mll.*
from polaris.polaris.MaterialLoanLimits mll
join Polaris.polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
where o.ParentOrganizationID = 16 and mll.MaterialTypeID = 10 and mll.PatronCodeID != 6

--rollback
--commit