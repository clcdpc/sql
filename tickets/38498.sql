--can you update all 3 VG material types to have a loan limit of 3 UNLESS the VG loan limit is 0

begin tran

update mll
set mll.MaxItems = 3
from Polaris.polaris.MaterialLoanLimits mll
join Polaris.Polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
where mll.MaterialTypeID in (29,30,31) --Videogame E/M/T
	and o.ParentOrganizationID = 105
	and mll.MaxItems > 0

--rollback
--commit