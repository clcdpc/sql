declare @settings table (orgid int, patroncodeid int, loanperiodid int, units int)

begin --insert values
	set nocount on
	insert into @settings values(17, 17, 24, 28)
	insert into @settings values(17, 18, 24, 28)
	insert into @settings values(17, 1, 24, 28)
	insert into @settings values(17, 19, 24, 0)
	insert into @settings values(17, 3, 24, 28)
	insert into @settings values(17, 6, 24, 28)
	insert into @settings values(17, 21, 24, 0)
	insert into @settings values(17, 22, 24, 0)
	insert into @settings values(17, 10, 24, 28)
	insert into @settings values(17, 11, 24, 28)
	insert into @settings values(17, 23, 24, 28)
	insert into @settings values(17, 24, 24, 0)
	insert into @settings values(17, 20, 24, 75)
	insert into @settings values(17, 12, 24, 28)
	insert into @settings values(17, 13, 24, 28)
	insert into @settings values(17, 14, 24, 28)
	insert into @settings values(17, 15, 24, 28)
	insert into @settings values(17, 25, 24, 28)
	insert into @settings values(17, 27, 24, 28)
	insert into @settings values(17, 26, 24, 28)
	insert into @settings values(17, 28, 24, 28)
	insert into @settings values(17, 16, 24, 28)
	insert into @settings values(18, 17, 24, 28)
	insert into @settings values(18, 18, 24, 28)
	insert into @settings values(18, 1, 24, 28)
	insert into @settings values(18, 19, 24, 0)
	insert into @settings values(18, 3, 24, 28)
	insert into @settings values(18, 6, 24, 28)
	insert into @settings values(18, 21, 24, 0)
	insert into @settings values(18, 22, 24, 0)
	insert into @settings values(18, 10, 24, 28)
	insert into @settings values(18, 11, 24, 28)
	insert into @settings values(18, 23, 24, 28)
	insert into @settings values(18, 24, 24, 0)
	insert into @settings values(18, 20, 24, 75)
	insert into @settings values(18, 12, 24, 28)
	insert into @settings values(18, 13, 24, 28)
	insert into @settings values(18, 14, 24, 28)
	insert into @settings values(18, 15, 24, 28)
	insert into @settings values(18, 25, 24, 28)
	insert into @settings values(18, 27, 24, 28)
	insert into @settings values(18, 26, 24, 28)
	insert into @settings values(18, 28, 24, 28)
	insert into @settings values(18, 16, 24, 28)
	set nocount off
end

begin tran

update lp
set lp.Units = s.units
from polaris.polaris.LoanPeriods lp
join @settings s
	on s.orgid = lp.OrganizationID and s.patroncodeid = lp.PatronCodeID and s.loanperiodid = lp.LoanPeriodCodeID
where lp.Units != s.units

--rollback
--commit