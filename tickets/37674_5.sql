--Please set the Loan Periods for patron code 'Kids Card' to be the same as 'Full Access' for all CML locations

begin tran

update lp_dest
set lp_dest.Units = lp_source.Units
from Polaris.polaris.LoanPeriods lp_dest
join polaris.Polaris.Organizations o
	on o.OrganizationID = lp_dest.OrganizationID
join polaris.Polaris.LoanPeriods lp_source
	on lp_source.OrganizationID = lp_dest.OrganizationID and lp_source.LoanPeriodCodeID = lp_dest.LoanPeriodCodeID
where o.ParentOrganizationID = 39 and lp_dest.PatronCodeID = 21 and lp_source.PatronCodeID = 1


--rollback
--commit