/*
Double each loan period (28 day books go to 56, New Books go from 14 days to 28 days, etc)

Set the due date for all currently checked out items to 6/30 (this was previously 6/22)
*/

begin tran

update lp
set lp.Units = lp.Units * 2
from polaris.Polaris.LoanPeriods lp
join Polaris.Polaris.Organizations o
	on o.OrganizationID = lp.OrganizationID
where o.ParentOrganizationID = 78
	and lp.Units * 2 < 999

update ic
set ic.DueDate = '6/30/2020 23:59:59'
from Polaris.Polaris.ItemCheckouts ic
join polaris.Polaris.Organizations o
	on o.OrganizationID = ic.OrganizationID
join polaris.Polaris.CircItemRecords cir
	on cir.ItemRecordID = ic.ItemRecordID
where ic.DueDate between getdate() and '6/30/2020'
	and cir.ElectronicItem = 0
	and o.ParentOrganizationID = 78


--rollback
--commit