begin -- insert values
	if (OBJECT_ID('tempdb..#values') is not null) 
	begin
		drop table #values
	end

	create table #values ( branchid int, patroncodeid int, materialtypeid int, maxitems int, maxrequestitems int )

	BULK INSERT #values
	FROM 'c:\temp\pcl_loan_limits_20180430.csv'
	WITH
	(
		FIRSTROW = 2,
		FIELDTERMINATOR = ',',
		ROWTERMINATOR = '\n',
		TABLOCK,
		KEEPNULLS -- Treat empty fields as NULLs.
	)
end


begin tran

update mll
set mll.MaxItems = v.maxitems, mll.MaxRequestItems = v.maxrequestitems
from Polaris.polaris.MaterialLoanLimits mll
join #values v
	on v.branchid = mll.OrganizationID and v.patroncodeid = mll.PatronCodeID and v.materialtypeid = mll.MaterialTypeID

--rollback
--commit


--drop table #values