--Collection Code: Tabletop Game
--Material Type/Load/Fine Codes: Kit

--Patron Restrictions: All BPL patrons have access, including Newly Registered. 
--Checkout Limit: 3 per card
--Hold Limit: 5
--Loan Period: 2 weeks
--Fines: fine free
--Max Fine: $5.00 (we're fine free but most things seem to still have it set at $5.00???)
--Fine Grace Period: 0

begin tran

update mll
set mll.MaxItems = 3
	,mll.MaxRequestItems = 5
from Polaris.Polaris.MaterialLoanLimits mll
where mll.OrganizationID = 85 and mll.MaterialTypeID = 20

update f
set f.Amount = 0
	,f.MaximumFine = 0
	,f.GraceUnits = 0
from Polaris.Polaris.Fines f
where f.OrganizationID = 85 and f.FineCodeID = 20

update lp
set lp.Units = 14
from polaris.polaris.LoanPeriods lp
where lp.OrganizationID = 85 and lp.LoanPeriodCodeID = 20

--rollback
--commit