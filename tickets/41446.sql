declare @settings table ( orgid int, pcid int, maxfine decimal(18,2), minfine decimal(18,2), totalitems int, totaloverdue int, totalholds int, totalill int )

begin
set nocount on
insert into @settings values (118, 17, 5, 0.1, 0, 10, 0, 0)
insert into @settings values (118, 18, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (118, 1, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (118, 19, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (118, 3, 5, 0.1, 200, 200, 200, 0)
insert into @settings values (118, 6, 5, 0.1, 5, 5, 0, 0)
insert into @settings values (118, 21, 50, 0.1, 5, 5, 0, 0)
insert into @settings values (118, 22, 5, 0.1, 0, 0, 0, 0)
insert into @settings values (118, 10, 5, 0.1, 5, 5, 5, 0)
insert into @settings values (118, 11, 5, 0.1, 5, 5, 5, 0)
insert into @settings values (118, 23, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (118, 24, 5, 0.1, 0, 10, 0, 0)
insert into @settings values (118, 20, 5, 0.1, 999, 999, 999, 999)
insert into @settings values (118, 12, 5, 0.1, 0, 5, 5, 0)
insert into @settings values (118, 13, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (118, 14, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (118, 15, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (118, 25, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (118, 27, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (118, 26, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (118, 28, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (118, 16, 5, 0.1, 25, 10, 25, 25)
insert into @settings values (17, 17, 5, 0.1, 0, 10, 0, 0)
insert into @settings values (17, 18, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (17, 1, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (17, 19, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (17, 3, 5, 0.1, 200, 200, 200, 0)
insert into @settings values (17, 6, 5, 0.1, 5, 5, 0, 0)
insert into @settings values (17, 21, 50, 0.1, 5, 5, 0, 0)
insert into @settings values (17, 22, 5, 0.1, 0, 0, 0, 0)
insert into @settings values (17, 10, 5, 0.1, 5, 5, 5, 0)
insert into @settings values (17, 11, 5, 0.1, 5, 5, 5, 0)
insert into @settings values (17, 23, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (17, 24, 5, 0.1, 0, 10, 0, 0)
insert into @settings values (17, 20, 5, 0.1, 999, 999, 999, 999)
insert into @settings values (17, 12, 5, 0.1, 0, 5, 5, 0)
insert into @settings values (17, 13, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (17, 14, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (17, 15, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (17, 25, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (17, 27, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (17, 26, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (17, 28, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (17, 16, 5, 0.1, 25, 10, 25, 25)
insert into @settings values (18, 17, 5, 0.1, 0, 10, 0, 0)
insert into @settings values (18, 18, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (18, 1, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (18, 19, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (18, 3, 5, 0.1, 200, 200, 200, 0)
insert into @settings values (18, 6, 5, 0.1, 5, 5, 0, 0)
insert into @settings values (18, 21, 50, 0.1, 5, 5, 0, 0)
insert into @settings values (18, 22, 5, 0.1, 0, 0, 0, 0)
insert into @settings values (18, 10, 5, 0.1, 5, 5, 5, 0)
insert into @settings values (18, 11, 5, 0.1, 5, 5, 5, 0)
insert into @settings values (18, 23, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (18, 24, 5, 0.1, 0, 10, 0, 0)
insert into @settings values (18, 20, 5, 0.1, 999, 999, 999, 999)
insert into @settings values (18, 12, 5, 0.1, 0, 5, 5, 0)
insert into @settings values (18, 13, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (18, 14, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (18, 15, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (18, 25, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (18, 27, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (18, 26, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (18, 28, 5, 0.1, 100, 5, 75, 0)
insert into @settings values (18, 16, 5, 0.1, 25, 10, 25, 25)
set nocount off
end

begin tran

update pll
set pll.MaxFine = s.maxfine
	,pll.MinFine = s.minfine
	,pll.TotalItems = s.totalitems
	,pll.TotalOverDue = s.totaloverdue
	,pll.TotalHolds = s.totalholds
	,pll.TotalILL = s.totalill
from Polaris.polaris.PatronLoanLimits pll
join @settings s
	on s.orgid = pll.OrganizationID and s.pcid = pll.PatronCodeID

--rollback
--commit