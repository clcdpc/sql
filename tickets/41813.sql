begin tran

insert into Polaris.Polaris.PermissionGroups
select distinct pg.ControlRecordID
		,pd2.PermissionID
		,g.GroupID
from Polaris.Polaris.PermissionGroups pg
join Polaris.Polaris.Groups g
	on g.GroupID = pg.GroupID
join Polaris.Polaris.ControlRecords cr
	on cr.ControlRecordID = pg.ControlRecordID
join Polaris.Polaris.PermissionDefs pd
	on pd.PermissionID = pg.PermissionID
join Polaris.Polaris.ControlRecordDefs crd
	on crd.ControlRecordDefID = cr.ControlRecordDefID and crd.ControlRecordDefID = pd.ControlRecordDefID
join Polaris.Polaris.PermissionDefs pd2
	on pd2.ControlRecordDefID = crd.ControlRecordDefID and pd2.PermissionNameID in (79,80)
where crd.ControlRecordDefID = 20
	and pd.PermissionNameID = 4
	and g.GroupID > 3

--rollback
--commit