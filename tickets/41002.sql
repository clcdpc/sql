declare @settings table ( orgid int, pcid int, mtid int, maxitems int, maxrequests int )

insert into @settings values (16, 17, 21, 0, 0)
insert into @settings values (16, 18, 21, 100, 75)
insert into @settings values (16, 1, 21, 100, 75)
insert into @settings values (16, 19, 21, 100, 75)
insert into @settings values (16, 3, 21, 100, 75)
insert into @settings values (16, 6, 21, 0, 0)
insert into @settings values (16, 21, 21, 0, 0)
insert into @settings values (16, 22, 21, 0, 0)
insert into @settings values (16, 10, 21, 5, 75)
insert into @settings values (16, 11, 21, 5, 75)
insert into @settings values (16, 23, 21, 100, 75)
insert into @settings values (16, 24, 21, 0, 0)
insert into @settings values (16, 20, 21, 999, 999)
insert into @settings values (16, 12, 21, 0, 75)
insert into @settings values (16, 13, 21, 100, 75)
insert into @settings values (16, 14, 21, 100, 75)
insert into @settings values (16, 15, 21, 100, 75)
insert into @settings values (16, 25, 21, 100, 75)
insert into @settings values (16, 27, 21, 100, 75)
insert into @settings values (16, 26, 21, 100, 75)
insert into @settings values (16, 28, 21, 100, 75)
insert into @settings values (16, 16, 21, 0, 0)

begin tran

update mll
set  mll.MaxItems = s.maxitems
	,mll.MaxRequestItems = s.maxrequests
from Polaris.Polaris.MaterialLoanLimits mll
join polaris.polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
join @settings s
	on s.orgid = o.ParentOrganizationID and s.pcid = mll.PatronCodeID and s.mtid = mll.MaterialTypeID
where mll.MaxItems != s.maxitems or mll.MaxRequestItems != s.maxrequests

--rollback
--commit