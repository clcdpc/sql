declare @fines table ( branchid int, pcid int, fcid int, amount decimal, maxfine decimal, graceunits int )
declare @loanperiods table ( branchid int, pcid int, lpid int, units int )
declare @loanlimits table ( branchid int, pcid int, mtid int, maxitems int, maxrequests int )

begin -- insert values
	set nocount on
	insert into @fines values(85, 17, 20, 0, 0, 0)
	insert into @fines values(85, 18, 20, 0, 0, 0)
	insert into @fines values(85, 1, 20, 1, 15, 0)
	insert into @fines values(85, 19, 20, 0, 0, 0)
	insert into @fines values(85, 3, 20, 1, 15, 0)
	insert into @fines values(85, 6, 20, 1, 15, 0)
	insert into @fines values(85, 21, 20, 0, 0, 0)
	insert into @fines values(85, 22, 20, 0, 0, 0)
	insert into @fines values(85, 10, 20, 1, 15, 0)
	insert into @fines values(85, 11, 20, 1, 15, 0)
	insert into @fines values(85, 23, 20, 0, 0, 0)
	insert into @fines values(85, 24, 20, 0, 0, 0)
	insert into @fines values(85, 20, 20, 0, 0, 0)
	insert into @fines values(85, 12, 20, 1, 15, 0)
	insert into @fines values(85, 13, 20, 1, 15, 0)
	insert into @fines values(85, 14, 20, 1, 15, 0)
	insert into @fines values(85, 15, 20, 1, 15, 0)
	insert into @fines values(85, 25, 20, 0, 0, 0)
	insert into @fines values(85, 27, 20, 0, 0, 0)
	insert into @fines values(85, 26, 20, 0, 0, 0)
	insert into @fines values(85, 28, 20, 0, 0, 0)
	insert into @fines values(85, 16, 20, 1, 15, 0)

	insert into @loanperiods values(85, 17, 20, 0)
	insert into @loanperiods values(85, 18, 20, 14)
	insert into @loanperiods values(85, 1, 20, 14)
	insert into @loanperiods values(85, 19, 20, 0)
	insert into @loanperiods values(85, 3, 20, 0)
	insert into @loanperiods values(85, 6, 20, 14)
	insert into @loanperiods values(85, 21, 20, 0)
	insert into @loanperiods values(85, 22, 20, 0)
	insert into @loanperiods values(85, 10, 20, 0)
	insert into @loanperiods values(85, 11, 20, 14)
	insert into @loanperiods values(85, 23, 20, 0)
	insert into @loanperiods values(85, 24, 20, 0)
	insert into @loanperiods values(85, 20, 20, 0)
	insert into @loanperiods values(85, 12, 20, 14)
	insert into @loanperiods values(85, 13, 20, 14)
	insert into @loanperiods values(85, 14, 20, 14)
	insert into @loanperiods values(85, 15, 20, 14)
	insert into @loanperiods values(85, 25, 20, 0)
	insert into @loanperiods values(85, 27, 20, 0)
	insert into @loanperiods values(85, 26, 20, 0)
	insert into @loanperiods values(85, 28, 20, 0)
	insert into @loanperiods values(85, 16, 20, 14)

	insert into @loanlimits values(85, 17, 20, 0, 0)
	insert into @loanlimits values(85, 18, 20, 1, 1)
	insert into @loanlimits values(85, 1, 20, 1, 1)
	insert into @loanlimits values(85, 19, 20, 0, 0)
	insert into @loanlimits values(85, 3, 20, 0, 0)
	insert into @loanlimits values(85, 6, 20, 1, 1)
	insert into @loanlimits values(85, 21, 20, 0, 0)
	insert into @loanlimits values(85, 22, 20, 0, 0)
	insert into @loanlimits values(85, 10, 20, 0, 0)
	insert into @loanlimits values(85, 11, 20, 0, 0)
	insert into @loanlimits values(85, 23, 20, 0, 0)
	insert into @loanlimits values(85, 24, 20, 0, 0)
	insert into @loanlimits values(85, 20, 20, 0, 0)
	insert into @loanlimits values(85, 12, 20, 1, 1)
	insert into @loanlimits values(85, 13, 20, 1, 1)
	insert into @loanlimits values(85, 14, 20, 1, 1)
	insert into @loanlimits values(85, 15, 20, 1, 1)
	insert into @loanlimits values(85, 25, 20, 0, 0)
	insert into @loanlimits values(85, 27, 20, 0, 0)
	insert into @loanlimits values(85, 26, 20, 0, 0)
	insert into @loanlimits values(85, 28, 20, 0, 0)
	insert into @loanlimits values(85, 16, 20, 0, 0)
	set nocount off

end


begin tran

update f 
set f.Amount = new.amount
	,f.MaximumFine = new.maxfine
	,f.GraceUnits = new.graceunits
from Polaris.polaris.Fines f
join @fines new
	on new.branchid = f.OrganizationID and new.pcid = f.PatronCodeID and new.fcid = f.FineCodeID

update lp
set lp.Units = new.units
from Polaris.Polaris.LoanPeriods lp
join @loanperiods new
	on new.branchid = lp.OrganizationID and new.pcid = lp.PatronCodeID and new.lpid = lp.LoanPeriodCodeID

update mll
set mll.MaxItems = new.maxitems
	,mll.MaxRequestItems = new.maxrequests
from Polaris.Polaris.MaterialLoanLimits mll
join @loanlimits new
	on new.branchid = mll.OrganizationID and new.pcid = mll.PatronCodeID and new.mtid = mll.MaterialTypeID

--rollback
--commit