declare @oldPickupLocations table ( orgid int )
insert into @oldPickupLocations values (142)

declare @newPickupBranchId int = 81


declare @logonBranchId int = null -- null means use old pickup branch
	   ,@polarisUserId int = 2691 -- a.mfields
	   ,@workstationId int = 2915 -- clcdevvm
	   ,@requestId int, @oldPickupBranchId int --cursor variables
	   
declare @statuses table ( id int )
insert into @statuses values (1),(3),(4),(5),(6) 
 
DECLARE HoldNotices CURSOR FOR
select shr.SysHoldRequestID, isnull(shr.NewPickupBranchID, shr.PickupBranchID)
from polaris.polaris.SysHoldRequests shr 
where isnull(shr.NewPickupBranchID, shr.PickupBranchID) in ( select * from @oldPickupLocations )
	and shr.SysHoldStatusID in ( select * from @statuses )
	and shr.Origin != 3
 
OPEN HoldNotices
 
FETCH NEXT FROM HoldNotices INTO @requestId, @oldPickupBranchId
 
WHILE @@FETCH_STATUS = 0
BEGIN
	declare @effectiveLogonBranchId int = isnull(@logonBranchId, @oldPickupBranchId)
    exec Polaris.Polaris.Circ_UpdatePickupBranchID @requestId,@newPickupBranchId,@polarisUserId,@workstationId,@effectiveLogonBranchId,33,null
    FETCH NEXT FROM HoldNotices INTO @requestId, @oldPickupBranchId
END
CLOSE HoldNotices
DEALLOCATE HoldNotices
