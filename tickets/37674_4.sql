begin tran
--Video PG (3) needs the same checkout limits, request limits and loan periods as Video PG-13/TV-14 (based on CML location exceptions) for the following patron codes:
--- Homebound by Mail - 19
--- Video up to PG/VG up to E - 25
--- Video up to PG-13/VG up to T - 26

-- Limits
-- Video PG (3) -> Video PG-13/TV-14 (51)
update mll_dest
set mll_dest.MaxItems = mll_source.MaxItems
	,mll_dest.MaxRequestItems = mll_source.MaxRequestItems
from polaris.polaris.MaterialLoanLimits mll_dest
join Polaris.polaris.organizations o
	on o.OrganizationID = mll_dest.OrganizationID
join Polaris.polaris.MaterialLoanLimits mll_source
	on mll_source.OrganizationID = mll_dest.OrganizationID and mll_source.PatronCodeID = mll_dest.PatronCodeID
where o.ParentOrganizationID = 39 and mll_dest.MaterialTypeID = 3 and mll_dest.PatronCodeID in (19,25,26) and mll_source.MaterialTypeID = 51

-- Loan period
-- Video PG (3) -> Video PG-13/TV-14 (48)
update lp_dest
set lp_dest.Units = lp_source.Units
from polaris.polaris.LoanPeriods lp_dest
join polaris.polaris.Organizations o
	on o.OrganizationID = lp_dest.OrganizationID
join Polaris.polaris.LoanPeriods lp_source
	on lp_source.OrganizationID = lp_dest.OrganizationID and lp_source.PatronCodeID = lp_dest.PatronCodeID
where o.ParentOrganizationID = 39 and lp_dest.LoanPeriodCodeID = 3 and lp_dest.PatronCodeID in (19,25,26) and lp_source.LoanPeriodCodeID = 48

----------------------------------------------------------

--The checkout limit and request limit for Video PG-13/TV-14 (51) should be 0 for the following patron codes for all CML locations: 
--- Video up to G/VG up to E - 25
--- Video up to PG/VG up to E - 27
update mll
set mll.MaxItems = 0
	,mll.MaxRequestItems = 0
from polaris.polaris.MaterialLoanLimits mll
join polaris.polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
where o.ParentOrganizationID = 39 and mll.MaterialTypeID = 51 and mll.PatronCodeID in (25,27)

--rollback
--commit