begin tran

-- update all patron record sets
update rs
set rs.OrganizationOwnerID = case o.OrganizationCodeID when 3 then o.ParentOrganizationID else pu.OrganizationID end
from polaris.Polaris.RecordSets rs
join polaris.polaris.PolarisUsers pu
on pu.PolarisUserID = rs.CreatorID
join polaris.polaris.Organizations o
on o.OrganizationID = pu.OrganizationID
where rs.OrganizationOwnerID = 1
	and rs.ObjectTypeID = 27 -- patron

-- update bib/item record sets after 30 days
update rs
set rs.OrganizationOwnerID = case o.OrganizationCodeID when 3 then o.ParentOrganizationID else pu.OrganizationID end
from polaris.Polaris.RecordSets rs
join polaris.polaris.PolarisUsers pu
on pu.PolarisUserID = rs.CreatorID
join polaris.polaris.Organizations o
on o.OrganizationID = pu.OrganizationID
where rs.OrganizationOwnerID = 1
	and rs.ObjectTypeID in ( 2,3 ) -- bib/item
	--and rs.CreationDate < dateadd(day, -30, getdate())

--rollback
--commit