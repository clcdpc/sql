begin -- insert values
	if (OBJECT_ID('tempdb..#patrons') is not null) 
	begin
		drop table #patrons
	end

	create table #patrons ( patronid int )

	BULK INSERT #patrons
	FROM 'c:\temp\cml_collections_patrons.csv'
	WITH
	(
		FIRSTROW = 2,
		FIELDTERMINATOR = ',',
		ROWTERMINATOR = '\n',
		TABLOCK,
		KEEPNULLS -- Treat empty fields as NULLs.
	)
end

select distinct pa.PatronID, pa.OutstandingAmount
from Polaris.polaris.PatronAccount pa 
join polaris.Polaris.Patrons p
	on p.PatronID = pa.PatronID
join polaris.polaris.Organizations o
	on o.OrganizationID = p.OrganizationID
where pa.FeeReasonCodeID = -2 and pa.PatronID in ( select * from #patrons ) and pa.TxnCodeID = 1 and pa.TxnDate > '5/1/2018' and pa.OutstandingAmount < pa.TxnAmount