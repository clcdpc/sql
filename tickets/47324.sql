begin tran

update br
set br.RecordOwnerID = 5
from polaris.polaris.BibliographicRecords br
join Polaris.polaris.BibliographicTags bt
	on bt.BibliographicRecordID = br.BibliographicRecordID
join polaris.polaris.BibliographicSubfields bs
	on bs.BibliographicTagID = bt.BibliographicTagID
where br.RecordOwnerID = 1
	and bt.TagNumber = 856
	and bs.Data like '%overdrive.com%'

--rollback
--commit