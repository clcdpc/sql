-- Full Access, Key Customers, Restricted 1, Restriced 2, Senior, Staff, Teacher/Educator, Business/Community and Video G.

-- 1,6,7,8,9,13,15,18,25

-- loan period 2 days
-- 0 fines
-- 1 max item / 0 max request

begin tran

update lp
set lp.Units = 2
from polaris.polaris.LoanPeriods lp
join polaris.polaris.Organizations o
	on o.OrganizationID = lp.OrganizationID
where o.ParentOrganizationID = 39 and lp.PatronCodeID in ( 1,6,7,8,9,13,15,18,25 ) and lp.LoanPeriodCodeID = 10

update f
set f.Amount = 0, f.MaximumFine = 0
from Polaris.polaris.Fines f
join polaris.polaris.organizations o
	on o.OrganizationID = f.OrganizationID
where o.ParentOrganizationID = 39 and f.PatronCodeID in ( 1,6,7,8,9,13,15,18,25 ) and f.FineCodeID = 10

update mll
set mll.MaxItems = 1, mll.MaxRequestItems = 0
from polaris.Polaris.MaterialLoanLimits mll
join polaris.polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
where o.ParentOrganizationID = 39 and mll.PatronCodeID in ( 1,6,7,8,9,13,15,18,25 ) and mll.MaterialTypeID = 10

--rollback
--commit