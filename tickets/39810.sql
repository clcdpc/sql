--checkout limit of 1 and a hold request limit of 0 for Loanable Equipment 1 (43) material type for patron codes Full Access (1), Kids (21) and Video up to PG13 (26)
begin tran

update mll
set mll.MaxItems = 1
	,mll.MaxRequestItems = 0
from Polaris.polaris.MaterialLoanLimits mll
join Polaris.polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
where o.ParentOrganizationID = 39 and mll.MaterialTypeID = 43 and mll.PatronCodeID in (1,21,26)


update lp
set lp.Units = 1
from Polaris.polaris.LoanPeriods lp
join polaris.polaris.Organizations o
	on o.OrganizationID = lp.OrganizationID
where o.ParentOrganizationID = 39 and lp.LoanPeriodCodeID = 40 and lp.PatronCodeID in (1,21,26)


--rollback
--commit