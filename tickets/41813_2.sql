--select * from Polaris.Polaris.PermissionUsers pu where pu.PermissionID in (498,499) and pu.PolarisUserID > 3



begin tran

delete pu
from Polaris.Polaris.PermissionUsers pu
join Polaris.Polaris.ControlRecords cr
	on cr.ControlRecordID = pu.ControlRecordID
where cr.ControlRecordDefID = 20 
	and pu.PermissionID in (498,499)
	and pu.PolarisUserID > 3

--rollback
--commit


select crd.ControlRecordName, pn.PermissionName, cr.OrganizationID, o.OrganizationCodeID, count(*) as [Number of Users]
from polaris.polaris.PermissionUsers pu
join polaris.polaris.ControlRecords cr
        on cr.ControlRecordID = pu.ControlRecordID
join polaris.polaris.ControlRecordDefs crd
        on crd.ControlRecordDefID = cr.ControlRecordDefID
join polaris.polaris.PermissionDefs pd
        on pd.ControlRecordDefID = crd.ControlRecordDefID and pd.PermissionID = pu.PermissionID
join polaris.polaris.PermissionNames pn 
        on pd.PermissionNameID = pn.PermissionNameID
join polaris.polaris.organizations o
        on o.OrganizationID = cr.OrganizationID
where pu.PolarisUserID > 2
group by crd.ControlRecordName, pn.PermissionName, cr.OrganizationID, o.OrganizationCodeID
order by count(*) desc