declare @map table ( sourceid int, destid int )
insert into @map values (8,22)
insert into @map values (3,34)

begin tran

update f_dest
set f_dest.Amount = f_source.Amount
	,f_dest.MaximumFine = f_source.MaximumFine
	,f_dest.GraceUnits = f_source.GraceUnits
from Polaris.Polaris.Fines f_dest
join Polaris.Polaris.Fines f_source
	on f_source.OrganizationID = f_dest.OrganizationID and f_source.PatronCodeID = f_dest.PatronCodeID
join @map m
	on m.sourceid = f_source.FineCodeID and m.destid = f_dest.FineCodeID
where f_dest.OrganizationID = 15

--rollback
--commit