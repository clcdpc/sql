/*
Our checkout policy has changed so that there is the overall limit of 50 items per card. 
If someone wanted 50 DVDs we would now let them. 
There should still only be one hotspot and one launchpad although those might be things we just enforce at the Circ counter. 
*/

begin tran

update pll
set pll.TotalItems = 50
from polaris.Polaris.PatronLoanLimits pll
join polaris.polaris.Organizations o
	on o.OrganizationID = pll.OrganizationID
where o.ParentOrganizationID = 8
	and pll.TotalItems > 50


update mll
set mll.maxitems = case when mll.MaterialTypeID in (33,40) then 1 when mll.MaxItems < 10 then mll.MaxItems / 2 else 50 end
	,mll.MaxRequestItems = case when mll.MaterialTypeID in (33,40) then 1 when mll.MaxItems < 50 then mll.MaxItems / 2 else 50 end
from polaris.polaris.MaterialLoanLimits mll
join polaris.polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
where o.ParentOrganizationID = 8
	and mll.MaxItems > 0

--rollback
--commit


