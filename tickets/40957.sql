begin tran

update mll
set mll.MaxItems = 1
	,mll.MaxRequestItems = 0
from polaris.polaris.MaterialLoanLimits mll
join Polaris.polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
where o.ParentOrganizationID = 14 and mll.PatronCodeID in (10,11) and mll.MaterialTypeID = 41

--rollback
--commit