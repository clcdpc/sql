begin tran

update pll
set pll.TotalItems = 3000
	,pll.TotalOverDue = 3000
from Polaris.Polaris.PatronLoanLimits pll
join polaris.polaris.Organizations o
	on o.OrganizationID = pll.OrganizationID
where o.ParentOrganizationID = 39 and pll.PatronCodeID = 24


update mll
set mll.MaxItems = 3000
from Polaris.Polaris.MaterialLoanLimits mll
join polaris.polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
where o.ParentOrganizationID = 39 
	and mll.PatronCodeID = 24
	and mll.MaxItems = 999


--rollback
--commit