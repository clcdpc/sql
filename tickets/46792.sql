--CML would like to increase our checkout limits for the various video material types from 20 to 30 where applicable.
--We'd also like to video group limit increased from 20 to 30. 
--I did notice that the Video PG checkout limit is currently set to 0 for patron code Video up to PG/VG up to E, but it should have been 20 so it should also be 30. 
--Additionally, the Video PG checkout limit for patron code Video up to G/VG up to E is currently 20, but it should be 0. 
--Any other video limits/patron code combinations for CML that are 0 or greater than 20 currently should not be changed.

begin tran

update mll
set mll.MaxItems = 30
from Polaris.Polaris.MaterialLoanLimits mll
join Polaris.Polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
where o.ParentOrganizationID = 39
	and ((mll.MaxItems = 20 and mll.MaterialTypeID in (49,50,3,51,52,53,18,42)) 
		or 
		(mll.PatronCodeID = 27 and mll.MaterialTypeID = 3))

update mll
set mll.MaxItems = 0
	,mll.MaxRequestItems = 0
from Polaris.Polaris.MaterialLoanLimits mll
join Polaris.Polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
where o.ParentOrganizationID = 39
	and mll.PatronCodeID = 25 and mll.MaterialTypeID = 3

update l
set l.Limit = 30
from Polaris.Polaris.SA_MaterialTypeGroups_Limits l
where l.GroupID in (21,66)
	and l.Limit = 20

--rollback
--commit


select * from polaris.polaris.materialtypes order by Description