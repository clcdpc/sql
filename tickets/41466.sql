--Loanable Equipment 1 (Baskets) (40) to 21 days for patron types full access (1), newly registered (11) and Teachers. (15)

begin tran

update lp
set lp.Units = 21
from Polaris.Polaris.LoanPeriods lp
join polaris.polaris.organizations o
	on o.OrganizationID = lp.OrganizationID
where o.ParentOrganizationID = 32
	and lp.PatronCodeID in (1,11,15)
	and lp.LoanPeriodCodeID = 40

--rollback
--commit