begin tran

update mll
set mll.MaxRequestItems = 400
from polaris.polaris.MaterialLoanLimits mll
join Polaris.Polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
where o.ParentOrganizationID = 78 and mll.PatronCodeID = 4 and mll.MaxRequestItems = 200

--rollback
--commit