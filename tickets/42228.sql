begin tran

update mll
set mll.MaxRequestItems = 0
from polaris.Polaris.MaterialLoanLimits mll
join Polaris.polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
where mll.PatronCodeID = 21
	and mll.MaterialTypeID = 52
	and o.ParentOrganizationID = 28

--rollback
--commit