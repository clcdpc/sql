begin tran

-- change the loan periods for Business/Community Card and Outreach to match our current Institutions (mostly 56 days) when logged in as CML Outreach only
update lp_dest
set lp_dest.Units = lp_source.Units
from Polaris.Polaris.LoanPeriods lp_dest
join Polaris.Polaris.LoanPeriods lp_source
	on lp_source.OrganizationID = lp_dest.OrganizationID and lp_source.LoanPeriodCodeID = lp_dest.LoanPeriodCodeID
where lp_dest.PatronCodeID in (18,23) and lp_dest.OrganizationID = 66 and lp_source.PatronCodeID = 5

-- change the loan periods for Homebound by Mail to match our current Homebound (mostly 65 days) when logged in as CML Outreach only
update lp_dest
set lp_dest.Units = lp_source.Units
from Polaris.Polaris.LoanPeriods lp_dest
join Polaris.Polaris.LoanPeriods lp_source
	on lp_source.OrganizationID = lp_dest.OrganizationID and lp_source.LoanPeriodCodeID = lp_dest.LoanPeriodCodeID
where lp_dest.PatronCodeID in (19) and lp_dest.OrganizationID = 66 and lp_source.PatronCodeID = 2

-- change the loan periods for School Delivery to 56 days for Book, Juvenile Book, New Book, and Paperback when logged in as CML School Delivery only
update lp
set lp.Units = 56
from polaris.Polaris.LoanPeriods lp
where lp.OrganizationID = 88 and lp.LoanPeriodCodeID in (8,15,22,24)

-- Change the checkout limits for Homebound by Mail and Business/Community Card to be the same as Homebound when the transacting library is CML Outreach only
update mll_dest
set mll_dest.MaxItems = mll_source.MaxItems
from polaris.Polaris.MaterialLoanLimits mll_dest
join Polaris.Polaris.MaterialLoanLimits mll_source
	on mll_source.OrganizationID = mll_dest.OrganizationID and mll_source.MaterialTypeID = mll_dest.MaterialTypeID
where mll_dest.OrganizationID = 66 and mll_dest.PatronCodeID in (19,18) and mll_source.PatronCodeID = 2

-- Change the checkout limits for School Delivery (patron code) for Inn-Reach Books and Audio and Inn-Reach Video to 0 for all CML locations.
update mll
set mll.MaxItems = 0
from polaris.Polaris.MaterialLoanLimits mll
join Polaris.Polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
where o.ParentOrganizationID = 39 and mll.PatronCodeID = 24 and mll.MaterialTypeID in (47,55)

--rollback
--commit