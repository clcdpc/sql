-- number of patrons placing holds through P-LKS and the number of items being held.

--select * from PolarisTransactions.polaris.TransactionTypes tt where tt.TransactionTypeDescription like '%hold%'

declare @startDate date = '7/1/2023'
declare @endDate date = '8/1/2023'
declare @branch int = 95

/* holds placed */
create table #holdsplaced ( TransactionID int, PatronID int, BibliographicRecordID int )

insert into #holdsplaced
select th.TransactionID, null, null
from PolarisTransactions.polaris.TransactionHeaders th
where th.TranClientDate between @startDate and @endDate
	and th.TransactionTypeID = 6005

update th
set th.PatronID = td_patron.numValue
	,th.BibliographicRecordID = td_bib.numValue
from #holdsplaced th
join PolarisTransactions.polaris.TransactionDetails td_branch
	on td_branch.TransactionID = th.TransactionID and td_branch.TransactionSubTypeID = 130
join PolarisTransactions.polaris.TransactionDetails td_patron
	on td_patron.TransactionID = th.TransactionID and td_patron.TransactionSubTypeID = 6
join PolarisTransactions.polaris.TransactionDetails td_bib
	on td_bib.TransactionID = th.TransactionID and td_bib.TransactionSubTypeID = 36
where td_branch.numValue = @branch
/* end holds placed */

/* holds ckoed */
create table #ckos ( TransactionID int, PatronID int, ItemRecordID int )

insert into #ckos
select th.TransactionID, null, null
from PolarisTransactions.polaris.TransactionHeaders th
where th.TranClientDate between @startDate and @endDate
	and th.TransactionTypeID = 6001
	and th.OrganizationID = @branch

update th
set th.PatronID = td_patron.numValue
	,th.ItemRecordID = td_item.numValue
from #ckos th
join PolarisTransactions.polaris.TransactionDetails td_patron
	on td_patron.TransactionID = th.TransactionID and td_patron.TransactionSubTypeID = 6
join PolarisTransactions.polaris.TransactionDetails td_item
	on td_item.TransactionID = th.TransactionID and td_item.TransactionSubTypeID = 38
/* end holds ckoed */

/* holds unclaimed */
create table #holdsunclaimed ( TransactionID int, PatronID int, ItemRecordID int )

insert into #holdsunclaimed
select th.TransactionID, null, null
from PolarisTransactions.polaris.TransactionHeaders th
where th.TranClientDate between @startDate and @endDate
	and th.TransactionTypeID = 6005

update th
set th.PatronID = td_patron.numValue
	,th.ItemRecordID = td_item.numValue
from #holdsunclaimed th
join PolarisTransactions.polaris.TransactionDetails td_branch
	on td_branch.TransactionID = th.TransactionID and td_branch.TransactionSubTypeID = 130
join PolarisTransactions.polaris.TransactionDetails td_patron
	on td_patron.TransactionID = th.TransactionID and td_patron.TransactionSubTypeID = 6
join PolarisTransactions.polaris.TransactionDetails td_item
	on td_item.TransactionID = th.TransactionID and td_item.TransactionSubTypeID = 38
where td_branch.numValue = @branch
/* end holds unclaimed */

select  (select count(distinct patronid) from #holdsplaced) [PatronsHoldsPlaced],
		(select count(distinct BibliographicRecordID) from #holdsplaced) [TitlesHoldsPlaced],
		(select count(distinct patronid) from #ckos) [PatronsPickedUp],
		(select count(distinct ItemRecordID) from #ckos) [ItemsPickedUp],
		(select count(distinct patronid) from #holdsunclaimed) [PatronsUnclaimed],
		(select count(distinct ItemRecordID) from #holdsunclaimed) [ItemsUnclaimed]



drop table #holdsplaced
drop table #ckos
drop table #holdsunclaimed


/*
select top 1000	td.TransactionID
		,th.TranClientDate 
		,th.OrganizationID
		,th.PolarisUserID
		,pu.Name [Username]
		,th.WorkstationID
		,w.ComputerName [ComputerName]
		,th.TransactionTypeID
		,tt.TransactionTypeDescription
		,td.TransactionSubTypeID
		,tst.TransactionSubTypeDescription
		,td.numValue
		,td.dateValue
		--,cast(coalesce(cast(td.numValue as varchar), cast(td.dateValue as varchar)) as varchar(max)) [Value]
		,coalesce(tstc.TransactionSubTypeCodeDesc,'') [SubTypeValue]
from PolarisTransactions.polaris.TransactionDetails td with (nolock) 
join PolarisTransactions.polaris.TransactionSubTypes tst with (nolock) on
	td.transactionsubtypeid = tst.TransactionSubTypeID 
join PolarisTransactions.polaris.TransactionHeaders th with (nolock) on
	td.TransactionID = th.TransactionID
join PolarisTransactions.polaris.TransactionTypes tt with (nolock) on
	tt.TransactionTypeID = th.TransactionTypeID
left join Polaris.Polaris.PolarisUsers pu
	on pu.PolarisUserID = th.PolarisUserID
left join Polaris.Polaris.Workstations w
	on w.WorkstationID = th.WorkstationID
left join PolarisTransactions.polaris.TransactionSubTypeCodes tstc with (nolock) ON
	tstc.TransactionSubTypeID = td.TransactionSubTypeID and tstc.TransactionSubTypeCode = td.numValue
where th.TransactionTypeID = 6001
	and th.organizationid = 95
order by th.TransactionID desc
*/