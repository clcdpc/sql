begin tran

-- Fine code
update cir
set cir.FineCodeID = fc.FineCodeID
from polaris.polaris.ItemRecordSets irs
join polaris.Polaris.CircItemRecords cir
	on cir.ItemRecordID = irs.ItemRecordID
join polaris.polaris.MaterialTypes mt
	on mt.MaterialTypeID = cir.MaterialTypeID
join polaris.polaris.FineCodes fc
	on fc.Description = mt.Description
where irs.RecordSetID = 138201

-- Loan period code
update cir
set cir.LoanPeriodCodeID = lpc.LoanPeriodCodeID
from polaris.polaris.ItemRecordSets irs
join polaris.Polaris.CircItemRecords cir
	on cir.ItemRecordID = irs.ItemRecordID
join polaris.polaris.MaterialTypes mt
	on mt.MaterialTypeID = cir.MaterialTypeID
join polaris.polaris.LoanPeriodCodes lpc
	on lpc.Description = mt.Description
where irs.RecordSetID = 138202

--rollback
--commit