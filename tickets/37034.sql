begin tran

update mll
set MaxItems = 1, MaxRequestItems = 1
from polaris.Polaris.MaterialLoanLimits mll
join polaris.Polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
where o.ParentOrganizationID = 6 and mll.MaterialTypeID in (43,44) and mll.PatronCodeID in (7,8,9,25,26,27)

--rollback
--commit