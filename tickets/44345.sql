begin tran

update mll_dest
set mll_dest.MaxItems = mll_source.MaxItems
	,mll_dest.MaxRequestItems = mll_source.MaxRequestItems
from polaris.polaris.MaterialLoanLimits mll_dest
join Polaris.Polaris.MaterialLoanLimits mll_source
	on mll_source.MaterialTypeID = mll_dest.MaterialTypeID and mll_source.OrganizationID = mll_dest.OrganizationID
join polaris.polaris.Organizations o
	on o.OrganizationID = mll_dest.OrganizationID
where mll_dest.PatronCodeID = 11
	and mll_source.PatronCodeID = 10
	and o.ParentOrganizationID = 32

--rollback
--commit