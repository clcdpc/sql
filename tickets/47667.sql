/*
Maximum of 75 items for all patron types, except Web Only and Branch Card
Maximum of 20 DVDs for all patron types, except Web Only and Branch Card
Maximum of 20 Music CDs for all patron types, except Web Only and Branch Card
*/

begin tran

update mll2
set mll2.MaxItems = mll.MaxItems
	,mll2.MaxRequestItems = mll.MaxRequestItems
from Polaris.Polaris.MaterialLoanLimits mll
join Polaris.Polaris.MaterialLoanLimits mll2
	on mll2.OrganizationID = mll.OrganizationID and mll2.MaterialTypeID = mll.MaterialTypeID and mll2.PatronCodeID in (25,26,27,28)
where mll.OrganizationID = 115
	and mll.PatronCodeID = 1
	and mll2.MaxItems > 0
	and mll.MaxItems != mll2.MaxItems


select *
from Polaris.dbo.CLC_Custom_MaterialLoanLimits mll
join Polaris.dbo.CLC_Custom_MaterialLoanLimits mll2
	on mll2.OrganizationID = mll.OrganizationID and mll2.MaterialTypeID = mll.MaterialTypeID and mll2.PatronCodeID in (25,26,27,28)
where mll.OrganizationID = 115
	and mll.PatronCodeID = 1
	and mll2.MaxItems > 0
	and mll.MaxItems != mll2.MaxItems

--rollback
--commit