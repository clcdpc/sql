-- update the "Videogame E" (29) material type for all Delaware (105) branches/patron codes?  
-- For any that don't have a current checkout limit of 0, can you update the loan and hold limits to 3 checkouts/6 holds

begin tran

update mll
set mll.MaxItems = 3
	,mll.MaxRequestItems = 6
from Polaris.Polaris.MaterialLoanLimits mll
join Polaris.Polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
where mll.MaterialTypeID = 29 
	and o.ParentOrganizationID = 105
	and mll.MaxItems > 0

--rollback
--commit