/*
Business/Community Card
Full Access
Homebound by Mail
ILL
Key Customers
Staff
Teachers/Educators
Video up to G/VG up to E
Video up to PG-13/VG up to T
Video up to PG/VG up to E
Video/VG Restricted
*/

begin tran

update pll
set pll.TotalHolds = 112
from Polaris.Polaris.PatronLoanLimits pll 
join Polaris.polaris.PatronCodes pc
	on pc.PatronCodeID = pll.PatronCodeID
join Polaris.polaris.Organizations o
	on o.OrganizationID = pll.OrganizationID
where o.ParentOrganizationID = 28
	and pc.Description in (
		'Business/Community Card',
		'Full Access',
		'Homebound by Mail',
		'ILL',
		'Key Customers',
		'Staff',
		'Teachers/Educators',
		'Video up to G/VG up to E',
		'Video up to PG-13/VG up to T',
		'Video up to PG/VG up to E',
		'Video/VG Restricted'
	)

--rollback
--commit