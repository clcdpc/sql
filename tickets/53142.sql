begin tran

update pscc
set Description = 'z ' + pscc.Description + ' (archive)'
from polaris.polaris.PatronStatClassCodes pscc
join polaris.polaris.organizations o
	on o.OrganizationID = pscc.OrganizationID
where o.ParentOrganizationID = 39
	and (pscc.Description like '%nationwide%' or pscc.Description like '%spark%' or pscc.Description like '%ready to read%')
	and pscc.Description not like '%archive%'

insert into polaris.polaris.PatronStatClassCodes
select o.OrganizationID, 'SQO'
from polaris.polaris.Organizations o
where o.ParentOrganizationID = 39
	and not exists ( select 1 from polaris.polaris.PatronStatClassCodes pscc where pscc.OrganizationID = o.OrganizationID and pscc.Description = 'SQO' )

--rollback
--commit