select *
from PolarisTransactions.dbo.clc_custom_viewtransactiondetails vtd
where vtd.TranClientDate > '2/1/2022'
	and vtd.TransactionSubTypeID = 38
	and vtd.numValue = 24464997


select *
from PolarisTransactions.dbo.clc_custom_viewtransactiondetails vtd
where vtd.TransactionID in (
	select th.TransactionID
	from PolarisTransactions.Polaris.TransactionHeaders th
	join PolarisTransactions.Polaris.TransactionDetails td
		on td.TransactionID = th.TransactionID and td.TransactionSubTypeID = 38
	where th.TranClientDate > '2/1/2022'
		and td.numValue = 16935920
)
order by vtd.TranClientDate desc, vtd.TransactionSubTypeID

select * from Polaris.polaris.CircItemRecords where Barcode = '1338202189'