--update any patron code that allows 35 total holds to allow 75 total holds, and any material type for each patron code that allows 35 holds, to allow 75 per material type.

begin tran

update pll
set pll.TotalHolds = 75
from polaris.Polaris.PatronLoanLimits pll
join polaris.polaris.Organizations o
	on o.OrganizationID = pll.OrganizationID
where o.ParentOrganizationID = 8
	and pll.TotalHolds = 35

update mll
set mll.MaxRequestItems = 75
from polaris.polaris.MaterialLoanLimits mll
join polaris.polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
where o.ParentOrganizationID = 8
	and mll.MaxRequestItems = 35

--rollback
--commit