-- =CONCATENATE("insert into @newpll values (", A2, ", ", C2, ", ", E2, ", ", F2, ", ", G2, ", ", H2, ", ", I2, ", ", J2, ")")

declare @newpll table (orgid int, pcid int, maxfine decimal(18,2), minfine decimal(18,2), totalitems int, totaloverdue int, totalholds int, totalill int)

-- PASTE EXCEL DATA BELOW
insert into @newpll values (8, 17, 999.99, 9.99, 100, 4, 75, 0)
insert into @newpll values (8, 18, 999.99, 9.99, 100, 4, 75, 0)
insert into @newpll values (8, 1, 999.99, 9.99, 100, 4, 75, 0)
insert into @newpll values (8, 19, 999.99, 9.99, 0, 0, 0, 0)
insert into @newpll values (8, 3, 999.99, 9.99, 100, 4, 75, 0)
insert into @newpll values (8, 6, 999.99, 9.99, 100, 4, 75, 0)
insert into @newpll values (8, 21, 999.99, 9.99, 3, 3, 3, 0)
insert into @newpll values (8, 22, 999.99, 9.99, 3, 4, 3, 0)
insert into @newpll values (8, 10, 999.99, 9.99, 100, 4, 75, 0)
insert into @newpll values (8, 11, 999.99, 9.99, 100, 4, 75, 0)
insert into @newpll values (8, 23, 999.99, 9.99, 100, 4, 75, 0)
insert into @newpll values (8, 24, 999.99, 9.99, 100, 4, 75, 0)
insert into @newpll values (8, 12, 999.99, 9.99, 0, 4, 2, 0)
insert into @newpll values (8, 13, 999.99, 9.99, 100, 4, 75, 0)
insert into @newpll values (8, 14, 999.99, 9.99, 100, 4, 75, 10)
insert into @newpll values (8, 15, 999.99, 9.99, 100, 4, 75, 0)
insert into @newpll values (8, 25, 999.99, 9.99, 100, 4, 75, 0)
insert into @newpll values (8, 27, 999.99, 9.99, 100, 4, 75, 0)
insert into @newpll values (8, 26, 999.99, 9.99, 100, 4, 75, 0)
insert into @newpll values (8, 28, 999.99, 9.99, 100, 4, 75, 0)
insert into @newpll values (8, 16, 999.99, 9.99, 0, 0, 0, 0)
-- PASTE EXCEL DATA ABOVE

begin tran

update pll
set pll.MaxFine = n.MaxFine
	,pll.MinFine = n.MinFine
	,pll.TotalItems = n.TotalItems
	,pll.TotalOverDue = n.TotalOverDue
	,pll.TotalHolds = n.TotalHolds
	,pll.TotalILL = n.totalill
from Polaris.Polaris.PatronLoanLimits pll
join polaris.polaris.organizations o
	on o.OrganizationID = pll.OrganizationID
join @newpll n
	on n.OrgID = o.ParentOrganizationID and n.pcid = pll.PatronCodeID

--rollback
--commit

