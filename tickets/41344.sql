begin tran

update mll_dest
set mll_dest.MaxItems = mll_source.MaxItems
	,mll_dest.MaxRequestItems = mll_source.MaxRequestItems
from Polaris.Polaris.MaterialLoanLimits mll_dest
join polaris.polaris.Organizations o
	on o.OrganizationID = mll_dest.OrganizationID
join polaris.Polaris.MaterialLoanLimits mll_source
	on mll_source.PatronCodeID = mll_dest.PatronCodeID and mll_source.MaterialTypeID = mll_dest.MaterialTypeID
where o.ParentOrganizationID = 14 
	and mll_dest.OrganizationID != 15 
	and mll_dest.MaterialTypeID = 39 
	and mll_source.OrganizationID = 15

--rollback
--commit