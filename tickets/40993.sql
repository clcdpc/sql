begin tran

update f_dest
set f_dest.MaximumFine = f_source.MaximumFine,
	f_dest.Amount = f_source.Amount
from polaris.polaris.Fines f_dest
join polaris.polaris.Organizations o
	on o.OrganizationID = f_dest.OrganizationID
join Polaris.polaris.Fines f_source
	on f_source.OrganizationID = f_dest.OrganizationID and f_source.FineCodeID = f_dest.FineCodeID and f_source.PatronCodeID = 1
where o.ParentOrganizationID = 8 and f_dest.PatronCodeID in (25,26,27,28) and (f_dest.Amount != f_source.Amount or f_dest.MaximumFine != f_source.MaximumFine)

--rollback
--commit