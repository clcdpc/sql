--change the checkout limit for patron code Outreach (23) to 10 for material type Video PG (3) for all CML locations. 
--The loan period for that patron code and material type combination should be 7 for all CML locations except for CML Outreach (66) it should be 56 days.

begin tran

update mll
set mll.MaxItems = 10, mll.MaxRequestItems = 5
from Polaris.polaris.MaterialLoanLimits mll
join Polaris.polaris.organizations o
	on o.OrganizationID = mll.OrganizationID
where o.ParentOrganizationID = 39 and mll.PatronCodeID = 23 and mll.MaterialTypeID = 3

update lp
set lp.Units = 7
from polaris.polaris.LoanPeriods lp
join polaris.polaris.Organizations o
	on o.OrganizationID = lp.OrganizationID
where o.ParentOrganizationID = 39 and lp.OrganizationID != 66 and lp.PatronCodeID = 23 and lp.LoanPeriodCodeID = 3

--rollback
--commit