select d.BibliographicRecordID
from (
	select top 10 br.BibliographicRecordID
	from polaris.polaris.BibliographicRecords br
	where exists ( 
		select 1 
		from Polaris.Polaris.CircItemRecords cir 
		where cir.AssociatedBibRecordID = br.BibliographicRecordID 
			and cir.AssignedBranchID = 83
			and cir.MaterialTypeID in (34,35) 
			and cir.FirstAvailableDate >= dateadd(day, -30, cast(getdate() as date)) 
		)
	order by newid()
) d