begin tran

update lp
set lp.Units = 14
from Polaris.polaris.LoanPeriods lp
join polaris.Polaris.LoanPeriodCodes lpc
	on lpc.LoanPeriodCodeID = lp.LoanPeriodCodeID
join polaris.polaris.PatronCodes pc
	on pc.PatronCodeID = lp.PatronCodeID
join polaris.Polaris.Organizations o
	on o.OrganizationID = lp.OrganizationID
where o.ParentOrganizationID = 105
	and lp.LoanPeriodCodeID = 40
	and lp.Units = 28
	and lpc.Description not like 'zzz%'
	and pc.Description not like 'zzz%'

--rollback
--commit