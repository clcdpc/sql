begin tran

update pll
set pll.TotalItems = 15
from polaris.polaris.PatronLoanLimits pll
join polaris.polaris.organizations o
	on o.OrganizationID = pll.OrganizationID
where o.ParentOrganizationID = 39 and pll.PatronCodeID = 21

--rollback
--commit