--Copy the fines table settings for patron code 16 (Web Only) from Fairfield Main (ID 9) to the rest of the FCD branches, IDs 10,11,12,13,27

begin tran

update f_dest
set	 f_dest.Amount = f_source.Amount
	,f_dest.MaximumFine = f_source.MaximumFine
	,f_dest.GraceUnits = f_source.GraceUnits
from polaris.polaris.Fines f_dest
join polaris.polaris.Fines f_source
	on f_source.PatronCodeID = f_dest.PatronCodeID and f_source.FineCodeID = f_dest.FineCodeID
where f_dest.OrganizationID in (10,11,12,13,27) and f_dest.PatronCodeID = 16 and f_source.OrganizationID = 9 and f_source.PatronCodeID = 16

--rollback
--commit