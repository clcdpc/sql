begin tran

update mll
set MaxItems = 2, MaxRequestItems = 1
from polaris.Polaris.MaterialLoanLimits mll
join polaris.polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
where o.ParentOrganizationID = 16 and mll.MaterialTypeID = 43

--rollback
--commit