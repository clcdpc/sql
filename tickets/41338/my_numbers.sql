declare @library int = 78
declare @dtbegindate date = '1/1/2018'
declare @dtenddate date = '1/1/2019'
declare @includeEmaterialInCirc bit = 1
declare @includeInnreachInCircs bit = 1


select
		COUNT(distinct td_item.numvalue) as Items, 
		(COUNT(distinct th.TransactionID) - COUNT(distinct td_renewal.transactionid)) as Checkouts, 
		COUNT(distinct td_renewal.transactionid) as Renewals,
		COUNT(distinct th.TransactionID) as TotalCheckoutsAndRenewals
from PolarisTransactions.polaris.TransactionHeaders th with (nolock) 
join PolarisTransactions.polaris.TransactionDetails td_coll with (nolock)	
	on th.TransactionID = td_coll.TransactionID and td_coll.TransactionSubTypeID = 61
join PolarisTransactions.polaris.TransactionDetails td_mt with (nolock)	
	on th.TransactionID = td_mt.TransactionID and td_mt.TransactionSubTypeID = 4
join PolarisTransactions.polaris.TransactionDetails td_item with (nolock) 
	on th.TransactionID = td_item.TransactionID and td_item.TransactionSubTypeID = 38
join PolarisTransactions.Polaris.TransactionDetails td_pc with (nolock)
	on td_pc.TransactionID = th.TransactionID and td_pc.TransactionSubTypeID = 7
left join PolarisTransactions.polaris.TransactionDetails td_renewal with (nolock) 
	on th.TransactionID = td_renewal.TransactionID and td_renewal.TransactionSubTypeID = 124
join polaris.polaris.Organizations o with (nolock) 
	on th.OrganizationID = o.OrganizationID
join Polaris.Polaris.MaterialTypes mt with (nolock) on
	td_mt.numValue = mt.MaterialTypeID
join Polaris.dbo.CLC_Custom_Material_Type_Groups mtg with (nolock) on
	mt.MaterialTypeID = mtg.MaterialTypeID
left join Polaris.Polaris.Collections c with (nolock) on 
	c.CollectionID = td_coll.numValue
where th.TransactionTypeID = 6001
and o.ParentOrganizationID = @library
and ( th.TranClientDate between @dtBeginDate and @dtEndDate )
and (@includeInnreachInCircs = 1 or td_pc.numValue != 20 )
and (@includeEmaterialInCirc = 1 or td_mt.numValue not in (34,35,36,37))




select data.OrganizationID, data.Name, data.MaterialType, data.Age, data.Items, data.Checkouts, data.Renewals, data.TotalCheckoutsAndRenewals 
from
(
select	th.OrganizationID,
		o.name,
		case 
			when c.Name like '%Juvenile%' or c.name like 'Launchpad' or c.Name like 'YA %' or c.name like 'zYA %' or c.name like 'z YA %' or c.name like 'L YA%' or c.name like 'Picture Book%' or c.name like 'zPicture Book%' or c.name like 'z Picture Book%' or c.name like 'Discovery Theme Bag' or c.name like 'Day Care Kit' or c.Name like 'kits' or c.Name like '%children%' or c.Name like '%teen%' then 'Childrens' 
			else 'Adult' 
		end [Age],
		case when td_pc.numValue = 20 then 'InnReach Sent Items' else mtg.MaterialGroup end [MaterialType],
		COUNT(distinct td_item.numvalue) as Items, 
		(COUNT(distinct th.TransactionID) - COUNT(distinct td_renewal.transactionid)) as Checkouts, 
		COUNT(distinct td_renewal.transactionid) as Renewals,
		COUNT(distinct th.TransactionID) as TotalCheckoutsAndRenewals
from PolarisTransactions.polaris.TransactionHeaders th with (nolock) 
join PolarisTransactions.polaris.TransactionDetails td_coll with (nolock)	
	on th.TransactionID = td_coll.TransactionID and td_coll.TransactionSubTypeID = 61
join PolarisTransactions.polaris.TransactionDetails td_mt with (nolock)	
	on th.TransactionID = td_mt.TransactionID and td_mt.TransactionSubTypeID = 4
join PolarisTransactions.polaris.TransactionDetails td_item with (nolock) 
	on th.TransactionID = td_item.TransactionID and td_item.TransactionSubTypeID = 38
join PolarisTransactions.Polaris.TransactionDetails td_pc with (nolock)
	on td_pc.TransactionID = th.TransactionID and td_pc.TransactionSubTypeID = 7
left join PolarisTransactions.polaris.TransactionDetails td_renewal with (nolock) 
	on th.TransactionID = td_renewal.TransactionID and td_renewal.TransactionSubTypeID = 124
join polaris.polaris.Organizations o with (nolock) 
	on th.OrganizationID = o.OrganizationID
join Polaris.Polaris.MaterialTypes mt with (nolock) on
	td_mt.numValue = mt.MaterialTypeID
join Polaris.dbo.CLC_Custom_Material_Type_Groups mtg with (nolock) on
	mt.MaterialTypeID = mtg.MaterialTypeID
left join Polaris.Polaris.Collections c with (nolock) on 
	c.CollectionID = td_coll.numValue
where th.TransactionTypeID = 6001
and o.ParentOrganizationID = @library
and ( th.TranClientDate between @dtBeginDate and @dtEndDate )
and (@includeInnreachInCircs = 1 or td_pc.numValue != 20 )
and (@includeEmaterialInCirc = 1 or td_mt.numValue not in (34,35,36,37))
group by case 
			when c.Name like '%Juvenile%' or c.name like 'Launchpad' or c.Name like 'YA %' or c.name like 'zYA %' or c.name like 'z YA %' or c.name like 'L YA%' or c.name like 'Picture Book%' or c.name like 'zPicture Book%' or c.name like 'z Picture Book%' or c.name like 'Discovery Theme Bag' or c.name like 'Day Care Kit' or c.Name like 'kits' or c.Name like '%children%' or c.Name like '%teen%' then 'Childrens' 
			else 'Adult' 
		end, th.OrganizationID,o.name, case when td_pc.numValue = 20 then 'InnReach Sent Items' else mtg.MaterialGroup end
)
data
order by organizationid, data.MaterialType, [Age]