-- Games and Toys - Homebound - 14
-- Games and Toys - Restricted 1 - 14
-- Games and Toys - Restricted 2 - 14
-- Games and Toys - Restricted 3 - 14

-- Juvenile Music CD/Cassette - ALL BUT Branch Card - 28 days

-- Juvenile Video Un-Rated - Outreach - 4
-- Juvenile Video Un-Rated - Video up to G/VG up to E - 4
-- Juvenile Video Un-Rated - Video up to PG/VG up to E - 4
-- Juvenile Video Un-Rated - Video up to PG-13/VG up to T - 4

-- Paperback - Business/Community Card - 28
-- Paperback - Outreach - 28

-- Video G / TV-G / TV-Y / TV-Y7 / TV-Y7 FV - Business/Community Card - 4
-- Video G / TV-G / TV-Y / TV-Y7 / TV-Y7 FV - Newly Registered - 4
-- Video G / TV-G / TV-Y / TV-Y7 / TV-Y7 FV - Newly Registered Minor - 4
-- Video G / TV-G / TV-Y / TV-Y7 / TV-Y7 FV - Outreach - 4
-- Video G / TV-G / TV-Y / TV-Y7 / TV-Y7 FV - Video up to G/VG up to E - 4
-- Video G / TV-G / TV-Y / TV-Y7 / TV-Y7 FV - Video up to PG/VG up to E - 4
-- Video G / TV-G / TV-Y / TV-Y7 / TV-Y7 FV - Video up to PG-13/VG up to T - 4

-- Video Non-Fiction - Business/Community Card - 7
-- Video Non-Fiction - Outreach - 7
-- Video Non-Fiction -Video up to G/VG up to E - 7
-- Video Non-Fiction -Video up to PG/VG up to E - 7
-- Video Non-Fiction -Video up to PG-13/VG up to T - 7

-- Video PG - Outreach - 4
-- Video PG - Video up to G/VG up to E - 4
-- Video PG - Video up to PG/VG up to E - 4
-- Video PG - Video up to PG-13/VG up to T - 4

-- Video PG-13 / TV-14 - Business/Community Card - 4
-- Video PG-13 / TV-14 - Outreach - 4
-- Video PG-13 / TV-14 - Video up to PG-13/VG up to T - 4

-- Video R / TV-MA - Business/Community Card - 4
-- Video R / TV-MA - Outreach - 4

-- Video Un-Rated - Business/Community Card - 4
-- Video Un-Rated - Outreach - 4
-- Video Un-Rated - Video up to G/VG up to E - 4
-- Video Un-Rated - Video up to PG/VG up to E - 4
-- Video Un-Rated - Video up to PG-13/VG up to T - 4

declare @settings table ( loanperiod varchar(255), patroncode varchar(255), days int )

insert into @settings values ('Games and Toys', 'Homebound', 14)
insert into @settings values ('Games and Toys', 'Restricted 1', 14)
insert into @settings values ('Games and Toys', 'Restricted 2', 14)
insert into @settings values ('Games and Toys', 'Restricted 3', 14)

insert into @settings values ('Juvenile Music CD/Cassette','Full Access', 28)
insert into @settings values ('Juvenile Music CD/Cassette','Homebound', 28)
insert into @settings values ('Juvenile Music CD/Cassette','ILL/MORE', 28)
insert into @settings values ('Juvenile Music CD/Cassette','In House', 28)
insert into @settings values ('Juvenile Music CD/Cassette','Institutions', 28)
insert into @settings values ('Juvenile Music CD/Cassette','Key Customers', 28)
insert into @settings values ('Juvenile Music CD/Cassette','Restricted 1', 28)
insert into @settings values ('Juvenile Music CD/Cassette','Restricted 2', 28)
insert into @settings values ('Juvenile Music CD/Cassette','Restricted 3', 28)
insert into @settings values ('Juvenile Music CD/Cassette','Newly Registered', 28)
insert into @settings values ('Juvenile Music CD/Cassette','Newly Registered Minor', 28)
insert into @settings values ('Juvenile Music CD/Cassette','Self Registered', 28)
insert into @settings values ('Juvenile Music CD/Cassette','Senior', 28)
insert into @settings values ('Juvenile Music CD/Cassette','Staff', 28)
insert into @settings values ('Juvenile Music CD/Cassette','Teachers/Educators', 28)
insert into @settings values ('Juvenile Music CD/Cassette','Web Only', 28)
insert into @settings values ('Juvenile Music CD/Cassette','Business/Community Card', 28)
insert into @settings values ('Juvenile Music CD/Cassette','Homebound by Mail', 28)
insert into @settings values ('Juvenile Music CD/Cassette','Kids Card', 28)
insert into @settings values ('Juvenile Music CD/Cassette','Limited Access', 28)
insert into @settings values ('Juvenile Music CD/Cassette','Outreach', 28)
insert into @settings values ('Juvenile Music CD/Cassette','School Delivery', 28)
insert into @settings values ('Juvenile Music CD/Cassette','Video up to G/VG up to E', 28)
insert into @settings values ('Juvenile Music CD/Cassette','Video up to PG-13/VG up to T', 28)
insert into @settings values ('Juvenile Music CD/Cassette','Video up to PG/VG up to E', 28)
insert into @settings values ('Juvenile Music CD/Cassette','Video/VG Restricted', 28)

insert into @settings values ('Juvenile Video Un-Rated', 'Outreach', 4)
insert into @settings values ('Juvenile Video Un-Rated', 'Video up to G/VG up to E', 4)
insert into @settings values ('Juvenile Video Un-Rated', 'Video up to PG/VG up to E', 4)
insert into @settings values ('Juvenile Video Un-Rated', 'Video up to PG-13/VG up to T', 4)

insert into @settings values ('Paperback', 'Business/Community Card', 28)
insert into @settings values ('Paperback', 'Outreach', 28)

insert into @settings values ('Video G / TV-G / TV-Y / TV-Y7 / TV-Y7 FV', 'Business/Community Card', 4)
insert into @settings values ('Video G / TV-G / TV-Y / TV-Y7 / TV-Y7 FV', 'Newly Registered', 4)
insert into @settings values ('Video G / TV-G / TV-Y / TV-Y7 / TV-Y7 FV', 'Newly Registered Minor', 4)
insert into @settings values ('Video G / TV-G / TV-Y / TV-Y7 / TV-Y7 FV', 'Outreach', 4)
insert into @settings values ('Video G / TV-G / TV-Y / TV-Y7 / TV-Y7 FV', 'Video up to G/VG up to E', 4)
insert into @settings values ('Video G / TV-G / TV-Y / TV-Y7 / TV-Y7 FV', 'Video up to PG/VG up to E', 4)
insert into @settings values ('Video G / TV-G / TV-Y / TV-Y7 / TV-Y7 FV', 'Video up to PG-13/VG up to T', 4)

insert into @settings values ('Video Non-Fiction', 'Business/Community Card', 7)
insert into @settings values ('Video Non-Fiction', 'Outreach', 7)
insert into @settings values ('Video Non-Fiction', 'Video up to G/VG up to E', 7)
insert into @settings values ('Video Non-Fiction', 'Video up to PG/VG up to E', 7)
insert into @settings values ('Video Non-Fiction', 'Video up to PG-13/VG up to T', 7)

insert into @settings values ('Video PG', 'Outreach', 4)
insert into @settings values ('Video PG', 'Video up to G/VG up to E', 4)
insert into @settings values ('Video PG', 'Video up to PG/VG up to E', 4)
insert into @settings values ('Video PG', 'Video up to PG-13/VG up to T', 4)

insert into @settings values ('Video PG-13 / TV-14', 'Business/Community Card', 4)
insert into @settings values ('Video PG-13 / TV-14', 'Outreach', 4)
insert into @settings values ('Video PG-13 / TV-14', 'Video up to PG-13/VG up to T', 4)

insert into @settings values ('Video R / TV-MA', 'Business/Community Card', 4)
insert into @settings values ('Video R / TV-MA', 'Outreach', 4)

insert into @settings values ('Video Un-Rated', 'Business/Community Card', 4)
insert into @settings values ('Video Un-Rated', 'Outreach', 4)
insert into @settings values ('Video Un-Rated', 'Video up to G/VG up to E', 4)
insert into @settings values ('Video Un-Rated', 'Video up to PG/VG up to E', 4)
insert into @settings values ('Video Un-Rated', 'Video up to PG-13/VG up to T', 4)


declare @settings2 table ( branch varchar(255), patroncode varchar(255), materialtype varchar(255), maxitems int, maxrequestitems int )
insert into @settings2 values ('Marysville Public Library', 'Outreach', 'Video PG', 10, 75)
insert into @settings2 values ('MPL Raymond Branch', 'Outreach', 'Video PG', 10, 75)
insert into @settings2 values ('Marysville Public Library', 'Restricted 1', 'Video Non-Fiction', 0, 0)
insert into @settings2 values ('MPL Raymond Branch', 'Restricted 1', 'Video Non-Fiction', 0, 0)
insert into @settings2 values ('Marysville Public Library', 'Restricted 2', 'Video Non-Fiction', 10, 75)
insert into @settings2 values ('Marysville Public Library', 'Restricted 2', 'Video Un-Rated', 10, 75)
insert into @settings2 values ('MPL Raymond Branch', 'Restricted 2', 'Video Non-Fiction', 10, 75)
insert into @settings2 values ('Marysville Public Library', 'Self Registered', 'Video PG-13 / TV-14', 0, 75)
insert into @settings2 values ('MPL Raymond Branch', 'Self Registered', 'Video PG-13 / TV-14', 0, 75)
insert into @settings2 values ('Marysville Public Library', 'Video up to G/VG up to E', 'Video Un-Rated', 10, 75)
insert into @settings2 values ('Marysville Public Library', 'Video up to PG/VG up to E', 'Video PG', 10, 10)
insert into @settings2 values ('MPL Raymond Branch', 'Video up to PG/VG up to E', 'Video PG', 10, 75)
insert into @settings2 values ('Marysville Public Library', 'Video up to PG-13/VG up to T', 'Video PG', 10, 75)
insert into @settings2 values ('MPL Raymond Branch', 'Video up to PG-13/VG up to T', 'Video PG', 10, 75)



begin tran

update lp
set lp.Units = s.days
from polaris.polaris.LoanPeriods lp
join polaris.polaris.LoanPeriodCodes lpc
	on lpc.LoanPeriodCodeID = lp.LoanPeriodCodeID
join polaris.polaris.PatronCodes pc
	on pc.PatronCodeID = lp.PatronCodeID
join polaris.polaris.Organizations o
	on o.OrganizationID = lp.OrganizationID
join @settings s
	on s.loanperiod = lpc.Description and s.patroncode = pc.Description
where o.ParentOrganizationID = 16

update mll
set mll.MaxItems = s.maxitems
	,mll.MaxRequestItems = s.maxrequestitems
from polaris.polaris.MaterialLoanLimits mll
join Polaris.Polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
join polaris.polaris.MaterialTypes mt
	on mt.MaterialTypeID = mll.MaterialTypeID
join polaris.polaris.PatronCodes pc
	on pc.PatronCodeID = mll.PatronCodeID
join @settings2 s
	on s.branch = o.Name and s.patroncode = pc.Description and s.materialtype = mt.Description



--rollback
--commit