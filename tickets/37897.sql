begin -- insert values
	if (OBJECT_ID('tempdb..#values') is not null) 
	begin
		drop table #values
	end

	create table #values ( branch varchar(255), patroncode varchar(255), materialtype varchar(255), maxitems int, maxrequestitems int )

	BULK INSERT #values
	FROM 'c:\temp\37897_fcd_new_mll.psv'
	WITH
	(
		FIRSTROW = 2,
		FIELDTERMINATOR = '|',
		ROWTERMINATOR = '\n',
		TABLOCK,
		KEEPNULLS -- Treat empty fields as NULLs.
	)
end

begin tran

update mll
set mll.MaxItems = v.maxitems
	,mll.MaxRequestItems = v.maxrequestitems
from Polaris.Polaris.MaterialLoanLimits mll
join Polaris.Polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
join Polaris.Polaris.PatronCodes pc
	on pc.PatronCodeID = mll.PatronCodeID
join polaris.Polaris.MaterialTypes mt
	on mt.MaterialTypeID = mll.MaterialTypeID
join #values v
	on v.branch = o.Name and v.patroncode = pc.Description and v.materialtype = mt.Description
where mll.MaterialTypeID not in (47,55) and ( v.maxitems != mll.MaxItems or v.maxrequestitems != v.maxrequestitems )

--rollback
--commit
