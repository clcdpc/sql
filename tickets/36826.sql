begin tran

insert into Polaris.polaris.GroupUsers
select 186, gu.PolarisUserID
from polaris.polaris.GroupUsers gu
left join Polaris.Polaris.UsersPPPP up on
	up.PolarisUserID = gu.PolarisUserID and up.AttrID = 659 and up.Value = 'Suspended'
where gu.GroupID = 222 and up.AttrID is null and not exists ( select 1 from Polaris.Polaris.GroupUsers where GroupID = 186 and PolarisUserID = gu.PolarisUserID )

--rollback
--commit