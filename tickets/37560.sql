--Computer Software (CDROM) --> Local A/V Material | 9 to 33
--Juvenile Board Book --> Juvenile Book | 14 to 15
--Video G --> Video G / TV-G / TV-Y / TV-Y7 / TV-Y7 FV | 2 to 49
--Video PG-13 --> Video PG-13 / TV-14 | 4 to 51
--Video Non-Fiction & Other Restricted --> Video R / TV-MA | 7 to 52
--Video R --> Video R / TV-MA | 5 to 52


--126813

declare @map table (oldmtid int, newmtid int)
insert into @map values (9,33), (14,15), (2,49), (4,51), (7,52), (5,52)

declare @updated table (itemid int)

begin tran

update cir
set cir.MaterialTypeID = m.newmtid
	,cir.FineCodeID = fc.FineCodeID
	,cir.LoanPeriodCodeID = lpc.LoanPeriodCodeID
output INSERTED.ItemRecordID into @updated
from Polaris.polaris.CircItemRecords cir
join Polaris.polaris.organizations o
	on o.OrganizationID = cir.AssignedBranchID
join @map m
	on m.oldmtid = cir.MaterialTypeID
join polaris.polaris.MaterialTypes mt
	on mt.MaterialTypeID = m.newmtid
join polaris.Polaris.FineCodes fc
	on fc.Description = mt.Description
join Polaris.polaris.LoanPeriodCodes lpc
	on lpc.Description = mt.Description
where o.ParentOrganizationID = 39

update mll
set mll.MaxItems = 5
from polaris.polaris.MaterialLoanLimits mll
join polaris.polaris.organizations o
	on o.OrganizationID = mll.OrganizationID
where o.ParentOrganizationID = 39 and mll.PatronCodeID in (1,5,6,9,13,14,15,18,21,25,26,27,28) and mll.MaterialTypeID in (33)

update mll
set mll.MaxItems = 0
from polaris.Polaris.MaterialLoanLimits mll
join polaris.polaris.organizations o
	on o.OrganizationID = mll.OrganizationID
where o.ParentOrganizationID = 39 and mll.PatronCodeID in (17,4,12,16,24) and mll.MaterialTypeID in (47,55)

--rollback
--commit