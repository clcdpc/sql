begin tran

update pr
set pr.RequestPickupBranchID = null
from Polaris.Polaris.Patrons p
join Polaris.Polaris.PatronRegistration pr
	on pr.PatronID = p.PatronID
where p.OrganizationID = 7
	and pr.RequestPickupBranchID = 7

--rollback
--commit