begin tran

update rs
set rs.OrganizationOwnerID = ars.OrganizationId
from AuthorSubscription.dbo.AuthorRecordSets ars
join Polaris.polaris.RecordSets rs
	on rs.RecordSetID = ars.RecordSetID
where ars.OrganizationId != rs.OrganizationOwnerID

--rollback
--commit