declare @settings table (orgid int, pcid int, mtid int, maxitems int, maxrequests int)

insert into @settings values (14, 21, 32, 0, 0)
insert into @settings values (14, 21, 1, 2, 2)
insert into @settings values (14, 21, 8, 2, 2)
insert into @settings values (14, 21, 38, 0, 0)
insert into @settings values (14, 21, 35, 0, 0)
insert into @settings values (14, 21, 34, 0, 0)
insert into @settings values (14, 21, 36, 0, 0)
insert into @settings values (14, 21, 37, 0, 0)
insert into @settings values (14, 21, 39, 0, 0)
insert into @settings values (14, 21, 40, 0, 0)
insert into @settings values (14, 21, 41, 1, 0)
insert into @settings values (14, 21, 47, 0, 0)
insert into @settings values (14, 21, 55, 0, 0)
insert into @settings values (14, 21, 13, 2, 2)
insert into @settings values (14, 21, 15, 2, 2)
insert into @settings values (14, 21, 16, 0, 0)
insert into @settings values (14, 21, 17, 0, 0)
insert into @settings values (14, 21, 19, 2, 2)
insert into @settings values (14, 21, 18, 0, 0)
insert into @settings values (14, 21, 42, 0, 0)
insert into @settings values (14, 21, 20, 0, 0)
insert into @settings values (14, 21, 43, 1, 0)
insert into @settings values (14, 21, 44, 0, 0)
insert into @settings values (14, 21, 33, 0, 0)
insert into @settings values (14, 21, 10, 0, 0)
insert into @settings values (14, 21, 45, 0, 0)
insert into @settings values (14, 21, 21, 0, 0)
insert into @settings values (14, 21, 22, 2, 2)
insert into @settings values (14, 21, 46, 0, 0)
insert into @settings values (14, 21, 24, 2, 2)
insert into @settings values (14, 21, 25, 2, 2)
insert into @settings values (14, 21, 48, 0, 0)
insert into @settings values (14, 21, 49, 0, 0)
insert into @settings values (14, 21, 50, 0, 0)
insert into @settings values (14, 21, 3, 0, 0)
insert into @settings values (14, 21, 51, 0, 0)
insert into @settings values (14, 21, 52, 0, 0)
insert into @settings values (14, 21, 53, 0, 0)
insert into @settings values (14, 21, 29, 0, 0)
insert into @settings values (14, 21, 31, 0, 0)
insert into @settings values (14, 21, 30, 0, 0)

begin tran

update mll
set mll.MaxItems = s.maxitems
	,mll.MaxRequestItems = s.maxrequests
from Polaris.Polaris.MaterialLoanLimits mll
join polaris.polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
join @settings s
	on s.orgid = o.ParentOrganizationID and s.pcid = mll.PatronCodeID and s.mtid = mll.MaterialTypeID
where mll.PatronCodeID not in (20) and mll.MaterialTypeID not in (47,55)


--rollback
--commit