--Please change loan and hold limits for loanable equipment 1 and 2 to the following:

--5 holds and 5 loans for Full Access, Key Customers, Newly Registered, Newly Registered Minor, Business/Community card, Teachers - 1,6,10,11,18,15
--5 holds and 0 loans for Self Registered - 12
--0 holds and 0 loans for Homebound by Mail, Kids Card, Limited Access, Outreach, School Delivery, New Video up to G/, New Video up to PG/, New Video up to PG-13/, New video Restricted 19,21,22,23,24,25,26,27,28

begin tran


update mll
set mll.MaxRequestItems =		
	case 
		when mll.PatronCodeID in (1,6,10,11,18,15, 12) then 5
		when mll.PatronCodeID in (19,21,22,23,24,25,26,27,28) then 0
		else mll.MaxRequestItems
	end

,mll.MaxItems = 
	case 
		when mll.PatronCodeID in (1,6,10,11,18,15) then 5
		when mll.PatronCodeID in (12, 19,21,22,23,24,25,26,27,28) then 0
		else mll.MaxItems
	end
from polaris.polaris.MaterialLoanLimits mll
join polaris.polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
where o.ParentOrganizationID = 6 and mll.MaterialTypeID in ( 43,44 )

--rollback
--commit