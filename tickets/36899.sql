-- RESULT: does not display
update br
set MARCBibStatus = '', MARCBibType = '', MARCBibLevel = ''
from Polaris.polaris.BibliographicRecords br 
where br.BibliographicRecordID = 2959294

-- RESULT: does not display
update br
set MARCBibStatus = 'n', MARCBibType = '', MARCBibLevel = ''
from Polaris.polaris.BibliographicRecords br 
where br.BibliographicRecordID = 2959294

-- RESULT: displays
update br
set MARCBibStatus = '', MARCBibType = 'a', MARCBibLevel = ''
from Polaris.polaris.BibliographicRecords br 
where br.BibliographicRecordID = 2959294

-- RESULT: does not display
update br
set MARCBibStatus = '', MARCBibType = '', MARCBibLevel = 'm'
from Polaris.polaris.BibliographicRecords br 
where br.BibliographicRecordID = 2959294

-- RESULT: displays
update br
set MARCBibStatus = 'n', MARCBibType = 'a', MARCBibLevel = ''
from Polaris.polaris.BibliographicRecords br 
where br.BibliographicRecordID = 2959294

-- RESULT: does not display
update br
set MARCBibStatus = 'n', MARCBibType = '', MARCBibLevel = 'm'
from Polaris.polaris.BibliographicRecords br 
where br.BibliographicRecordID = 2959294

-- RESULT: displays
update br
set MARCBibStatus = '', MARCBibType = 'a', MARCBibLevel = 'm'
from Polaris.polaris.BibliographicRecords br 
where br.BibliographicRecordID = 2959294

-- RESULT: displays
update br
set MARCBibStatus = 'n', MARCBibType = 'a', MARCBibLevel = 'm'
from Polaris.polaris.BibliographicRecords br 
where br.BibliographicRecordID = 2959294