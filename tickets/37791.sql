begin -- insert values
	if (OBJECT_ID('tempdb..#values') is not null) 
	begin
		drop table #values
	end

	create table #values ( libraryid int, materialtypeid int, materialtypedesc varchar(255), patroncodeid int, patroncodedesc varchar(255), ckolimit int, holdlimit int, finecodeid int, finerate decimal(18,2), maxfine decimal(18,2), graceunits int, loanperiodid int, loanperiod int )

	BULK INSERT #values
	FROM 'c:\temp\new_material_type_material_loan_limits.csv'
	WITH
	(
		FIRSTROW = 2,
		FIELDTERMINATOR = ',',
		ROWTERMINATOR = '\n',
		TABLOCK,
		KEEPNULLS -- Treat empty fields as NULLs.
	)
end

begin tran

update lp
set lp.Units = v.loanperiod
from Polaris.polaris.LoanPeriods lp
join polaris.polaris.Organizations o
	on o.OrganizationID = lp.OrganizationID
join #values v
	on v.libraryid = o.ParentOrganizationID and v.loanperiodid = lp.LoanPeriodCodeID and v.patroncodeid = lp.PatronCodeID
where o.ParentOrganizationID = 19 and lp.LoanPeriodCodeID > 34

--rollback
--commit