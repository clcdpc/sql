declare @dummy int;


begin -- insert values
	if (OBJECT_ID('tempdb..#patrons') is not null) 
	begin
		drop table #patrons
	end

	create table #patrons ( patronid int )

	BULK INSERT #patrons
	FROM 'c:\temp\cml_collections_patrons.csv'
	WITH
	(
		FIRSTROW = 2,
		FIELDTERMINATOR = ',',
		ROWTERMINATOR = '\n',
		TABLOCK,
		KEEPNULLS -- Treat empty fields as NULLs.
	)
end

declare patronCursor cursor for
select distinct p.PatronID, pa.TxnID
from Polaris.polaris.PatronAccount pa 
join polaris.Polaris.Patrons p
	on p.PatronID = pa.PatronID
join polaris.polaris.Organizations o
	on o.OrganizationID = p.OrganizationID
where pa.FeeReasonCodeID = -2 and pa.PatronID in ( select * from #patrons ) and pa.TxnCodeID = 1 and pa.TxnDate > '5/1/2018'

declare @patronid int
declare @txnid int

begin tran

open patronCursor
fetch next from patronCursor into @patronid, @txnid
while @@FETCH_STATUS = 0
begin
	
	exec Polaris.Polaris.Circ_WritePatronAccountTxnEx @patronid	-- patron id
													 ,@txnid	-- transaction id
													 ,NULL		-- fee reason
													 ,6			-- txn code
													 ,12.0000	-- amount
													 ,NULL		-- item record
													 ,NULL		-- txn date
													 ,NULL		-- due date
													 ,NULL		-- payment method
													 ,5			-- branch
													 ,2915		-- workstation
													 ,425		-- user
													 ,NULL		-- note
													 ,NULL		-- @nILSStoreTransactionID
													 ,0			-- manual charge
													 ,null		-- new txn id (out)

	fetch next from patronCursor into @patronid, @txnid
end

close patronCursor
deallocate patronCursor

--rollback
--commit