begin tran

declare @excluded_libraries table (orgid int)
insert into @excluded_libraries values (2),(8),(86),(114)

declare @groups table (groupid int)

insert into @groups values (315),(316),(317),(318),(319)


declare @current int = 0

while ((select COUNT(*) from @groups) > 0)
begin
	set @current = (select MIN(groupid) from @groups)

	;with pg
	as
	(
		select cr.ControlRecordID, pd.PermissionID, pss.Description [Subsystem], crd.ControlRecordName, pn.PermissionName, cr.OrganizationID, crd.SystemFlag, crd.LibraryFlag, crd.BranchFlag
		from Polaris.Polaris.ControlRecords cr
		join Polaris.Polaris.ControlRecordDefs crd on
			cr.ControlRecordDefID = crd.ControlRecordDefID
		join Polaris.Polaris.PermissionDefs pd on
			pd.ControlRecordDefID = crd.ControlRecordDefID
		join Polaris.Polaris.PermissionNames pn on
			pd.PermissionNameID = pn.PermissionNameID
		join Polaris.Polaris.SA_PermissionSubsystems pss on
			pss.SubsystemID = crd.SubSystemID
		join Polaris.Polaris.PermissionGroups pg on
			pg.permissionid = pd.permissionid and pg.controlrecordid = cr.controlrecordid
		where pg.groupid = @current and crd.subsystemid = 2
	)

	insert into Polaris.Polaris.PermissionGroups
	select cr.ControlRecordID, pd.PermissionID, @current
	from Polaris.Polaris.ControlRecords cr
	join Polaris.Polaris.ControlRecordDefs crd on
		cr.ControlRecordDefID = crd.ControlRecordDefID
	join Polaris.Polaris.PermissionDefs pd on
		pd.ControlRecordDefID = crd.ControlRecordDefID
	join Polaris.Polaris.PermissionNames pn on
		pd.PermissionNameID = pn.PermissionNameID
	join Polaris.Polaris.SA_PermissionSubsystems pss on
		pss.SubsystemID = crd.SubSystemID
	join Polaris.Polaris.Organizations o on
		o.OrganizationID = cr.OrganizationID
	where crd.ControlRecordName not like '%Patron record sets%' 
	and o.ParentOrganizationID not in (select * from @excluded_libraries) and pd.permissionid in (select permissionid from pg) 
	and not exists (select * from Polaris.Polaris.PermissionGroups pg2 where pg2.ControlRecordID = cr.ControlRecordID and pg2.PermissionID = pd.PermissionID and pg2.GroupID = @current)
	order by cr.ControlRecordID, pd.PermissionID

	delete from @groups where groupid = @current
end



select g.GroupID, g.GroupName, crd.ControlRecordName [ControlRecord], pn.PermissionName, o.Name, o.OrganizationCodeID
from polaris.polaris.PermissionGroups pg 
join Polaris.Polaris.Groups g
	on g.GroupID = pg.GroupID
join polaris.Polaris.ControlRecords cr
	on cr.ControlRecordID = pg.ControlRecordID
join polaris.polaris.ControlRecordDefs crd
	on crd.ControlRecordDefID = cr.ControlRecordDefID
join Polaris.Polaris.PermissionDefs pd
	on pd.PermissionID = pg.PermissionID
join Polaris.polaris.PermissionNames pn
	on pn.PermissionNameID = pd.PermissionNameID
join polaris.Polaris.Organizations o
	on o.OrganizationID = cr.OrganizationID
where pg.GroupID in (315,316,317,318,319)
order by g.GroupName, crd.ControlRecordName, pn.PermissionName




--rollback
--commit

--select * from Polaris.Polaris.PermissionGroups pg where pg.GroupID = @groupid order by pg.ControlRecordID, pg.PermissionID

