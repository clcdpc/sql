select distinct
	 nq.PatronID
	,'2'
	,cir.ItemRecordID
	,ISNULL(br.BrowseTitle, '') + ISNULL('  ' + i.Designation, '') [BrowseTitle]
	,IsNull(shr.LastStatusTransitionDate, ir.LastStatusTransitionDate)
	,cir.Barcode
	,o.OrganizationID [PickupOrganizationID]
	,case when ir.ILLRequestID is not null then 'Y' else 'N' end [illInd]
from Results.Polaris.NotificationQueue nq
join Polaris.CircItemRecords cir
	on nq.ItemRecordID = cir.ItemRecordID
join Polaris.polaris.BibliographicRecords br
	on br.BibliographicRecordID = cir.AssociatedBibRecordID
left join Polaris.MfhdIssues i
	on cir.ItemRecordID = i.ItemRecordID
left join Polaris.SysHoldRequests shr
	on nq.ItemRecordID = shr.TrappingItemRecordID
left join Polaris.ILLRequests ir
	on nq.ItemRecordID = ir.ItemRecordID
join Polaris.polaris.Organizations o
	on o.OrganizationID = coalesce(shr.NewPickupBranchID, shr.PickupBranchID, ir.PickupBranchID)
where nq.DeliveryOptionID = 1
	and nq.NotificationTypeID = 2
	and o.ParentOrganizationID = 39