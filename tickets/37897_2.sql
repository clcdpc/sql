begin tran

update lp
set lp.Units = 7
from polaris.polaris.LoanPeriods lp
join polaris.Polaris.Organizations o
	on o.OrganizationID = lp.OrganizationID
where o.ParentOrganizationID = 8 and lp.PatronCodeID in (25,26,27,28) and lp.LoanPeriodCodeID in (46,47,48,49,50)


--rollback
--commit