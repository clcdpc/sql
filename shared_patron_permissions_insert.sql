begin tran

declare @excluded_libraries table (orgid int)
insert into @excluded_libraries values (2),(8),(86)

declare @groups table (groupid int)

--insert into @groups
--select distinct pg.GroupID
--from Polaris.Polaris.PermissionGroups pg
--join Polaris.Polaris.Groups g on
--	pg.GroupID = g.GroupID	
--where g.GroupName not like 'ALE%' and g.GroupName not like 'FCD%' and g.GroupName not like 'LPL%' and g.GroupName not like 'Scoville%' and g.GroupName not like 'PAT-%' and g.GroupID >= 25

insert into @groups
select distinct g.GroupID
from Polaris.Polaris.Groups g
join Polaris.Polaris.PermissionGroups pg
	on pg.GroupID = g.GroupID
join Polaris.Polaris.ControlRecords cr
	on cr.ControlRecordID = pg.ControlRecordID
join Polaris.Polaris.ControlRecordDefs crd
	on crd.ControlRecordDefID = cr.ControlRecordDefID
where crd.SubSystemID = 2 and g.GroupName not like 'ALE%' and g.GroupName not like 'FCD%' and g.GroupName not like 'LPL%' and g.GroupName not like 'Scoville%' and g.GroupID >= 25



declare @current int = 0

while ((select COUNT(*) from @groups) > 0)
begin
	set @current = (select MIN(groupid) from @groups)

	;with pg
	as
	(
		select cr.ControlRecordID, pd.PermissionID, pss.Description [Subsystem], crd.ControlRecordName, pn.PermissionName, cr.OrganizationID, crd.SystemFlag, crd.LibraryFlag, crd.BranchFlag
		from Polaris.Polaris.ControlRecords cr
		join Polaris.Polaris.ControlRecordDefs crd on
			cr.ControlRecordDefID = crd.ControlRecordDefID
		join Polaris.Polaris.PermissionDefs pd on
			pd.ControlRecordDefID = crd.ControlRecordDefID
		join Polaris.Polaris.PermissionNames pn on
			pd.PermissionNameID = pn.PermissionNameID
		join Polaris.Polaris.SA_PermissionSubsystems pss on
			pss.SubsystemID = crd.SubSystemID
		join Polaris.Polaris.PermissionGroups pg on
			pg.permissionid = pd.permissionid and pg.controlrecordid = cr.controlrecordid
		where pg.groupid = @current and crd.subsystemid = 2
	)

	insert into Polaris.Polaris.PermissionGroups
	select cr.ControlRecordID, pd.PermissionID, @current
	from Polaris.Polaris.ControlRecords cr
	join Polaris.Polaris.ControlRecordDefs crd on
		cr.ControlRecordDefID = crd.ControlRecordDefID
	join Polaris.Polaris.PermissionDefs pd on
		pd.ControlRecordDefID = crd.ControlRecordDefID
	join Polaris.Polaris.PermissionNames pn on
		pd.PermissionNameID = pn.PermissionNameID
	join Polaris.Polaris.SA_PermissionSubsystems pss on
		pss.SubsystemID = crd.SubSystemID
	join Polaris.Polaris.Organizations o on
		o.OrganizationID = cr.OrganizationID
	where crd.ControlRecordName not like '%Patron record sets%' 
	and o.ParentOrganizationID not in (select * from @excluded_libraries) and pd.permissionid in (select permissionid from pg) 
	and not exists (select * from Polaris.Polaris.PermissionGroups pg2 where pg2.ControlRecordID = cr.ControlRecordID and pg2.PermissionID = pd.PermissionID and pg2.GroupID = @current)
	order by cr.ControlRecordID, pd.PermissionID

	delete from @groups where groupid = @current
end




--rollback
--commit

--select * from Polaris.Polaris.PermissionGroups pg where pg.GroupID = @groupid order by pg.ControlRecordID, pg.PermissionID

