declare @txnBranchId int = 22
declare @beginDate date = '1/1/2024'
declare @endDate date = '12/31/2024'


select top 100 d.AuthorName
		,count(distinct d.TxnId) [Circs]
from (
	select polaris.dbo.CLC_Custom_StripNonAlphaNumericCharacters(author.AuthorName, '') [AuthorName]
			,cko.TxnId
	from polaris.dbo.CLC_Custom_First_Time_CKOs cko
	join polaris.polaris.CircItemRecords cir
		on cir.ItemRecordID = cko.ItemRecordId
	outer apply (
		select top 1 bs.Data [AuthorName] 
		from polaris.polaris.BibliographicTags bt
		join polaris.polaris.BibliographicSubfields bs
			on bs.BibliographicTagID = bt.BibliographicTagID and bs.Subfield = 'a'
		where bt.TagNumber in (100, 110)
			and bt.BibliographicRecordID = cir.AssociatedBibRecordID
		order by bt.Sequence
	) author
	where author.AuthorName is not null
		and cko.TxnOrgId = @txnBranchId
		and cko.TxnDate between @beginDate and @endDate
) d
group by d.AuthorName
order by [Circs] desc