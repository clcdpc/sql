/*
begin tran

update pr
set  pr.PhoneVoice1 = case dev.TxtPhoneNumber when 1 then substring(pn.NonBlockingStatusNotes, patindex('%7/1[23]/2023 - Phone number%', pn.NonBlockingStatusNotes) + 25, 10) else pr.PhoneVoice1 end
	,pr.Phone1CarrierID = case dev.TxtPhoneNumber when 1 then 23 else pr.Phone1CarrierID end

	,pr.PhoneVoice2 = case dev.TxtPhoneNumber when 2 then substring(pn.NonBlockingStatusNotes, patindex('%7/1[23]/2023 - Phone number%', pn.NonBlockingStatusNotes) + 25, 10) else pr.PhoneVoice2 end
	,pr.Phone2CarrierID = case dev.TxtPhoneNumber when 2 then 23 else pr.Phone2CarrierID end

	,pr.PhoneVoice3 = case dev.TxtPhoneNumber when 3 then substring(pn.NonBlockingStatusNotes, patindex('%7/1[23]/2023 - Phone number%', pn.NonBlockingStatusNotes) + 25, 10) else pr.PhoneVoice3 end
	,pr.Phone3CarrierID = case dev.TxtPhoneNumber when 3 then 23 else pr.Phone3CarrierID end

	,pr.DeliveryOptionID = dev.DeliveryOptionID
	,pr.EnableSMS = dev.EnableSMS
	,pr.TxtPhoneNumber = dev.TxtPhoneNumber
from polaris.polaris.PatronRegistration pr
join polaris.polaris.PatronNotes pn
	on pn.PatronID = pr.PatronID
join devdb.polaris.polaris.PatronRegistration dev
	on dev.PatronID = pr.PatronID
where pn.NonBlockingStatusNotes like '%7/1[23]/2023 - Phone number [0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9] is undeliverable: DR009 CarrierID: 23%'

--rollback
--commit
*/

begin tran

declare @patrons table ( PatronID int )

insert into @patrons
select pr.PatronID
from polaris.polaris.PatronRegistration pr
join polaris.polaris.PatronNotes pn
	on pn.PatronID = pr.PatronID
where pn.NonBlockingStatusNotes like '%7/1[23]/2023 - Phone number [0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9] is undeliverable: DR009 CarrierID: 23%'
	--and case pr.TxtPhoneNumber when 1 then pr.PhoneVoice1 when 2 then pr.PhoneVoice2 when 3 then pr.PhoneVoice3 end like ' %'
	and isnull(pr.PhoneVoice2, pr.PhoneVoice1) = ''
	and pr.TxtPhoneNumber is null

update pr
set  pr.PhoneVoice1 = case when pr.PhoneVoice1 = '' then substring(pn.NonBlockingStatusNotes, patindex('%7/1[23]/2023 - Phone number%', pn.NonBlockingStatusNotes) + 25, 10) else pr.PhoneVoice1 end
	,pr.Phone1CarrierID = case when pr.PhoneVoice1 = '' then 23 else pr.Phone1CarrierID end

	,pr.PhoneVoice2 = case when pr.PhoneVoice2 = '' then substring(pn.NonBlockingStatusNotes, patindex('%7/1[23]/2023 - Phone number%', pn.NonBlockingStatusNotes) + 25, 10) else pr.PhoneVoice2 end
	,pr.Phone2CarrierID = case when pr.PhoneVoice2 = '' then 23 else pr.Phone2CarrierID end

	,pr.PhoneVoice3 = case when pr.PhoneVoice3 = '' then substring(pn.NonBlockingStatusNotes, patindex('%7/1[23]/2023 - Phone number%', pn.NonBlockingStatusNotes) + 25, 10) else pr.PhoneVoice3 end
	,pr.Phone3CarrierID = case when pr.PhoneVoice3 = '' then 23 else pr.Phone3CarrierID end

	,pr.DeliveryOptionID = dev.DeliveryOptionID
	,pr.EnableSMS = dev.EnableSMS
	,pr.TxtPhoneNumber = dev.TxtPhoneNumber
from polaris.polaris.PatronRegistration pr
join polaris.polaris.PatronNotes pn
	on pn.PatronID = pr.PatronID
left join devdb.polaris.polaris.PatronRegistration dev
	on dev.PatronID = pr.PatronID
outer apply ( 
	select top 1 nh.DeliveryOptionId
	from results.polaris.NotificationHistory nh
	where nh.PatronId = pr.PatronID 
		and nh.NoticeDate > dateadd(day, -30, getdate())
	group by nh.DeliveryOptionId
	order by count(distinct nh.ItemRecordId) desc
) nh2
where pn.NonBlockingStatusNotes like '%7/1[23]/2023 - Phone number [0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9] is undeliverable: DR009 CarrierID: 23%'
	--and case pr.TxtPhoneNumber when 1 then pr.PhoneVoice1 when 2 then pr.PhoneVoice2 when 3 then pr.PhoneVoice3 end like ' %'
	and isnull(pr.PhoneVoice2, pr.PhoneVoice1) = ''
	and pr.TxtPhoneNumber is null

select pr.PatronID
		,pr.PhoneVoice1
		,pr.Phone1CarrierID
		,pr.PhoneVoice2
		,pr.Phone2CarrierID
		,pr.PhoneVoice3
		,pr.Phone3CarrierID
		,pr.DeliveryOptionID
		,pr.EnableSMS
		,pr.UpdateDate
		,pr.TxtPhoneNumber
		,pr.EmailAddress
		,nh2.DeliveryOptionId
		,substring(pn.NonBlockingStatusNotes, patindex('%7/1[23]/2023 - Phone number%', pn.NonBlockingStatusNotes) + 25, 10) [NotePhoneNumber]
from polaris.polaris.PatronRegistration pr
join polaris.polaris.PatronNotes pn
	on pn.PatronID = pr.PatronID
join polaris.polaris.PatronRegistration dev
	on dev.PatronID = pr.PatronID
outer apply ( 
	select top 1 nh.DeliveryOptionId
	from results.polaris.NotificationHistory nh
	where nh.PatronId = pr.PatronID 
		and nh.NoticeDate > dateadd(day, -30, getdate())
	group by nh.DeliveryOptionId
	order by count(distinct nh.ItemRecordId) desc
) nh2		
where pn.NonBlockingStatusNotes like '%7/1[23]/2023 - Phone number [0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9] is undeliverable: DR009 CarrierID: 23%'
	--and case pr.TxtPhoneNumber when 1 then pr.PhoneVoice1 when 2 then pr.PhoneVoice2 when 3 then pr.PhoneVoice3 end like ' %'
	and isnull(pr.PhoneVoice2, pr.PhoneVoice1) = ''
	and pr.TxtPhoneNumber is null
order by pr.PatronID


