select distinct d.EmailAddress + ',' + d.Barcode
from
(
	select distinct o.ParentOrganizationID [LibraryId], pr.EmailAddress, min(p.Barcode) [Barcode]
	from Polaris.Polaris.PatronRegistration pr
	join Polaris.Polaris.Patrons p	
		on p.PatronID = pr.PatronID
	join Polaris.Polaris.Organizations o
		on o.OrganizationID = p.OrganizationID
	group by o.ParentOrganizationID, pr.EmailAddress
	union all
	select distinct o.ParentOrganizationID [LibraryId], pr.AltEmailAddress, min(p.Barcode) [Barcode]
	from Polaris.Polaris.PatronRegistration pr
	join Polaris.Polaris.Patrons p	
		on p.PatronID = pr.PatronID
	join Polaris.Polaris.Organizations o
		on o.OrganizationID = p.OrganizationID
	group by o.ParentOrganizationID, pr.AltEmailAddress
) d
where d.EmailAddress like '%@%.%'
	and d.LibraryId = 32