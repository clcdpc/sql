select nq.NotificationQueueID
from Results.Polaris.NotificationQueue nq 
join polaris.polaris.Patrons p
	on p.PatronID = nq.PatronID
join Polaris.polaris.PatronRegistration pr
	on pr.PatronID = p.PatronID
join Polaris.polaris.Organizations o
	on o.OrganizationID = p.OrganizationID
where nq.CreationDate < dateadd(day, -1, getdate())
	and exists ( select 1 from PolarisTransactions.Polaris.NotificationLog nl where nl.PatronID = nq.PatronID and nl.NotificationDateTime > nq.CreationDate and nl.DeliveryOptionID = nq.DeliveryOptionID )
	and nq.NotificationTypeID != 3
	and nq.DeliveryOptionID != 1
	and (o.ParentOrganizationID not in (32,39) or pr.DeliveryOptionID not in (3,4,5))