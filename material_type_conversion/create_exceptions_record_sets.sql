begin tran

declare @libraryid int

declare libCursor cursor for
select distinct m.LibraryID
from Polaris.dbo.CLC_Custom_Old_To_New_Material_Type_Mapping m
where m.OldMaterialTypeID != m.NewMaterialTypeID

set nocount on

open libCursor
fetch next from libCursor into @libraryid
while @@FETCH_STATUS = 0
begin
	insert into Polaris.polaris.RecordSets (Name, ObjectTypeID, CreatorID, OrganizationOwnerID, CreationDate, Note)
	select 'Material Type Conversion Exceptions', 3, 425, @libraryid, getdate(),'Material Type Conversion Exceptions | created automatically as part of the material type cleanup project'
	
	insert into Polaris.dbo.CLC_Custom_Old_To_New_Material_Type_Mapping 
	select @libraryid, -99, -99, -99, @@identity

	fetch next from libCursor into @libraryid
end

set nocount off

insert into Polaris.Polaris.ItemRecordSets
select cir.ItemRecordID, m.RecordSetID
from polaris.polaris.CircItemRecords cir
join polaris.polaris.organizations o
	on o.OrganizationID = cir.AssignedBranchID
join Polaris.dbo.CLC_Custom_Old_To_New_Material_Type_Mapping m
	on m.LibraryID = o.ParentOrganizationID and m.OldMaterialTypeID = -99
where (o.ParentOrganizationID < 104 and o.ParentOrganizationID not in (4,16,78)) and cir.MaterialTypeID not in (34,35,36,37,47,48,55)  and not exists ( select 1 from polaris.dbo.CLC_Custom_Old_To_New_Material_Type_Mapping m where m.LibraryID = o.ParentOrganizationID and m.OldMaterialTypeID = cir.MaterialTypeID and (m.OldCollectionID = cir.AssignedCollectionID or m.OldCollectionID = -1) )


--commit
--rollback