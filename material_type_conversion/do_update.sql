declare @library int = 2

begin tran

update cir
set cir.MaterialTypeID = m.NewMaterialTypeID, cir.LoanPeriodCodeID = lpc.LoanPeriodCodeID, FineCodeID = fc.FineCodeID
from polaris.Polaris.CircItemRecords cir
join Polaris.Polaris.Organizations o
	on o.OrganizationID = cir.AssignedBranchID
join polaris.Polaris.Organizations o2
	on o2.OrganizationID = o.ParentOrganizationID
join Polaris.dbo.CLC_Custom_Old_To_New_Material_Type_Mapping m
	on m.LibraryID = o.ParentOrganizationID and m.OldMaterialTypeID = cir.MaterialTypeID and m.OldCollectionID = coalesce(cir.AssignedCollectionID, -1)
join polaris.polaris.MaterialTypes mt
	on mt.MaterialTypeID = m.NewMaterialTypeID
join Polaris.polaris.LoanPeriodCodes lpc
	on lpc.Description = mt.Description
join polaris.polaris.FineCodes fc
	on fc.Description = mt.Description
where m.OldMaterialTypeID != m.NewMaterialTypeID and o.ParentOrganizationID = @library

select cir.ItemRecordID, cir.MaterialTypeID, m.NewMaterialTypeID, cir.AssignedCollectionID, m.OldCollectionID, cir.LoanPeriodCodeID [old lp], lpc.LoanPeriodCodeID [new lp], cir.FineCodeID [old fc], fc.FineCodeID [new fc]
from polaris.Polaris.CircItemRecords cir
join Polaris.Polaris.Organizations o
	on o.OrganizationID = cir.AssignedBranchID
join polaris.Polaris.Organizations o2
	on o2.OrganizationID = o.ParentOrganizationID
join Polaris.dbo.CLC_Custom_Old_To_New_Material_Type_Mapping m
	on m.LibraryID = o.ParentOrganizationID and m.OldMaterialTypeID = cir.MaterialTypeID and m.OldCollectionID = coalesce(cir.AssignedCollectionID, -1)
join polaris.polaris.MaterialTypes mt
	on mt.MaterialTypeID = m.NewMaterialTypeID
join Polaris.polaris.LoanPeriodCodes lpc
	on lpc.Description = mt.Description
join polaris.polaris.FineCodes fc
	on fc.Description = mt.Description
where m.OldMaterialTypeID != m.NewMaterialTypeID and o.ParentOrganizationID = @library
order by cir.ItemRecordID

--rollback
--commit