-- ADD CODE TO DELETE AND REBUILD RECORD SETS

declare @rebuild_recordsets bit = 1


begin tran

if (@rebuild_recordsets = 1)
begin
	delete from Polaris.polaris.ItemRecordSets
	where RecordSetID in (
		select m.RecordSetID
		from polaris.dbo.CLC_Custom_Old_To_New_Material_Type_Mapping m
		where m.RecordSetID > 0
	)

	delete from Polaris.polaris.RecordSets
	where RecordSetID in (
		select m.RecordSetID
		from polaris.dbo.CLC_Custom_Old_To_New_Material_Type_Mapping m
		where m.RecordSetID > 0
	)

	update Polaris.dbo.CLC_Custom_Old_To_New_Material_Type_Mapping
	set RecordSetID = 0
end


declare @libraryid int

declare valueCursor cursor for
select m.LibraryID, m.OldMaterialTypeID, m.OldCollectionID, m.NewMaterialTypeID
from Polaris.dbo.CLC_Custom_Old_To_New_Material_Type_Mapping m
where m.OldMaterialTypeID != m.NewMaterialTypeID

declare @oldmtid int
declare @oldcid int
declare @newmtid int

set nocount on

open valueCursor
fetch next from valueCursor into @libraryid, @oldmtid, @oldcid, @newmtid
while @@FETCH_STATUS = 0
begin
	
	declare @oldmt varchar(255) = ( select Description from polaris.Polaris.MaterialTypes where MaterialTypeID = @oldmtid )
	declare @oldc varchar(255) = ( select Name from polaris.Polaris.Collections where CollectionID = @oldcid )
	declare @newmt varchar(255) = ( select Description from polaris.Polaris.MaterialTypes where MaterialTypeID = @newmtid )

	insert into Polaris.polaris.RecordSets (Name, ObjectTypeID, CreatorID, OrganizationOwnerID, CreationDate, Note)
	select	left(@oldmt + ' + ' + isnull(@oldc, 'None') + ' = ' + @newmt, 80) -- Name
			,3			-- Type
			,425		-- Creator
			,@libraryid	-- Owner
			,getdate()	-- Creation Date
			,@oldmt + ' + ' + isnull(@oldc, 'None') + ' = ' + @newmt + ' | created automatically as part of the material type cleanup project'
	
	update Polaris.dbo.CLC_Custom_Old_To_New_Material_Type_Mapping
	set RecordSetID = @@identity
	where LibraryID = @libraryid and OldMaterialTypeID = @oldmtid and OldCollectionID = @oldcid and NewMaterialTypeID = @newmtid and LibraryID = @libraryid
	

	fetch next from valueCursor into @libraryid, @oldmtid, @oldcid, @newmtid
end

close valueCursor
deallocate valueCursor

set nocount off 

-- *** DO INSERT
insert into Polaris.polaris.ItemRecordSets
select cir.ItemRecordID, m.RecordSetID
from polaris.Polaris.CircItemRecords cir
join Polaris.Polaris.Organizations o
	on o.OrganizationID = cir.AssignedBranchID
join polaris.dbo.CLC_Custom_Old_To_New_Material_Type_Mapping m
	on m.LibraryID = o.ParentOrganizationID and m.OldMaterialTypeID = cir.MaterialTypeID and (m.OldCollectionID = cir.AssignedCollectionID or m.OldCollectionID = -1)
join polaris.polaris.MaterialTypes oldmt
	on oldmt.MaterialTypeID = cir.MaterialTypeID
join polaris.Polaris.Collections oldc
	on oldc.CollectionID = cir.AssignedCollectionID
join polaris.Polaris.MaterialTypes newmt
	on newmt.MaterialTypeID = m.NewMaterialTypeID
where oldmt.Description != newmt.Description

--rollback
--commit




--select o2.Name, oldmt.Description [Old MaterialType], oldc.Name [Old Collection], newmt.Description [New MaterialType], count(*) [Items]
--from polaris.Polaris.CircItemRecords cir
--join Polaris.Polaris.Organizations o
--	on o.OrganizationID = cir.AssignedBranchID
--join polaris.Polaris.Organizations o2
--	on o2.OrganizationID = o.ParentOrganizationID
--join Polaris.dbo.CLC_Custom_Old_To_New_Material_Type_Mapping m
--	on m.LibraryID = o.ParentOrganizationID and m.OldMaterialTypeID = cir.MaterialTypeID and (m.OldCollectionID = cir.AssignedCollectionID or m.OldCollectionID = -1)
--join polaris.polaris.MaterialTypes oldmt
--	on oldmt.MaterialTypeID = cir.MaterialTypeID
--left join polaris.Polaris.Collections oldc
--	on oldc.CollectionID = cir.AssignedCollectionID
--join polaris.Polaris.MaterialTypes newmt
--	on newmt.MaterialTypeID = m.NewMaterialTypeID
--where oldmt.Description != newmt.Description
--group by o2.Name, oldmt.Description, oldc.Name, newmt.Description
--order by o2.Name, oldmt.Description, oldc.Name, newmt.Description
--order by v.rsid

--rollback
--commit
