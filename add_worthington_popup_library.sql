/******* TODO *******
set a different sip password anyway, just in case?
*********************/

/******* TO TEST*******
does new branch get added as pickup branch?
address, webpage, phone numbers, etc
suppress in reg form
adding additional closed dates
***********************/

/******* NOTES *******
mpl school branches might not be that weird as far as the pickup branch lists go, or set up incorrectly
notice thing removed, shouldn't be possible for notices to be wrong initially
hours of operation could be added but probably easier to do in client
	-can add we just need to figure out how to expose the setting to the script since it's just stored as a string
suppressing branch from reg for should be only thing that needs done for reg form stuff
don't think we need to do anything with the 'branch will own items' since that should only come into play for rtf/iad stuff and no shot i'm working those beasts into this process right now
self reg form stuff doesn't do anything with adding, assuming it can inherit that from the parent, and only disables them in the clc form
circ sheet stuff is probably unnecessary but could be added later easy enough
**********************/

select 'start'

declare @libraryId int = 32
declare @copyFromBranch int = 33 -- if 0 will use first branch of library
declare @creatorId int = 2691 -- a.mfields

-- Names/Abbreviation
declare @branchName nvarchar(255) = 'WL Pop Up Library'
declare @branchDisplayName nvarchar(255) = '' -- uses branch name if blank
declare @branchDialoutName nvarchar(255) = '' -- uses branch name if blank
declare @branchAbbreviation nvarchar(10) = 'WLPUL'

-- sip stuff
declare @addSipPort bit = 0
-- can ignore if above is 0
declare @sipPassword varchar(50) = 'MFTEST1234567890'
declare @sipWorkstationId int = 3843

-- Contact Person
declare @contactPersonId int = 36 -- -1 uses copy from branch
-- OR
declare @contactPersonName nvarchar(255) = ''
declare @contactPersonEmail nvarchar(255) = ''
declare @ContactPersonPhoneNumber1 varchar(255) = ''
declare @ContactPersonPhoneNumber2 varchar(255) = ''
declare @ContactPersonFaxNumber varchar(255) = ''

-- these are id numbers
declare @orgaddr1 varchar(10) = '18'
declare @acqshippingaddr varchar(10) = '' -- will use orgaddr1 if blank
declare @acqbillingaddr varchar(10) = '' -- will use orgaddr1 if blank
declare @psprofmailaddr varchar(10) = '' -- will use orgaddr1 if blank

-- other branch stuff, 'real' values not ids
declare @orgphone1 varchar(15) = '614-807-2643'
declare @orgphone2 varchar(15) = ''
declare @orgfaxnum varchar(15) = ''
declare @orgemail varchar(255) = ''
declare @orghomepage varchar(255) = 'www.worthingtonlibraries.org/'
declare @acqtaxnumber varchar(255) = ''
declare @acqsanprefix varchar(255) = ''
declare @acqsan varchar(255) = ''
declare @acqsansuffix varchar(255) = ''
declare @org_error_reporting_email varchar(255) = ''

-- will disable in clc form if not enabled
declare @enableSelfReg bit = 0
declare @enableBranchInOutreach bit = 1
declare @suppressFromPacBranchSwitching bit = 1

-- new branch will get all closed dates from 'source' branch, but we can add others here
declare @additionalClosedDates table ( DateClosed date )
--insert into @additionalClosedDates values ('1/1/2001'

-- Specify which settings to copy
declare 
     @copyCollections bit = 1                      -- Set to 1 to copy collections, 0 to skip
    ,@copyPatronLoanLimits bit = 1                 -- Set to 1 to copy patron loan limits, 0 to skip
    ,@copyMaterialLoanLimits bit = 1               -- Set to 1 to copy material loan limits, 0 to skip
    ,@copyLoanPeriods bit = 1                      -- Set to 1 to copy loan periods, 0 to skip
    ,@copyPatronBlocks bit = 1                     -- Set to 1 to copy patron blocks, 0 to skip
    ,@copyItemBlocks bit = 1                       -- Set to 1 to copy item blocks, 0 to skip
    ,@copyFines bit = 1                            -- Set to 1 to copy fines, 0 to skip
    ,@copyShelfLocations bit = 1                   -- Set to 1 to copy shelf locations, 0 to skip
    ,@copyDatesClosed bit = 1                      -- Set to 1 to copy dates closed, 0 to skip
    ,@copySearchDatabases bit = 1                  -- Set to 1 to copy search databases, 0 to skip
    ,@copyStatisticalCodes bit = 1                 -- Set to 1 to copy statistical codes, 0 to skip
    ,@copyPatronStatClassCodes bit = 1             -- Set to 1 to copy patron statistical class codes, 0 to skip
    ,@copyBibOverlaySettings bit = 1               -- Set to 1 to copy bib overlay settings, 0 to skip
    ,@copyCallNumberHierarchies bit = 1            -- Set to 1 to copy call number hierarchies, 0 to skip
    ,@copySysHoldRoutingSequences bit = 1          -- Set to 1 to copy sys hold routing sequences, 0 to skip
    ,@copySAFloatingMatrix bit = 1                 -- Set to 1 to copy SA floating matrix, 0 to skip
    ,@copyOrganizationsFloatingCollections bit = 1 -- Set to 1 to copy organizations floating collections, 0 to skip
    ,@copySAFloatingMatrixCollectionLimits bit = 1 -- Set to 1 to copy SA floating matrix collection limits, 0 to skip
    ,@copySAFloatingMatrixMaterialLimits bit = 1   -- Set to 1 to copy SA floating matrix material limits, 0 to skip
    ,@copySAFloatingMatrixMaterialTypes bit = 1    -- Set to 1 to copy SA floating matrix material types, 0 to skip
    ,@copySAPaymentMethods bit = 1                 -- Set to 1 to copy SA payment methods, 0 to skip
    ,@copySAPACLimitBy bit = 1                     -- Set to 1 to copy SA PAC limit by, 0 to skip
	

/**********************************************

NOTHING BELOW SHOULD NEED CHANGED

***********************************************/

-- use orgaddr1 for other addresses if not supplied
if (len(@acqshippingaddr) < 1) begin select @acqshippingaddr = @orgaddr1 end
if (len(@acqbillingaddr) < 1) begin select @acqbillingaddr = @orgaddr1 end
if (len(@psprofmailaddr) < 1) begin select @psprofmailaddr = @orgaddr1 end

-- use branch name if other names not supplied
if (len(@branchDisplayName) < 1) begin select @branchDisplayName = @branchName end
if (len(@branchDialoutName) < 1) begin select @branchDialoutName = @branchName end

declare @newOrgId int = 0
declare @isSharedPatron bit = case when @libraryId in (8,86) then 0 else 1 end

select @newOrgId = o.OrganizationID from polaris.polaris.Organizations o where o.Name = @branchName and o.Abbreviation = @branchAbbreviation

begin -- check stuff before we modify anything, especially because we can't use a transaction

	if (@addSipPort = 1) begin
		if (len(@sipPassword) < 10) begin select 'invalid sip password'; return; end
		if (@sipWorkstationId = 0) begin select 'invalid sip workstation id'; return; end
	end

	-- make sure a contact person is supplied somehow
	if (@contactPersonId = 0 and @contactPersonName = '') begin select 'contact person id or info must be supplied'; return; end
end

if (@newOrgId = 0)
begin
	-- add new contact person if need be
	if (@contactPersonId = 0) 
	begin
		-- insert new contact person
		insert into polaris.polaris.SA_ContactPersons(Name, EmailAddress, PhoneNumberOne, PhoneNumberTwo, FaxNumber) values (@contactPersonName, @contactPersonEmail, @ContactPersonPhoneNumber1, @ContactPersonPhoneNumber2, @ContactPersonFaxNumber)
		-- grab their id
		select @contactPersonId = max(SA_ContactPersonID) from polaris.polaris.SA_ContactPersons where Name = @contactPersonName
	end

	if (@contactPersonId = -1)
	begin
		select @contactPersonId = o.SA_ContactPersonID from polaris.polaris.Organizations o where o.OrganizationID = @copyFromBranch
	end

	-- insert actual branch
	insert into Polaris.Polaris.Organizations (ParentOrganizationID, OrganizationCodeID, Name, Abbreviation, SA_ContactPersonID, DisplayName, CreatorID, CreationDate) values (@libraryId, 3, @branchname, @branchAbbreviation, @contactPersonId, @branchDisplayName, @creatorId, getdate())

	-- get new org id cause @@identity doesn't work in this case
	select @newOrgId = max(organizationid) from polaris.polaris.Organizations where ParentOrganizationID = @libraryId and OrganizationCodeID = 3
end

-- insert dialout branch name
insert into clc_notices.dbo.Dialout_Strings (OrganizationID, StringTypeID, Value) values (@newOrgId, 8, @branchDialoutName)

-- add sip port
if (@addSipPort = 1 and not exists ( select 1 from polaris.polaris.SIPServicePorts ssp where ssp.OrganizationID = @newOrgId )) begin insert into polaris.polaris.SIPServicePorts values ( @newOrgId, (select max(TCPIPPortNumber) + 1 from polaris.polaris.SIPServicePorts ) ) end

-- suppress from branch switching
if (@suppressFromPacBranchSwitching = 1 and not exists ( select 1 from polaris.polaris.SA_PAC_SuppressedBranches b where b.OrganizationID = 1 and b.SuppressedOrgID = @newOrgId )) begin insert into polaris.polaris.SA_PAC_SuppressedBranches values (1, @newOrgId) end

-- suppress in clc reg form if necessary
if (@enableSelfReg = 0) begin insert into clcdb.dbo.RegistrationFormSettings values (@newOrgId, 'disable_branch', '', 1)

-- insert any additional closed dates
insert into polaris.polaris.DatesClosed
select @newOrgId, DateClosed
from @additionalClosedDates acd
where not exists ( select 1 from polaris.polaris.DatesClosed dc where dc.OrganizationID = @newOrgId and dc.DateClosed = acd.DateClosed )

/*******************/

begin -- initial basic branch settings
	declare @settings table ( mnemonic nvarchar(255), theval nvarchar(max) )

	insert into @settings values
		 ('orgphone1', @orgphone1)
		,('orgphone2', @orgphone2)
		,('orgfaxnum', @orgfaxnum)
		,('orgemail', @orgemail)
		,('orghomepage', @orghomepage)
		,('acqtaxnumber', @acqtaxnumber)
		,('acqsanprefix', @acqsanprefix)
		,('acqsan', @acqsan)
		,('acqsansuffix', @acqsansuffix)
		,('org_error_reporting_email', @org_error_reporting_email)
		,('orgaddr1', @orgaddr1)
		,('acqshippingaddr', @acqshippingaddr)
		,('acqbillingaddr', @acqbillingaddr)
		,('psprofmailaddr', @psprofmailaddr)
		,('orgcontact', cast(@contactPersonId as nvarchar(10)))

	if (@addSipPort = 1)
	begin
		insert into @settings values
		 ('3MPARMDEFAULTWSID', cast(@sipWorkstationId as nvarchar(10)))
		,('3MPARMPASSWORD', polaris.dbo.ILS_ObfuscateText(@sipPassword))
	end

	-- loop through settings and set them like the client does
	declare valueCursor cursor for
	select s.mnemonic, s.theval from @settings s

	declare @currentSetting nvarchar(255)
	declare @currentVal nvarchar(max)

	open valueCursor
	fetch next from valueCursor into @currentSetting, @currentVal
	while @@FETCH_STATUS = 0
	begin
		if (len(@currentVal) > 0)
		begin
			exec Polaris.Polaris.SA_SetValue @currentSetting,N'Branch',@newOrgId,@currentVal--, 1
		end
		fetch next from valueCursor into @currentSetting, @currentVal
	end

	close valueCursor
	deallocate valueCursor
end

-- add to simply reports
insert into polaris.polaris.RWRITER_BranchUserSecurity
select distinct bus.PolarisUserID, @newOrgId
from polaris.polaris.RWRITER_BranchUserSecurity bus
where not exists ( select 1 from polaris.polaris.RWRITER_BranchUserSecurity _bus where _bus.PolarisUserID = bus.PolarisUserID and _bus.OrganizationID = @newOrgId )

begin -- add as pickup branch
	-- add as suppressed initially
	if (not exists (select 1 from polaris.polaris.HoldsPickupBranchesExclude hpbe where hpbe.OrganizationID = 1 and hpbe.ExcludedBranchID = @newOrgId ))
	begin
		insert into polaris.polaris.HoldsPickupBranchesExclude values (1, @newOrgId, 1)
	end
	--if (@isSharedPatron = 1) begin
	--	insert into polaris.polaris.HoldsPickupBranchesExclude
	--	select distinct e.OrganizationID, @newOrgId, 0
	--	from polaris.polaris.HoldsPickupBranchesExclude e
	--	join polaris.polaris.Organizations o
	--		on o.OrganizationID = e.OrganizationID
	--	join polaris.polaris.Organizations o2
	--		on o2.OrganizationID = e.ExcludedBranchID
	--	where e.Exclude = 0
	--		and o.OrganizationID not in (1, o2.OrganizationID, o2.ParentOrganizationID)
	--	order by e.OrganizationID
	--end
	--else begin
	--	insert into polaris.polaris.HoldsPickupBranchesExclude values (@libraryId, @newOrgId, 0)
	--end
end

begin -- acquire 'source' branches settings	

	-- fix source branch id
	select @copyFromBranch = case @copyFromBranch when 0 then ( select top 1 OrganizationID from polaris.polaris.Organizations where ParentOrganizationID = @libraryId order by OrganizationID) else @copyFromBranch end

	-- Copy Collections
	if @copyCollections = 1
	begin
		delete oc 
		from Polaris.Polaris.OrganizationsCollections oc 
		where oc.OrganizationID = @newOrgId 
		and not exists ( 
			select 1 
			from Polaris.polaris.OrganizationsCollections _oc 
			where _oc.OrganizationID = @copyFromBranch 
			and _oc.CollectionID = oc.CollectionID
		)

		insert into Polaris.polaris.OrganizationsCollections
		select @newOrgId, oc.CollectionID
		from Polaris.polaris.OrganizationsCollections oc
		where oc.OrganizationID = @copyFromBranch
		and not exists ( 
			select 1 
			from Polaris.polaris.OrganizationsCollections oc2 
			where oc2.OrganizationID = @newOrgId 
			and oc2.CollectionID = oc.CollectionID 
		)
	end

	-- Copy Patron Loan Limits
	if @copyPatronLoanLimits = 1
	begin
		update pll_dest
		set pll_dest.MaxFine = pll_src.MaxFine
			,pll_dest.MinFine = pll_src.MinFine
			,pll_dest.TotalItems = pll_src.TotalItems
			,pll_dest.TotalOverDue = pll_src.TotalOverDue
			,pll_dest.TotalHolds = pll_src.TotalHolds
			,pll_dest.TotalILL = pll_src.TotalILL
			,pll_dest.TotalReserveItems = pll_src.TotalReserveItems
		from polaris.polaris.PatronLoanLimits pll_dest
		join polaris.polaris.PatronLoanLimits pll_src
			on pll_src.PatronCodeID = pll_dest.PatronCodeID
		where pll_dest.OrganizationID = @newOrgId
			and pll_src.OrganizationID = @copyFromBranch
	end

	-- Copy Material Loan Limits
	if @copyMaterialLoanLimits = 1
	begin
		update mll_dest
		set mll_dest.MaxItems = mll_src.MaxItems
			,mll_dest.MaxRequestItems = mll_src.MaxRequestItems 
		from polaris.polaris.MaterialLoanLimits mll_dest
		join Polaris.polaris.MaterialLoanLimits mll_src
			on mll_src.PatronCodeID = mll_dest.PatronCodeID 
			and mll_src.MaterialTypeID = mll_dest.MaterialTypeID
		where mll_dest.OrganizationID = @newOrgId
			and mll_src.OrganizationID = @copyFromBranch
	end

	-- Copy Loan Periods
	if @copyLoanPeriods = 1
	begin
		update lp2
		set lp2.TimeUnit = lp.TimeUnit, lp2.Units = lp.Units
		from Polaris.polaris.loanperiods lp 
		join polaris.Polaris.LoanPeriods lp2
			on lp.PatronCodeID = lp2.PatronCodeID 
			and lp.LoanPeriodCodeID = lp2.LoanPeriodCodeID
		where lp.OrganizationID = @copyFromBranch 
			and lp2.OrganizationID = @newOrgId
	end

	-- Copy Patron Blocks
	if @copyPatronBlocks = 1
	begin
		delete from polaris.polaris.PatronStopsByBranch
		where OrganizationID = @newOrgId

		insert into Polaris.Polaris.PatronStopsByBranch
		select @newOrgId, pssb.PatronStopID, pssb.SequenceID, pssb.DisplayInPAC, pssb.Selected
		from polaris.Polaris.PatronStopsByBranch pssb
		where pssb.OrganizationID = @copyFromBranch
	end

	if @copyItemBlocks = 1
	begin
		delete from polaris.polaris.ItemBlockDescriptions where OrganizationID = @newOrgId

		declare @maxIbdId int = 0
		select @maxIbdId = max(ItemBlockID)
		from polaris.polaris.ItemBlockDescriptions

		insert into polaris.polaris.ItemBlockDescriptions
		select @newOrgId, @maxIbdId + ibd.SequenceID, ibd.Description, ibd.SequenceID
		from polaris.polaris.ItemBlockDescriptions ibd
		where ibd.OrganizationID = @copyFromBranch
	end

	-- Copy Fines
	if @copyFines = 1
	begin
		update f_dest
		set f_dest.Amount = f_src.Amount
			,f_dest.MaximumFine = f_src.MaximumFine
			,f_dest.GraceUnits = f_src.GraceUnits
		from polaris.polaris.Fines f_dest
		join polaris.polaris.Fines f_src
			on f_src.PatronCodeID = f_dest.PatronCodeID 
			and f_src.FineCodeID = f_dest.FineCodeID
		where f_dest.OrganizationID = @newOrgId
			and f_src.OrganizationID = @copyFromBranch
	end

	-- Copy Shelf Locations
	if @copyShelfLocations = 1
	begin
		delete from Polaris.Polaris.ShelfLocations 
		where OrganizationID = @newOrgId

		select * into #temp_sl
		from Polaris.Polaris.ShelfLocations sl
		where sl.OrganizationID = @copyFromBranch

		update #temp_sl set organizationid = @newOrgId
		insert into Polaris.Polaris.ShelfLocations select * from #temp_sl
		drop table #temp_sl
	end

	-- Copy Dates Closed
	if @copyDatesClosed = 1
	begin
		delete from Polaris.Polaris.DatesClosed 
		where OrganizationID = @newOrgId

		select * into #temp_dc
		from Polaris.Polaris.DatesClosed dc
		where dc.OrganizationID = @copyFromBranch

		update #temp_dc set organizationid = @newOrgId
		insert into Polaris.Polaris.DatesClosed select * from #temp_dc
		drop table #temp_dc
	end

	-- Copy Search Databases
	if @copySearchDatabases = 1
	begin
		delete from Polaris.Polaris.SearchDatabases 
		where OrganizationID = @newOrgId

		select * into #temp_sd
		from Polaris.Polaris.SearchDatabases sd
		where sd.OrganizationID = @copyFromBranch

		alter table #temp_sd drop column SearchDBID

		update #temp_sd set organizationid = @newOrgId
		insert into Polaris.Polaris.SearchDatabases select * from #temp_sd
		drop table #temp_sd
	end

	-- Copy Statistical Codes
	if @copyStatisticalCodes = 1
	begin
		delete from Polaris.Polaris.StatisticalCodes 
		where OrganizationID = @newOrgId

		select * into #temp_sc
		from Polaris.Polaris.StatisticalCodes sc
		where sc.OrganizationID = @copyFromBranch

		update #temp_sc set organizationid = @newOrgId
		insert into Polaris.Polaris.StatisticalCodes select * from #temp_sc
		drop table #temp_sc
	end

	-- Copy Patron Statistical Class Codes
	if @copyPatronStatClassCodes = 1
	begin
		delete from Polaris.Polaris.PatronStatClassCodes 
		where OrganizationID = @newOrgId

		insert into Polaris.polaris.PatronStatClassCodes (OrganizationID, Description)
		select @newOrgId, Description
		from polaris.polaris.PatronStatClassCodes
		where OrganizationID = @copyFromBranch
	end

	-- Copy Bib Overlay Settings
	if @copyBibOverlaySettings = 1
	begin
		-- Copy Bib Overlay Settings Logic
		delete from Polaris.Polaris.BibOverlayRetentionIndOne where RetentionID in (select RetentionID from Polaris.Polaris.BibOverlayRetentionTags where OrganizationID = @newOrgId)
		delete from Polaris.Polaris.BibOverlayRetentionIndTwo where RetentionID in (select RetentionID from Polaris.Polaris.BibOverlayRetentionTags where OrganizationID = @newOrgId)
		delete from Polaris.Polaris.BibOverlayRetentionTags where OrganizationID = @newOrgId

		select * into #temp_bort from Polaris.polaris.BibOverlayRetentionTags where OrganizationID = @copyFromBranch

		update #temp_bort set OrganizationID = @newOrgId

		declare @temp_mapping table (id int, oldid int, tag int, org int, importprofile int, retain int)

		merge into polaris.polaris.BibOverlayRetentionTags using #temp_bort as temp on 1 = 0
		when not matched then
			insert (RetainTagNumber, OrganizationID, ImportProfileID, Retain)
			values (temp.RetainTagNumber, temp.OrganizationID, temp.ImportProfileID, temp.Retain)
		output inserted.IdentityCol, temp.RetentionID, temp.RetainTagNumber, temp.OrganizationID, temp.ImportProfileID, temp.Retain
		into @temp_mapping;

		select borio.* into #temp_borio
		from Polaris.Polaris.BibOverlayRetentionIndOne borio 
		where borio.RetentionID in (select RetentionID from Polaris.Polaris.BibOverlayRetentionTags where OrganizationID = @copyFromBranch)

		insert into Polaris.Polaris.BibOverlayRetentionIndOne
		select t.id, tb.RetainIndicator
		from #temp_borio tb
		join @temp_mapping t on tb.RetentionID = t.oldid

		select borit.* into #temp_borit
		from Polaris.Polaris.BibOverlayRetentionIndTwo borit
		where borit.RetentionID in (select RetentionID from Polaris.Polaris.BibOverlayRetentionTags where OrganizationID = @copyFromBranch)

		insert into Polaris.Polaris.BibOverlayRetentionIndTwo
		select t.id, tb.RetainIndicator
		from #temp_borit tb
		join @temp_mapping t on tb.RetentionID = t.oldid

		drop table #temp_bort
		drop table #temp_borio
		drop table #temp_borit
	end

	-- Copy Call Number Hierarchies
	if @copyCallNumberHierarchies = 1
	begin
		-- Copy Call Number Hierarchies Logic
		delete from Polaris.Polaris.CallNumberHierarchies where OrganizationID = @newOrgId

		select * into #temp_cnh
		from Polaris.Polaris.CallNumberHierarchies cnh
		where cnh.OrganizationID = @copyFromBranch

		update #temp_cnh set organizationid = @newOrgId
		insert into Polaris.Polaris.CallNumberHierarchies select * from #temp_cnh
		drop table #temp_cnh
	end

	-- Copy Sys Hold Routing Sequences
	if @copySysHoldRoutingSequences = 1
	begin
		-- Copy Sys Hold Routing Sequences Logic
		delete from Polaris.Polaris.SysHoldRoutingSequences where RequesterBranchID = @newOrgId

		select * into #temp_shrs
		from Polaris.Polaris.SysHoldRoutingSequences shrs
		where shrs.RequesterBranchID = @copyFromBranch

		update #temp_shrs set RequesterBranchID = @newOrgId
		insert into Polaris.Polaris.SysHoldRoutingSequences select * from #temp_shrs
		drop table #temp_shrs
	end

	-- Copy SA Floating Matrix
	if @copySAFloatingMatrix = 1
	begin
		-- Copy SA Floating Matrix Logic
		delete from Polaris.Polaris.SA_FloatingMatrix where ItemHomeBranchID = @newOrgId

		select * into #temp_safm
		from Polaris.Polaris.SA_FloatingMatrix safm
		where safm.ItemHomeBranchID = @copyFromBranch

		update #temp_safm set ItemHomeBranchID = @newOrgId
		insert into Polaris.Polaris.SA_FloatingMatrix select * from #temp_safm
		drop table #temp_safm
	end

	-- Copy Organizations Floating Collections
	if @copyOrganizationsFloatingCollections = 1
	begin
		-- Copy Organizations Floating Collections Logic
		delete from Polaris.Polaris.OrganizationsFloatingCollections where HomeBranchID = @newOrgId

		select * into #temp_ofc
		from Polaris.Polaris.OrganizationsFloatingCollections ofc
		where ofc.HomeBranchID = @copyFromBranch

		update #temp_ofc set HomeBranchID = @newOrgId
		insert into Polaris.Polaris.OrganizationsFloatingCollections select * from #temp_ofc
		drop table #temp_ofc
	end

	-- Copy SA Floating Matrix Collection Limits
	if @copySAFloatingMatrixCollectionLimits = 1
	begin
		-- Copy SA Floating Matrix Collection Limits Logic
		delete from Polaris.Polaris.SA_FloatingMatrixCollectionLimits where ItemCheckInBranchID = @newOrgId

		select * into #temp_safmcl
		from Polaris.Polaris.SA_FloatingMatrixCollectionLimits safmcl
		where safmcl.ItemCheckInBranchID = @copyFromBranch

		update #temp_safmcl set ItemCheckInBranchID = @newOrgId
		insert into Polaris.Polaris.SA_FloatingMatrixCollectionLimits select * from #temp_safmcl
		drop table #temp_safmcl
	end

	-- Copy SA Floating Matrix Material Limits
	if @copySAFloatingMatrixMaterialLimits = 1
	begin
		-- Copy SA Floating Matrix Material Limits Logic
		delete from Polaris.Polaris.SA_FloatingMatrixMaterialLimits where ItemCheckInBranchID = @newOrgId

		select * into #temp_safmml
		from Polaris.Polaris.SA_FloatingMatrixMaterialLimits safmml
		where safmml.ItemCheckInBranchID = @copyFromBranch

		update #temp_safmml set ItemCheckInBranchID = @newOrgId
		insert into Polaris.Polaris.SA_FloatingMatrixMaterialLimits select * from #temp_safmml
		drop table #temp_safmml
	end

	-- Copy SA Floating Matrix Material Types
	if @copySAFloatingMatrixMaterialTypes = 1
	begin
		-- Copy SA Floating Matrix Material Types Logic
		delete from Polaris.Polaris.SA_FloatingMatrixMaterialTypes where ItemHomeBranchID = @newOrgId

		select * into #temp_safmmt
		from Polaris.Polaris.SA_FloatingMatrixMaterialTypes safmmt
		where safmmt.ItemHomeBranchID = @copyFromBranch

		update #temp_safmmt set ItemHomeBranchID = @newOrgId
		insert into Polaris.Polaris.SA_FloatingMatrixMaterialTypes select * from #temp_safmmt
		drop table #temp_safmmt
	end

	-- Copy SA Payment Methods
	if @copySAPaymentMethods = 1
	begin
		-- Copy SA Payment Methods Logic
		delete from Polaris.Polaris.SA_PaymentMethods where OrganizationID = @newOrgId

		insert into polaris.Polaris.SA_PaymentMethods
		select @newOrgId, pm.PaymentMethodID, pm.DisplayOrder, pm.Selected
		from polaris.polaris.SA_PaymentMethods pm
		where pm.OrganizationID = @copyFromBranch
	end

	-- Copy PAC Limit By
	if @copySAPACLimitBy = 1
	begin
		-- Copy PAC Limit By Logic
		delete from Polaris.Polaris.SA_PACLimitBy where OrganizationID = @newOrgId

		insert into polaris.Polaris.SA_PACLimitBy
		select @newOrgId, lb.LimitDescription, lb.CCLFilter, lb.Enabled, lb.DisplayOrder, lb.Protected
		from Polaris.Polaris.SA_PACLimitBy lb
		where lb.OrganizationID = @copyFromBranch
	end
end

begin -- add to permission groups if shared patron branch
	if (@isSharedPatron = 1)
	begin
		;with permissionBranchCounts as (
			select pg.GroupID, cr.ControlRecordDefID, pd.PermissionNameID, count(distinct o.ParentOrganizationID) [BranchCount]
			from Polaris.Polaris.PermissionGroups pg
			join Polaris.Polaris.ControlRecords cr
				on cr.ControlRecordID = pg.ControlRecordID
			join Polaris.Polaris.ControlRecordDefs crd
				on crd.ControlRecordDefID = cr.ControlRecordDefID
			join Polaris.polaris.PermissionDefs pd
				on pd.PermissionID = pg.PermissionID and pd.ControlRecordDefID = crd.ControlRecordDefID
			join Polaris.polaris.organizations o
				on o.OrganizationID = cr.OrganizationID
			group by pg.GroupID, cr.ControlRecordDefID, pd.PermissionNameID
		), allPerms as (
			select distinct
					 g.GroupID
					,g.GroupName
					,pp_new.PermissionID
					,pp_new.ControlRecordID
					,pp_new.ControlRecordName
					,pp_new.PermissionName
					,pp_new.OrganizationID
					,o_new.Name
					,pp_new.ControlRecordDefID
					,pp_new.PermissionNameID
					,pp_source.SubSystem
					,pbc.BranchCount
			from Polaris.Polaris.Groups g
			join polaris.Polaris.PermissionGroups pg
				on pg.GroupID = g.GroupID
			join Polaris.dbo.CLC_Custom_PolarisPermissions pp_source
				on pp_source.ControlRecordID = pg.ControlRecordID and pp_source.PermissionID = pg.PermissionID
			join Polaris.Polaris.Organizations o_source
				on o_source.OrganizationID = pp_source.OrganizationID
			join Polaris.dbo.CLC_Custom_PolarisPermissions pp_new
				on pp_new.ControlRecordName not like '%patron record sets%' and pp_new.ControlRecordDefID = pp_source.ControlRecordDefID and pp_new.PermissionNameID = pp_source.PermissionNameID and pp_new.OrganizationID != pp_source.OrganizationID
			join polaris.Polaris.Organizations o_new
				on o_new.OrganizationID = pp_new.OrganizationID
			join permissionBranchCounts pbc
				on pbc.GroupID = pg.GroupID and pbc.ControlRecordDefID = pp_source.ControlRecordDefID and pbc.PermissionNameID = pp_source.PermissionNameID
			where (pp_source.SubSystemID = 2 or o_new.ParentOrganizationID = o_source.ParentOrganizationID) -- allow other subsystems for same library
				-- excluded sources
				and case o_source.OrganizationCodeID when 2 then o_source.OrganizationID when 3 then o_source.ParentOrganizationID end not in (14)
				-- exclude non-shared patron libraries from new permissions, ALE in shared patron as of 4/13/2023
				and ( case o_new.OrganizationCodeID when 2 then o_new.OrganizationID when 3 then o_new.ParentOrganizationID end not in (4,8,86) )
				-- exclude non-shared patron library groups
				and g.GroupName not like 'FCD%' and g.GroupName not like 'LPL%' and g.GroupName not like 'Scoville%'
				-- only add permissions to valid groups
				and g.GroupID >= 22
				-- exclude archived branches
				and (o_new.Name not like 'z%')-- or o_new.OrganizationID = 92)
				-- exclude 'deny item request'
				and pp_new.PermissionNameID not in (35)
				-- new org is one of the ones specified
				and o_new.OrganizationID in (@newOrgId)
				-- group has permissions at at least 5 branches or shares the parent org
				and (pbc.BranchCount > 5 or o_new.ParentOrganizationID = o_source.ParentOrganizationID)
				--and (exists ( select 1 from Polaris.dbo.CLC_Custom_PermissionGroups _pg where _pg.GroupID = g.GroupID and _pg.ControlRecordDefID = pp_source.ControlRecordDefID and _pg.PermissionNameID = pp_source.PermissionNameID and _pg.LibraryId != o_source.ParentOrganizationID ) or o_new.ParentOrganizationID = o_source.ParentOrganizationID )
		)

		insert into polaris.polaris.PermissionGroups
		select ap.ControlRecordID, ap.PermissionID, ap.GroupID
		from allPerms ap
		left join polaris.polaris.PermissionGroups pg
			on pg.GroupID = ap.GroupID and pg.ControlRecordID = ap.ControlRecordID and pg.PermissionID = ap.PermissionID
		where pg.GroupID is null
	end
end

begin -- add branch to outreach
	if (@enableBranchInOutreach = 1)
	begin
		declare @outreachLibraries table (libraryId int)
		insert into @outreachLibraries values (16),(19),(32),(78)

		update b
		set b.Included = 1
		from polaris.polaris.SA_ORS_IncludedBranches b
		join Polaris.polaris.Organizations o
			on o.OrganizationID = b.OrganizationID
		join Polaris.polaris.Organizations o2
			on o2.OrganizationID = b.BranchID
		where (o.ParentOrganizationID in (select * from @outreachLibraries) or o.OrganizationID in (select * from @outreachLibraries))
			and b.BranchID = @newOrgId
	end
end

begin -- add to hold trapping preference group
	insert into polaris.polaris.SA_TrappingPreferenceGroups
	select tpg.GroupID, @newOrgid
	from polaris.polaris.SA_TrappingPreferenceGroups tpg
	where tpg.BranchID = @copyFromBranch
		and not exists ( select 1 from polaris.polaris.SA_TrappingPreferenceGroups _tpg where _tpg.BranchID = @newOrgid and _tpg.GroupID = tpg.GroupID )
end

select 'done'