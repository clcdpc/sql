declare @sourceOrg int = 5
declare @highestOldOrg int = 104

declare @orgs table ( orgid int )
insert into @orgs select organizationid from Polaris.Polaris.Organizations o where o.OrganizationID > @highestOldOrg and o.OrganizationCodeID = 3

begin tran

insert into Polaris.polaris.BibOverlayRetentionTags (BibOverlayRetentionTags.RetainTagNumber, OrganizationID, ImportProfileID, Retain)
select bort.RetainTagNumber, o.orgid, bort.ImportProfileID, bort.Retain
from polaris.Polaris.BibOverlayRetentionTags bort
cross join @orgs o
where bort.OrganizationID = @sourceOrg

insert into Polaris.Polaris.BibOverlayRetentionIndOne
select bort2.RetentionID, borio.RetainIndicator
from polaris.polaris.BibOverlayRetentionTags bort
join polaris.polaris.BibOverlayRetentionIndOne borio on
	borio.RetentionID = bort.RetentionID
join polaris.polaris.BibOverlayRetentionTags bort2
	on bort2.RetainTagNumber = bort.RetainTagNumber
where bort.OrganizationID = @sourceOrg and bort2.OrganizationID > @highestOldOrg

insert into Polaris.Polaris.BibOverlayRetentionIndTwo
select bort2.RetentionID, borit.RetainIndicator
from polaris.polaris.BibOverlayRetentionTags bort
join polaris.polaris.BibOverlayRetentionIndTwo borit on
	borit.RetentionID = bort.RetentionID
join polaris.polaris.BibOverlayRetentionTags bort2
	on bort2.RetainTagNumber = bort.RetainTagNumber
where bort.OrganizationID = @sourceOrg and bort2.OrganizationID > @highestOldOrg

--rollback
--commit