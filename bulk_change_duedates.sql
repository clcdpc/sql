begin tran

update ic
set ic.DueDate = cast(coalesce(dr.ItemDueDate, dr.EndDate) as datetime) + .999999
--output deleted.ItemRecordID
--		,deleted.DueDate
--		into Polaris.dbo.CLC_Custom_DueDateBackup2
from Polaris.Polaris.ItemCheckouts ic
join polaris.Polaris.Organizations o
	on o.OrganizationID = ic.OrganizationID
join polaris.Polaris.CircItemRecords cir
	on cir.ItemRecordID = ic.ItemRecordID
join Polaris.dbo.CLC_Custom_CV_Closed_Date_Ranges dr
	on dr.LibraryId = o.ParentOrganizationID
where ic.DueDate between '3/12/2020' and coalesce(dr.ItemDueDate, dr.EndDate)
	and cir.ElectronicItem = 0
	and coalesce(dr.ItemDueDate, dr.EndDate) != cast(ic.DueDate as date)

--select @@rowcount
--select count(*) from polaris.dbo.CLC_Custom_DueDateBackup2

--rollback
--commit




/*
select o.ParentOrganizationID, ic.DueDate, cast(coalesce(dr.ItemDueDate, dr.EndDate) as datetime) + .9999999, count(*)
from Polaris.Polaris.ItemCheckouts ic
join polaris.Polaris.Organizations o
	on o.OrganizationID = ic.OrganizationID
join polaris.Polaris.CircItemRecords cir
	on cir.ItemRecordID = ic.ItemRecordID
join Polaris.dbo.CLC_Custom_CV_Closed_Date_Ranges dr
	on dr.LibraryId = o.ParentOrganizationID
where ic.DueDate between '3/12/2020' and coalesce(dr.ItemDueDate, dr.EndDate)
	and cir.ElectronicItem = 0
group by o.ParentOrganizationID, cast(coalesce(dr.ItemDueDate, dr.EndDate) as datetime) + .9999999, ic.DueDate
order by o.ParentOrganizationID, ic.DueDate





select o.ParentOrganizationID, ic.DueDate, count(*)
from Polaris.Polaris.ItemCheckouts ic
join polaris.Polaris.Organizations o
	on o.OrganizationID = ic.OrganizationID
join polaris.Polaris.CircItemRecords cir
	on cir.ItemRecordID = ic.ItemRecordID
join Polaris.dbo.CLC_Custom_CV_Closed_Date_Ranges dr
	on dr.LibraryId = o.ParentOrganizationID
where cir.ElectronicItem = 0
	and ic.DueDate > '4/1/2020'
group by o.ParentOrganizationID, ic.DueDate
order by o.ParentOrganizationID, ic.DueDate

*/


--select * from polaris.polaris.Organizations o where o.OrganizationCodeID = 2

/*

USE [Polaris]
GO

CREATE TABLE [dbo].[CLC_Custom_DueDateBackup2](
	[ItemRecordID] [int] NOT NULL,
	[OldDueDate] [date] NOT NULL
) ON [PRIMARY]
GO

*/


/*

begin tran

update ic
set ic.DueDate = ddb.OldDueDate
from Polaris.polaris.ItemCheckouts ic
join polaris.dbo.CLC_Custom_DueDateBackup ddb
	on ddb.ItemRecordID = ic.ItemRecordID

--commit
--rollback

*/
-- 856



--select * from Polaris.polaris.ItemCheckouts ic where ic.ItemRecordID = 856