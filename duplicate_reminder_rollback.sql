if exists (select * from dbo.sysobjects where id = object_id(N'[Polaris].[Notices_AlmostOverdue]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Polaris].[Notices_AlmostOverdue]
GO

CREATE PROCEDURE Notices_AlmostOverdue
/************************************************************/
--	This sp is called by ReminderAlmostOverdue.rdl.
/************************************************************/
	@orgID int,	-- patron's branch org id
	@patronID int
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT @orgID = IsNull(@orgID, 0);
	SELECT @patronID = ISNULL(@patronID, 0);

	SELECT
		cr.NotificationTypeID,
		cr.PatronID, 
		cr.PatronName, 
		cr.EmailAddress,
		cr.PatronAddress,
		cr.EmailFormatID,
		ItemRecordID, 
		DueDate, 
		LEFT(BrowseTitle, 100) AS BrowseTitle, 
		ItemFormat, 
		ItemAssignedBranch, 
		Renewals,
		ReportingOrgName,
		ReportingOrgAddress,
		ReportingOrgPhone,
		CASE WHEN cr.AutoRenewal = 1 THEN NULL ELSE Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'NT_REMINDER_OVD_EM_TEXT') END AS [OVDEmailText],
		Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'NT_EMAIL_FOOTER') AS [EmailFooter],
		Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'LBL_DUEDATE') as [LBL_DUEDATE],
		Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'LBL_TITLE') as [LBL_TITLE],
		Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'LBL_FORMAT') as [LBL_FORMAT],
		Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'LBL_FROM') as [LBL_FROM],
		Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'LBL_RENEWALS') as [LBL_RENEWALS],
		Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'LBL_NEWDUEDATE') as [LBL_NEWDUEDATE],
		Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'NT_REMINDER_RENEW_EM_HEADER') AS [RenewEmailText],
		cr.ReportingOrgID,
		cr.AutoRenewal
	FROM
		Results.Polaris.CircReminders cr WITH (NOLOCK)
	WHERE
		cr.NotificationTypeID = 7 AND cr.AutoRenewal = 0
		AND @orgID = CASE @orgID WHEN 0 THEN @orgID ELSE cr.ReportingOrgID END
		AND @patronID = CASE @patronID WHEN 0 THEN @patronID ELSE cr.PatronID END
	ORDER BY cr.AutoRenewal DESC;
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Polaris].[Notices_AutoRenewal]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Polaris].[Notices_AutoRenewal]
GO

CREATE PROCEDURE Notices_AutoRenewal
/************************************************************/
--	This sp is called by ReminderAlmostOverdue.rdl.
/************************************************************/
	@orgID int,	-- patron's branch org id
	@patronID int
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT @orgID = IsNull(@orgID, 0);
	SELECT @patronID = ISNULL(@patronID, 0);

	SELECT
		cr.PatronID, 
		cr.PatronName, 
		cr.EmailAddress,
		cr.PatronAddress,
		cr.EmailFormatID,
		ItemRecordID, 
		DueDate, 
		LEFT(BrowseTitle, 100) AS BrowseTitle, 
		ItemFormat, 
		ItemAssignedBranch, 
		Renewals,
		ReportingOrgName,
		ReportingOrgAddress,
		ReportingOrgPhone,
		Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'NT_REMINDER_RENEW_EM_HEADER') AS [EmailText],
		Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'NT_EMAIL_FOOTER') AS [EmailFooter],
		Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'LBL_NEWDUEDATE') as [LBL_DUEDATE],
		Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'LBL_TITLE') as [LBL_TITLE],
		Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'LBL_FORMAT') as [LBL_FORMAT],
		Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'LBL_FROM') as [LBL_FROM],
		Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'LBL_RENEWALS') as [LBL_RENEWALS]
	FROM
		Results.Polaris.CircReminders cr WITH (NOLOCK)
	WHERE
		cr.NotificationTypeID = 7 AND cr.AutoRenewal = 1
		AND @orgID = CASE @orgID WHEN 0 THEN @orgID ELSE cr.ReportingOrgID END
		AND @patronID = CASE @patronID WHEN 0 THEN @patronID ELSE cr.PatronID END;
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Polaris].[Notices_ReminderAlmostOverdue]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Polaris].[Notices_ReminderAlmostOverdue]
GO

CREATE PROCEDURE Notices_ReminderAlmostOverdue
/************************************************************/
--	This sp is called by ReminderAlmostOverdue.rdl.
/************************************************************/
	@orgID int,	-- patron's branch org id
	@patronID int
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT @orgID = IsNull(@orgID, 0);
	SELECT @patronID = ISNULL(@patronID, 0);

	SELECT
		cr.NotificationTypeID,
		cr.PatronID, 
		cr.PatronName, 
		cr.EmailAddress,
		cr.PatronAddress,
		cr.EmailFormatID,
		ItemRecordID, 
		DueDate, 
		LEFT(BrowseTitle, 100) AS BrowseTitle, 
		ItemFormat, 
		ItemAssignedBranch, 
		Renewals,
		ReportingOrgName,
		ReportingOrgAddress,
		ReportingOrgPhone,
		CASE WHEN cr.AutoRenewal = 1 THEN NULL ELSE Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'NT_REMINDER_OVD_EM_TEXT') END AS [OVDEmailText],
		Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'NT_EMAIL_FOOTER') AS [EmailFooter],
		Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'LBL_DUEDATE') as [LBL_DUEDATE],
		Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'LBL_TITLE') as [LBL_TITLE],
		Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'LBL_FORMAT') as [LBL_FORMAT],
		Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'LBL_FROM') as [LBL_FROM],
		Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'LBL_RENEWALS') as [LBL_RENEWALS],
		Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'LBL_NEWDUEDATE') as [LBL_NEWDUEDATE],
		Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'NT_REMINDER_RENEW_EM_HEADER') AS [RenewEmailText],
		cr.ReportingOrgID,
		cr.AutoRenewal
	FROM
		Results.Polaris.CircReminders cr WITH (NOLOCK)
	WHERE
		cr.NotificationTypeID = 7
		AND @orgID = CASE @orgID WHEN 0 THEN @orgID ELSE cr.ReportingOrgID END
		AND @patronID = CASE @patronID WHEN 0 THEN @patronID ELSE cr.PatronID END;
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Polaris].[Notices_ReminderAlmostOverdueOtherItems]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Polaris].[Notices_ReminderAlmostOverdueOtherItems]
GO

CREATE PROCEDURE Notices_ReminderAlmostOverdueOtherItems
/************************************************************/
--	This sp is called by AlmostOverdueOtherItems.rdl.
/************************************************************/
	@PatronID int	
AS
	SET NOCOUNT ON
		
	SELECT 
		distinct
		ic.ItemRecordID, 
		ic.DueDate, 
		LEFT(vi.Title, 50) AS Title,
		LEFT(vi.SortTitle, 50) AS SortTitle,
		vi.MaterialType,
		vi.AssignedBranch,
		CASE
			WHEN IsNull(cv.RenewalLimit, 0) > IsNull(ic.Renewals, 0) THEN IsNull(cv.RenewalLimit, 0) - IsNull(ic.Renewals, 0)
			ELSE 0
		END AS Renewals, 
		CASE 
			WHEN DATEDIFF(Day, ic.DueDate, GETDATE()) >= 0 THEN 'Yes' 
			ELSE 'No' 
		END AS Overdue,

		Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'LBL_DUEDATE') as [LBL_DUEDATE],
		Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'LBL_TITLE') as [LBL_TITLE],
		Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'LBL_FORMAT') as [LBL_FORMAT],
		Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'LBL_FROM') as [LBL_FROM],
		Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'LBL_RENEWALS') as [LBL_RENEWALS],
		Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'LBL_DUE?') as [LBL_DUE],
		Polaris.SA_GetMultiLingualStringValue(7, cr.AdminLanguageID, cr.ReportingOrgID, 'TXT_ALMOSTOVERDUE_OTHERITEMS') as [TEXT]
	FROM 
		Results.Polaris.CircReminders cr WITH (NOLOCK)
		INNER JOIN Polaris.ItemCheckouts ic WITH (NOLOCK) ON (cr.PatronID = ic.PatronID)
		INNER JOIN Polaris.ViewItemRecords vi WITH (NOLOCK) ON (ic.ItemRecordID = vi.ItemRecordID)
		INNER JOIN Polaris.CircReserveItemRecords_View cv WITH (NOLOCK) ON (ic.ItemRecordID = cv.ItemRecordID)
		LEFT OUTER JOIN Results.Polaris.CircReminders rd WITH (NOLOCK) ON (ic.ItemRecordID = rd.ItemRecordID AND ic.PatronID = rd.PatronID)
	WHERE 
		cr.PatronID = @PatronID
		AND cr.NotificationTypeID in (7,20)
		AND rd.ItemRecordID IS NULL
	ORDER BY 
		ic.DueDate, SortTitle
GO