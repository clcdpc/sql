USE [Polaris]
GO

/****** Object:  StoredProcedure [Polaris].[PAPI_HoldRequestCancel]    Script Date: 7/25/2016 10:17:53 AM ******/
DROP PROCEDURE [Polaris].[PAPI_HoldRequestCancel]
GO

/****** Object:  StoredProcedure [Polaris].[PAPI_HoldRequestCancel]    Script Date: 7/25/2016 10:17:53 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [Polaris].[PAPI_HoldRequestCancel]
	/* 
	 *	Stored Procedure: PAPI_HoldRequestCancel
	 *  Created: 2008-02-21 JDY
	 *
	 *	Return Codes:
	 *		    0 - Success
	 *		   -1 - Failure
	 *		-4000 - Invalid application ID supplied
	 *		-4002 - Invalid workstation ID supplied
	 *		-4003 - Invalid user ID supplied
	 *		-4201 - Invalid request ID supplied
	 *		-4202 - Invalid current org ID supplied
	 *		-4203 - Cannot cancel held hold request
	 *
	 *	Input Parameters:
	 *		@nApplicationID		int					ID of application making this API call	
	 *			1 �	Polaris Staff Client
	 *			2 �	PowerPAC
	 *			3 �	ActivePAC
	 *			4 �	Express Check
	 *			5 - CPAC
	 *			11 - MobilePAC
	 *			100 � Third Party	
	 *		@nRequestID			int					ID of hold request
	 *		@nCurrentOrgID		int					ID of branch calling this procedure
	 *		@nWorkstationID		int					ID of workstation calling this procedure
	 *		@nUserID			int					ID of Polaris user calling this procedure
	 *
	 *	Output Parameters:
	 *		@szErrorMessage		varchar(255)		Error message text
	 *
	 */
	@nApplicationID int, 
	@nRequestID int,
	@nCurrentOrgID int,
	@nWorkstationID int,
	@nUserID int,
	@szErrorMessage varchar(255) output
AS
BEGIN
	set nocount on

	declare @nResult int,
		@nPickupBranchID int,
		@nItemRecordID int,
		@nBibRecordID int,
		@nPatronID int,
		@nPatronBranchID int,
		@nTxnID int, 
		@nTxnTypeID int,
		@nSubSystemID int,
		@dtTxnDate datetime,
		@nPatronCodeID int,
		@nPatronStatClassID int,
		@nItemAssignedBranchID int,
		@nItemAssignedCollectionID int,
		@nItemStatisticalCodeID int		

	set @nResult = 0

	-- Check Application ID
	if (@nApplicationID is null or @nApplicationID = 0 or (@nApplicationID > 4 and @nApplicationID <> 11 and @nApplicationID <> 100))
		return -4000;

	-- Check Request ID
	if (@nRequestID is null or @nRequestID = 0)
		return -4201;
	if not exists (select SysHoldRequestID from SysHoldRequests with (nolock) where SysHoldRequestID = @nRequestID)
		return -4201

	-- Check Workstation ID
	if (@nWorkstationID is null or @nWorkstationID = 0)
		return -4002;
	if not exists (select WorkstationID from Workstations with (nolock) where WorkstationID = @nWorkstationID)
		return -4002

	-- Check User ID
	if (@nUserID is null or @nUserID = 0)
		return -4003;
	if not exists (select PolarisUserID from PolarisUsers with (nolock) where PolarisUserID = @nUserID)
		return -4003

	-- Check Current Org ID
	if (@nCurrentOrgID is null or @nCurrentOrgID = 0)
		return -4202;
	if not exists (select OrganizationID from Organizations with (nolock) where OrganizationID = @nCurrentOrgID)
		return -4202

	if exists (select * from SysHoldRequests with (nolock) where SysHoldRequestID = @nRequestID and SysHoldStatusID = 6)
		return -4203
		
	-- Cancel hold request
	begin try
		-- Determine subsystem
		if (@nApplicationID = 1)
			set @nSubSystemID = dbo.ILSTransactionSubTypeCode::SUBSYSTEM_TYPE_STAFFCLIENT;
		else if (@nApplicationID = 4)
			set @nSubSystemID = dbo.ILSTransactionSubTypeCode::SUBSYSTEM_TYPE_EXPRESSCHECK;
		else if (@nApplicationID = 100)
			set @nSubSystemID = dbo.ILSTransactionSubTypeCode::SUBSYSTEM_TYPE_THIRDPARTYAPP;			
		else
			set @nSubSystemID = dbo.ILSTransactionSubTypeCode::SUBSYSTEM_TYPE_PAC;
		
		-- 38	Request cancelled by patron via self check
		-- 42	Request cancelled by patron via Mobile Pac
		-- 43	Request cancelled by patron via Power Pac		
		DECLARE @nCancelActionTakenID INT = 38;
		if (@nApplicationID = 11)
			select @nCancelActionTakenID = 42
		else if (@nApplicationID IN (2,3,4))
			select @nCancelActionTakenID = 43
			
		begin transaction
			
		exec Polaris.Circ_CancelHoldRequest @nRequestID, 
			@nCurrentOrgID, @nUserID, @nWorkstationID, @nSubSystemID, @nCancelActionTakenID

		commit transaction
	end try
	begin catch
		set @szErrorMessage =  ERROR_MESSAGE()

		-- Test XACT_STATE for 1 or -1.
		-- XACT_STATE = 0 means there is no transaction and
		-- a COMMIT or ROLLBACK would generate an error.

		-- Test if the transaction is uncommittable.
		if (XACT_STATE()) <> 0
			rollback transaction;		
		return -1
	end catch
	
	select @nPatronID = patronid from polaris.polaris.SysHoldRequests with (nolock) where SysHoldRequestID = @nRequestID
	
	exec polaris.PAPI_UpdatePatronLastActivityDate @nPatronID

	return @nResult
END

GO


