;with fixed521 as (
	select bt.BibliographicRecordID
			,bs.Data [raw521]
			,case 
				when 
					(bs.data like '%not rated%' or bs.data like '%unrated%' or bs.data like '%un rated%' or bs.data like '%un-rated%') 
					and not (bs.data like 'Rated%' or bs.data like 'MPAA Rating%') 
				then 
					'not rated' 
				else 
					replace(replace(bs.Data, 'rated', 'rating'), ':', '') 
				end [fixed521]
	from polaris.polaris.BibliographicTags bt
	join polaris.polaris.BibliographicSubfields bs
		on bs.BibliographicTagID = bt.BibliographicTagID
	where bt.TagNumber = 521
	and bs.Subfield = 'a'
	and (bs.data like '%rating%' or bs.data like '%rated%' or bs.Data like 'preschool%')
	and (bs.Data not like '%CHV%' and bs.Data not like '%Canada%' and bs.Data not like '%Canadian%' and bs.data not like '%OFBR%' and bs.data not like '%BBFC%' and bs.data not like '%ofrb%' and bs.data not like '%cmpda%' and bs.Data not like '%chr%' and bs.data not like '%Recommended for ages%' and bs.data not like '%13up%')
), ratings as (
	select *
		,case when f.fixed521 != 'not rated' and f.fixed521 like '%rating%' then replace(replace(replace(RIGHT(f.fixed521,CHARINDEX('gnitar',REVERSE(f.fixed521),0)-1), '-', ''), '.', ''), ' ', '') else f.fixed521 end [Rating]
	from fixed521 f
), fixedratings as (
	select *,
		case
			when r.Rating like 'g%' or r.rating like 'tv[yg]%' or r.rating like 'preschool%' or r.Rating like '3up%' then 49
			when r.Rating like 'pg13%' or r.rating like '1[34]%' or r.Rating like 'tv14%' then 51
			when r.Rating like 'PG%' or r.rating like 'tvpg%' then 3
			when (r.rating like 'r%' and r.rating not like 'recc%') or r.rating like '1[68]%'  or r.Rating like 'tvma%' then 52
			when r.Rating = 'not rated' or r.rating like 'ur%' or r.rating like 'nr%' or r.rating like 'unk%' then 53
			else -1
		end [FixedRating]		
	from ratings r
)

--select * from ratings where BibliographicRecordID = 406858

select cir.ItemRecordID
	,cir.AssociatedBibRecordID
	,r.Rating
	,r.raw521
	,mt.Description [new_mt]
	,mt2.Description [old_mt]
from fixedratings r
join polaris.polaris.CircItemRecords cir
	on cir.AssociatedBibRecordID = r.BibliographicRecordID
join polaris.Polaris.BibliographicRecords br
	on br.BibliographicRecordID = cir.AssociatedBibRecordID
join polaris.polaris.MaterialTypes mt
	on mt.MaterialTypeID = r.FixedRating
join polaris.Polaris.MaterialTypes mt2
	on mt2.MaterialTypeID = cir.MaterialTypeID
where 
	--r.FixedRating not in (-1,53)
	--and br.PrimaryMARCTOMID != 1
	--and cir.MaterialTypeID not in (r.fixedrating,10,16,20,33)
	--and cir.ItemStatusID not in (13)
	--and exists ( select 1 from polaris.polaris.CircItemRecords cir2 where cir2.AssociatedBibRecordID = cir.AssociatedBibRecordID and cir2.MaterialTypeID in (3,49,50,51,52,53) )
	--and not exists ( select 1 from polaris.polaris.CircItemRecords cir2 where cir2.AssociatedBibRecordID = cir.AssociatedBibRecordID and cir2.MaterialTypeID in (18,50) )
	--and 
	r.BibliographicRecordID = 406858
order by cir.AssociatedBibRecordID


--21191100