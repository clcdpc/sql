declare @pickupLibrary int = 8

update shr
set shr.HoldNotificationDate = null
	,shr.HoldNotification2ndDate = null
from Polaris.Polaris.SysHoldRequests shr
join polaris.polaris.Organizations o
	on o.OrganizationID = isnull(shr.NewPickupBranchID, shr.PickupBranchID)
where shr.SysHoldStatusID = 6
	and o.ParentOrganizationID = @pickupLibrary

DECLARE @nItemRecordID INT, 
        @nNotificationTypeID INT,
        @nPatronID INT,
        @nBranchID INT
 
DECLARE HoldNotices CURSOR FOR
SELECT 
       shr.TrappingItemRecordID,
       2,
       shr.PatronID, 
       shr.pickupBranchID
FROM Polaris.Polaris.SysHoldRequests shr
join polaris.polaris.Organizations o
	on o.OrganizationID = isnull(shr.NewPickupBranchID, shr.PickupBranchID)
WHERE shr.SysHoldStatusID = 6
AND shr.HoldNotificationDate IS NULL
and o.ParentOrganizationID = @pickupLibrary
and shr.TrappingItemRecordID is not null
 
OPEN HoldNotices
 
FETCH NEXT FROM HoldNotices INTO @nItemRecordID, @nNotificationTypeID, @nPatronID, @nBranchID
 
WHILE @@FETCH_STATUS = 0
BEGIN
       EXEC Polaris.Polaris.AddNotification @nItemRecordID, @nNotificationTypeID, @nPatronID, @nBranchID
       FETCH NEXT FROM HoldNotices INTO @nItemRecordID, @nNotificationTypeID, @nPatronID, @nBranchID
END
CLOSE HoldNotices
DEALLOCATE HoldNotices



/* 

select * from results.Polaris.NotificationQueue nq where nq.NotificationTypeID = 2 

*/