--handle sms.oplin.org adddresses - 6143703755@sms.oplin.org

if (OBJECT_ID('tempdb..#patrons') is null) 
begin
	create table #patrons ( patronid int, legacy_mobile varchar(255) )

	insert into #patrons
	select pr.PatronID, polaris.dbo.CLC_Custom_StripCharacters(split.Item)
	from Polaris.polaris.PatronRegistration pr
	join polaris.polaris.patrons p
		on p.PatronID = pr.PatronID
	cross apply Polaris.dbo.CLC_Custom_SplitString(pr.EmailAddress, '@') split
	where pr.EmailAddress like '%@sms.oplin.org' and split.ItemIndex = 0
end

begin tran

-- Patrons WITH a phone number that matches their legacy mobile number
update pr
set	DeliveryOptionID = 8
	,pr.Phone1CarrierID = case when d.NewTxtPhone = 1 then 23 else null end 
	,pr.Phone2CarrierID = case when d.NewTxtPhone = 2 then 23 else null end
	,pr.Phone3CarrierID = case when d.NewTxtPhone = 3 then 23 else null end
	,pr.TxtPhoneNumber = d.NewTxtPhone
	,EmailAddress = null

from (
select	pr.PatronID
		,pr.EmailAddress
		,p2.legacy_mobile
		,pr.PhoneVoice1
		,pr.PhoneVoice2
		,pr.PhoneVoice3
		,pr.TxtPhoneNumber
		,case p2.legacy_mobile 
			when Polaris.dbo.CLC_Custom_StripCharacters(pr.PhoneVoice1) then 1 
			when Polaris.dbo.CLC_Custom_StripCharacters(pr.PhoneVoice2) then 2 
			when Polaris.dbo.CLC_Custom_StripCharacters(pr.PhoneVoice3) then 3 
		end [NewTxtPhone]
from Polaris.polaris.PatronRegistration pr
join polaris.polaris.patrons p
	on p.PatronID = pr.PatronID
join #patrons p2
	on p2.patronid = p.PatronID
where len(p2.legacy_mobile) = 10
) d
join Polaris.polaris.PatronRegistration pr
	on pr.PatronID = d.PatronID
where d.NewTxtPhone is null

-- Patrons WITHOUT a phone number matching their legacy mobile number
update pr
set pr.TxtPhoneNumber = 3
	,pr.PhoneVoice3 = d.legacy_mobile
	,pr.Phone3CarrierID = 23
	,pr.EmailAddress = null
	,pr.DeliveryOptionID = 8
from (
select	pr.PatronID
		,pr.EmailAddress
		,p2.legacy_mobile
		,pr.PhoneVoice1
		,pr.PhoneVoice2
		,pr.PhoneVoice3
		,pr.TxtPhoneNumber
		,case p2.legacy_mobile 
			when Polaris.dbo.CLC_Custom_StripCharacters(pr.PhoneVoice1) then 1 
			when Polaris.dbo.CLC_Custom_StripCharacters(pr.PhoneVoice2) then 2 
			when Polaris.dbo.CLC_Custom_StripCharacters(pr.PhoneVoice3) then 3 
		end [NewTxtPhone]
from Polaris.polaris.PatronRegistration pr
join polaris.polaris.patrons p
	on p.PatronID = pr.PatronID
join #patrons p2
	on p2.patronid = p.PatronID
where len(p2.legacy_mobile) = 10
) d
join Polaris.polaris.PatronRegistration pr
	on pr.PatronID = d.PatronID
where d.NewTxtPhone is null

--rollback
--commit



-- PROBLEM EMAIL ADDRESSES 

--select pr.PatronID, pr.EmailAddress, polaris.dbo.CLC_Custom_StripCharacters(split.Item), pr.PhoneVoice1, pr.PhoneVoice2, pr.PhoneVoice3, Polaris.dbo.CLC_Custom_StripCharacters(pr.PhoneVoice1), len(split.Item)
--from Polaris.polaris.PatronRegistration pr
--join polaris.polaris.patrons p
--	on p.PatronID = pr.PatronID
--cross apply Polaris.dbo.CLC_Custom_SplitString(pr.EmailAddress, '@') split
--where pr.EmailAddress like '%@sms.oplin.org' and split.ItemIndex = 0 and len(polaris.dbo.CLC_Custom_StripCharacters(split.Item)) != 10
