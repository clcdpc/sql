/*

exec Polaris.Circ_UpdatePickupBranchID 30562469,81,2691,2915,81,33,@p7 output

CREATE PROCEDURE [Polaris].[Circ_UpdatePickupBranchID]
	@nSysHoldRequestID int,
	@nNewPickupBranchID int,
	@nUserID int,
	@nWorkstationID int,
	@nTxnBranchID int,
	@nSubSystemID int,	

*/

declare @statuses table ( id int )
declare @oldPickupLocations table ( orgid int )


--begin tran


insert into @statuses values (1),(3),(4),(5),(6)
insert into @oldPickupLocations values (18)

declare @newPickupBranchId int = 17


declare @logonBranchId int = null -- null means use old pickup branch

declare @polarisUserId int = 2691 -- a.mfields
declare @workstationId int = 2915 -- clcdevvm
declare @subSystemId int = 33 -- don't change



declare @requestId int
declare @oldPickupBranchId int
 
DECLARE HoldNotices CURSOR FOR
select shr.SysHoldRequestID, isnull(shr.NewPickupBranchID, shr.PickupBranchID)
from polaris.polaris.SysHoldRequests shr 
where isnull(shr.NewPickupBranchID, shr.PickupBranchID) in ( select * from @oldPickupLocations )
	and shr.SysHoldStatusID in ( select * from @statuses )
	and shr.Origin != 3
 
OPEN HoldNotices
 
FETCH NEXT FROM HoldNotices INTO @requestId, @oldPickupBranchId
 
WHILE @@FETCH_STATUS = 0
BEGIN
	declare @effectiveLogonBranchId int = isnull(@logonBranchId, @oldPickupBranchId)
    exec Polaris.Polaris.Circ_UpdatePickupBranchID @requestId,@newPickupBranchId,@polarisUserId,@workstationId,@effectiveLogonBranchId,@subSystemId,null
    FETCH NEXT FROM HoldNotices INTO @requestId, @oldPickupBranchId
END
CLOSE HoldNotices
DEALLOCATE HoldNotices

--rollback
--commit


--select * from polaris.Polaris.Organizations where ParentOrganizationID = 39


select * from Polaris.Polaris.SysHoldRequests shr 
join Polaris.polaris.SysHoldStatuses s
	on s.SysHoldStatusID = shr.SysHoldStatusID
where isnull(shr.NewPickupBranchID, shr.PickupBranchID) in ( select * from @oldPickupLocations )
	and shr.SysHoldStatusID in ( select * from @statuses )

