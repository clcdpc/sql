select nq.*, pr.EmailAddress, br.BrowseTitle, o.Name
from Results.polaris.NotificationQueue nq
join polaris.polaris.PatronRegistration pr
	on pr.PatronID = nq.PatronID
left join polaris.polaris.CircItemRecords cir
	on cir.ItemRecordID = nq.ItemRecordID
left join polaris.polaris.BibliographicRecords br
	on br.BibliographicRecordID = cir.AssociatedBibRecordID
join polaris.polaris.Organizations o
	on o.OrganizationID = nq.ReportingOrgID
where nq.DeliveryOptionID = 2
	and nq.ReportingOrgID in (3,5,7,9,10,11,12,13,15,17,18,20,21,22,24,26,27,29,30,31,33,34,35,36,37,38,75,76,79,80,81,82,83,85,87,89,91,93,94,95,96,97,98,100,101,102,103,104,106,107,108,109,110,111,113,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,138,139,140,141,142,143,144,145,150,151,152,153,154)
	and not exists ( 
		select 1 
		from PolarisTransactions.polaris.NotificationLog nl 
		where nl.PatronID = nq.PatronID 
			and nl.DeliveryOptionID = nq.DeliveryOptionID 
			and nl.NotificationTypeID = nq.NotificationTypeID 
			and nl.NotificationDateTime > nq.CreationDate 
			and nl.NotificationStatusID = 14 
		)
	and nq.CreationDate < (
		select msdb.dbo.agent_datetime(sjs.last_run_date,sjs.last_run_time)
		from msdb.dbo.sysjobs j
		join msdb.dbo.sysjobsteps sjs
			on sjs.job_id = j.job_id
		where j.job_id = '5C75D5B4-ED32-4281-88C7-AFC30FFCBA75'
			and sjs.step_id = 3
	)
order by nq.CreationDate

select top 100 p.Barcode, pr.PasswordHash, pr.ObfuscatedPassword
from polaris.polaris.patrons p
join polaris.polaris.PatronRegistration pr
	on pr.PatronID = p.PatronID
where p.Barcode = '2000234493'

/*
patronid, itemid, creationdate
1270799, *, * - spam failure in messagebee, no update in polaris log


*/

select * from polaris.polaris.NotificationStatuses

select * from polaris.polaris.PAPIUsers order by accessid

select top 100 * from polaris.dbo.CLC_Custom_First_Time_CKOs
select top 100 * from polaris.polaris.ItemRecordHistory

select * from polaris.polaris.NotificationTypes

select * 
from PolarisTransactions.polaris.NotificationLog nl 
where nl.NotificationDateTime > cast((getdate() - 5) as date) 
	and nl.DeliveryOptionID = 2
	and nl.NotificationStatusID = 14
	and nl.NotificationTypeID in (1,12,13)
	and nl.ReportingOrgID in (3,9,10,11,12,13,24,27,95,103,104,126,138,143,145,154)
order by nl.NotificationDateTime


select * 
from PolarisTransactions.polaris.NotificationLog nl 
where nl.NotificationDateTime > cast((getdate() - 3) as date)
	and nl.PatronID = 2552248
order by nl.NotificationDateTime

select *
from results.polaris.NotificationHistory nh
where nh.NoticeDate > cast((getdate() -3) as date)
	and nh.PatronId = 2552248
order by nh.NoticeDate

select * from results.polaris.NotificationQueue nq where nq.PatronID = 2735029


select datepart(hour, nl.NotificationDateTime) [hour]
		,datepart(minute, nl.NotificationDateTime) [minute]
		,count(*)
from PolarisTransactions.polaris.NotificationLog nl
where nl.DeliveryOptionID in (2,8)
	and	nl.Details like '%messagebee'
	and nl.NotificationDateTime > '1/31/2023'
group by datepart(hour, nl.NotificationDateTime), datepart(minute, nl.NotificationDateTime)
order by [hour], [minute]


select * from polaris.polaris.organizations