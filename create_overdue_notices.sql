declare @reportingOrgId int = 3
declare @deliveryOptionId int = 2
declare @notificationTypeId int = 1
declare @patronId int = 1752752

insert into Results.polaris.NotificationQueue (ItemRecordID,NotificationTypeID,PatronID,DeliveryOptionID,Processed,ReportingOrgID,IsAdditionalTxt)
SELECT
	ic.ItemRecordID, 
	@notificationTypeId, -- notification type
	pr.PatronID, 
	@deliveryOptionId, -- delivery option
	0, 
	@reportingOrgId, -- reporting branch,
	0
FROM 
	Polaris.ItemCheckouts ic
	inner join Polaris.patronregistration pr
		ON (ic.patronid = pr.patronid)		
	inner join Polaris.Patrons p
		ON (pr.PatronID = p.PatronID)
	inner join Polaris.CircReserveItemRecords_View ci
		ON (ic.ItemRecordID = ci.ItemRecordID)
	inner join Polaris.CircItemRecords cir
		ON (ic.ItemRecordID = cir.ItemRecordID)
WHERE
	pr.PatronID = @patronId


