select * from Results.Polaris.NotificationQueue nq where nq.NotificationTypeID = 8 and nq.ReportingOrgID between 40 and 74

select * from Polaris.Polaris.organizations

select top 100 * from PolarisTransactions.polaris.TransactionHeaders th where th.TransactionTypeID = 6001 order by th.TranClientDate desc

select * from PolarisTransactions.Polaris.TransactionSubTypes tst where tst.TransactionSubTypeDescription like '%leap%'
select * from PolarisTransactions.polaris.TransactionTypes tt where tt.TransactionTypeDescription like '%leap%'
select * 
from PolarisTransactions.Polaris.TransactionSubTypeCodes tstc 
join PolarisTransactions.polaris.TransactionSubTypes tst
	on tst.TransactionSubTypeID = tstc.TransactionSubTypeID
where tstc.TransactionSubTypeCodeDesc like '%leap%'


select tt.TransactionTypeID, tt.TransactionTypeDescription, count(*)
from PolarisTransactions.Polaris.TransactionHeaders th
join PolarisTransactions.Polaris.TransactionDetails td
	on td.TransactionID = th.TransactionID
join PolarisTransactions.Polaris.TransactionTypes tt
	on tt.TransactionTypeID = th.TransactionTypeID
where th.TranClientDate > '1/1/2021'
	and td.TransactionSubTypeID = 235
group by tt.TransactionTypeID, tt.TransactionTypeDescription

select * from PolarisTransactions.polaris.TransactionTypes tt order by tt.TransactionTypeDescription


-- ckis
select tstc.TransactionSubTypeCodeDesc, count(*)
from PolarisTransactions.Polaris.TransactionHeaders th
left join PolarisTransactions.polaris.TransactionDetails td
	on td.TransactionID = th.TransactionID and td.TransactionSubTypeID = 128
left join PolarisTransactions.Polaris.TransactionSubTypeCodes tstc
	on tstc.TransactionSubTypeID = td.TransactionSubTypeID and tstc.TransactionSubTypeCode = td.numValue
where th.TransactionTypeID = 6002
	and th.TranClientDate between '10/1/2021' and getdate()
group by tstc.TransactionSubTypeCodeDesc

-- ckos
select tstc.TransactionSubTypeCodeDesc, count(*)
from PolarisTransactions.Polaris.TransactionHeaders th
left join PolarisTransactions.polaris.TransactionDetails td
	on td.TransactionID = th.TransactionID and td.TransactionSubTypeID = 145
left join PolarisTransactions.Polaris.TransactionSubTypeCodes tstc
	on tstc.TransactionSubTypeID = td.TransactionSubTypeID and tstc.TransactionSubTypeCode = td.numValue
where th.TransactionTypeID = 6001
	and th.TranClientDate between '10/1/2021' and getdate()
group by tstc.TransactionSubTypeCodeDesc

-- logins
select tstc.TransactionSubTypeCodeDesc, count(*)
from PolarisTransactions.Polaris.TransactionHeaders th
left join PolarisTransactions.polaris.TransactionDetails td
	on td.TransactionID = th.TransactionID and td.TransactionSubTypeID = 235
left join PolarisTransactions.Polaris.TransactionSubTypeCodes tstc
	on tstc.TransactionSubTypeID = td.TransactionSubTypeID and tstc.TransactionSubTypeCode = td.numValue
where th.TransactionTypeID = 7200
	and th.TranClientDate between '1/1/2022' and getdate()
group by tstc.TransactionSubTypeCodeDesc

-- holds
select tstc.TransactionSubTypeCodeDesc, count(*)
from PolarisTransactions.Polaris.TransactionHeaders th
left join PolarisTransactions.polaris.TransactionDetails td
	on td.TransactionID = th.TransactionID and td.TransactionSubTypeID = 128
left join PolarisTransactions.Polaris.TransactionSubTypeCodes tstc
	on tstc.TransactionSubTypeID = td.TransactionSubTypeID and tstc.TransactionSubTypeCode = td.numValue
where th.TransactionTypeID = 6005
	and th.TranClientDate between '10/1/2021' and getdate()
group by tstc.TransactionSubTypeCodeDesc


select tt.TransactionTypeDescription, tstc.TransactionSubTypeCodeDesc, count(*)
from PolarisTransactions.Polaris.TransactionHeaders th
left join PolarisTransactions.polaris.TransactionDetails td
	on td.TransactionID = th.TransactionID and td.TransactionSubTypeID = 128
left join PolarisTransactions.Polaris.TransactionSubTypeCodes tstc
	on tstc.TransactionSubTypeID = td.TransactionSubTypeID and tstc.TransactionSubTypeCode = td.numValue
join PolarisTransactions.polaris.TransactionTypes tt
	on tt.TransactionTypeID = th.TransactionTypeID
where th.TranClientDate between '1/1/2022' and getdate()
group by tt.TransactionTypeDescription, tstc.TransactionSubTypeCodeDesc




select tt.TransactionTypeDescription, tstc.TransactionSubTypeCodeDesc, count(distinct th.TransactionID)
from PolarisTransactions.Polaris.TransactionHeaders th
join PolarisTransactions.Polaris.TransactionDetails td
	on td.TransactionID = th.TransactionID
join PolarisTransactions.Polaris.TransactionSubTypes tst
	on tst.TransactionSubTypeID = td.TransactionSubTypeID
join PolarisTransactions.Polaris.TransactionSubTypeCodes tstc
	on tstc.TransactionSubTypeID = tst.TransactionSubTypeID and tstc.TransactionSubTypeCode = td.numValue
join PolarisTransactions.Polaris.TransactionTypes tt
	on tt.TransactionTypeID = th.TransactionTypeID
where th.TranClientDate > '10/1/2021'
	and tstc.TransactionSubTypeCodeDesc like '%leap%'
group by tt.TransactionTypeDescription, tstc.TransactionSubTypeCodeDesc
order by tt.TransactionTypeDescription, tstc.TransactionSubTypeCodeDesc


select vtd.TransactionTypeDescription, vtd.TransactionSubTypeDescription, count(distinct vtd.TransactionID)
from PolarisTransactions.dbo.clc_custom_viewtransactiondetails vtd
where vtd.TranClientDate > '10/1/2021'
	and exists ( select 1 from PolarisTransactions.Polaris.TransactionSubTypeCodes tstc where tstc.TransactionSubTypeID = vtd.TransactionSubTypeID and tstc.TransactionSubTypeCodeDesc like '%leap%' )
group by vtd.TransactionTypeDescription, vtd.TransactionSubTypeDescription
order by vtd.TransactionTypeDescription, vtd.TransactionSubTypeDescription





select tstc.TransactionSubTypeCode, tstc.TransactionSubTypeCodeDesc, count(*)
from PolarisTransactions.Polaris.TransactionHeaders th
left join PolarisTransactions.polaris.TransactionDetails td
	on td.TransactionID = th.TransactionID and td.TransactionSubTypeID = 235
left join PolarisTransactions.Polaris.TransactionSubTypeCodes tstc
	on tstc.TransactionSubTypeID = td.TransactionSubTypeID and tstc.TransactionSubTypeCode = td.numValue
where th.TransactionTypeID = 7200
	and th.TranClientDate between '10/1/2021' and getdate()
group by tstc.TransactionSubTypeCode, tstc.TransactionSubTypeCodeDesc



select	o2.Name [Library]
		,o.Name [Branch]
		,w.ComputerName [Workstation]
		,count(case when td.numValue = 38 then 1 end) [LeapLogins]
		,count(case when td.numValue = 33 then 1 end) [StaffClientLogins]
		,count(th.TransactionID) [TotalLogins]
		,cast(cast(count(case when td.numValue = 38 then 1 end) / 1.0 / count(th.TransactionID) as decimal(18,2)) * 100 as int) [PercentLeap]
from PolarisTransactions.Polaris.TransactionHeaders th
join PolarisTransactions.polaris.TransactionDetails td
	on td.TransactionID = th.TransactionID and td.TransactionSubTypeID = 235
join PolarisTransactions.Polaris.TransactionSubTypeCodes tstc
	on tstc.TransactionSubTypeID = td.TransactionSubTypeID and tstc.TransactionSubTypeCode = td.numValue
join Polaris.Polaris.Workstations w
	on w.WorkstationID = th.WorkstationID
join Polaris.Polaris.Organizations o
	on o.OrganizationID = w.OrganizationID
join Polaris.polaris.Organizations o2
	on o2.OrganizationID = o.ParentOrganizationID
where th.TransactionTypeID = 7200
	and th.TranClientDate between '11/1/2021' and getdate()
	and w.WorkstationID in ( select distinct _th.WorkstationID from PolarisTransactions.Polaris.TransactionHeaders _th where _th.TransactionTypeID in (6001) and _th.TranClientDate > '10/1/2021' )
	--and exists ( select 1 from PolarisTransactions.Polaris.TransactionHeaders _th where _th.WorkstationID = th.WorkstationID and _th.TransactionTypeID in (6001,6002) and _th.TranClientDate > '10/1/2021' )
group by o2.Name, o.Name, w.ComputerName
order by [PercentLeap] desc






select	 pu.Name [StaffUser]
		,count(case when td.numValue = 38 then 1 end) [LeapLogins]
		,count(case when td.numValue = 33 then 1 end) [StaffClientLogins]
		,count(th.TransactionID) [TotalLogins]
		,cast(cast(count(case when td.numValue = 38 then 1 end) / 1.0 / count(th.TransactionID) as decimal(18,2)) * 100 as int) [PercentLeap]
from PolarisTransactions.Polaris.TransactionHeaders th
join PolarisTransactions.polaris.TransactionDetails td
	on td.TransactionID = th.TransactionID and td.TransactionSubTypeID = 235
join PolarisTransactions.Polaris.TransactionSubTypeCodes tstc
	on tstc.TransactionSubTypeID = td.TransactionSubTypeID and tstc.TransactionSubTypeCode = td.numValue
join Polaris.Polaris.Workstations w
	on w.WorkstationID = th.WorkstationID
join Polaris.Polaris.PolarisUsers pu
	on pu.PolarisUserID = th.PolarisUserID
join Polaris.Polaris.Organizations o
	on o.OrganizationID = w.OrganizationID
join Polaris.polaris.Organizations o2
	on o2.OrganizationID = o.ParentOrganizationID
where th.TransactionTypeID = 7200
	and th.TranClientDate between '10/1/2021' and getdate()
	and not exists (
		select 1
		from Polaris.dbo.CLC_Custom_PermissionGroups pg
		join Polaris.Polaris.GroupUsers gu
			on gu.GroupID = pg.GroupID
		where gu.PolarisUserID = pu.polarisusersid
			and pg.ControlRecordDefID = 49
			and pg.PermissionNameID = 4
	)
group by pu.Name
order by [PercentLeap] desc