select t.name [Column]
		,pm.class_desc
		,pm.permission_name
from sys.database_principals p
join sys.database_permissions pm
	on pm.grantee_principal_id = p.principal_id
join sys.tables t
	on t.object_id = pm.major_id
where p.name = 'clcdpc\author_subscription'