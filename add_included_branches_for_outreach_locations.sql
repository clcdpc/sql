declare @libraries table (libraryId int)
insert into @libraries values (16),(19),(32),(78)


begin tran

update b
set b.Included = 1
from polaris.polaris.SA_ORS_IncludedBranches b
join Polaris.polaris.Organizations o
	on o.OrganizationID = b.OrganizationID
join Polaris.polaris.Organizations o2
	on o2.OrganizationID = b.BranchID
where (o.ParentOrganizationID in (select * from @libraries) or o.OrganizationID in (select * from @libraries)) 
and b.BranchID not in ( 37  -- WL Old Worthington After Hours Pickup Locker
					   ,38 -- WL Northwest After Hours Pickup Locker
					   ,54 -- CML Operations Center
					   ,55 -- CML OSU Thompson
					   ,61 -- zzzdonotuseCMLTabletDispenser
					   ,64 -- CML ILL
					   ,66 -- CML Outreach
					   ,67 -- CML R2R
					   ,68 -- CML Parsons Drive-up Window
					   ,69 -- CML Whitehall Drive-up Window
					   ,70 -- CML Shepard Drive-up Window
					   ,71 -- CML Northern Lights Drive-up Window
					   ,73 -- CML New Albany Drive-up Window
					   ,74 -- CML R2R Bookmobile
					   ,82 -- UA Outreach
					   ,88 -- CML School Delivery
					   ,89 -- WL Materials Vending Lazelle
					   ,92 -- zzzdonotuse CML Local History and Genealogy
					   ,91 -- WL Mobile
					   ,94 -- WL Worthington Park After Hours Pickup Locker
					   ,95 -- Pickerington 24 Hour Pickup Lockers
					   ,96 -- WL Materials Vending Worthington Rec
					   ,97 -- WL Northwest Drive Up Window
					   ,98 -- PCL Circleville Middle School
					  ,100 -- WL Worthington Schools
					  ,101 -- SPLGC Mobile
					  ,102 -- SPLWL Mobile
					  ,104 -- Pickerington Sycamore Plaza 24 Hour Pickup Lockers
					  ,116 -- Delaware County Library Orange Drive-Up Window
					  ,140 -- MPL Trinity Lutheran School
					  ,141 -- MPL St. John's Lutheran School
					  ,142 -- UA Tremont Curbside Pickup
					  ,143 -- Pickerington Main Drive-Up Window
					  ,144 -- Wagnalls Pickup Locker
					  ,145 -- FCDL Main Lockers
					  ,146 -- CML Whetstone Curbside
					  ,147 -- CML Gahanna Curbside
					  ,148 -- CML Hilliard Curbside
					  ,149 -- CML Dublin Curbside
					  ,150 -- Grandview Hts Drive-Thru Pickup
					  ,151 -- MPL Raymond Branch Pickup Lockers
					  ,152 -- Marysville Public Library Pickup Lockers
					  ,153 -- Delaware County District Library Liberty Branch
					  ,117 -- Marysville Early College High School
					  ,118 -- Marysville High School
					  ,119 -- MPL Bunsold Middle School
					  ,120 -- MPL Creekview Intermediate
					  ,121 -- MPL Edgewood Elementary
					  ,122 -- MPL Mill Valley Elementary
					  ,123 -- MPL Navin Elementary
					  ,124 -- MPL Northwood Elementary
					  ,125 -- MPL Raymond Elementary
					  ,126 -- Fairfield County Millersport 24 Hour Library Kiosk
					  ,127 -- Plain City JA Junior High
					  ,128 -- Plain City JA High School
					  ,129 -- Plain City JA Canaan Middle School
					  ,130 -- Plain City Homebound
					  ,131 -- Plain City Pickup Lockers
					  ,132 -- zfuturePlain City JA High School4
					  ,133 -- zfuturePlain City JA High School5
					  ,134 -- zfuturePlain City JA High School6
					  ,135 -- zfuturePlain City JA High School7
					  ,136 -- zfuturePlain City JA High School8
					  ,137 -- CML-Columbus City Schools
					  ,138 -- Fairfield County Baltimore 24 Hour Pickup Lockers
					  ,139 -- CLC Test branch
) and b.Included = 0

--rollback
--commit
