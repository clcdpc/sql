USE [Polaris]
GO
/****** Object:  StoredProcedure [Polaris].[PAPI_PatronPreferencesGet]    Script Date: 1/11/2016 11:08:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [Polaris].[PAPI_PatronPreferencesGet]
    @nApplicationID INT,
	@nPatronID INT,
	@bReadingListEnabled BIT OUTPUT,
    @nDeliveryMethodID INT OUTPUT,
    @szDeliveryMethodDescription VARCHAR(32) OUTPUT,
    @nDeliveryEmailFormatID INT OUTPUT,
    @szDeliveryEmailFormatDescription VARCHAR(32) OUTPUT
AS

DECLARE @nResult INT
SELECT @nResult = 0

-- Check to see if patron exists --

IF NOT EXISTS(SELECT 1 FROM Patrons (NOLOCK) WHERE PatronID=@nPatronID)
BEGIN
	SELECT @nResult = -3000  -- Patron doesn't exist 
	RETURN @nResult
END

-- Reading List and Delivery Method --

SELECT 
	@bReadingListEnabled = ReadingList, 
	@nDeliveryMethodID = coalesce(pr.DeliveryOptionID, 0),
    @szDeliveryMethodDescription = coalesce(do.DeliveryOption, 'none'),
	@nDeliveryEmailFormatID = pr.EmailFormatID,
    @szDeliveryEmailFormatDescription = ef.EmailFormat
FROM 
	PatronRegistration pr (NOLOCK)
	left join DeliveryOptions do (NOLOCK) on
		pr.deliveryoptionid = do.deliveryoptionid
	join EmailFormat ef (NOLOCK) on
		pr.emailformatid = ef.emailformatid
WHERE 
	pr.PatronID = @nPatronID
RETURN @nResult
