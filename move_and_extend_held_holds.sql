declare @oldPickupBranchId int = 47
declare @newPickupBranchId int = 0  -- if left at zero holds will stay where they're at

declare @newHoldTillDate datetime = '10/22/2022' 

set @newHoldTillDate = dateadd(second, 86399, @newHoldTillDate)

declare @logonBranchId int = @oldPickupBranchId
	   ,@polarisUserId int = 2691 -- a.mfields
	   ,@workstationId int = 2915 -- clcdevvm
	   ,@requestId int			  -- ignore, cursor variables

declare @holds table ( SysHoldRequestID int, PatronID int, ItemRecordID int )
insert into @holds
select shr.SysHoldRequestID, shr.PatronID, shr.TrappingItemRecordID
from Polaris.polaris.SysHoldRequests shr
join Polaris.Polaris.Organizations o
	on isnull(shr.NewPickupBranchID, shr.PickupBranchID) = o.OrganizationID
where shr.HoldTillDate < @newHoldTillDate
	and shr.TrappingItemRecordID is not null
	and shr.SysHoldStatusID in (6,8)
	and o.OrganizationID = @oldPickupBranchId
	and shr.Origin != 3

begin tran

-- create item record set
insert into polaris.polaris.RecordSets (Name, ObjectTypeID, OrganizationOwnerID, CreatorID, CreationDate, RecordStatusID)
select 'clc hold extend script items ' + format(getdate(), 'yyyy-MM-dd HH:mm:ss'), 3, 1, @polarisUserId, getdate(), 1

declare @itemRecordSetID int = ( select @@identity )

insert into polaris.polaris.ItemRecordSets
select distinct ItemRecordID, @itemRecordSetID from @holds


-- create patron record set
insert into polaris.polaris.RecordSets (Name, ObjectTypeID, OrganizationOwnerID, CreatorID, CreationDate, RecordStatusID)
select 'clc hold extend script patrons ' + format(getdate(), 'yyyy-MM-dd HH:mm:ss'), 3, 1, @polarisUserId, getdate(), 1

declare @patronRecordSetID int = ( select @@identity )

insert into polaris.polaris.PatronRecordSets
select distinct @patronRecordSetID, PatronID from @holds

-- update HoldTillDate
update shr
set HoldTillDate = @newHoldTillDate
from polaris.polaris.SysHoldRequests shr
join @holds h
	on h.SysHoldRequestID = shr.SysHoldRequestID

-- change pickup location, if necessary
if (@newPickupBranchID > 0)
begin 
	DECLARE HoldNotices CURSOR FOR
	select h.SysHoldRequestID from @holds h

	OPEN HoldNotices
 
	FETCH NEXT FROM HoldNotices INTO @requestId
 
	WHILE @@FETCH_STATUS = 0
	BEGIN		
		exec Polaris.Polaris.Circ_UpdatePickupBranchID @requestId,@newPickupBranchId,@polarisUserId,@workstationId,@logonBranchId,33,null
		FETCH NEXT FROM HoldNotices INTO @requestId
	END
	CLOSE HoldNotices
	DEALLOCATE HoldNotices
end

--rollback
--commit

select * from @holds

select shr.*
from Polaris.polaris.SysHoldRequests shr
join Polaris.Polaris.Organizations o
	on isnull(shr.NewPickupBranchID, shr.PickupBranchID) = o.OrganizationID
where shr.HoldTillDate < @newHoldTillDate
	and shr.TrappingItemRecordID is not null
	and shr.SysHoldStatusID in (6,8)
	and o.OrganizationID = @oldPickupBranchId
	and shr.Origin != 3

select shr.*
from Polaris.polaris.SysHoldRequests shr
join Polaris.Polaris.Organizations o
	on isnull(shr.NewPickupBranchID, shr.PickupBranchID) = o.OrganizationID
where shr.HoldTillDate < @newHoldTillDate
	and shr.TrappingItemRecordID is not null
	and shr.SysHoldStatusID in (6,8)
	and o.OrganizationID = @newPickupBranchId
	and shr.Origin != 3

select * from polaris.polaris.itemrecordsets irs where irs.RecordSetID = @itemRecordSetID
select * from polaris.polaris.PatronRecordSets prs where prs.RecordSetID = @patronRecordSetID