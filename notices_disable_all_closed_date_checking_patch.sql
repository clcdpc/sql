USE [CLC_Notices]
GO

/****** Object:  View [dbo].[PolarisNotifications]    Script Date: 7/13/2018 3:31:53 PM ******/
DROP VIEW [dbo].[PolarisNotifications]
GO

/****** Object:  View [dbo].[PolarisNotifications]    Script Date: 7/13/2018 3:31:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[PolarisNotifications]
AS

with hoursofoperation(orgid, [hours]) as 
(
	select o.OrganizationID, isnull(opb.Value, opl.Value) [HoursOfOperation]
	from Polaris.Polaris.Organizations o
	left join Polaris.polaris.OrganizationsPPPP opb
		on opb.OrganizationID = o.OrganizationID and opb.AttrID = 96
	left join Polaris.polaris.OrganizationsPPPP opl
		on o.ParentOrganizationID = opl.OrganizationID and opl.AttrID = 96
	where o.OrganizationCodeID = 3
),
notices as (
	select nq.PatronID, nq.NotificationTypeID, nq.ItemRecordID, nq.DeliveryOptionID, nq.ReportingOrgID
	from results.Polaris.NotificationQueue nq
	union all
	select cr.PatronID, cr.NotificationTypeID, cr.ItemRecordID, cr.DeliveryOptionID, cr.ReportingOrgID
	from Results.polaris.CircReminders cr
)

select
	n.NotificationTypeID
	,p.PatronID
	,p.Barcode [PatronBarcode]
	,pr.NameFirst
	,pr.NameLast
	,a.StreetOne [PatronStreetOne]
	,a.StreetTwo [PatronStreetTwo]
	,pc.City [PatronCity]
	,pc.State [PatronState]
	,pc.PostalCode [PatronZip]
	,isnull(n.ItemRecordID, 0) [ItemRecordID]
	,p.OrganizationID [PatronBranch]
	,o_patron.Abbreviation [PatronBranchAbbr]
	,isnull(o_patron.ParentOrganizationID, 0) [PatronLibrary]
	,o_patron2.Abbreviation [PatronLibraryAbbr]
	,n.DeliveryOptionID
	,pr.EmailAddress
	,pr.AltEmailAddress
	,pr.EmailFormatID
	,case n.DeliveryOptionID
		when 3 then pr.PhoneVoice1
		when 4 then pr.PhoneVoice2
		when 5 then pr.PhoneVoice3
		when 8 then
			case pr.TxtPhoneNumber
				when 1 then pr.PhoneVoice1
				when 2 then pr.PhoneVoice2
				when 3 then pr.PhoneVoice3
				else ''
			end
		else ''
	end [DeliveryPhone]
	,isnull(n.ReportingOrgID,0) [ReportingBranchID]
	,o_notice.Abbreviation [ReportingBranchAbbr]	
	,o_notice.Name [ReportingBranchName]	
	,saa_notice.StreetOne [ReportingBranchStreetOne]
	,saa_notice.StreetTwo [ReportingBranchStreetTwo]
	,saa_notice.City [ReportingBranchCity]
	,saa_notice.State [ReportingBranchState]
	,saa_notice.PostalCode [ReportingBranchZip]
	,isnull(o_notice.ParentOrganizationID,0) [ReportingLibraryID]
	,coalesce(shr.HoldTillDate, ir.HoldTillDate) [HoldTillDate]
	,ic.DueDate [DueDate]
	,br.BrowseTitle [Title]
	,hoo.hours
	,isnull(l.AdminLanguageID, 1033) [PatronLanguage]
	,case pr.TxtPhoneNumber
		when 1 then pr.PhoneVoice1
		when 2 then pr.PhoneVoice2
		when 3 then pr.PhoneVoice3
		else ''
	end [TxtPhoneNumber]
from notices n
join Polaris.Polaris.Patrons p
	on n.PatronID = p.PatronID
join Polaris.Polaris.PatronRegistration pr
	on p.PatronID = pr.PatronID
join Polaris.Polaris.Organizations o_patron
	on p.OrganizationID = o_patron.OrganizationID
join Polaris.Polaris.Organizations o_patron2
	on o_patron.ParentOrganizationID = o_patron2.OrganizationID
join Polaris.Polaris.Organizations o_notice
	on n.ReportingOrgID = o_notice.OrganizationID
join polaris.polaris.OrganizationsPPPP op_notice_addr
	on op_notice_addr.OrganizationID = n.ReportingOrgID and op_notice_addr.AttrID = 55
join polaris.Polaris.SA_Addresses saa_notice
	on saa_notice.SA_AddressID = op_notice_addr.Value
left join polaris.polaris.CircItemRecords cir
	on cir.ItemRecordID = n.ItemRecordID
left join Polaris.Polaris.SysHoldRequests shr
	on shr.PatronID = p.PatronID and n.ItemRecordID = shr.TrappingItemRecordID
left join Polaris.polaris.ILLRequests ir
	on ir.PatronID = n.PatronID and ir.ItemRecordID = n.ItemRecordID
left join Polaris.polaris.BibliographicRecords br
	on br.BibliographicRecordID = cir.AssociatedBibRecordID
left join hoursofoperation hoo
	on hoo.orgid = n.ReportingOrgID
left join polaris.polaris.PatronAddresses pa
	on pa.PatronID = p.PatronID and pa.AddressTypeID = 2
left join polaris.Polaris.Addresses a
	on a.AddressID = pa.AddressID
left join Polaris.polaris.PostalCodes pc
	on pc.PostalCodeID = a.PostalCodeID
left join Polaris.polaris.ItemCheckouts ic
	on ic.PatronID = n.PatronID and ic.ItemRecordID = n.ItemRecordID
left join polaris.Polaris.Languages l
	on l.LanguageID = pr.LanguageID
where n.DeliveryOptionID in (2, 3, 4, 5) 
and n.NotificationTypeID in (1, 2, 10, 11, 12, 13) 

GO


