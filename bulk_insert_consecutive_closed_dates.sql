declare @orgid int = 79


declare @cal table ( thedate date )

insert into @cal
SELECT  TOP 500
        Date = DATEADD(DAY, ROW_NUMBER() OVER(ORDER BY a.object_id) - 1, getdate())
FROM    sys.all_objects a
        CROSS JOIN sys.all_objects b;


begin tran;insert into Polaris.Polaris.DatesClosed
select o.OrganizationID, c.thedate
from polaris.polaris.Organizations o
cross join @cal c
where not exists ( select 1 from Polaris.polaris.DatesClosed dc where dc.OrganizationID = o.OrganizationID and dc.DateClosed = c.thedate )
	and c.thedate between cast(getdate() as date) and '12/31/2020'
	and o.OrganizationID = @orgid
order by o.OrganizationID, c.thedate

--rollback
--commit

--select * from polaris.polaris.Organizations o where o.OrganizationCodeID = 2

select * from polaris.polaris.DatesClosed dc where dc.OrganizationID = @orgid and dc.DateClosed > '1/1/2020'