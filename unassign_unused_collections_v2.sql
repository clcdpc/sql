begin tran

delete oc
from polaris.polaris.organizationscollections oc
join polaris.polaris.Organizations o ON
	oc.OrganizationID = o.OrganizationID
join polaris.polaris.collections c ON
	oc.CollectionID = c.CollectionID
where oc.organizationid != 5
and (c.name like 'quick pick%' and (o.ParentOrganizationID != 39 and o.OrganizationID != 5) or c.Name not like 'quick pick%' )
and not EXISTS ( select 1 from polaris.polaris.CircItemRecords cir2
				 join polaris.polaris.Organizations o2 on 
					cir2.AssignedBranchID = o2.OrganizationID where o.ParentOrganizationID = o2.ParentOrganizationID and oc.CollectionID = cir2.AssignedCollectionID 
				)
and not EXISTS ( select 1 from polaris.polaris.ItemTemplates it where it.AssignedCollectionID = oc.CollectionID and it.AssignedBranchID = oc.OrganizationID )
and not EXISTS ( select 1 from polaris.polaris.TextualHoldingsNotes thn where thn.CollectionID = oc.CollectionID and thn.OrganizationID = oc.OrganizationID )
and not EXISTS ( select 1 from polaris.polaris.SHRCopies shrc where shrc.DestinationCollectionID = oc.CollectionID and shrc.OrganizationID = oc.OrganizationID )


--select oc.OrganizationID, oc.CollectionID, c.Name, count(cir.ItemRecordID)
--from polaris.polaris.OrganizationsCollections oc
--join polaris.polaris.collections c on
--	c.CollectionID = oc.CollectionID
--left join polaris.polaris.circitemrecords cir ON
--	c.CollectionID = cir.AssignedCollectionID and cir.AssignedBranchID = oc.OrganizationID
--group by oc.OrganizationID, oc.CollectionID, c.Name

--rollback
--commit
