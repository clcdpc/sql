declare @dummy int; -- Dummy statement to make code folding work correctly

begin -- Route setup
	declare @routes table ( orgid int, rte int)
	-- Route 1
	insert into @routes values (49, 1) -- CML Main
	insert into @routes values (57, 1) -- CML Reynoldsburg
	insert into @routes values(103, 1) -- Pickerington Sycamore
	insert into @routes values (11, 1) -- FCDL Baltimore
	insert into @routes values (24, 1) -- Pickerington
	insert into @routes values (26, 1) -- Wagnalls
	insert into @routes values (99, 1) -- CML Canal Winchester
	insert into @routes values (60, 1) -- CML Southeast
	-- Route 2
	insert into @routes values ( 3, 2) -- Alexandria
	insert into @routes values (51, 2) -- CML New Albany
	insert into @routes values (47, 2) -- CML Linden
	insert into @routes values (53, 2) -- CML Northside
	insert into @routes values (62, 2) -- CML Whetstone
	insert into @routes values (34, 2) -- Old Worthington
	insert into @routes values (35, 2) -- Worthington Park
	insert into @routes values (46, 2) -- CML Karl Road
	insert into @routes values (52, 2) -- CML Northern Lights
	-- Route 3
	insert into @routes values (58, 3) -- CML Shepard
	insert into @routes values (22, 3) -- PCL Younkin
	insert into @routes values (20, 3) -- PCL Main
	insert into @routes values (10, 3) -- FCDL Johns
	insert into @routes values ( 9, 3) -- FCDL Main
	insert into @routes values (12, 3) -- FCDL Bremen
	insert into @routes values (13, 3) -- FCDL Northwest
	-- Route 4
	insert into @routes values (43, 4) -- CML Gahanna
	insert into @routes values (63, 4) -- CML Whitehall
	insert into @routes values (48, 4) -- CML Livingston
	insert into @routes values (40, 4) -- CML Driving Park
	insert into @routes values (50, 4) -- CML MLK
	insert into @routes values (44, 4) -- CML Hilliard
	insert into @routes values (80, 4) -- UA Lane
	insert into @routes values (81, 4) -- UA Tremont 
	insert into @routes values (79, 4) -- UA Miller Park
	-- Route 5
	insert into @routes values (42, 5) -- CML Franklinton
	insert into @routes values (45,	5) -- CML Hilltop
	insert into @routes values (31, 5) -- CML Westland
	insert into @routes values (29, 5) -- SWPL Grove City
	insert into @routes values (59, 5) -- CML South High
	insert into @routes values (77,	5) -- CML Marion Franklin
	insert into @routes values (56, 5) -- CML Parsons
	insert into @routes values (85, 5) -- Bexley
	-- Route 6
	insert into @routes values (33, 6) -- WL Northwest
	insert into @routes values (41, 6) -- CML Dublin
	insert into @routes values (17, 6) -- Marysville Main
	insert into @routes values (15, 6) -- Plain City
	insert into @routes values (87, 6) -- London
	insert into @routes values ( 7, 6) -- Grandview

	-- Special additions
	insert into @routes values (95, (select rte from @routes where orgid = 24)) -- Add Pickerington Pickup Locker to the same route as Pickerington Library to ensure they get the proper responders added	
	insert into @routes values (104, (select rte from @routes where orgid = 103)) -- Add Sycamore Pickup Locker to the same route as Sycamore Library to ensure they get the proper responders added	
end

begin -- CML branch orders
	declare @cml table ( orgid int, seq int, q int )
	insert into @cml values(47, 1 , 1)	-- CML Linden Branch
	insert into @cml values(42, 2 , 1)	-- CML Franklinton Branch
	insert into @cml values(63, 3 , 1)	-- CML Whitehall Branch
	insert into @cml values(48, 4 , 1)	-- CML Livingston Branch
	insert into @cml values(44, 5 , 1)	-- CML Hilliard Branch
	insert into @cml values(57, 6 , 1)	-- CML Reynoldsburg Branch
	insert into @cml values(45, 7 , 1)	-- CML Hilltop Branch
	insert into @cml values(56, 8 , 1)	-- CML Parsons Branch
	insert into @cml values(50, 9 , 1)	-- CML Martin Luther King Branch
	insert into @cml values(40, 10, 1)	-- CML Driving Park Branch
	insert into @cml values(58, 11, 1)	-- CML Shepard Branch
	insert into @cml values(59, 12, 1)	-- CML South High Branch
	insert into @cml values(52, 13, 1)	-- CML Northern Lights Branch
	insert into @cml values(53, 14, 1)	-- CML Northside Branch
	insert into @cml values(99, 15, 1)	-- CML Canal Winchester Branch
	insert into @cml values(62, 16, 1)	-- CML Whetstone Branch
	insert into @cml values(43, 17, 1)	-- CML Gahanna Branch
	insert into @cml values(46, 18, 1)	-- CML Karl Road Branch
	insert into @cml values(41, 19, 1)	-- CML Dublin Branch
	insert into @cml values(51, 20, 1)	-- CML New Albany Branch
	insert into @cml values(60, 21, 1)	-- CML Southeast Branch
	insert into @cml values(49, 22, 1)	-- CML Columbus Main Library
	insert into @cml values(77, 23, 1)	-- CML Marion-Franklin Branch
	insert into @cml values(65, 24, 1)	-- CML Tech Services
	insert into @cml values(54, 25, 1)	-- CML Operations Center
	insert into @cml values(64, 26, 1)	-- CML ILL

	-- Queue 2
	insert into @cml values(56, 1 , 2)	-- CML Parsons Branch
	insert into @cml values(40, 2 , 2)	-- CML Driving Park Branch
	insert into @cml values(59, 3 , 2)	-- CML South High Branch
	insert into @cml values(53, 4 , 2)	-- CML Northside Branch
	insert into @cml values(41, 5 , 2)	-- CML Dublin Branch
	insert into @cml values(51, 6 , 2)	-- CML New Albany Branch
	insert into @cml values(60, 7 , 2)	-- CML Southeast Branch
	insert into @cml values(50, 8 , 2)	-- CML Martin Luther King Branch
	insert into @cml values(47, 9 , 2)	-- CML Linden Branch
	insert into @cml values(58, 10, 2)	-- CML Shepard Branch
	insert into @cml values(42, 11, 2)	-- CML Franklinton Branch
	insert into @cml values(48, 12, 2)	-- CML Livingston Branch
	insert into @cml values(63, 13, 2)	-- CML Whitehall Branch
	insert into @cml values(52, 14, 2)	-- CML Northern Lights Branch
	insert into @cml values(77, 15, 2)	-- CML Marion-Franklin Branch
	insert into @cml values(49, 16, 2)	-- CML Columbus Main Library
	insert into @cml values(44, 17, 2)	-- CML Hilliard Branch
	insert into @cml values(57, 18, 2)	-- CML Reynoldsburg Branch
	insert into @cml values(45, 19, 2)	-- CML Hilltop Branch
	insert into @cml values(62, 20, 2)	-- CML Whetstone Branch
	insert into @cml values(43, 21, 2)	-- CML Gahanna Branch
	insert into @cml values(46, 22, 2)	-- CML Karl Road Branch
	insert into @cml values(99, 23, 2)	-- CML Canal Winchester Branch
	insert into @cml values(65, 24, 2)	-- CML Tech Services
	insert into @cml values(54, 25, 2)	-- CML Operations Center
	insert into @cml values(64, 26, 2)	-- CML ILL

	-- Queue 3
	insert into @cml values(50, 1 , 3)	-- CML Martin Luther King Branch
	insert into @cml values(58, 2 , 3)	-- CML Shepard Branch
	insert into @cml values(52, 3 , 3)	-- CML Northern Lights Branch
	insert into @cml values(62, 4 , 3)	-- CML Whetstone Branch
	insert into @cml values(43, 5 , 3)	-- CML Gahanna Branch
	insert into @cml values(46, 6 , 3)	-- CML Karl Road Branch
	insert into @cml values(49, 7 , 3)	-- CML Columbus Main Library
	insert into @cml values(63, 8 , 3)	-- CML Whitehall Branch
	insert into @cml values(47, 9 , 3)	-- CML Linden Branch
	insert into @cml values(56, 10, 3)	-- CML Parsons Branch
	insert into @cml values(42, 11, 3)	-- CML Franklinton Branch
	insert into @cml values(40, 12, 3)	-- CML Driving Park Branch
	insert into @cml values(53, 13, 3)	-- CML Northside Branch
	insert into @cml values(48, 14, 3)	-- CML Livingston Branch
	insert into @cml values(59, 15, 3)	-- CML South High Branch
	insert into @cml values(41, 16, 3)	-- CML Dublin Branch
	insert into @cml values(51, 17, 3)	-- CML New Albany Branch
	insert into @cml values(60, 18, 3)	-- CML Southeast Branch
	insert into @cml values(99, 19, 3)	-- CML Canal Winchester Branch
	insert into @cml values(77, 20, 3)	-- CML Marion-Franklin Branch
	insert into @cml values(44, 21, 3)	-- CML Hilliard Branch
	insert into @cml values(57, 22, 3)	-- CML Reynoldsburg Branch
	insert into @cml values(45, 23, 3)	-- CML Hilltop Branch
	insert into @cml values(65, 24, 3)	-- CML Tech Services
	insert into @cml values(54, 25, 3)	-- CML Operations Center
	insert into @cml values(64, 26, 3)	-- CML ILL

end

begin -- CML branch order 'queue' setup
	declare @queues table ( orgid int, q int )
	-- Queue1
	insert into @queues values (17, 1)
	insert into @queues values (21, 1)
	insert into @queues values (22, 1)
	insert into @queues values (24, 1)
	insert into @queues values (30, 1)
	insert into @queues values (42, 1)
	insert into @queues values (44, 1)
	insert into @queues values (45, 1)
	insert into @queues values (47, 1)
	insert into @queues values (48, 1)
	insert into @queues values (55, 1)
	insert into @queues values (57, 1)
	insert into @queues values (63, 1)
	insert into @queues values (65, 1)
	insert into @queues values (66, 1)
	insert into @queues values (67, 1)
	insert into @queues values (69, 1)
	insert into @queues values (74, 1)
	insert into @queues values (80, 1)
	insert into @queues values (81, 1)
	insert into @queues values (82, 1)
	insert into @queues values (87, 1)
	insert into @queues values (92, 1)
	insert into @queues values (97, 1)
	insert into @queues values (99, 1)
	-- Queue 2
	insert into @queues values (9 , 2)
	insert into @queues values (10, 2)
	insert into @queues values (13, 2)
	insert into @queues values (18, 2)
	insert into @queues values (33, 2)
	insert into @queues values (34, 2)
	insert into @queues values (35, 2)
	insert into @queues values (36, 2)
	insert into @queues values (37, 2)
	insert into @queues values (38, 2)
	insert into @queues values (40, 2)
	insert into @queues values (41, 2)
	insert into @queues values (51, 2)
	insert into @queues values (53, 2)
	insert into @queues values (56, 2)
	insert into @queues values (59, 2)
	insert into @queues values (60, 2)
	insert into @queues values (68, 2)
	insert into @queues values (73, 2)
	insert into @queues values (76, 2)
	insert into @queues values (77, 2)
	insert into @queues values (88, 2)
	-- Queue 3
	insert into @queues values (3 , 3)
	insert into @queues values (7 , 3)
	insert into @queues values (11, 3)
	insert into @queues values (12, 3)
	insert into @queues values (15, 3)
	insert into @queues values (20, 3)
	insert into @queues values (26, 3)
	insert into @queues values (29, 3)
	insert into @queues values (31, 3)
	insert into @queues values (43, 3)
	insert into @queues values (46, 3)
	insert into @queues values (49, 3)
	insert into @queues values (50, 3)
	insert into @queues values (52, 3)
	insert into @queues values (54, 3)
	insert into @queues values (58, 3)
	insert into @queues values (62, 3)
	insert into @queues values (75, 3)
	insert into @queues values (79, 3)
	insert into @queues values (83, 3)
	insert into @queues values (85, 3)
	insert into @queues values (94, 3)
	insert into @queues values (95, 3)
	insert into @queues values (96, 3)
end

begin -- Self responder only setup
	declare @self_responders table ( orgid int )
	insert into @self_responders values (21),	-- PCL Outreach
										(27),	-- FCDL Outreach Services
										(30),	-- SPL Outreach
										(36),	-- WL Outreach
										(66),	-- CML Outreach
										(67),	-- CML R2R
										(74),	-- CML R2R Bookmobile
										(82),	-- UA Outreach
										(83),	-- UA Technical Services
										(93),	-- GHPL Book Cart
										(95),	-- Pickerington 24 Hour Pickup Lockers
										(101),	-- SPLGC Mobile
										(102)	-- SPLWL Mobile
end

begin -- Excluded branches setup
	declare @exclude table ( orgid int )
	insert into @exclude values (37),	-- WL Old Worthington After Hours Pickup Locker
								(38),	-- WL Northwest After Hours Pickup Locker
								(55),	-- CML OSU Thompson
								(61),	-- zzzdonotuseCML Weinland Branch
								(68),	-- CML Parsons Drive-Up Window
								(69),	-- CML Whitehall Drive-up Window
								(70),	-- ZZZ CML Do Not Use Formerly R2R Northland
								(71),	-- ZZZ CML Do Not Use Formerly R2R Hilltop
								(72),	-- ZZZ CML Do Not Use Formerly R2R Whitehall
								(73),	-- CML New Albany Drive-up Window
								(88),	-- CML School Delivery
								(89),	-- WL Materials Vending Lazelle
								(91),	-- WL Mobile
								(92),	-- CML Local History and Genealogy
								(94),	-- WL Worthington Park After Hours Pickup Locker
								(95),	-- Pickerington 24 Hour Pickup Lockers
								(96),	-- WL Materials Vending Worthington Rec
								(97),	-- WL Northwest Drive Up Window
								(98),	-- PCL Circleville Middle School
								(100),	-- WL Device Lending Old Worthington
								(104)	-- Pickerington Sycamore Plaza 24 Hour Pickup Lockers
end

begin -- Special branch orders to make locations first for their alternative pickup locations
	declare @special_orders table ( orgid int, responder int, seq int )
	insert into @special_orders values ( 94, 35, -98 ) -- Worthington Park as #1 for their pickup Locker
	insert into @special_orders values ( 97, 33, -98 ) -- Northwest as #1 for their drive up window
	insert into @special_orders values ( 38, 33, -98 ) -- Northwest as #1 for their pickup locker
	insert into @special_orders values ( 37, 34, -98 ) -- Old Worthington as #1 for their pickup locker
	insert into @special_orders values ( 95, 24, -98 ) -- Pickerington as #1 for their pickup locker
	insert into @special_orders values (104,103, -98 ) -- Sycamore as #1 for their pickup locker
end

begin -- MaxDaysInRTF exceptions for small branches
	declare @maxdays table ( orgid int, maxdays int )
	insert into @maxdays values (3, 1)	-- Alexandria Public Library
	insert into @maxdays values (15, 1) -- Plain City Public Library
end

declare @table table ( requester int, requestername varchar(255), responder int, respondername varchar(255), seq int, maxdays int, flag int )

insert into @table
select	o.OrganizationID [Requester ID], 
		o.Name [Requester Name],
		o2.OrganizationID [Responder ID],
		o2.Name [Responder Name],
		case 
			when o.OrganizationID = o2.OrganizationID then -99
			when o2.OrganizationID = 92 then 99
			when o2.ParentOrganizationID = 39 or (o.ParentOrganizationID = 39 and o2.ParentOrganizationID = 39) then COALESCE((select seq from @cml cml2 where cml2.orgid = data.orgid and cml2.q = (select q from @queues where [@queues].orgid = o.OrganizationID)),99)	
			when o.ParentOrganizationID = o2.ParentOrganizationID then 
				case 
					when ((select COUNT(*) from @special_orders so where so.orgid = o.OrganizationID and so.responder = o2.OrganizationID) > 0) then (select seq from @special_orders so where so.orgid = o.OrganizationID and so.responder = o2.OrganizationID)
					when o2.name like '%main%' or o2.name like '%tremont%' then -2 					
					else -1 
				end									
			else 99 
		end [_sequence],
		case when o.OrganizationID = o2.OrganizationID then 5 else COALESCE(m.maxdays,5) end [MaxDaysInRTF],
		1 [PrimarySecondaryFlag]
from polaris.Polaris.Organizations o
cross join 
(
	select distinct COALESCE(r.orgid, c.orgid, o3.OrganizationID) [orgid], o3.ParentOrganizationID [library], r.rte, case when o3.ParentOrganizationID = 39 then 1 else 0 end [is_cml]
	from Polaris.Polaris.Organizations o3
	left join @routes r on
		o3.OrganizationID = r.orgid
	full outer join @cml c on
		r.orgid = c.orgid
	where o3.OrganizationCodeID = 3
) data
join Polaris.Polaris.Organizations o2 on
	data.orgid = o2.OrganizationID
left join @maxdays m on
	o2.OrganizationID = m.orgid
where (data.is_cml = 1 or data.rte = (select rte from @routes r where r.orgid = o.OrganizationID) or o2.ParentOrganizationID = o.ParentOrganizationID) and o.OrganizationCodeID = 3
order by o.OrganizationID, _sequence

-- create secondary queue
insert into @table
select	o.OrganizationID
		,o.Name
		,o2.OrganizationID
		,o2.Name
		,rank() over(partition by o.OrganizationID order by o2.OrganizationID)
		,case when o.OrganizationID = o2.OrganizationID then 5 else COALESCE(m.maxdays,5) end
		,2
from polaris.polaris.Organizations o
cross join Polaris.polaris.organizations o2
left join @maxdays m
	on o2.OrganizationID = m.orgid
where o.OrganizationCodeID = 3 and o2.OrganizationCodeID = 3 --and o2.OrganizationID not in ( select * from @exclude )

-- delete only self-respondes from other branches
delete t
from @table t
join @self_responders e on
	t.responder = e.orgid
where t.requester != e.orgid and t.flag = 1

-- remove responders that should not be in the rtf
delete from @table
where responder in ( select * from @exclude )


--begin tran; delete from Polaris.Polaris.SysHoldRoutingSequences; insert into Polaris.Polaris.SysHoldRoutingSequences
select	t.requester, 
		t.responder, 
		ROW_NUMBER() over(partition by t.flag, t.requester order by t.seq, t.respondername) [sortorder], 
		t.maxdays, 
		t.flag
from @table t
order by t.flag, t.requester, t.seq

--rollback
--commit

--select * from polaris.polaris.SysHoldRoutingSequences s where s.PrimarySecondaryFlag = 1 order by s.RequesterBranchID, s.Sequence