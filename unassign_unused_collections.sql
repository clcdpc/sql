begin tran

delete oc
from polaris.polaris.organizationscollections oc
join polaris.polaris.collections c ON
	oc.CollectionID = c.CollectionID
where oc.organizationid != 5 and c.name not like 'quick pick%' 
and not EXISTS ( select 1 from polaris.polaris.CircItemRecords cir where oc.OrganizationID = cir.AssignedBranchID and oc.CollectionID = cir.AssignedCollectionID )
and not EXISTS ( select 1 from polaris.polaris.ItemTemplates it where it.AssignedCollectionID = oc.CollectionID and it.AssignedBranchID = oc.OrganizationID )
and not EXISTS ( select 1 from polaris.polaris.TextualHoldingsNotes thn where thn.CollectionID = oc.CollectionID and thn.OrganizationID = oc.OrganizationID )
and not EXISTS ( select 1 from polaris.polaris.SHRCopies shrc where shrc.DestinationCollectionID = oc.CollectionID and shrc.OrganizationID = oc.OrganizationID )

-- test version
delete oc
from polaris.polaris.organizationscollections oc
join polaris.polaris.Organizations o ON
	oc.OrganizationID = o.OrganizationID
join polaris.polaris.collections c ON
	oc.CollectionID = c.CollectionID
where oc.organizationid != 5
and (c.name like 'quick pick%' and (o.ParentOrganizationID != 39 and o.OrganizationID != 5) or c.Name not like 'quick pick%' )
and not EXISTS ( select 1 from polaris.polaris.CircItemRecords cir where oc.OrganizationID = cir.AssignedBranchID and oc.CollectionID = cir.AssignedCollectionID )
and not EXISTS ( select 1 from polaris.polaris.ItemTemplates it where it.AssignedCollectionID = oc.CollectionID and it.AssignedBranchID = oc.OrganizationID )
and not EXISTS ( select 1 from polaris.polaris.TextualHoldingsNotes thn where thn.CollectionID = oc.CollectionID and thn.OrganizationID = oc.OrganizationID )
and not EXISTS ( select 1 from polaris.polaris.SHRCopies shrc where shrc.DestinationCollectionID = oc.CollectionID and shrc.OrganizationID = oc.OrganizationID )


delete oc
from polaris.polaris.OrganizationsCollections oc
join polaris.polaris.collections c on
	oc.CollectionID = c.CollectionID
join polaris.polaris.organizations o on
	oc.OrganizationID = o.OrganizationID
where c.name like 'quick pick%' and (o.ParentOrganizationID != 39 or oc.OrganizationID != 5) 
and not exists (select 1 from polaris.polaris.circitemrecords cir where cir.AssignedBranchID = oc.OrganizationID and cir.AssignedCollectionID = oc.CollectionID )
and not exists (select 1 from polaris.polaris.ItemTemplates it where it.OrganizationOwnerID = oc.OrganizationID and it.AssignedCollectionID = oc.CollectionID )


select distinct o.ParentOrganizationID--, oc.CollectionID, c.Name
from polaris.polaris.OrganizationsCollections oc
join polaris.polaris.collections c on
	oc.CollectionID = c.CollectionID
join polaris.polaris.organizations o on
	oc.OrganizationID = o.OrganizationID
where c.name like 'quick pick%' and (o.ParentOrganizationID = 39 or oc.OrganizationID = 5)


--rollback
--commit




--44781



select count(*)
from polaris.polaris.OrganizationsCollections oc
where exists ( select 1 from polaris.polaris.circitemrecords cir where cir.AssignedBranchID = oc.OrganizationID and cir.AssignedCollectionID = oc.CollectionID )