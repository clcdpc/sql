if (OBJECT_ID('tempdb..#bibs') is null) 
begin
	create table #bibs ( bibid int, rating varchar(max), raw521a varchar(max))

	;with fixed521 as (
		select bt.BibliographicRecordID, bs.Data [raw521], case when bs.data like '%not rated%' or bs.data like '%unrated%' or bs.data like '%un rated%' or bs.data like '%un-rated%' then 'not rated' else replace(replace(bs.Data, 'rated', 'rating'), ':', '') end [fixed521]
		from polaris.polaris.BibliographicTags bt
		join polaris.polaris.BibliographicSubfields bs
			on bs.BibliographicTagID = bt.BibliographicTagID
		where bt.TagNumber = 521
		and bs.Subfield = 'a'
		and bt.BibliographicRecordID in (
				select br.BibliographicRecordID
				from polaris.Polaris.BibliographicRecords br
				join polaris.polaris.CircItemRecords cir
					on cir.AssociatedBibRecordID = br.BibliographicRecordID
				where br.PrimaryMARCTOMID in ( 33, 40 )
				group by br.BibliographicRecordID
				having min(cir.AssignedBranchID) > 104
		)
		and (bs.data like '%rating%' or bs.data like '%rated%' or bs.Data like 'preschool%')
	), ratings as (
		select *, case when f.fixed521 != 'not rated' and f.fixed521 like '%rating%' then replace(replace(replace(RIGHT(f.fixed521,CHARINDEX('gnitar',REVERSE(f.fixed521),0)-1), '-', ''), '.', ''), ' ', '') else f.fixed521 end [Rating]
		from fixed521 f
	), fixedratings as (
		select *,
			case
				when r.Rating like 'g%' or r.rating like 'tv[yg]%' or r.rating like 'preschool%' or r.Rating like '3up%' then 'G'
				when r.Rating like 'pg13%' or r.rating like '1[34]%' or r.Rating like 'tv14%' then 'PG13'
				when r.Rating like 'PG%' or r.rating like 'tvpg%' or r.Rating like 'tvma%' then 'PG'
				when (r.rating like 'r%' and r.rating not like 'recc%') or r.rating like '1[68]%' then 'R'
				when r.Rating = 'not rated' or r.rating like 'ur%' or r.rating like 'nr%' or r.rating like 'unk%' then 'Not Rated'
				else 'Other'
			end [FixedRating]
		from ratings r
	)

	insert into #bibs
	select r.BibliographicRecordID, r.FixedRating, r.raw521 from fixedratings r

	--select bt.BibliographicRecordID, case when bs.data like 'rated%' then ltrim(rtrim(replace(replace(bs.Data, 'Rated', ''), '.', ''))) else ltrim(rtrim(split2.Item)) end [Rating], bs.Data
	--from polaris.polaris.BibliographicTags bt
	--join polaris.polaris.BibliographicSubfields bs
	--	on bs.BibliographicTagID = bt.BibliographicTagID
	--cross apply Polaris.dbo.CLC_Custom_SplitString(bs.Data, ':') split1
	--cross apply Polaris.dbo.CLC_Custom_SplitString(split1.Item, '.') split2
	--where bt.TagNumber = 521
	--	and bs.Subfield = 'a'
	--	and bt.BibliographicRecordID in (
	--		select br.BibliographicRecordID
	--		from polaris.Polaris.BibliographicRecords br
	--		join polaris.polaris.CircItemRecords cir
	--			on cir.AssociatedBibRecordID = br.BibliographicRecordID
	--		where br.PrimaryMARCTOMID in ( 33, 40 )
	--		group by br.BibliographicRecordID
	--		having min(cir.AssignedBranchID) > 104
	--	)
	--	and (split1.ItemIndex = 1 or bs.data like 'rated%' or bs.Data = 'preschool' or bs.data = 'preschool.')
	--	and (split2.ItemIndex = 0 or bs.data like 'rated%' or bs.Data = 'preschool' or bs.data = 'preschool.')
	--	and (bs.data like '%rating%' or bs.data like '%rated%' or bs.Data = 'preschool' or bs.data = 'preschool.')
end

--select b.rating, count(*)
--from #bibs b
--group by b.rating
--order by count(*) desc

--select data.RecordSetID, count(distinct data.bibid)
--from (
--	select *, 
--		case 
--			when b.rating = 'g' then 122992 
--			when b.rating = 'pg13' then 122993
--			when b.rating = 'pg' then 122994
--			when b.rating = 'r' then 122995
--			when b.rating = 'not rated' then 122996
--			else 122997
--		end [RecordSetID]
--	from #bibs b 
--) data
--group by data.RecordSetID


begin tran

delete from polaris.polaris.BibRecordSets where RecordSetID in ( 122992,122993,122994,122995,122996,122997 )


insert into polaris.polaris.BibRecordSets
select * 
from (
	select distinct b.bibid, 
	case 
		when b.rating = 'g' then 122992 
		when b.rating = 'pg13' then 122993
		when b.rating = 'pg' then 122994
		when b.rating = 'r' then 122995
		when b.rating = 'not rated' then 122996
		else 122997
	end [RecordSetID]
	from #bibs b
) tmp

--select distinct b.bibid, 
--	case 
--		when b.rating = 'g' then 122992 
--		when b.rating = 'pg13' then 122993
--		when b.rating = 'pg' then 122994
--		when b.rating = 'r' then 122995
--		when b.rating = 'not rated' then 122996
--		else 122997
--	end [RecordSetID]
--from #bibs b


--select b.bibid, b.rating, count(*)
--from #bibs b
--group by b.bibid, b.rating
--order by count(*) desc

-- Other = 122997
insert into Polaris.polaris.BibRecordSets
select br.BibliographicRecordID, 122997
from polaris.Polaris.BibliographicRecords br
join polaris.polaris.CircItemRecords cir
	on cir.AssociatedBibRecordID = br.BibliographicRecordID
where br.PrimaryMARCTOMID in ( 33, 40 ) and br.BibliographicRecordID not in ( select distinct brs.BibliographicRecordID from polaris.polaris.BibRecordSets brs where brs.RecordSetID in ( 122992,122993,122994,122995,122996,122997 ) )
group by br.BibliographicRecordID
having min(cir.AssignedBranchID) > 104

-- Video G / TV-G / TV-Y / TV-Y7 / TV-Y7 FV = 122992
--insert into polaris.polaris.BibRecordSets
--select distinct b.bibid, 122992
--from #bibs b
--where b.rating like 'g%' or b.rating = 'tv-g' or b.rating = 'tv-7' or b.rating like 'tv-y%' or b.rating = 'tvy' or b.rating = 'tvy7' or b.rating like '3%' or b.rating = 'tv g' or b.rating = 'rated g' or b.rating = 'tvg' or b.rating like '%rated 3up%' or b.rating like '%preschool%'


---- Video PG-13 / TV-14 = 122993
--insert into polaris.polaris.BibRecordSets
--select distinct b.bibid, 122993
--from #bibs b
--where b.rating like 'pg-13%' or b.rating like 'pg13%' or b.rating = 'tv 14' or b.rating like 'tv-14%' or b.rating like 'tv14%' or b.rating like '14%' or b.rating like '13%' or b.rating = 'rated pg-13%' or b.rating like 'rated pg13%' or b.rating like 'Rated tv14%'


---- Video PG / TV-PG = 122994
--insert into polaris.polaris.BibRecordSets
--select distinct b.bibid, 122994
--from #bibs b
--where (b.rating like 'pg%' and b.rating not like 'pg-%' and b.rating not like 'pg1%') or b.rating like 'tv-pg%' or b.rating = 'tvpg' or b.rating = 'tv pg' or b.rating like 'rated pg%'

---- Video R / TV-MA = 122995
--insert into polaris.polaris.BibRecordSets
--select distinct b.bibid, 122995
--from #bibs b
--where (b.rating like 'r%' and ((b.rating not like 'ra%' and b.rating not like 're%') or b.rating like 'rated r%')) or b.rating like 'tv-ma%' or b.rating = 'tvma' or b.rating like '16%' or b.rating like '18%' or b.rating = 'tv ma'

---- Video Un-Rated = 122996
--insert into polaris.polaris.BibRecordSets
--select distinct b.bibid, 122996
--from #bibs b
--where b.rating like 'not rated%' or b.rating like 'nr%' or b.rating = 'unrated' or b.rating = 'ur' or b.rating like 'rated nr%' or b.rating like 'Rated UNK%' or b.rating like 'rated ur%'

--insert into Polaris.polaris.BibRecordSets
--select distinct bt.BibliographicRecordID, 122996
--from polaris.polaris.BibliographicTags bt
--join polaris.polaris.BibliographicSubfields bs
--	on bs.BibliographicTagID = bt.BibliographicTagID
--where bt.TagNumber = 521
--	and bs.Subfield = 'a'
--	and bt.BibliographicRecordID in (
--		select br.BibliographicRecordID
--		from polaris.Polaris.BibliographicRecords br
--		join polaris.polaris.CircItemRecords cir
--			on cir.AssociatedBibRecordID = br.BibliographicRecordID
--		where br.PrimaryMARCTOMID in ( 33, 40 )
--		group by br.BibliographicRecordID
--		having min(cir.AssignedBranchID) > 104
--	)
--	and (bs.data like '%not rated%' or bs.data like '%not-rated%')
--	and bt.BibliographicRecordID not in ( select brs.BibliographicRecordID from polaris.polaris.BibRecordSets brs where RecordSetID = 122996 )



select distinct brs.BibliographicRecordID from polaris.polaris.BibRecordSets brs where brs.RecordSetID in ( 122992,122993,122994,122995,122996,122997 ) 


--rollback
--commit

-- drop table #bibs

-- Video Un-Rated = 122996
-- Video G / TV-G / TV-Y / TV-Y7 / TV-Y7 FV = 122992
-- Video PG = 122994
-- Video PG-13 / TV-14 = 122993
-- Video R / TV-MA = 122995
-- Other = 122997