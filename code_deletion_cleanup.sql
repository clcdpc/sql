-- Set this to 0 to perform the actual updates/deletions.
-- By default will only show a list of all code usages
declare @report_only bit = 1

declare @code_names table ( codedesc varchar(255) )

insert into @code_names values ('code1')--,('code2'),('code3')

-- code id tables
declare @material_type_ids	  table ( codeid int )
declare @fine_code_ids		  table ( codeid int )
declare @loan_period_code_ids table ( codeid int )

-- Update these values to automatically replace the values in ItemTemplates
declare @replacement_mt_id  int = 0
declare @replacement_fc_id  int = 0
declare @replacement_lpc_id int = 0

insert into @material_type_ids
select materialtypeid
from Polaris.polaris.MaterialTypes mt where mt.Description in ( select codedesc from @code_names )

insert into @fine_code_ids
select fc.FineCodeID
from Polaris.polaris.FineCodes fc where fc.Description in ( select codedesc from @code_names )

insert into @loan_period_code_ids
select lpc.LoanPeriodCodeID
from polaris.polaris.LoanPeriodCodes lpc where lpc.Description in ( select codedesc from @code_names )

-- check for conditions that should be fixed in client
if ( exists (select 1 from polaris.polaris.CircItemRecords cir where cir.MaterialTypeID in ( select codeid from @material_type_ids )) )
begin
	raiserror('One or more material types are assigned to item records, update in client', 0, 1) with nowait
	goto endofscript
end

if ( exists (select 1 from polaris.polaris.CircItemRecords cir where cir.FineCodeID in ( select codeid from @fine_code_ids ))
  or exists (select 1from polaris.polaris.ReserveItemRecords rir where rir.PrimaryFineCodeID in ( select codeid from @fine_code_ids )) )
begin
	raiserror('One or more fine codes are assigned to item records, update in client', 0, 1) with nowait
	goto endofscript
end

if ( exists (select 1 from polaris.polaris.CircItemRecords cir where cir.LoanPeriodCodeID in ( select codeid from @loan_period_code_ids )) 
  or exists (select 1 from polaris.polaris.ReserveItemRecords rir where rir.PrimaryLoanPeriodCodeID in ( select codeid from @loan_period_code_ids )))
begin
	raiserror('One or more loan period codes are assigned to item records, update in client', 0, 1) with nowait
	goto endofscript
end

select '-------------------- Material Type Usage --------------------'
select * from polaris.polaris.ItemTemplates it							where MaterialTypeID in ( select codeid from @material_type_ids )
select * from polaris.polaris.POLines po								where MaterialTypeID in ( select codeid from @material_type_ids )
select * from polaris.polaris.InvLines i								where i.MaterialTypeID in ( select codeid from @material_type_ids )
select * from polaris.polaris.MaterialLoanLimits mt						where MaterialTypeID in ( select codeid from @material_type_ids )
select * from polaris.polaris.SA_FloatingMatrixMaterialTypes sa			where sa.MaterialTypeID in ( select codeid from @material_type_ids )
select * from polaris.polaris.SA_FloatingMatrixMaterialLimits sa		where sa.MaterialTypeID in ( select codeid from @material_type_ids )
select * from polaris.polaris.SA_FloatingMatrixMaterialLimits sa		where sa.MaterialTypeID in ( select codeid from @material_type_ids )
select * from polaris.polaris.SA_BorrowByMail_MaterialTypesPermitted sa	where sa.MaterialTypeID in ( select codeid from @material_type_ids )
select * from polaris.polaris.SA_BorrowByMail_MaterialTypesPermitted sa	where sa.MaterialTypeID in ( select codeid from @material_type_ids )
select * from polaris.polaris.SA_BranchShelvingStatusDurations sa		where sa.MaterialTypeID in ( select codeid from @material_type_ids )
select * from polaris.polaris.SA_LostItemReplacementCosts sa			where sa.MaterialTypeID in ( select codeid from @material_type_ids )
select * from polaris.polaris.SA_ShelvingStatusDurations sa				where sa.MaterialTypeID in ( select codeid from @material_type_ids )
select * from polaris.polaris.OfflinePolSystem_MaterialTypes sa			where sa.MaterialTypeID in ( select codeid from @material_type_ids )
select * from polaris.polaris.SA_NCIPMediumTypes_Outgoing sa			where sa.MaterialTypeID in ( select codeid from @material_type_ids )
select * from polaris.polaris.LoanLimits								where MaterialTypeID in ( select codeid from @material_type_ids )
select * from polaris.polaris.SA_MaterialTypeGroups_Definitions			where MaterialTypeID in ( select codeid from @material_type_ids )

select '-------------------- Fine Code Usage --------------------'
select * from polaris.Polaris.Fines				 where FineCodeID in ( select * from @fine_code_ids )
select * from polaris.polaris.ReserveItemRecords where PrimaryFineCodeID in ( select * from @fine_code_ids )

select '--------------- Loan Period Code Usage --------------------'
select * from polaris.polaris.ItemTemplates where LoanPeriodCodeID in ( select * from @loan_period_code_ids )
select * from polaris.polaris.LoanPeriods	where LoanPeriodCodeID in ( select * from @loan_period_code_ids )

if (@report_only = 1)
begin
	RAISERROR('Report only mode',0,1) with nowait
	goto endofscript
end

Begin Tran

/*DELETE A MATERIAL TYPE*/

-- update any Item Templates
if ( (select count(*) from polaris.polaris.ItemTemplates it where MaterialTypeID in ( select codeid from @material_type_ids )) > 0 and @replacement_mt_id > 0 )
begin
	update polaris.polaris.ItemTemplates
	set MaterialTypeID = @replacement_mt_id
	where MaterialTypeID in ( select codeid from @material_type_ids )
end

-- update any Invoice Lines
if ( (select count(*) from polaris.polaris.InvLines i where i.MaterialTypeID in ( select codeid from @material_type_ids )) > 0 and @replacement_mt_id > 0)
begin
	update polaris.polaris.InvLines
	set MaterialTypeID = @replacement_mt_id
	where MaterialTypeID in ( select codeid from @material_type_ids )
end

-- delete material type usage
delete from polaris.polaris.MaterialLoanLimits						where MaterialTypeID in ( select codeid from @material_type_ids )
delete from polaris.polaris.SA_FloatingMatrixMaterialTypes			where MaterialTypeID in ( select codeid from @material_type_ids )
delete from polaris.polaris.SA_FloatingMatrixMaterialLimits			where MaterialTypeID in ( select codeid from @material_type_ids )
delete from polaris.polaris.SA_BorrowByMail_MaterialTypesPermitted	where MaterialTypeID in ( select codeid from @material_type_ids )
delete from polaris.polaris.SA_BranchShelvingStatusDurations		where MaterialTypeID in ( select codeid from @material_type_ids )
delete from polaris.polaris.SA_LostItemReplacementCosts				where MaterialTypeID in ( select codeid from @material_type_ids )
delete from polaris.polaris.SA_ShelvingStatusDurations				where MaterialTypeID in ( select codeid from @material_type_ids )
delete from polaris.polaris.OfflinePolSystem_MaterialTypes			where MaterialTypeID in ( select codeid from @material_type_ids )
delete from polaris.polaris.SA_NCIPMediumTypes_Outgoing				where MaterialTypeID in ( select codeid from @material_type_ids )
delete from polaris.polaris.LoanLimits								where MaterialTypeID in ( select codeid from @material_type_ids )
delete from polaris.polaris.SA_MaterialTypeGroups_Definitions		where MaterialTypeID in ( select codeid from @material_type_ids )
delete from polaris.polaris.MaterialTypes							where MaterialTypeID in ( select codeid from @material_type_ids )

/*DELETE A FINE CODE*/

-- update any Item Templates
if ( (select count(*) from polaris.polaris.ItemTemplates it where it.FineCodeID in ( select codeid from @fine_code_ids )) > 0 and @replacement_fc_id > 0 )
begin
	update polaris.polaris.ItemTemplates
	set FineCodeID = @replacement_fc_id
	where FineCodeID in ( select codeid from @fine_code_ids )
end

-- delete fine code usage
delete from polaris.polaris.Fines		where FineCodeID in ( select * from @fine_code_ids )
delete from polaris.polaris.FineCodes   where FineCodeID in ( select * from @fine_code_ids )

/*DELETE A LOAN PERIOD CODE*/
if ( (select count(*) from polaris.polaris.ItemTemplates where LoanPeriodCodeID in ( select * from @loan_period_code_ids )) > 0 and @replacement_lpc_id > 0 )
begin
	update polaris.polaris.ItemTemplates
	set LoanPeriodCodeID = @replacement_lpc_id
	where LoanPeriodCodeID in ( select * from @loan_period_code_ids )
end

-- delete loan period code usage
delete from polaris.polaris.LoanPeriods		where LoanPeriodCodeID in ( select * from @loan_period_code_ids )
delete from polaris.polaris.LoanPeriodCodes where LoanPeriodCodeID in ( select * from @loan_period_code_ids )


select * from polaris.polaris.MaterialTypes	  where MaterialTypeID in ( select * from @material_type_ids )
select * from Polaris.Polaris.LoanPeriodCodes where LoanPeriodCodeID in ( select * from @loan_period_code_ids )
select * from Polaris.polaris.FineCodes		  where FineCodeID in ( select * from @fine_code_ids )


--rollback
--commit

endofscript: