begin tran

declare @highest_correct_org int = 104

declare @orgs table ( orgid int )

insert into @orgs
select organizationid
from Polaris.Polaris.Organizations o
where o.OrganizationID > @highest_correct_org and o.OrganizationCodeID = 3

declare @max_ibd_id int = ( select max(itemblockid) from polaris.polaris.itemblockdescriptions )

;with data as (
	select ibd.Description [block], ibd.SequenceID
	from Polaris.polaris.ItemBlockDescriptions ibd
	where ibd.OrganizationID = 3
)

insert into Polaris.Polaris.ItemBlockDescriptions
select o.orgid, rank() over(order by o.orgid, SequenceID) + @max_ibd_id, d.block, d.SequenceID
from @orgs o
cross join data d

select * from Polaris.Polaris.ItemBlockDescriptions


--rollback
--commit