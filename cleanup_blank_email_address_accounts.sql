-- run this first to check for any
select pr.PatronID
from polaris.polaris.PatronRegistration pr 
join Polaris.polaris.Patrons p
	on p.PatronID = pr.PatronID
where pr.EmailAddress = ''
	and p.Barcode is not null 
	and len(p.Barcode) > 0



-- run this to fix the accounts, making sure to commit the transaction
-- can problably run during the day but waiting until after hours won't hurt
begin tran

update pr
set EmailAddress = NULL
from Polaris.Polaris.PatronRegistration pr
where pr.EmailAddress = ''

--rollback
--commit