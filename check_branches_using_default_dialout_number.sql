select o.OrganizationID
		,o.Name
		,d.*
from polaris.polaris.Organizations o
left join (
	select * 
	from CLC_Notices.dbo.SettingValues sv
	where sv.Mnemonic = 'dialout_from_phone'
) d
	on d.OrganizationID = o.OrganizationID or d.OrganizationID = o.ParentOrganizationID
where o.OrganizationCodeID = 3
	and o.ParentOrganizationID not in (4,39)
	and d.Value is null