begin tran

declare @rsid int = 0

update ird
set ird.PhysicalCondition = ird.PublicNote, ird.PublicNote = null
from Polaris.Polaris.ItemRecordDetails ird
where ird.ItemRecordID in ( select ItemRecordID from Polaris.Polaris.ItemRecordSets where RecordSetID = @rsid )
and ird.PublicNote is not null

select ird.ItemRecordID, ird.PublicNote, ird.PhysicalCondition
from Polaris.Polaris.ItemRecordDetails ird
where ird.ItemRecordID in ( select ItemRecordID from Polaris.Polaris.ItemRecordSets where RecordSetID = @rsid )
and ird.PhysicalCondition is not null

--commit
--rollback