declare @creation table ( txndate datetime, holdrequestid int, patronid int )
declare @checkout table ( txndate datetime, holdrequestid int, patronid int )

declare @date date = '4/15/2017'

insert into @creation 
select th.TranClientDate, td_shr.numValue [HoldRequestID], td_p.numValue [PatronID]
from PolarisTransactions.polaris.TransactionHeaders th
join PolarisTransactions.polaris.TransactionDetails td_shr ON
	th.TransactionID = td_shr.TransactionID and td_shr.TransactionSubTypeID = 233
join PolarisTransactions.polaris.TransactionDetails td_p ON
	th.TransactionID = td_p.TransactionID and td_p.TransactionSubTypeID = 6
where th.TransactionTypeID = 6005 and th.TranClientDate > @date

insert into @checkout
select th.TranClientDate, td_shr.numValue [HoldRequestID], td_p.numValue [PatronID]
from PolarisTransactions.polaris.TransactionHeaders th
join PolarisTransactions.polaris.TransactionDetails td_shr ON
	th.TransactionID = td_shr.TransactionID and td_shr.TransactionSubTypeID = 233
join PolarisTransactions.polaris.TransactionDetails td_p ON
	th.TransactionID = td_p.TransactionID and td_p.TransactionSubTypeID = 6
where th.TransactionTypeID = 6001 and th.TranClientDate > @date and td_shr.numValue is not null

select c.holdrequestid, c.patronid [Creator], c.txndate [CreationDate], cko.patronid [Checkout], cko.txndate [CheckoutDate]
from @creation c
join @checkout cko on
	c.holdrequestid = cko.holdrequestid
where c.patronid != cko.patronid
order by c.holdrequestid