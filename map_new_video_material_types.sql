--maps old video material types to their new counter-parts

declare @map table ( oldmt int, newmt int )
insert into @map values (2,49)
insert into @map values (6,18)
insert into @map values (7,50)
insert into @map values (4,51)
insert into @map values (5,52)
insert into @map values (54,18)

begin tran

update cir
set cir.MaterialTypeID = m.newmt
from polaris.Polaris.CircItemRecords cir
join @map m
	on m.oldmt = cir.MaterialTypeID
where cir.AssignedBranchID > 104

-- check
if (OBJECT_ID('tempdb..#existing') is null) 
begin
	create table #existing ( bibid int, mtid int, mt varchar(255), itemcount int, itemrank int )

	insert into #existing
	select	cir.AssociatedBibRecordID [bibid]
			,cir.MaterialTypeID [mtid]
			,mt.Description [mt]
			,count(distinct cir.ItemRecordID) [itemcount]
			,rank() over(partition by cir.AssociatedBibRecordID order by count(distinct cir.ItemRecordID) desc, mt.Description desc) [itemrank]
	from Polaris.polaris.CircItemRecords cir
	join polaris.Polaris.MaterialTypes mt
		on mt.MaterialTypeID = cir.MaterialTypeID
	where cir.MaterialTypeID in (2,3,4,5,6,7,49,50,51,52,53) 
		and cir.AssignedBranchID < 104
	group by cir.AssociatedBibRecordID, cir.MaterialTypeID, mt.Description
end

select cir.MaterialTypeID, count(*)
from polaris.polaris.CircItemRecords cir 
where cir.AssignedBranchID > 104 
	and cir.MaterialTypeID in (49,50,51,52,18) 
	and cir.AssociatedBibRecordID in ( select bibid from #existing )
group by cir.MaterialTypeID

--rollback
--commit

--drop table #existing