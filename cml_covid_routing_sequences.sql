/*

I'm sorry to spring this on you at the last minute, but CML had a change in plans for how we'll be using the Daily Pull List. We are going to have 3 (technically 4) curbside locations: 
CML Parsons Branch (56) (and CML Parsons Drive-Up Window (68) ), CML Hilliard Branch (44) and CML Gahanna Branch (43).

Can you please set up the queue so that all 4 of those curbside locations can fill requests for each other at check-in? 
Additionally, we have 8 other locations that we'd like to fill requests for those curbside locations. Please set up the following to locations for filling requests for those 4 curbside locations:
CML Dublin Branch			41
CML Columbus Main Library	49
CML New Albany Branch		51
CML Northern Lights Branch	52
CML Shepard Branch			58
CML Southeast Branch		60
CML Whetstone Branch		62
CML Whitehall Branch		63

Mike update primary RTF queue for CML Linden, FRA and MLK

*/

--select * from Polaris.Polaris.Organizations o where o.ParentOrganizationID = 39 order by name

begin tran

delete from polaris.polaris.SysHoldRoutingSequences where RequesterBranchID in (41,42,43,44,46,47,49,50,51,52,53,56,57,58,60,62,63,65,68,69,70,71,73) and PrimarySecondaryFlag = 1 

insert into Polaris.polaris.SysHoldRoutingSequences
select requester.OrganizationID [Requester]
		,responder.OrganizationID [Responder]
		,rank() over(partition by requester.OrganizationID order by case when requester.OrganizationID = responder.OrganizationID then -99 else responder.OrganizationID end)
		,33
		,1
from Polaris.polaris.Organizations requester
cross join Polaris.polaris.organizations responder
where requester.OrganizationID in (41,42,43,44,46,47,49,50,51,52,53,56,57,58,60,62,63,65,68,69,70,71,73)
	and responder.OrganizationID in (41,42,43,44,46,47,49,50,51,52,53,56,57,58,60,62,63,65,68,69,70,71,73)

select * from Polaris.Polaris.SysHoldRoutingSequences shrs where shrs.RequesterBranchID in (41,42,43,44,46,47,49,50,51,52,53,56,57,58,60,62,63,65,68,69,70,71,73) and shrs.PrimarySecondaryFlag = 1


--rollback
--commit




select requester.OrganizationID [Requester]
		,responder.OrganizationID [Responder]
		,rank() over(partition by requester.OrganizationID order by case when requester.OrganizationID = responder.OrganizationID then -99 else responder.OrganizationID end)
		,33
		,1
from Polaris.polaris.Organizations requester
cross join Polaris.polaris.organizations responder
where requester.OrganizationID in (48, 99, 49, 40, 42, 45, 47, 77, 50, 53, 54, 55, 66, 88, 58, 70, 59)
	and responder.OrganizationID in (requester.OrganizationID, 65)

--(48, 99, 49, 40, 42, 45, 47, 77, 50, 53, 54, 55, 66, 88, 58, 70, 59)

select * from polaris.polaris.SysHoldRoutingSequences where RequesterBranchID in (56,68,44,43) and PrimarySecondaryFlag = 1 order by RequesterBranchID, Sequence



/*
�	CML Columbus Main Library can be a responder for the new curbside locations (we�d like this completed any time between Fri, 5/29 and Mon, 6/1/20 before 9:00 AM).
�	Set up all curbside locations to be responders (can fill requests at check-in) for each other (we�d like this completed any time between Fri, 5/29 and Mon, 6/1/20 before 9:00 AM):

('CML Columbus Main Library',
'CML Dublin Branch',
'CML Gahanna Branch',
'CML Hilliard Branch',
'CML Karl Road Branch',
'CML New Albany Branch ',
'CML New Albany Drive-up Window',
'CML Northern Lights Branch',
'CML Northern Lights Drive-up Window',
'CML Parsons Branch',
'CML Parsons Drive-up Window',
'CML Reynoldsburg Branch',
'CML Southeast Branch',
'CML Whetstone Branch',
'CML Whitehall Branch',
'CML Whitehall Drive-up Window')

*/

select *
from Polaris.Polaris.Organizations o
where o.Name in ('CML Columbus Main Library',
'CML Dublin Branch',
'CML Gahanna Branch',
'CML Hilliard Branch',
'CML Karl Road Branch',
'CML New Albany Branch ',
'CML New Albany Drive-up Window',
'CML Northern Lights Branch',
'CML Northern Lights Drive-up Window',
'CML Parsons Branch',
'CML Parsons Drive-up Window',
'CML Reynoldsburg Branch',
'CML Southeast Branch',
'CML Whetstone Branch',
'CML Whitehall Branch',
'CML Whitehall Drive-up Window')
order by o.Name

select * from polaris.Polaris.organizations where name in ('CML Columbus Main Library', 'CML Tech Services','CML Dublin Branch','CML Franklinton Branch','CML Gahanna Branch','CML Hilliard Branch','CML Karl Road Branch','CML Linden Branch','CML Martin Luther King Branch','CML New Albany Branch ','CML New Albany Drive-up Window','CML Northern Lights Branch','CML Northern Lights Drive-up Window','CML Northside Branch ','CML Parsons Branch','CML Parsons Drive-up Window','CML Reynoldsburg Branch','CML Shepard Branch ','CML Shepard Drive-up Window','CML Southeast Branch','CML Whetstone Branch','CML Whitehall Branch','CML Whitehall Drive-up Window')




select requester.OrganizationID [Requester]
		,responder.OrganizationID [Responder]
		,rank() over(partition by requester.OrganizationID order by case when requester.OrganizationID = responder.OrganizationID then -99 else responder.OrganizationID end)
		,33
		,1
from Polaris.polaris.Organizations requester
cross join Polaris.polaris.organizations responder
where requester.OrganizationID in (49,41,43,44,46,51,73,52,71,56,68,57,60,62,63,69)
	and responder.OrganizationID in (49,41,43,44,46,51,73,52,71,56,68,57,60,62,63,69,65)