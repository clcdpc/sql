USE [Polaris]
GO

/****** Object:  StoredProcedure [Polaris].[CLC_Custom_Rpt_Workslip_dev]    Script Date: 8/8/2022 9:52:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





ALTER PROCEDURE [Polaris].[CLC_Custom_Rpt_Workslip]
/*-----------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------*/      
	@POLineItemID int,
	@sTempTable varchar(255) = null
AS
BEGIN
	SET NOCOUNT ON

	declare @bibid int, @count int, @totalquantity int, @quantity int, @poli int, 
			@fundid int, @fundname varchar(50), @altname varchar(50), @itemCount int,
			@destination varchar(255), @collection varchar(255), @materialType varchar(255),
			@segid int, @fundqty int, @invnumber varchar(39), @baseCurrency int, @currCode CHAR(3), 
			@showItemData VARCHAR(3) = 'Yes', @retCode INT = 0, @orgID INT = 0, @strnOCLCNumber NVARCHAR(50),
			@sSQLCmd nvarchar(max)

	create table #Workslip
	(BibliographicRecordID int, POLineItemID int, BrowseTitle nvarchar(255), BrowseAuthor nvarchar(255), ReqName varchar(50), SupplierName varchar(50),
	NoteStaff varchar(255), NotePub varchar(255), ISBN_ISSN varchar(50), LCCN varchar(50), OtherCntrlNumber varchar(50), ListPrice money, StatusDate datetime, 
	FundID int, Name varchar(50), AlternativeName varchar(50), QuantRec int, TotalQuant int, SegID int, PONumber varchar(39), NumHolds int, Alert bit,
	BarCode varchar(20), ItemNumber varchar(255), CallNumber nvarchar(370), MaterialType varchar(80), Destination varchar(80), Location varchar(50),
	ItemCount int, SegmentNumber int, NormalTitle nvarchar(255), FundTotalQty int, InvNumber varchar(39), BaseCurrency int, CurrCode char(3), OCLCNumber nvarchar(50), ShelfLocation varchar(255)
	,instructions1 varchar(255), instructions2 varchar(255), instructions3 varchar(255))

	begin	
		
		select @sSQLCmd = 
			'Insert #Workslip
			(BibliographicRecordID, POLineItemID, BrowseTitle, BrowseAuthor, ReqName, SupplierName, 
			NoteStaff, NotePub, ISBN_ISSN, LCCN, OtherCntrlNumber, ListPrice, StatusDate, FundID,
			SegID, PONumber, Alert, BarCode, ItemNumber, CallNumber, 
			MaterialType, Destination, Location, SegmentNumber, NormalTitle, ShelfLocation)

		SELECT distinct
			POLines.BibliographicRecordID,
			POLines.POLineItemID,
			BibliographicRecords.BrowseTitle,
			BibliographicRecords.BrowseAuthor,
			POLines.ReqName,
			Suppliers.SupplierName,
			POLines.NoteStaff,
			POLines.NotePub,
			POLines.ISBN_ISSN,
			POLines.MARCLCCN,
			POLines.CatalogNumber,
			POLines.ListPriceUnitBase,
			POLines.StatusDate,
			Funds.FundID,
			POLineItemSegments.POLineItemSegmentID AS SegID,
			PurchaseOrders.PONumber + '' '' + COALESCE(PurchaseOrders.PONumberSuf,'''') AS PONumber,
			POLines.Alert,
			COALESCE(c.BarCode, ''0'') AS BarCode,
			COALESCE(CAST(c.ItemRecordID as varchar(20)), '''') + '' / '' + COALESCE(s.Description, '''') AS ItemNumber, 		
			COALESCE(ir.CallNumberVolumeCopy, '''') AS CallNumber, 		
			COALESCE(m.Description, '''') AS MaterialType, 
			COALESCE(col.Name, '''') AS DestCollection, 
			COALESCE(o.Name, '''') As Location,
			POLineItemSegments.POLISegmentNumber AS SegmentNumber,
			POLines.NormalTitle,
			sl.Description
		FROM   
			Polaris.POLines (NOLOCK)
			INNER JOIN Polaris.PurchaseOrders WITH (NOLOCK) ON POLines.PurchaseOrderID = PurchaseOrders.PurchaseOrderID
			INNER JOIN Polaris.POLineItemSegments WITH (NOLOCK) ON POLines.POLineItemID = POLineItemSegments.POLineItemID
			INNER JOIN Polaris.BibliographicRecords WITH (NOLOCK) ON POLines.BibliographicRecordID = BibliographicRecords.BibliographicRecordID
			INNER JOIN Polaris.Suppliers WITH (NOLOCK) ON PurchaseOrders.SupplierID = Suppliers.SupplierID
			LEFT OUTER JOIN Polaris.POLineItemSegmentAmts WITH (NOLOCK) ON POLineItemSegments.POLineItemSegmentID = POLineItemSegmentAmts.POLineItemSegmentID
			LEFT OUTER JOIN Polaris.Funds WITH (NOLOCK) ON POLineItemSegmentAmts.FundID = Funds.FundID
			LEFT OUTER JOIN Polaris.LineItemSegmentToItemRecord AS t WITH (NOLOCK) ON t.POLineItemSegmentID = POLineItemSegments.POLineItemSegmentID 
			LEFT OUTER JOIN Polaris.CircItemRecords AS c WITH (NOLOCK) ON t.ItemRecordID = c.ItemRecordID
			LEFT OUTER JOIN Polaris.ItemStatuses AS s WITH (NOLOCK) ON c.ItemStatusID = s.ItemStatusID
			LEFT OUTER JOIN Polaris.ItemRecordDetails AS ir WITH (NOLOCK) ON c.ItemRecordID = ir.ItemRecordID
			LEFT OUTER JOIN Polaris.MaterialTypes AS m WITH (NOLOCK) ON c.MaterialTypeID = m.MaterialTypeID 
			LEFT OUTER JOIN Polaris.Collections AS col WITH (NOLOCK) ON c.AssignedCollectionID = col.CollectionID
			LEFT OUTER JOIN Polaris.Organizations AS o WITH (NOLOCK) ON c.AssignedBranchID = o.OrganizationID
			LEFT OUTER JOIN Polaris.ShelfLocations as sl WITH (NOLOCK) ON c.ShelfLocationID = sl.ShelfLocationID AND sl.OrganizationID = c.AssignedBranchID
		WHERE  '

	IF (@sTempTable is not null and @sTempTable <> '')
	BEGIN
		select @sSQLCmd = @sSQLCmd + 'POLineItemSegments.POLineItemID IN (Select RecordID from ' + @sTempTable + ')'
	END
	ELSE
	BEGIN
		SELECT @sSQLCmd = @sSQLCmd + ' POLineItemSegments.POLineItemID = ' + LTrim(Str(@POLineItemID))
	END

		select @sSQLCmd = @sSQLCmd + 
						'GROUP BY
							POLines.BibliographicRecordID, 
							POLines.POLineItemID, 
							BibliographicRecords.BrowseTitle, 
							BibliographicRecords.BrowseAuthor,
							POLines.ReqName, 
							Suppliers.SupplierName, 
							POLines.NoteStaff, 
							POLines.NotePub, 
							POLines.ISBN_ISSN, 
							POLines.MARCLCCN,
							POLines.CatalogNumber,
							POLines.ListPriceUnitBase,
							POLines.StatusDate,
							Funds.FundID,
							POLineItemSegments.POLineItemSegmentID,
							PurchaseOrders.PONumber,
							PurchaseOrders.PONumberSuf,
							POLines.Alert,
							c.BarCode, 
							c.ItemRecordID, 
							s.Description,
							ir.CallNumberVolumeCopy,
							m.Description, 
							col.Name, 
							o.Name,
							POLineItemSegments.POLISegmentNumber,
							POLines.NormalTitle,
							sl.Description
						ORDER BY
							POLineItemSegments.POLISegmentNumber'

		exec (@sSQLCmd) 

		-- CLC MF - split NoteStaff into three columns
		declare @noteSplit table ( polineid int, ind int, val varchar(255) )

		insert into @noteSplit
		select w.POLineItemID, s.ItemIndex, s.Item
		from #Workslip w
		cross apply Polaris.dbo.CLC_Custom_SplitString2(replace(w.NoteStaff, char(13) + char(10) + char(13) + char(10), nchar(9999)), nchar(9999)) s		

		update w
		set w.instructions1 = ns1.val
			,w.instructions2 = ns2.val
			,w.instructions3 = ns3.val
		from #Workslip w
		left join @noteSplit ns1
			on ns1.polineid = w.POLineItemID and ns1.ind = 0
		left join @noteSplit ns2
			on ns2.polineid = w.POLineItemID and ns2.ind = 1
		left join @noteSplit ns3
			on ns3.polineid = w.POLineItemID and ns3.ind = 2
		-- END CLC MF - split NoteStaff into three columns
		
		declare rpt_cur insensitive cursor 
		for select distinct BibliographicRecordID, POLineItemID, FundID, SegID from #Workslip
		for read only
		
		open rpt_cur
		fetch next from rpt_cur into @bibid, @poli, @fundid, @segid
		while (@@FETCH_STATUS = 0)
		begin
			exec Polaris.Circ_GetHoldCount 2, @bibid, @count out
			
			select @totalquantity = SUM(COALESCE(ps.QuantRec, 0))
			from Polaris.POLineItemSegments AS ps WITH (NOLOCK)
			Where ps.POLineItemID = @poli

			select @itemCount = COUNT(its.ItemRecordID) 
			from Polaris.LineItemSegmentToItemRecord AS its WITH (NOLOCK) 
			inner join Polaris.POLineItemSegments AS ps WITH (NOLOCK) on ps.POLineItemSegmentID = its.POLineItemSegmentID
			Where ps.POLineItemID = @poli

			if(@itemCount = 0)-- get Material Type, Collection and Destination from POLI and POLI Segment
			begin
				select 
					@materialType = COALESCE(m.Description, ''), 
					@collection = COALESCE(col.Name, ''), 
					@destination = COALESCE(o.Name, '')
				from Polaris.POLineItemSegments AS ps WITH (NOLOCK)
				INNER JOIN  Polaris.POLines as pl WITH (NOLOCK) ON ps.POLineItemID = pl.POLineItemID
				LEFT OUTER JOIN Polaris.MaterialTypes AS m WITH (NOLOCK) ON pl.MaterialTypeID = m.MaterialTypeID 
				LEFT OUTER JOIN Polaris.Collections AS col WITH (NOLOCK) ON ps.DestinationCollectionID = col.CollectionID
				LEFT OUTER JOIN Polaris.Organizations AS o WITH (NOLOCK) ON ps.DestinationOrgID = o.OrganizationID
				Where ps.POLineItemsegmentID = @segid

				Update #Workslip
				set
					MaterialType = @materialType,
					Destination = @collection,
					Location = @destination
				Where SegID = @segid
			end

			Update #Workslip 
			Set NumHolds = @count, TotalQuant = @totalquantity, ItemCount = @itemCount
			Where BibliographicRecordID = @bibid and POLineItemID = @poli
		
			select DISTINCT
				@fundname = f.Name,
				@altname = f.AlternativeName, 
				@quantity = s.QuantRec
			from 
				Polaris.Funds as f WITH (NOLOCK)
			inner join 
				Polaris.POLineItemSegmentAmts as a WITH (nolock) on f.FundID = a.FundID
			inner join 
				Polaris.POLineItemSegments as s WITH (nolock) on a.POLineItemSegmentID = s.POLineItemSegmentID
			Where 
				s.POLineItemSegmentID = @segid
			and
				f.FundID = @fundid

			select DISTINCT
				@fundqty = SUM(s.QuantRec)
			from 
				Polaris.POLineItemSegments as s WITH (nolock) 
			inner join 
				Polaris.POLineItemSegmentAmts as a WITH (nolock) on a.POLineItemSegmentID = s.POLineItemSegmentID
			inner join 
				Polaris.Funds as f WITH (NOLOCK) on f.FundID = a.FundID
			Where 
				f.FundID = @fundid
			And
				s.POLineItemID = @poli


			UPDATE #Workslip 
			SET 
				[Name] = @fundname, 
				AlternativeName = @altname,
				QuantRec = @quantity,
				fundTotalQty = @fundqty
			Where SegID = @segid and FundID = @fundid

			SELECT @invNumber = CASE WHEN ( (SELECT COUNT(InvLineItemID) FROM Polaris.InvLines WITH (NOLOCK) WHERE POLineItemID = @poli) > 1 ) 
									 THEN 
										'[Multiple]'
									WHEN  ( (SELECT COUNT(DISTINCT BIG02) FROM EDIInvoiceHeader as h with(nolock)
											left outer join EDIStructure as s with (nolock) on h.StructureID = s.StructureID 
											left outer join EDIInvoiceDetail as d with (nolock) on h.StructureID = d.StructureID AND h.ST02 = d.ST02
											WHERE s.Invoiced = 0 
											AND  d.IT101 = CAST(@poli AS VARCHAR(20)) )
											> 1 ) 
									THEN '[Multiple]'
									WHEN  ( (SELECT COUNT(InvLineItemID) FROM Polaris.InvLines WITH (NOLOCK) WHERE POLineItemID = @poli) > 0) 
												AND
											(SELECT COUNT(DISTINCT BIG02) FROM EDIInvoiceHeader as h with(nolock)
											left outer join EDIStructure as s with (nolock) on h.StructureID = s.StructureID
											left outer join EDIInvoiceDetail as d with (nolock) on h.StructureID = d.StructureID AND h.ST02 = d.ST02
											WHERE s.Invoiced = 0 
											AND  d.IT101 = CAST(@poli AS VARCHAR(20)) )	> 0
									THEN 
										'[Multiple]'
									WHEN 
										(SELECT COUNT(InvLineItemID) FROM Polaris.InvLines WITH (NOLOCK) WHERE POLineItemID = @poli) = 1
									THEN 	
										(SELECT inv.InvNumber + ' ' + IsNull(inv.InvNumberSuf, '')
										FROM Polaris.Invoices AS inv WITH (NOLOCK)
										LEFT OUTER JOIN Polaris.InvLines AS il WITH (NOLOCK) ON inv.InvoiceID = il.InvoiceID
										WHERE il.POLineItemID = @poli)
									WHEN 
										(SELECT COUNT(DISTINCT BIG02) FROM EDIInvoiceHeader as h with(nolock)
										left outer join EDIStructure as s with (nolock) on h.StructureID = s.StructureID
										left outer join EDIInvoiceDetail as d with (nolock) on h.StructureID = d.StructureID AND h.ST02 = d.ST02
										WHERE s.Invoiced = 0 
										AND  d.IT101 = CAST(@poli AS VARCHAR(20))) = 1 
									THEN
										(SELECT DISTINCT(IsNULL(BIG02, '')) FROM EDIInvoiceHeader as h with(nolock)
										left outer join EDIStructure as s with (nolock) on h.StructureID = s.StructureID
										left outer join EDIInvoiceDetail as d with (nolock) on h.StructureID = d.StructureID AND h.ST02 = d.ST02
										WHERE s.Invoiced = 0 
										AND  d.IT101 = CAST(@poli AS VARCHAR(20)))
									ELSE
										''
							END

			UPDATE #Workslip
			SET InvNumber = @invnumber
			WHERE POLineItemID = @poli

			-- Set the OCLCNumber
			SET @strnOCLCNumber = N''
			SELECT TOP 1 
				@strnOCLCNumber = SUBSTRING(Data, 1, 50)			
			FROM 
				Polaris.BibliographicTags BT WITH (NOLOCK)
				INNER JOIN Polaris.BibliographicSubfields BS WITH (NOLOCK)
					ON BT.BibliographicTagID = BS.BibliographicTagID
			WHERE
				BT.BibliographicRecordID = @bibid
				AND BT.TagNumber = 35
				AND BS.Subfield = 'a'
				AND BS.Data LIKE '%oc%'
			ORDER BY
				BT.Sequence DESC

			UPDATE #Workslip
			SET OCLCNumber = @strnOCLCNumber
			WHERE BibliographicRecordID = @bibid


			/********************************************************
				If the SA parameter to display Item Record data is 
				set to NO make sure there is no item data to return
			********************************************************/
			select @orgID = OrganizationID FROM Polaris.PurchaseOrders WITH (NOLOCK) WHERE PurchaseOrderID = (Select PurchaseOrderID FROM Polaris.POLines WITH (NOLOCK) WHERE POLineItemID = @poli)
			exec @retCode = Polaris.Polaris.SA_GetValueByOrg 'ACQ_POLI_WORKSLIP_ONORDER_DATA', @orgID, 1, @showItemData OUT
			IF (@showItemData = 'No')
			BEGIN
				UPDATE 
					#Workslip
				SET 
					BarCode = NULL, 
					ItemNumber = NULL, 
					CallNumber = NULL,
					ItemCount = 0
				WHERE 
					POLineItemID = @poli
			END
			
			select @showItemData = 'Yes', @retCode = 0
			
			fetch next from rpt_cur into @bibid, @poli, @fundid, @segid

		end
		close rpt_cur
		deallocate rpt_cur

	end
	
	SELECT @baseCurrency = OP.Value, 
		@currCode = Cur.Code 
	FROM Polaris.OrganizationsPPPP OP WITH (NOLOCK) 
	LEFT OUTER JOIN Polaris.Currencies Cur WITH (NOLOCK) ON Cur.CurrencyID != OP.Value 
	LEFT OUTER JOIN Polaris.Suppliers Sup WITH (NOLOCK) ON Cur.CurrencyID = Sup.CurrencyID
	LEFT OUTER JOIN Polaris.PurchaseOrders po WITH (NOLOCK) ON Sup.SupplierID = po.SupplierID
	WHERE OP.AttrID = 2548/*'SYSDEFCURRENCY'*/
		AND po.PurchaseOrderID = (SELECT PurchaseOrderID FROM Polaris.POLines WHERE POLineItemID = @poli)

	UPDATE 
		#Workslip
	SET 
		BaseCurrency = @baseCurrency,
		CurrCode = @currCode

	select DISTINCT BibliographicRecordID, POLineItemID, BrowseTitle, BrowseAuthor, ReqName, SupplierName,
		NoteStaff, NotePub, ISBN_ISSN, LCCN, OtherCntrlNumber, ListPrice, StatusDate, 
		FundID, Name, AlternativeName, QuantRec, TotalQuant, SegID, PONumber, NumHolds, Alert,
		BarCode, ItemNumber, CallNumber, MaterialType, Destination, Location,
		ItemCount, SegmentNumber, NormalTitle, FundTotalQty, 
		InvNumber, BaseCurrency, CurrCode, OCLCNumber, ShelfLocation 
		,instructions1,instructions2,instructions3
	from #Workslip
	Order By NormalTitle, Location

	drop table #Workslip

	RETURN
END

GO


