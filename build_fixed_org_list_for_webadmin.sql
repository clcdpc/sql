select  cast(rank() over(order by o.OrganizationCodeID, o.Name) as varchar(max)) + ':"' + case o.OrganizationCodeID  when 1 then 'SYSTEM' when 2 then 'LIBRARY' when 3 then 'BRANCH' end + ': ' + o.Name + '",'
from Polaris.Polaris.Organizations o
order by o.OrganizationCodeID, o.Name