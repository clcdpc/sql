/*
select *
from polaris.polaris.SysHoldRequests shr
where shr.BibliographicRecordID = 3870535
order by shr.ExpirationDate
*/

select dateadd(day, 1, shr.ExpirationDate)
from polaris.polaris.SysHoldRequests shr
join polaris.polaris.BibRecordSets brs
	on brs.BibliographicRecordID = shr.BibliographicRecordID and brs.RecordSetID = 229724
where shr.SysHoldStatusID in (1,3,4)