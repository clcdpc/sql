-- drop table #bibs

if (OBJECT_ID('tempdb..#bibs') is null) 
begin
	create table #bibs ( bibid int, rating varchar(max), raw521a varchar(max))

	-- fix the raw 521$a value by either changing it to 'not rated' or replacing 'rated' and ':' to a general format of MPAA Rating G, etc
	;with fixed521 as (
		select bt.BibliographicRecordID, bs.Data [raw521], case when bs.data like '%not rated%' or bs.data like '%unrated%' or bs.data like '%un rated%' or bs.data like '%un-rated%' then 'not rated' else replace(replace(bs.Data, 'rated', 'rating'), ':', '') end [fixed521]
		from polaris.polaris.BibliographicTags bt
		join polaris.polaris.BibliographicSubfields bs
			on bs.BibliographicTagID = bt.BibliographicTagID
		where bt.TagNumber = 521
		and bs.Subfield = 'a'
		and bt.BibliographicRecordID in (
				select br.BibliographicRecordID
				from polaris.Polaris.BibliographicRecords br
				join polaris.polaris.CircItemRecords cir
					on cir.AssociatedBibRecordID = br.BibliographicRecordID
				where br.PrimaryMARCTOMID in ( 33, 40 )
				group by br.BibliographicRecordID
				having min(cir.AssignedBranchID) > 104
		)
		and (bs.data like '%rating%' or bs.data like '%rated%' or bs.Data like 'preschool%')
	), 

	-- strip the rating value data out of the fixed 521$a value
	ratings as (
		select *, case when f.fixed521 != 'not rated' and f.fixed521 like '%rating%' then replace(replace(replace(RIGHT(f.fixed521,CHARINDEX('gnitar',REVERSE(f.fixed521),0)-1), '-', ''), '.', ''), ' ', '') else f.fixed521 end [Rating]
		from fixed521 f
	), 

	-- turn the raw rating values into clean values
	fixedratings as (
		select *,
			case
				when r.Rating like 'g%' or r.rating like 'tv[yg]%' or r.rating like 'preschool%' or r.Rating like '3up%' then 'G'
				when r.Rating like 'pg13%' or r.rating like '1[34]%' or r.Rating like 'tv14%' then 'PG13'
				when r.Rating like 'PG%' or r.rating like 'tvpg%' then 'PG'
				when (r.rating like 'r%' and r.rating not like 'recc%') or r.rating like '1[68]%' or r.Rating like 'tvma%' then 'R'
				when r.Rating = 'not rated' or r.rating like 'ur%' or r.rating like 'nr%' or r.rating like 'unk%' then 'NR'
				else 'Other'
			end [FixedRating]
		from ratings r
	)

	-- insert fixed rating values into temp table
	insert into #bibs
	select r.BibliographicRecordID, r.FixedRating, r.raw521 
	from fixedratings r
end

select * from #bibs where bibid = 2850445

begin tran

delete from polaris.polaris.BibRecordSets where RecordSetID in ( 122992,122993,122994,122995,122996,122997 )

declare @sortorder table (rating varchar(10), sortvalue int)

insert into @sortorder values ('g',		100)
insert into @sortorder values ('pg',	200)
insert into @sortorder values ('pg13',	300)
insert into @sortorder values ('nr',	400)
insert into @sortorder values ('r',		500)
insert into @sortorder values ('other', 999)

insert into polaris.polaris.BibRecordSets
select tmp.bibid, tmp.RecordSetID
from (
	select distinct b.bibid, 
	case 
		when b.rating = 'g' then 122992 
		when b.rating = 'pg13' then 122993
		when b.rating = 'pg' then 122994
		when b.rating = 'r' then 122995
		when b.rating = 'nr' then 122996
		else 122997
	end [RecordSetID]
	, rank() over(partition by b.bibid order by so.sortvalue desc) [rank]
	from #bibs b
	join @sortorder so
		on so.rating = b.rating
) tmp
where tmp.rank = 1
--select * 
--from (
--	select distinct b.bibid, 
--	case 
--		when b.rating = 'g' then 122992 
--		when b.rating = 'pg13' then 122993
--		when b.rating = 'pg' then 122994
--		when b.rating = 'r' then 122995
--		when b.rating = 'nr' then 122996
--		else 122997
--	end [RecordSetID]
--	from #bibs b
--	join @sortorder so
--		on so.rating = b.rating
--) tmp


--select tmp.bibid, tmp.RecordSetID
--from (
--	select distinct b.bibid, 
--	case 
--		when b.rating = 'g' then 122992 
--		when b.rating = 'pg13' then 122993
--		when b.rating = 'pg' then 122994
--		when b.rating = 'r' then 122995
--		when b.rating = 'nr' then 122996
--		else 122997
--	end [RecordSetID]
--	, rank() over(partition by b.bibid order by so.sortvalue desc) [rank]
--	from #bibs b
--	join @sortorder so
--		on so.rating = b.rating
--) tmp
--where tmp.rank = 1
--order by tmp.bibid


 --Other = 122997
insert into Polaris.polaris.BibRecordSets
select br.BibliographicRecordID, 122997
from polaris.Polaris.BibliographicRecords br
join polaris.polaris.CircItemRecords cir
	on cir.AssociatedBibRecordID = br.BibliographicRecordID
where br.PrimaryMARCTOMID in ( 33, 40 ) and br.BibliographicRecordID not in ( select distinct brs.BibliographicRecordID from polaris.polaris.BibRecordSets brs where brs.RecordSetID in ( 122992,122993,122994,122995,122996,122997 ) )
group by br.BibliographicRecordID
having min(cir.AssignedBranchID) > 104

select distinct brs.BibliographicRecordID from polaris.polaris.BibRecordSets brs where brs.RecordSetID in ( 122992,122993,122994,122995,122996,122997 )

--rollback
--commit

-- drop table #bibs