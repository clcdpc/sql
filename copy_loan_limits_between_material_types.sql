declare @library int = 0

declare @map table ( destmt int, sourcemt int )
insert into @map values ( 0, 0 )

begin tran

update mll_dest
set mll_dest.MaxItems = mll_source.MaxItems
	,mll_dest.MaxRequestItems = mll_source.MaxRequestItems
from polaris.Polaris.MaterialLoanLimits mll_dest
join Polaris.polaris.MaterialLoanLimits mll_source
	on mll_source.OrganizationID = mll_dest.OrganizationID and mll_source.PatronCodeID = mll_dest.PatronCodeID
join Polaris.polaris.Organizations o
	on o.OrganizationID = mll_dest.OrganizationID
join @map m
	on m.destmt = mll_dest.MaterialTypeID and m.sourcemt = mll_source.MaterialTypeID
where o.ParentOrganizationID = @library

--rollback
--commit