--select * from polaris.Polaris.Organizations

declare @pickupBranches table ( orgid int )
insert into @pickupBranches 
values (59)

 /*
 select *
from Polaris.Polaris.SysHoldRequests shr
join polaris.polaris.Organizations o
	on o.OrganizationID = isnull(shr.NewPickupBranchID, shr.PickupBranchID)
where shr.SysHoldStatusID = 6
	and o.OrganizationID in ( 99)
	--and shr.LastStatusTransitionDate >= '5/14/2020'
	--and shr.LastStatusTransitionDate <= '3/13/2020 6:00 PM'
	--and not exists ( select 1 from results.Polaris.NotificationHistory nh where nh.PatronId = shr.PatronID and nh.ItemRecordId = shr.TrappingItemRecordID and nh.NotificationTypeId = 2 and nh.NoticeDate >= shr.LastStatusTransitionDate )
	--and not exists ( select 1 from results.Polaris.NotificationQueue nq where nq.PatronID = shr.PatronID and nq.ItemRecordID = shr.TrappingItemRecordID )
order by shr.LastStatusTransitionDate

*/



update shr
set shr.HoldNotificationDate = null
	,shr.HoldNotification2ndDate = null
from Polaris.Polaris.SysHoldRequests shr
join polaris.polaris.Organizations o
	on o.OrganizationID = isnull(shr.NewPickupBranchID, shr.PickupBranchID)
where shr.SysHoldStatusID = 6
	and o.OrganizationID in ( select orgid from @pickupBranches )

DECLARE @nItemRecordID INT, 
        @nNotificationTypeID INT,
        @nPatronID INT,
        @nBranchID INT
 
DECLARE HoldNotices CURSOR FOR
SELECT 
       shr.TrappingItemRecordID,
       2,
       shr.PatronID, 
       shr.pickupBranchID
FROM Polaris.Polaris.SysHoldRequests shr
join polaris.polaris.Organizations o
	on o.OrganizationID = isnull(shr.NewPickupBranchID, shr.PickupBranchID)
WHERE shr.SysHoldStatusID = 6
AND shr.HoldNotificationDate IS NULL
and o.OrganizationID in ( select orgid from @pickupBranches )
and shr.TrappingItemRecordID is not null
 
OPEN HoldNotices
 
FETCH NEXT FROM HoldNotices INTO @nItemRecordID, @nNotificationTypeID, @nPatronID, @nBranchID
 
WHILE @@FETCH_STATUS = 0
BEGIN
       EXEC Polaris.Polaris.AddNotification @nItemRecordID, @nNotificationTypeID, @nPatronID, @nBranchID
       FETCH NEXT FROM HoldNotices INTO @nItemRecordID, @nNotificationTypeID, @nPatronID, @nBranchID
END
CLOSE HoldNotices
DEALLOCATE HoldNotices


/* 

select * from results.Polaris.NotificationQueue nq where nq.NotificationTypeID = 2 and nq.DeliveryOptionID not in (1,8) and nq.ReportingOrgID in (113)




select * from results.Polaris.NotificationHistory nh where nh.ReportingOrgId = 3 and nh.NoticeDate > '5/1/2020' order by nh.NoticeDate



select shr.SysHoldRequestID, shr.PatronID, shr.TrappingItemRecordID, shr.LastStatusTransitionDate
from Polaris.Polaris.SysHoldRequests shr
join polaris.polaris.Organizations o
	on o.OrganizationID = isnull(shr.NewPickupBranchID, shr.PickupBranchID)
where shr.SysHoldStatusID = 6
	and o.OrganizationID in ( 63,69 )
	and not exists ( select 1 from results.Polaris.NotificationHistory nh where nh.PatronId = shr.PatronID and nh.ItemRecordId = shr.TrappingItemRecordID and nh.NotificationTypeId = 2 and nh.NoticeDate >= shr.LastStatusTransitionDate )
	and not exists ( select 1 from results.Polaris.NotificationQueue nq where nq.PatronID = shr.PatronID and nq.ItemRecordID = shr.TrappingItemRecordID )
order by shr.LastStatusTransitionDate


select * from results.polaris.NotificationHistory nh where nh.PatronId = 1047270 and nh.NotificationTypeId = 2 and nh.ItemRecordId = 20483471 order by nh.noticedate desc

*/

/*

select *
from Polaris.Polaris.SysHoldRequests shr
join polaris.polaris.Organizations o
	on o.OrganizationID = isnull(shr.NewPickupBranchID, shr.PickupBranchID)
where shr.SysHoldStatusID = 6
	and o.OrganizationID in ( 3 )
	and not exists ( select 1 from results.Polaris.NotificationHistory nh where nh.PatronId = shr.PatronID and nh.ItemRecordId = shr.TrappingItemRecordID and nh.NotificationTypeId = 2 and nh.NoticeDate >= shr.LastStatusTransitionDate )
	and not exists ( select 1 from results.Polaris.NotificationQueue nq where nq.PatronID = shr.PatronID and nq.ItemRecordID = shr.TrappingItemRecordID )
*/