USE [msdb]
GO

/****** Object:  Job [CLC Custom TwilioMailRollover]    Script Date: 9/23/2015 4:20:00 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 9/23/2015 4:20:01 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'CLC Custom TwilioMailRollover', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Rolls patrons unable to be contacted by the twilio dialout process.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Rollover Notices]    Script Date: 9/23/2015 4:20:02 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Rollover Notices', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'--update results.polaris.NotificationQueue
--set DeliveryOptionID = 1, Processed = 1
--where PatronID in 
--(
--	select PatronID
--	from PolarisTransactions.polaris.NotificationLog 
--	where DeliveryOptionID in ( 3,4,5 ) 
--	and CONVERT(varchar, notificationdatetime, 101) = convert(varchar, GETDATE(), 101)
--	and NotificationStatusID != 1
--	and PatronID not in (select PatronID from PolarisTransactions.Polaris.NotificationLog where NotificationStatusID = 1 and CONVERT(varchar, notificationdatetime, 101) = convert(varchar, GETDATE(), 101) and DeliveryOptionID in ( 3,4,5 ))
--	group by patronid
--	having COUNT(*) > 1
--)
--AND DeliveryOptionID in (3, 4, 

declare @libraries table ( orgid int )
insert into @libraries values (2), (6), (8), (14), (16), (19), (23), (25), (78), (84), (86)

update results.polaris.NotificationQueue
set DeliveryOptionID = 1, Processed = 1
from results.polaris.NotificationQueue nq
join polaris.polaris.Patrons p on
	p.PatronID = nq.PatronID
join polaris.polaris.Organizations o on
	p.OrganizationID = o.OrganizationID
where nq.PatronID in 
(
	select PatronID
    from PolarisTransactions.polaris.NotificationLog 
    where DeliveryOptionID in ( 3,4,5 ) 
    and CONVERT(varchar, notificationdatetime, 101) = convert(varchar, GETDATE(), 101)
    and NotificationStatusID != 1
    and PatronID not in (select PatronID from PolarisTransactions.Polaris.NotificationLog where NotificationStatusID = 1 and CONVERT(varchar, notificationdatetime, 101) = convert(varchar, GETDATE(), 101) and DeliveryOptionID in ( 3,4,5 ))
	group by patronid
    having COUNT(*) > 1
)
and nq.DeliveryOptionID in (3, 4, 5)
and o.ParentOrganizationID in ( select * from @libraries )', 
		@database_name=N'Results', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'daily', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20111209, 
		@active_end_date=99991231, 
		@active_start_time=220000, 
		@active_end_time=235959, 
		@schedule_uid=N'6f2d2802-384a-4919-a000-4c980eb3f93a'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO


