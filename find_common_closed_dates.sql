with dates as (
	select dc.DateClosed
			,count(distinct o.ParentOrganizationID) [libraries]
	from polaris.polaris.DatesClosed dc
	join polaris.polaris.organizations o
		on o.OrganizationID = dc.OrganizationID
	where dc.DateClosed between '1/1/2017' and '1/1/2023'
	group by dc.DateClosed
	having count(distinct o.ParentOrganizationID) > 10
)

select format(d.DateClosed, 'MM/dd') [day]
		,count(distinct datepart(year, d.DateClosed)) [years]
from dates d
group by format(d.DateClosed, 'MM/dd')
having count(distinct datepart(year, d.DateClosed)) > 2






select *
from polaris.polaris.DatesClosed dc
join polaris.polaris.organizations o
	on o.OrganizationID = dc.OrganizationID
where dc.DateClosed between '1/1/2017' and '1/1/2023'
	and format(dc.DateClosed, 'MM/dd') = '05/27'
order by dc.DateClosed


declare @dates table ( thedate date )
insert into @dates values ('1/1/2024'), ('12/24/2024'), ('12/25/2024'), ('12/31/2024')

declare @nums table (thenum int)

insert into @nums
SELECT  TOP 50 ROW_NUMBER() OVER(ORDER BY a.object_id) - 1
FROM    sys.all_objects a
        CROSS JOIN sys.all_objects b

select *
from (
	select o.OrganizationID, d.thedate, dateadd(year, n.thenum, d.thedate) [DateClosed]
	from @dates d
	cross join @nums n
	cross join polaris.polaris.organizations o
) d
where not exists ( select 1 from polaris.polaris.DatesClosed dc where dc.OrganizationID = d.OrganizationID and dc.DateClosed = d.DateClosed )
order by d.DateClosed, d.OrganizationID

