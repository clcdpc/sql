with nt_ls_map as (
	select 0 [NotificationTypeId] ,'NT_COMBINE_SMS_TEXT' [smsNoticeMnemonic] union all
	select 1 ,'NT_OVD_SMS_TEXT' union all
	select 2 ,'NT_HOLD_SMS_TEXT' union all
	select 3 ,'NT_CANCEL_SMS_TEXT' union all
	select 70,'NT_REMINDER_OVD_SMS_TEXT' union all
	select 71,'NT_REMINDER_RENEW_SMS_TEXT' union all
	select 8 ,'NT_FINE_SMS_TEXT' union all
	select 9 ,'NT_REMINDER_INACTIVE_SMS_TEXT' union all
	select 10,'NT_REMINDER_EXPIRE_SMS_TEXT' union all
	select 11,'NT_BILL_SMS_TEXT' union all
	select 12,'NT_OVD_SMS_TEXT' union all
	select 13,'NT_OVD_SMS_TEXT' union all
	select 18,'NT_HOLD_SMS_TEXT' union all
	select 20,'NT_BILL_SMS_TEXT'
), notices as (
	select nq.PatronID, nq.NotificationTypeID, nq.ItemRecordID, nq.DeliveryOptionID, nq.ReportingOrgID, nq.IsAdditionalTxt
	from results.Polaris.NotificationQueue nq
	where nq.DeliveryOptionID = 8
		and datediff(day, nq.CreationDate, getdate()) < 7
	union all
	select distinct cr.PatronID, case cr.NotificationTypeID when 7 then case cr.AutoRenewal when 1 then 71 else 70 end else cr.NotificationTypeID end [NotificationTypeID] , 0, cr.DeliveryOptionID, cr.ReportingOrgID, cr.IsAdditionalTxt
	from Results.polaris.CircReminders cr
	where cr.DeliveryOptionID = 8
)

select   nq.PatronID
		,nq.NotificationTypeID
		,nq.ItemRecordID
		,CLC_Notices.dbo.RemoveNonNumeric(case pr.TxtPhoneNumber when 1 then pr.PhoneVoice1 when 2 then pr.PhoneVoice2 when 3 then pr.PhoneVoice3 end) [SmsDeliveryPhone]
		,coalesce(br.BrowseTitle, brshr2.BrowseTitle, ir.BrowseTitle) [BrowseTitle]
		,nq.ReportingOrgID
		,shr.HoldTillDate
		,o.DisplayName [ReportingOrgName]
		,Polaris.Polaris.SA_GetMultiLingualStringValue(7, 1033, nq.ReportingOrgID, map.smsNoticeMnemonic) [MessageTemplate]
from notices nq
join nt_ls_map map
	on map.notificationTypeId = nq.NotificationTypeID
join Polaris.Polaris.PatronRegistration pr
	on pr.PatronID = nq.PatronID
join polaris.Polaris.organizations o
	on o.OrganizationID = nq.ReportingOrgID
left join Polaris.Polaris.CircItemRecords cir
	on cir.ItemRecordID = nq.ItemRecordID
left join Polaris.Polaris.BibliographicRecords br
	on br.BibliographicRecordID = cir.AssociatedBibRecordID
left join Polaris.Polaris.SysHoldRequests shr
	on shr.TrappingItemRecordID = nq.ItemRecordID
left join Polaris.Polaris.SysHoldRequests shr2
	on shr2.SysHoldRequestID = nq.ItemRecordID
left join Polaris.Polaris.BibliographicRecords brshr2
	on brshr2.BibliographicRecordID = shr2.BibliographicRecordID
left join Polaris.Polaris.ILLRequests ir
	on ir.ILLRequestID = case when nq.ItemRecordID < 0 then nq.ItemRecordID * -1 else 0 end
order by nq.PatronID





select cr.PatronID, 0 [ItemRecordId], case cr.AutoRenewal when 1 then 71 else 70 end [NotificationTypeId], cr.ReportingOrgID
from Results.polaris.CircReminders cr
where cr.DeliveryOptionID = 8
/*
select * from Results.Polaris.CircReminders

select * from results.Polaris.NotificationQueue nq where nq.DeliveryOptionID = 8 and nq.NotificationTypeID = 3

select * from CLC_Notices.dbo.TelnyxSmsMessages m where m.Body like 'Your request has been cancelled for title: %'




select * from Polaris.polaris.CircItemRecords cir where cir.ItemRecordID = 37071513
select * from Polaris.Polaris.SysHoldRequests shr where shr.PatronID = 1479788
select * from Polaris.Polaris.SysHoldStatuses

PatronId
SmsDeliveryPhone
BrowseTitle
ReportingOrgId
ReportingOrgName
MessageTemplate
HasOverdues



---130857
select top 100 * from Polaris.Polaris.ILLRequests ir where ir.PatronID = 285503
select * from Polaris.Polaris.InnReachRequests r where r.PatronID = 285503
select * from Polaris.Polaris.SysHoldRequests shr where shr.PatronID = 285503

*/