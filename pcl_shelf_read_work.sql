declare @collections int = 5
declare @assignedBranchId int = 20
declare @cutterStart varchar(50) = 'U'
declare @cutterEnd varchar(50) = 'ZZZZ'
declare @classStart varchar(50) = 'b'
declare @classEnd varchar(50) = 'bz'
declare @itemStatuses int = 1

select @cutterStart = isnull(@cutterStart, '')
		,@cutterEnd = isnull(@cutterEnd, 'zzzz')
		,@classStart = isnull(@classStart, '')
		,@classEnd = isnull(@classEnd, 'zzzz')

select
	cir.Barcode as ItemBarcode
   ,ird.CallNumber
   ,br.BrowseTitle
   ,br.BrowseAuthor
   ,ird.CutterNumber
   ,ird.ClassificationNumber
from polaris.Polaris.CircItemRecords cir with (nolock)
inner join polaris.Polaris.ItemRecordDetails ird with (nolock)
	on cir.ItemRecordID = ird.ItemRecordID
inner join polaris.Polaris.BibliographicRecords br with (nolock)
	on cir.AssociatedBibRecordID = br.BibliographicRecordID
where cir.AssignedBranchID in (@assignedBranchId)
	and cir.AssignedCollectionID in (@collections)
	and isnull(ird.CutterNumber,'') between @cutterStart and @cutterEnd
	and isnull(ird.ClassificationNumber,'') between @classStart and @classEnd
	and cir.ItemStatusID in (@itemStatuses)
option (recompile)


/*
select
	cir.Barcode as ItemBarcode
   ,ird.CallNumber
   ,br.BrowseTitle
   ,br.BrowseAuthor
from Polaris.CircItemRecords cir with (nolock)
inner join Polaris.ItemRecordDetails ird with (nolock)
	on (cir.ItemRecordID = ird.ItemRecordID)
inner join Polaris.BibliographicRecords br with (nolock)
	on (cir.AssociatedBibRecordID = br.BibliographicRecordID)
where cir.AssignedBranchID in (20)
	and cir.AssignedCollectionID in (258, 88)
	and ird.CutterNumber between 'H' and 'Lzzzz'
	and ird.CutterNumber is not null
	and cir.ItemStatusID in (1)  
*/