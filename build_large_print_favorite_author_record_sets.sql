declare @libraryId int = 78

begin tran

declare @authorId int

declare @authors table ( authorid int)

insert into @authors
select ars.AuthorId
from AuthorSubscription.dbo.AuthorRecordSets ars
where ars.OrganizationId = @libraryId
	and ars.LargePrint = 0

declare valueCursor cursor for
select authorid from @authors

set nocount on

open valueCursor
fetch next from valueCursor into @authorId
while @@FETCH_STATUS = 0
begin
	
	declare @rsid int
	exec AuthorSubscription.dbo.CreateAuthorRecordSet @libraryId, @authorId, 1, @rsid output

	insert into AuthorSubscription.dbo.AuthorRecordSets (AuthorId, OrganizationId, RecordSetId, Modifier, ModificationDate, LargePrint)
	values (@authorId, @libraryId, @rsid, 'clcdpc\mfields', getdate(), 1)
	
	fetch next from valueCursor into @authorId
end

close valueCursor
deallocate valueCursor

--rollback
--commit


select *
from AuthorSubscription.dbo.AuthorRecordSets ars
where ars.OrganizationId = @libraryId