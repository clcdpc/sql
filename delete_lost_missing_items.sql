USE [Polaris]
GO
/****** Object:  StoredProcedure [dbo].[CLC_Custom_Delete_Lost_Missing_and_Withdrawn_Items]    Script Date: 9/17/2015 10:22:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[CLC_Custom_Delete_Lost_Missing_and_Withdrawn_Items]
	@userid int = null,
	@workstationid int = null,
	@key int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if (@userid is null or @workstationid is null)
	begin
		raiserror( 'Usage:
	exec polaris.dbo.CLC_Custom_Delete_Lost_Missing_and_Withdrawn_Items @userid, @workstationid, @key (total number of items to delete, leave blank on first run)',9,1)
		return
	end

    declare @items table (itemid int)

	insert into @items
	select cir.ItemRecordID
	from Polaris.Polaris.CircItemRecords cir
	where ( (cir.ItemStatusID in (10,11) and cir.ItemStatusDate < DATEADD(year, -1, DATEADD(day, DATEDIFF(day, 0, GETDATE()), 0))) or (cir.itemstatusid = 7 and cir.ItemStatusDate < DATEADD(year, -3, DATEADD(day, DATEDIFF(day, 0, GETDATE()), 0))) )


	if (@key = ( select COUNT(*) from @items ))
	begin
		RAISERROR ('key is correct, deleting lost/missing items' , 0, 1) WITH NOWAIT
		declare @numdeleted INT = 0;
		declare @currentitem INT = 0;
		while ((select COUNT(*) from @items) > 0)
		begin 
			set @currentitem = ( select MIN(itemid) from @items )
			exec Polaris.Polaris.Cat_DeleteItemRecordProcessing @currentitem,5,@userid,@workstationid,1,1,1,N'<ROOT></ROOT>'
			delete from @items where itemid = @currentitem
			set @numdeleted = @numdeleted + 1
			if (@numdeleted % 100 = 0) 
			begin
				declare @msg VARCHAR(50) = 'deleted ' + cast(@numdeleted as VARCHAR) + ' items'
				RAISERROR (@msg , 0, 1) WITH NOWAIT
			end
		end
	end
	else
	begin
		select [status], items
		from
		(
		select	COUNT(case when cir.itemstatusid = 7 then 1 end) as [Lost], 
				COUNT(case when cir.itemstatusid = 10 then 1 end) as [Missing],
				COUNT(case when cir.itemstatusid = 11 then 1 end) as [Withdrawn],
				COUNT(*) [Total]
		from Polaris.Polaris.CircItemRecords cir
		join @items i on cir.itemrecordid = i.itemid
		) as data
		unpivot (items for [status] in ( [Lost], [Missing], [Withdrawn], [Total])
		) p

		select cir.ItemRecordID, cir.Barcode, cir.ItemStatusID, cir.ItemStatusDate, cir.LastCircTransactionDate
		from Polaris.Polaris.CircItemRecords cir
		join @items i on cir.itemrecordid = i.itemid
		order by cir.ItemStatusID, cir.ItemStatusDate
	end

END
