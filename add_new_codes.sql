begin tran

declare @pcodes table ( name varchar(255) )

insert into @pcodes values ('Limited Access')
insert into @pcodes values ('Branch Card')
insert into @pcodes values ('Business/Community Card')
insert into @pcodes values ('Homebound by Mail')
insert into @pcodes values ('ILL')
insert into @pcodes values ('Kids Card')
insert into @pcodes values ('Video up to PG/VG up to E')
insert into @pcodes values ('Video up to G/VG up to E')
insert into @pcodes values ('Video up to PG-13/VG up to T')
insert into @pcodes values ('Video/VG Restricted')
insert into @pcodes values ('Outreach')
insert into @pcodes values ('School Delivery')


insert into polaris.Polaris.PatronCodes
select rank() over(order by name) + 16, 'zzNEW ' + name from @pcodes


--------------------------------------------------------------


declare @mtypes table (name varchar(255))

insert into @mtypes values ('Book Club Kits')
insert into @mtypes values ('Games and Toys')
insert into @mtypes values ('Hotspots')
insert into @mtypes values ('In House Equipment')
insert into @mtypes values ('Juvenile Video Un-Rated')
insert into @mtypes values ('Loanable Equipment 1')
insert into @mtypes values ('Loanable Equipment 2')
insert into @mtypes values ('Maps & Pamphlets')
insert into @mtypes values ('Office')
insert into @mtypes values ('SearchOhio')
insert into @mtypes values ('Traditional ILL')
insert into @mtypes values ('Video Un-Rated')
insert into @mtypes values ('Video G / TV-G / TV-Y / TV-Y7 / TV-Y7 FV')
insert into @mtypes values ('Video Non-Fiction')
insert into @mtypes values ('Video PG-13 / TV-14')
insert into @mtypes values ('Video R / TV-MA')
insert into @mtypes values ('Juvenile Video Nonfiction')

insert into Polaris.Polaris.MaterialTypes
select rank() over(order by name) + 37, 'zzNEW ' + name from @mtypes

insert into Polaris.Polaris.LoanPeriodCodes
select rank() over(order by name) + 34, 'zzNEW ' + name from @mtypes

insert into Polaris.Polaris.FineCodes
select rank() over(order by name) + 34, 'zzNEW ' + name from @mtypes


--------------------------------------------------------------


update lp
set Units = 0
from polaris.polaris.LoanPeriods lp
where lp.LoanPeriodCodeID > 34 or lp.PatronCodeID > 16

update pll
set pll.TotalItems = 0, pll.TotalHolds = 0, TotalILL = 0, TotalReserveItems = 0
from polaris.polaris.PatronLoanLimits pll
where pll.PatronCodeID > 16

update mll
set mll.MaxItems = 0, mll.MaxRequestItems = 0
from polaris.polaris.MaterialLoanLimits mll
where mll.MaterialTypeID > 34 or mll.PatronCodeID > 16

select * 
from polaris.Polaris.Fines f 
where f.FineCodeID > 34

--select lp.OrganizationID, lp.PatronCodeID, lp.LoanPeriodCodeID, lp.TimeUnit, lp.Units from polaris.polaris.LoanPeriods lp
--union all
--select o.OrganizationID, pc.PatronCodeID, lpc.LoanPeriodCodeID, 1 [TimeUnit], 0 [Units]
--from polaris.Polaris.LoanPeriodCodes lpc
--cross join Polaris.polaris.PatronCodes pc
--cross join polaris.polaris.Organizations o
--where lpc.LoanPeriodCodeID > 34 and o.OrganizationCodeID = 3
--order by o.OrganizationID, pc.PatronCodeID, lpc.LoanPeriodCodeID

--rollback
--commit



