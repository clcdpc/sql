USE [CLC_Notices]
GO
/****** Object:  Table [dbo].[Dialin_Ignore_List]    Script Date: 11/14/2015 6:39:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Dialin_Ignore_List](
	[PhoneNumber] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Dialin_Ignore_List] PRIMARY KEY CLUSTERED 
(
	[PhoneNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Dialin_Patrons]    Script Date: 11/14/2015 6:39:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Dialin_Patrons](
	[Barcode] [varchar](50) NOT NULL,
	[PhoneNumber] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Dialin_Patrons] PRIMARY KEY CLUSTERED 
(
	[Barcode] ASC,
	[PhoneNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Dialout_String_Types]    Script Date: 11/14/2015 6:39:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Dialout_String_Types](
	[StringTypeID] [int] NOT NULL,
	[Description] [varchar](max) NOT NULL,
 CONSTRAINT [PK_CLC_Custom_Dialout_String_Types] PRIMARY KEY CLUSTERED 
(
	[StringTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Dialout_Strings]    Script Date: 11/14/2015 6:39:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Dialout_Strings](
	[StringID] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationID] [int] NOT NULL,
	[StringTypeID] [int] NOT NULL,
	[Value] [varchar](max) NOT NULL,
 CONSTRAINT [PK_Dialout_Strings] PRIMARY KEY CLUSTERED 
(
	[StringID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmailDomains]    Script Date: 11/14/2015 6:39:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmailDomains](
	[Domain] [varchar](255) NOT NULL,
	[OrganizationID] [int] NOT NULL,
 CONSTRAINT [PK_EmailDomains] PRIMARY KEY CLUSTERED 
(
	[Domain] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Log]    Script Date: 11/14/2015 6:39:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Log](
	[CreateDate] [datetime] NOT NULL,
	[Origin] [varchar](255) NULL,
	[LogLevel] [varchar](255) NULL,
	[Message] [varchar](max) NULL,
	[Exception] [varchar](max) NULL,
	[StackTrace] [varchar](max) NULL,
	[PatronID] [int] NULL,
	[PatronBarcode] [varchar](max) NULL,
	[CallSid] [varchar](255) NULL,
	[CallStatus] [varchar](50) NULL,
	[ItemRecordID] [int] NULL,
	[NotificationTypeID] [int] NULL,
	[Action] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RecordSets]    Script Date: 11/14/2015 6:39:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RecordSets](
	[RecordSetID] [int] NOT NULL,
	[OrganizationID] [int] NOT NULL,
	[RecordSetTypeID] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RecordSetTypes]    Script Date: 11/14/2015 6:39:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RecordSetTypes](
	[RecordSetTypeID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SMS_Queue]    Script Date: 11/14/2015 6:39:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SMS_Queue](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PhoneNumber] [varchar](50) NOT NULL,
	[Message] [varchar](255) NOT NULL,
	[InsertDate] [datetime] NOT NULL,
 CONSTRAINT [PK_SMS_Queue] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[PolarisNotifications]    Script Date: 11/14/2015 6:39:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*NotificationTypeID = nq.NotificationTypeID,
Patron = nq.Patron,
DeliveryOptionID = nq.DeliveryOptionID,
DeliveryString = nq.DeliveryOptionID == 3 ? nq.Patron.PatronRegistration.PhoneVoice1 :
			nq.DeliveryOptionID == 4 ? nq.Patron.PatronRegistration.PhoneVoice2 :
			nq.DeliveryOptionID == 5 ? nq.Patron.PatronRegistration.PhoneVoice3 :
			"",
ReportingOrg = nq.Organization,
ItemRecordID = nq.ItemRecordID ?? 0,
HoldTillDate = nq.SysHoldRequest.HoldTillDate*/
CREATE VIEW [dbo].[PolarisNotifications]
AS
SELECT        nq.NotificationTypeID, p.PatronID, p.Barcode AS PatronBarcode, pr.NameFirst, pr.NameLast, ISNULL(nq.ItemRecordID, 0) AS ItemRecordID, p.OrganizationID AS PatronBranch, 
                         o_patron.Abbreviation AS PatronBranchAbbr, o_patron.ParentOrganizationID AS PatronLibrary, o_patron2.Abbreviation AS PatronLibraryAbbr, nq.DeliveryOptionID, 
                         CASE nq.DeliveryOptionID WHEN 3 THEN pr.PhoneVoice1 WHEN 4 THEN pr.PhoneVoice2 WHEN 5 THEN pr.PhoneVoice3 END AS DeliveryString, ISNULL(nq.ReportingOrgID, 0) AS ReportingBranchID, 
                         o_notice.Abbreviation AS ReportingBranchAbbr, ISNULL(o_notice.ParentOrganizationID, 0) AS ReportingLibraryID, shr.HoldTillDate
FROM            Results.Polaris.NotificationQueue AS nq WITH (nolock) INNER JOIN
                         Polaris.Polaris.Patrons AS p WITH (nolock) ON nq.PatronID = p.PatronID INNER JOIN
                         Polaris.Polaris.PatronRegistration AS pr WITH (nolock) ON p.PatronID = pr.PatronID INNER JOIN
                         Polaris.Polaris.Organizations AS o_patron WITH (nolock) ON p.OrganizationID = o_patron.OrganizationID INNER JOIN
                         Polaris.Polaris.Organizations AS o_patron2 WITH (nolock) ON o_patron.ParentOrganizationID = o_patron2.OrganizationID INNER JOIN
                         Polaris.Polaris.Organizations AS o_notice WITH (nolock) ON nq.ReportingOrgID = o_notice.OrganizationID LEFT OUTER JOIN
                         Polaris.Polaris.SysHoldRequests AS shr WITH (nolock) ON shr.PatronID = p.PatronID AND nq.ItemRecordID = shr.TrappingItemRecordID
WHERE        (nq.DeliveryOptionID IN (3, 4, 5)) AND (nq.NotificationTypeID IN (1, 2, 12, 13))


GO
/****** Object:  View [dbo].[PolarisOrganizations]    Script Date: 11/14/2015 6:39:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[PolarisOrganizations]
AS
SELECT        OrganizationID, ParentOrganizationID, OrganizationCodeID, Name, Abbreviation, SA_ContactPersonID, CreatorID, ModifierID, CreationDate, ModificationDate, DisplayName
FROM            Polaris.Polaris.Organizations


GO
/****** Object:  View [dbo].[TodaysHoldCalls]    Script Date: 11/14/2015 6:39:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TodaysHoldCalls]
AS
SELECT DISTINCT p.PatronID, nh.ReportingOrgId, ISNULL(o.ParentOrganizationID, 0) AS ReportingLibraryID
FROM            Results.Polaris.NotificationHistory AS nh WITH (nolock) INNER JOIN
                         Polaris.Polaris.Patrons AS p WITH (nolock) ON nh.PatronId = p.PatronID INNER JOIN
                         Polaris.Polaris.Organizations AS o ON o.OrganizationID = nh.ReportingOrgId
WHERE        (CAST(FLOOR(CAST(GETDATE() AS float)) AS datetime) = CAST(FLOOR(CAST(nh.NoticeDate AS float)) AS datetime)) AND (nh.DeliveryOptionId IN (3, 4, 5)) AND (nh.NotificationTypeId = 2)

GO
ALTER TABLE [dbo].[SMS_Queue] ADD  CONSTRAINT [DF_SMS_Queue_InsertDate]  DEFAULT (getdate()) FOR [InsertDate]
GO
ALTER TABLE [dbo].[Dialout_Strings]  WITH CHECK ADD  CONSTRAINT [FK_Dialout_Strings_Dialout_String_Types] FOREIGN KEY([StringTypeID])
REFERENCES [dbo].[Dialout_String_Types] ([StringTypeID])
GO
ALTER TABLE [dbo].[Dialout_Strings] CHECK CONSTRAINT [FK_Dialout_Strings_Dialout_String_Types]
GO
/****** Object:  StoredProcedure [dbo].[CLC_Custom_Refresh_DatesClosed_For_Dialout]    Script Date: 11/14/2015 6:39:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Michael Fields
-- Create date: 4/2/2013
-- Description:	Clears the dates closed for CLC Electronic Library and then reinserts
--				dates that the specified number of libraries have branches closed.
-- =============================================
CREATE PROCEDURE [dbo].[CLC_Custom_Refresh_DatesClosed_For_Dialout]
	@orgid int, @threshold float
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @libraries decimal = ( select count(*) from polaris.polaris.Organizations where OrganizationCodeID = 2 and OrganizationID != 4 )
	
    delete from polaris.polaris.DatesClosed where OrganizationID = @orgid

	insert into polaris.polaris.DatesClosed
	select	@orgid,
			DateClosed
	from 
	(
		select	DateClosed,
				COUNT(distinct o.ParentOrganizationID) as NumClosed
		from polaris.polaris.DatesClosed dc
		join polaris.polaris.Organizations o
			on dc.OrganizationID = o.OrganizationID
		where dc.DateClosed >= DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()), 0) -- first day of current year
		group by DateClosed
	) as sub
	where (NumClosed / @libraries) >= @threshold
	order by DateClosed

END


GO
/****** Object:  StoredProcedure [dbo].[GetCheckedOutItems]    Script Date: 11/14/2015 6:39:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetCheckedOutItems]
	@patronId int,
	@itemType int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @dueDate datetime = case when @itemType = 1 then current_timestamp + 10000 else current_timestamp end;

    select	ic.ItemRecordID,
			cir.Barcode,
			br.BrowseTitle,
			ic.OrganizationID,
			ic.CheckOutDate,
			ic.DueDate
	from Polaris.Polaris.ItemCheckouts ic
	join Polaris.Polaris.CircItemRecords cir on
		ic.ItemRecordID = cir.ItemRecordID
	join Polaris.Polaris.BibliographicRecords br on
		cir.AssociatedBibRecordID = br.BibliographicRecordID
	where ic.PatronID = @patronId and ic.CheckOutDate <= @dueDate
END

GO
/****** Object:  StoredProcedure [dbo].[GetClosedDates]    Script Date: 11/14/2015 6:39:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetClosedDates]
	@orgid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select * from Polaris.Polaris.DatesClosed dc where dc.OrganizationID = @orgid
END


/****** Object:  StoredProcedure [dbo].[CLC_Custom_Refresh_DatesClosed_For_Dialout]    Script Date: 6/5/2015 7:46:31 AM ******/
SET ANSI_NULLS ON

GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "nh"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 232
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "p"
            Begin Extent = 
               Top = 6
               Left = 270
               Bottom = 136
               Right = 456
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "o"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 240
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'TodaysHoldCalls'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'TodaysHoldCalls'
GO
