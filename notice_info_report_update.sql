--Below are the notice settings that libraries have enabled but aren't included on the report:

--1275 - NSPARMRMPATEXP - Patron record expiration
--1277 - NSPARMRMINACTV - Inactive patron
--1693 - NSPARMRMROUTING - Serial Routing

--Below are new notices that are in our future upgrade that we may want to add:

--Miscellaneous charges notice - email, print, SMS
--	LBL_MANBILL_TOTALMSCCHARGES
--	NT_MANBILL_EMAIL_HEADER
--	NT_MANBILL_EMAIL_BODY
--	NT_MANBILL_PRINT_HEADER
--	NT_MANBILL_PRINT_BODY

--Secured account notice
--	NT_MSG_SECURED_NOPAY

--Address verification
--	NVAVER_REASON
--	NVAVER_TEXT
--	NVAVER_BLOCKTEXT

--Summary of charges (billing statement)
--	TXT_BILLEDPATRON_STATEMENT1

--Fines for minor patrons to be sent to parents
-- TXT_FINE_MINORPATRON

select * from Polaris.Polaris.AdminAttributes aa where aa.AttrDesc like '%expiration%'

select * from Polaris.polaris.SA_DefaultMultiLingualStrings where Mnemonic like '%charge%' and LanguageID = 1033

select * from polaris.Polaris.adminattributes aa where aa.Mnemonic = 'NSPARM_ROUTING_METHOD'

select * from polaris.Polaris.organizationspppp where AttrID = 2160


select * from polaris.Polaris.SA_DefaultMultiLingualStrings s where s.Mnemonic like '%expira%' and s.LanguageID = 1033




select *
from Polaris.polaris.SA_DefaultMultiLingualStrings s
where s.Mnemonic in 
(
'LBL_MANBILL_TOTALMSCCHARGES',
'NT_MANBILL_EMAIL_HEADER',
'NT_MANBILL_EMAIL_BODY',
'NT_MANBILL_PRINT_HEADER',
'NT_MANBILL_PRINT_BODY',
'NT_MSG_SECURED_NOPAY',
'NVAVER_REASON',
'NVAVER_TEXT',
'NVAVER_BLOCKTEXT',
'TXT_BILLEDPATRON_STATEMENT1',
'TXT_FINE_MINORPATRON'
) and s.LanguageID = 1033

select * from polaris.polaris.AdminAttributes aa where aa.AttrID in (1275,1277,1693)