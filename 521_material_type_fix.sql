--begin tran

;with fixed521 as (
	select bt.BibliographicRecordID
			,bs.Data [raw521]
			,case when bs.data like '%not rated%' or bs.data like '%unrated%' or bs.data like '%un rated%' or bs.data like '%un-rated%' then 'not rated' else replace(replace(bs.Data, 'rated', 'rating'), ':', '') end [fixed521]
			,rank() over(partition by bt.BibliographicRecordID order by bt.Sequence) [tagrank]
	from polaris.polaris.BibliographicTags bt
	join polaris.polaris.BibliographicSubfields bs
		on bs.BibliographicTagID = bt.BibliographicTagID
	where bt.TagNumber = 521
	and bs.Subfield = 'a'
	and (bs.data like '%rating%' or bs.data like '%rated%' or bs.Data like 'preschool%')
	and (bs.Data not like '%CHV%' and bs.Data not like '%Canada%' and bs.Data not like '%Canadian%' and bs.data not like '%OFBR%' and bs.data not like '%BBFC%' and bs.data not like '%ofrb%' and bs.data not like '%cmpda%' and bs.Data not like '%chr%' and bs.data not like '%Recommended for ages%' and bs.data not like '%13up%' and bs.Data not like '%ratings vary%' and bs.data not like '%various ratings%' and bs.Data not like '%all others%' and bs.data not like '%(others)%')
	and len(bs.Data) < 90
), ratings as (
	select *
		,case when f.fixed521 != 'not rated' and f.fixed521 like '%rating%' then replace(replace(replace(RIGHT(f.fixed521,CHARINDEX('gnitar',REVERSE(f.fixed521),0)-1), '-', ''), '.', ''), ' ', '') else f.fixed521 end [Rating]
	from fixed521 f
	where f.tagrank = 1
		and len(f.fixed521) - len(replace(f.fixed521, 'rating', '12345')) < 2
), fixedratings as (
	select *,
		case
			when r.Rating like 'g%' or r.rating like 'tv[yg]%' or r.rating like 'preschool%' or r.Rating like '3up%' then 49
			when r.Rating like 'pg13%' or r.rating like '1[34]%' or r.Rating like 'tv14%' then 51
			when r.Rating like 'PG%' or r.rating like 'tvpg%' then 3
			when (r.rating like 'r%' and r.rating not like 'recc%') or r.rating like '1[68]%'  or r.Rating like 'tvma%' then 52
			when r.Rating = 'not rated' or r.rating like 'ur%' or r.rating like 'nr%' or r.rating like 'unk%' then 53
			else -1
		end [FixedRating]
	from ratings r
), excludedBibs as (
	select distinct bt.BibliographicRecordID
	from polaris.Polaris.BibliographicTags bt
	join polaris.Polaris.BibliographicSubfields bs
		on bs.BibliographicTagID = bt.BibliographicTagID
	where bt.TagNumber = 490
		and bs.Subfield = 'a'
		and bs.Data = 'binge box'
)

-- ****************************************************************
-- ****************************************************************
-- MAKE SURE BINGE BOXES ARE BEING EXCLUDED NEXT TIME THIS IS RUN
-- ****************************************************************
-- ****************************************************************


--update cir set cir.MaterialTypeID = r.FixedRating
select cir.ItemRecordID	,cir.AssociatedBibRecordID,cir.MaterialTypeID,r.FixedRating,r.Rating,r.raw521,mt.Description [new_mt],mt2.Description [old_mt]
from fixedratings r
join polaris.polaris.CircItemRecords cir
	on cir.AssociatedBibRecordID = r.BibliographicRecordID
join polaris.Polaris.BibliographicRecords br
	on br.BibliographicRecordID = cir.AssociatedBibRecordID
join polaris.polaris.MaterialTypes mt
	on mt.MaterialTypeID = r.FixedRating
join polaris.Polaris.MaterialTypes mt2
	on mt2.MaterialTypeID = cir.MaterialTypeID
where r.FixedRating not in (-1,53)
	and br.PrimaryMARCTOMID != 1
	and cir.MaterialTypeID not in (r.fixedrating,10,16,20,33)
	and cir.ItemStatusID not in (11,13)
	and exists ( select 1 from polaris.polaris.CircItemRecords cir2 where cir2.AssociatedBibRecordID = cir.AssociatedBibRecordID and cir2.MaterialTypeID in (3,49,50,51,52,53) )
	and not exists ( select 1 from polaris.polaris.CircItemRecords cir2 where cir2.AssociatedBibRecordID = cir.AssociatedBibRecordID and cir2.MaterialTypeID in (18,50) )
	and not exists ( select 1 from excludedBibs eb where eb.BibliographicRecordID = cir.AssociatedBibRecordID )
order by r.BibliographicRecordID, cir.ItemRecordID
--rollback
--commit

/*
select cir.ItemRecordID
	,cir.AssociatedBibRecordID
	,cir.MaterialTypeID
	,r.FixedRating
	,r.Rating
	,r.raw521
	,mt.Description [new_mt]
	,mt2.Description [old_mt]
from fixedratings r
join polaris.polaris.CircItemRecords cir
	on cir.AssociatedBibRecordID = r.BibliographicRecordID
join polaris.Polaris.BibliographicRecords br
	on br.BibliographicRecordID = cir.AssociatedBibRecordID
join polaris.polaris.MaterialTypes mt
	on mt.MaterialTypeID = r.FixedRating
join polaris.Polaris.MaterialTypes mt2
	on mt2.MaterialTypeID = cir.MaterialTypeID
where r.FixedRating not in (-1,53)
	and br.PrimaryMARCTOMID != 1
	and cir.MaterialTypeID not in (r.fixedrating,10,16,20,33)
	and cir.ItemStatusID not in (13)
	and exists ( select 1 from polaris.polaris.CircItemRecords cir2 where cir2.AssociatedBibRecordID = cir.AssociatedBibRecordID and cir2.MaterialTypeID in (3,49,50,51,52,53) )
	and not exists ( select 1 from polaris.polaris.CircItemRecords cir2 where cir2.AssociatedBibRecordID = cir.AssociatedBibRecordID and cir2.MaterialTypeID in (18,50) )
order by r.FixedRating, r.raw521, cir.AssociatedBibRecordID
*/

--select * from fixed521 f where f.BibliographicRecordID = 3565775

/*
select r.*, mt.Description
from fixedratings r
left join Polaris.Polaris.MaterialTypes mt
	on mt.MaterialTypeID = r.FixedRating
where  r.raw521 like '%;%'
	and exists ( select 1 from polaris.Polaris.CircItemRecords cir where cir.AssociatedBibRecordID = r.BibliographicRecordID and cir.MaterialTypeID in (49,51,3,52,53) )
	--and (r.FixedRating > 0 or not exists ( select 1 from fixedratings _fr where _fr.BibliographicRecordID = r.BibliographicRecordID and _fr.FixedRating > 0 ))
	and r.FixedRating not in (-1,53)
order by trim(replace(r.raw521, '"', ''))

*/