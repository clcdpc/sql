select o.Name
from polaris.polaris.Organizations o
left join (
	select d.branch
			,max(d.TimeStart) [LastRunDate]
	from (
		select el.TimeStart
			,cast(substring(el.Parameters, patindex('%=%', el.Parameters) + 1, patindex('%&%', el.Parameters) - patindex('%=%', el.Parameters) - 1) as int) [branch]
			,el.UserName
		from prodreports.ReportServer.dbo.ExecutionLog el
		where el.ReportID = 'DF6D1E03-DFCE-4BD4-B794-2913FAD6297A'
	) d
	group by d.branch
) d
	on d.branch = o.OrganizationID
where o.OrganizationCodeID = 3
	and o.ParentOrganizationID not in (4)
	and o.Name not like 'z%'
	and (d.LastRunDate is null or d.LastRunDate < dateadd(day, -26, getdate()))
order by o.Name


select distinct o2.Name
from polaris.polaris.Organizations o
left join (
	select d.branch
			,max(d.TimeStart) [LastRunDate]
	from (
		select el.TimeStart
			,cast(substring(el.Parameters, patindex('%=%', el.Parameters) + 1, patindex('%&%', el.Parameters) - patindex('%=%', el.Parameters) - 1) as int) [branch]
			,el.UserName
		from prodreports.ReportServer.dbo.ExecutionLog el
		where el.ReportID = 'DF6D1E03-DFCE-4BD4-B794-2913FAD6297A'
	) d
	group by d.branch
) d
	on d.branch = o.OrganizationID
join polaris.polaris.Organizations o2
	on o2.OrganizationID = o.ParentOrganizationID
where o.OrganizationCodeID = 3
	and o.ParentOrganizationID not in (4)
	and o.Name not like 'z%'
	and (d.LastRunDate is null or d.LastRunDate < dateadd(day, -50, getdate()))
order by o2.Name
