USE [master]
GO
/****** Object:  Database [CLC_Notices]    Script Date: 7/11/2018 11:13:03 AM ******/
CREATE DATABASE [CLC_Notices]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CLC_Notices', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\CLC_Notices.mdf' , SIZE = 9408KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'CLC_Notices_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\CLC_Notices_log.ldf' , SIZE = 114432KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [CLC_Notices] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CLC_Notices].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CLC_Notices] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CLC_Notices] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CLC_Notices] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CLC_Notices] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CLC_Notices] SET ARITHABORT OFF 
GO
ALTER DATABASE [CLC_Notices] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CLC_Notices] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CLC_Notices] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CLC_Notices] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CLC_Notices] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CLC_Notices] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CLC_Notices] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CLC_Notices] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CLC_Notices] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CLC_Notices] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CLC_Notices] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CLC_Notices] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CLC_Notices] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CLC_Notices] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CLC_Notices] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CLC_Notices] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CLC_Notices] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CLC_Notices] SET RECOVERY FULL 
GO
ALTER DATABASE [CLC_Notices] SET  MULTI_USER 
GO
ALTER DATABASE [CLC_Notices] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CLC_Notices] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CLC_Notices] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CLC_Notices] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [CLC_Notices] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'CLC_Notices', N'ON'
GO
USE [CLC_Notices]
GO
/****** Object:  User [polaris]    Script Date: 7/11/2018 11:13:04 AM ******/
CREATE USER [polaris] FOR LOGIN [polaris] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [clcdpc\notices]    Script Date: 7/11/2018 11:13:04 AM ******/
CREATE USER [clcdpc\notices] FOR LOGIN [CLCDPC\notices] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_datareader] ADD MEMBER [polaris]
GO
ALTER ROLE [db_datareader] ADD MEMBER [clcdpc\notices]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [clcdpc\notices]
GO
/****** Object:  Schema [HangFire]    Script Date: 7/11/2018 11:13:04 AM ******/
CREATE SCHEMA [HangFire]
GO
/****** Object:  UserDefinedFunction [dbo].[RemoveNonNumeric]    Script Date: 7/11/2018 11:13:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[RemoveNonNumeric]
(	
	-- Add the parameters for the function here
	@strText varchar(1000)
)
RETURNS varchar(1000) 
AS
BEGIN 
	WHILE PATINDEX('%[^0-9]%', @strText) > 0
    BEGIN
        SET @strText = STUFF(@strText, PATINDEX('%[^0-9]%', @strText), 1, '')
    END
    RETURN @strText
END


GO
/****** Object:  Table [dbo].[Dialin_Ignore_List]    Script Date: 7/11/2018 11:13:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dialin_Ignore_List](
	[PhoneNumber] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Dialin_Ignore_List] PRIMARY KEY CLUSTERED 
(
	[PhoneNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dialin_Patrons]    Script Date: 7/11/2018 11:13:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dialin_Patrons](
	[Barcode] [varchar](50) NOT NULL,
	[PhoneNumber] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Dialin_Patrons] PRIMARY KEY CLUSTERED 
(
	[Barcode] ASC,
	[PhoneNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dialin_Usage]    Script Date: 7/11/2018 11:13:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dialin_Usage](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PatronID] [int] NOT NULL,
	[BranchID] [int] NOT NULL,
	[Timestamp] [datetime] NOT NULL,
 CONSTRAINT [PK_Dialin_Usage] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dialout_String_Types]    Script Date: 7/11/2018 11:13:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dialout_String_Types](
	[StringTypeID] [int] NOT NULL,
	[Description] [varchar](max) NOT NULL,
 CONSTRAINT [PK_CLC_Custom_Dialout_String_Types] PRIMARY KEY CLUSTERED 
(
	[StringTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dialout_Strings]    Script Date: 7/11/2018 11:13:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dialout_Strings](
	[StringID] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationID] [int] NOT NULL,
	[StringTypeID] [int] NOT NULL,
	[Value] [varchar](max) NOT NULL,
 CONSTRAINT [PK_Dialout_Strings] PRIMARY KEY CLUSTERED 
(
	[StringID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmailDomains]    Script Date: 7/11/2018 11:13:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailDomains](
	[Domain] [varchar](255) NOT NULL,
	[OrganizationID] [int] NOT NULL,
 CONSTRAINT [PK_EmailDomains] PRIMARY KEY CLUSTERED 
(
	[Domain] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmailHistorySearchLog]    Script Date: 7/11/2018 11:13:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailHistorySearchLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Username] [varchar](255) NOT NULL,
	[Query] [varchar](max) NOT NULL,
 CONSTRAINT [PK_EmailHistorySearchLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RecordSets]    Script Date: 7/11/2018 11:13:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RecordSets](
	[RecordSetID] [int] NOT NULL,
	[OrganizationID] [int] NOT NULL,
	[RecordSetTypeID] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RecordSetTypes]    Script Date: 7/11/2018 11:13:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RecordSetTypes](
	[RecordSetTypeID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SettingTypes]    Script Date: 7/11/2018 11:13:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SettingTypes](
	[SettingID] [int] IDENTITY(1,1) NOT NULL,
	[Mnemonic] [varchar](255) NOT NULL,
	[Description] [varchar](max) NULL,
	[DefaultValue] [varchar](max) NOT NULL,
 CONSTRAINT [PK_SettingTypes] PRIMARY KEY CLUSTERED 
(
	[SettingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SettingValues]    Script Date: 7/11/2018 11:13:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SettingValues](
	[OrganizationID] [int] NOT NULL,
	[Mnemonic] [varchar](255) NOT NULL,
	[LanguageID] [int] NOT NULL,
	[Value] [varchar](max) NOT NULL,
 CONSTRAINT [PK_SettingValues] PRIMARY KEY CLUSTERED 
(
	[OrganizationID] ASC,
	[Mnemonic] ASC,
	[LanguageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SMS_Group]    Script Date: 7/11/2018 11:13:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SMS_Group](
	[PhoneNumber] [varchar](50) NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_SMS_Group] PRIMARY KEY CLUSTERED 
(
	[PhoneNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[AggregatedCounter]    Script Date: 7/11/2018 11:13:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[AggregatedCounter](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](100) NOT NULL,
	[Value] [bigint] NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_CounterAggregated] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Counter]    Script Date: 7/11/2018 11:13:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Counter](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](100) NOT NULL,
	[Value] [smallint] NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_Counter] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Hash]    Script Date: 7/11/2018 11:13:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Hash](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](100) NOT NULL,
	[Field] [nvarchar](100) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[ExpireAt] [datetime2](7) NULL,
 CONSTRAINT [PK_HangFire_Hash] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Job]    Script Date: 7/11/2018 11:13:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Job](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StateId] [int] NULL,
	[StateName] [nvarchar](20) NULL,
	[InvocationData] [nvarchar](max) NOT NULL,
	[Arguments] [nvarchar](max) NOT NULL,
	[CreatedAt] [datetime] NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_Job] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[JobParameter]    Script Date: 7/11/2018 11:13:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[JobParameter](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[JobId] [int] NOT NULL,
	[Name] [nvarchar](40) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_HangFire_JobParameter] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[JobQueue]    Script Date: 7/11/2018 11:13:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[JobQueue](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[JobId] [int] NOT NULL,
	[Queue] [nvarchar](50) NOT NULL,
	[FetchedAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_JobQueue] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[List]    Script Date: 7/11/2018 11:13:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[List](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](100) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_List] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Schema]    Script Date: 7/11/2018 11:13:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Schema](
	[Version] [int] NOT NULL,
 CONSTRAINT [PK_HangFire_Schema] PRIMARY KEY CLUSTERED 
(
	[Version] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Server]    Script Date: 7/11/2018 11:13:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Server](
	[Id] [nvarchar](100) NOT NULL,
	[Data] [nvarchar](max) NULL,
	[LastHeartbeat] [datetime] NOT NULL,
 CONSTRAINT [PK_HangFire_Server] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[Set]    Script Date: 7/11/2018 11:13:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Set](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](100) NOT NULL,
	[Score] [float] NOT NULL,
	[Value] [nvarchar](256) NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_Set] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [HangFire].[State]    Script Date: 7/11/2018 11:13:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[State](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[JobId] [int] NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
	[Reason] [nvarchar](100) NULL,
	[CreatedAt] [datetime] NOT NULL,
	[Data] [nvarchar](max) NULL,
 CONSTRAINT [PK_HangFire_State] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[PolarisNotifications]    Script Date: 7/11/2018 11:13:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[PolarisNotifications]
AS

with hoursofoperation(orgid, [hours]) as 
(
	select o.OrganizationID, isnull(opb.Value, opl.Value) [HoursOfOperation]
	from Polaris.Polaris.Organizations o
	left join Polaris.polaris.OrganizationsPPPP opb
		on opb.OrganizationID = o.OrganizationID and opb.AttrID = 96
	left join Polaris.polaris.OrganizationsPPPP opl
		on o.ParentOrganizationID = opl.OrganizationID and opl.AttrID = 96
	where o.OrganizationCodeID = 3
),
notices as (
	select nq.PatronID, nq.NotificationTypeID, nq.ItemRecordID, nq.DeliveryOptionID, nq.ReportingOrgID
	from results.Polaris.NotificationQueue nq
	union all
	select cr.PatronID, cr.NotificationTypeID, cr.ItemRecordID, cr.DeliveryOptionID, cr.ReportingOrgID
	from Results.polaris.CircReminders cr
)

select
	n.NotificationTypeID
	,p.PatronID
	,p.Barcode [PatronBarcode]
	,pr.NameFirst
	,pr.NameLast
	,a.StreetOne [PatronStreetOne]
	,a.StreetTwo [PatronStreetTwo]
	,pc.City [PatronCity]
	,pc.State [PatronState]
	,pc.PostalCode [PatronZip]
	,isnull(n.ItemRecordID, 0) [ItemRecordID]
	,p.OrganizationID [PatronBranch]
	,o_patron.Abbreviation [PatronBranchAbbr]
	,isnull(o_patron.ParentOrganizationID, 0) [PatronLibrary]
	,o_patron2.Abbreviation [PatronLibraryAbbr]
	,n.DeliveryOptionID
	,pr.EmailAddress
	,pr.AltEmailAddress
	,pr.EmailFormatID
	,case n.DeliveryOptionID
		when 3 then pr.PhoneVoice1
		when 4 then pr.PhoneVoice2
		when 5 then pr.PhoneVoice3
		when 8 then
			case pr.TxtPhoneNumber
				when 1 then pr.PhoneVoice1
				when 2 then pr.PhoneVoice2
				when 3 then pr.PhoneVoice3
				else ''
			end
		else ''
	end [DeliveryPhone]
	,isnull(n.ReportingOrgID,0) [ReportingBranchID]
	,o_notice.Abbreviation [ReportingBranchAbbr]	
	,o_notice.Name [ReportingBranchName]	
	,saa_notice.StreetOne [ReportingBranchStreetOne]
	,saa_notice.StreetTwo [ReportingBranchStreetTwo]
	,saa_notice.City [ReportingBranchCity]
	,saa_notice.State [ReportingBranchState]
	,saa_notice.PostalCode [ReportingBranchZip]
	,isnull(o_notice.ParentOrganizationID,0) [ReportingLibraryID]
	,coalesce(shr.HoldTillDate, ir.HoldTillDate) [HoldTillDate]
	,ic.DueDate [DueDate]
	,br.BrowseTitle [Title]
	,hoo.hours
	,isnull(l.AdminLanguageID, 1033) [PatronLanguage]
	,case pr.TxtPhoneNumber
		when 1 then pr.PhoneVoice1
		when 2 then pr.PhoneVoice2
		when 3 then pr.PhoneVoice3
		else ''
	end [TxtPhoneNumber]
from notices n
join Polaris.Polaris.Patrons p
	on n.PatronID = p.PatronID
join Polaris.Polaris.PatronRegistration pr
	on p.PatronID = pr.PatronID
join Polaris.Polaris.Organizations o_patron
	on p.OrganizationID = o_patron.OrganizationID
join Polaris.Polaris.Organizations o_patron2
	on o_patron.ParentOrganizationID = o_patron2.OrganizationID
join Polaris.Polaris.Organizations o_notice
	on n.ReportingOrgID = o_notice.OrganizationID
join polaris.polaris.OrganizationsPPPP op_notice_addr
	on op_notice_addr.OrganizationID = n.ReportingOrgID and op_notice_addr.AttrID = 55
join polaris.Polaris.SA_Addresses saa_notice
	on saa_notice.SA_AddressID = op_notice_addr.Value
left join polaris.polaris.CircItemRecords cir
	on cir.ItemRecordID = n.ItemRecordID
left join Polaris.Polaris.SysHoldRequests shr
	on shr.PatronID = p.PatronID and n.ItemRecordID = shr.TrappingItemRecordID
left join Polaris.polaris.ILLRequests ir
	on ir.PatronID = n.PatronID and ir.ItemRecordID = n.ItemRecordID
left join Polaris.polaris.BibliographicRecords br
	on br.BibliographicRecordID = cir.AssociatedBibRecordID
left join hoursofoperation hoo
	on hoo.orgid = n.ReportingOrgID
left join polaris.polaris.PatronAddresses pa
	on pa.PatronID = p.PatronID and pa.AddressTypeID = 2
left join polaris.Polaris.Addresses a
	on a.AddressID = pa.AddressID
left join Polaris.polaris.PostalCodes pc
	on pc.PostalCodeID = a.PostalCodeID
left join Polaris.polaris.ItemCheckouts ic
	on ic.PatronID = n.PatronID and ic.ItemRecordID = n.ItemRecordID
left join polaris.Polaris.Languages l
	on l.LanguageID = pr.LanguageID
where n.DeliveryOptionID in (2, 3, 4, 5) 
and n.NotificationTypeID in (1, 2, 10, 11, 12, 13) 
and not exists (select 1 from polaris.Polaris.DatesClosed dc where (dc.OrganizationID = p.OrganizationID or dc.OrganizationID = o_patron.ParentOrganizationID) and cast(dc.DateClosed as date) = cast(getdate() as date))
and isnull(hoo.hours,'') not like '%' + left(datename(dw, getdate()), 3) + 'closed%'

GO
/****** Object:  View [dbo].[PolarisOrganizations]    Script Date: 7/11/2018 11:13:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[PolarisOrganizations]
AS
SELECT        OrganizationID, ParentOrganizationID, OrganizationCodeID, Name, Abbreviation, SA_ContactPersonID, CreatorID, ModifierID, CreationDate, ModificationDate, DisplayName
FROM            Polaris.Polaris.Organizations


GO
/****** Object:  View [dbo].[TodaysHoldCalls]    Script Date: 7/11/2018 11:13:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[TodaysHoldCalls]
AS
select distinct
	p.PatronID
	,nh.ReportingOrgId
	,isnull(o.ParentOrganizationID, 0) as ReportingLibraryID
from Results.Polaris.NotificationHistory nh
join Polaris.Polaris.Patrons p
	on nh.PatronId = p.PatronID
join Polaris.Polaris.Organizations o
	on o.OrganizationID = nh.ReportingOrgId
where cast(getdate() as date) = cast(nh.NoticeDate as date)
and nh.DeliveryOptionId in (3, 4, 5)
and nh.NotificationTypeId = 2
and exists (select 1 from Polaris.Polaris.SysHoldRequests shr where shr.PatronID = nh.PatronId and shr.TrappingItemRecordID = nh.ItemRecordId and shr.SysHoldStatusID = 6)

INSERT [dbo].[Dialout_String_Types] ([StringTypeID], [Description]) VALUES (1, N'Goodbye')
GO
INSERT [dbo].[Dialout_String_Types] ([StringTypeID], [Description]) VALUES (2, N'Greeting')
GO
INSERT [dbo].[Dialout_String_Types] ([StringTypeID], [Description]) VALUES (3, N'Hold date format string')
GO
INSERT [dbo].[Dialout_String_Types] ([StringTypeID], [Description]) VALUES (4, N'First hold message format string')
GO
INSERT [dbo].[Dialout_String_Types] ([StringTypeID], [Description]) VALUES (5, N'Additonal hold message format string')
GO
INSERT [dbo].[Dialout_String_Types] ([StringTypeID], [Description]) VALUES (6, N'placeholder')
GO
INSERT [dbo].[Dialout_String_Types] ([StringTypeID], [Description]) VALUES (7, N'Intro')
GO
INSERT [dbo].[Dialout_String_Types] ([StringTypeID], [Description]) VALUES (8, N'Branch name')
GO
INSERT [dbo].[Dialout_String_Types] ([StringTypeID], [Description]) VALUES (9, N'Overdue')
GO
INSERT [dbo].[Dialout_String_Types] ([StringTypeID], [Description]) VALUES (10, N'Renewal')
GO
INSERT [dbo].[Dialout_String_Types] ([StringTypeID], [Description]) VALUES (11, N'Repeat')
GO
INSERT [dbo].[RecordSetTypes] ([RecordSetTypeID], [Description]) VALUES (1, N'Bounce')
GO
INSERT [dbo].[RecordSetTypes] ([RecordSetTypeID], [Description]) VALUES (2, N'Spam')
GO
INSERT [dbo].[RecordSetTypes] ([RecordSetTypeID], [Description]) VALUES (3, N'SMS Stop')
GO
SET IDENTITY_INSERT [dbo].[SettingTypes] ON 
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (1, N'email_hold_template_html', N'Email notice hold template URL', N'/content/templates/default/hold.html')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (2, N'email_overdue_template_html', N'', N'/content/templates/default/overdue.html')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (4, N'email_bill_template_html', N' ', N'/content/templates/default/bill.html')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (5, N'email_almost_overdue_template_html', N' ', N'/content/templates/default/almostoverdue.html')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (6, N'email_second_hold_template_html', N' ', N'/content/templates/default/secondhold.html')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (7, N'emaiL_combined_template_html', N' ', N'/content/templates/default/combined.html')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (8, N'email_hold_item_row_format', N' ', N'<tr class="item-row">
	<td class="item-title">{item_title}</td>
	<td>{item_author}</td>
	<td>{item_material_type}</td>
	<td>{item_barcode}</td>
	<td>{held_date}</td>
</tr>')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (10, N'email_overdue_item_format', N' ', N'<tr class="item-row">
	<td class="item-title">{item_title}</td>
	<td>{item_author}</td>
	<td>{item_material_type}</td>
	<td>{item_barcode}</td>
	<td>{due_date}</td>
</tr>')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (11, N'email_bill_item_format', N' ', N'<tr class="item-row">
	<td class="item-title">{item_title}</td>
	<td>{item_author}</td>
	<td>{item_material_type}</td>
	<td>{item_barcode}</td>
	<td>{due_date}</td>
</tr>')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (12, N'email_reminder_item_format', N' ', N'<tr class="item-row">
	<td class="item-title">{item_title}</td>
	<td>{item_author}</td>
	<td>{item_material_type}</td>
	<td>{item_barcode}</td>
	<td>{due_date}</td>
</tr>')
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_SettingTypes_Mnemonic]    Script Date: 7/11/2018 11:13:05 AM ******/
ALTER TABLE [dbo].[SettingTypes] ADD  CONSTRAINT [IX_SettingTypes_Mnemonic] UNIQUE NONCLUSTERED 
(
	[Mnemonic] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UX_HangFire_CounterAggregated_Key]    Script Date: 7/11/2018 11:13:05 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UX_HangFire_CounterAggregated_Key] ON [HangFire].[AggregatedCounter]
(
	[Key] ASC
)
INCLUDE ( 	[Value]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_HangFire_Counter_Key]    Script Date: 7/11/2018 11:13:05 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Counter_Key] ON [HangFire].[Counter]
(
	[Key] ASC
)
INCLUDE ( 	[Value]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_Hash_ExpireAt]    Script Date: 7/11/2018 11:13:05 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Hash_ExpireAt] ON [HangFire].[Hash]
(
	[ExpireAt] ASC
)
INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_HangFire_Hash_Key]    Script Date: 7/11/2018 11:13:05 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Hash_Key] ON [HangFire].[Hash]
(
	[Key] ASC
)
INCLUDE ( 	[ExpireAt]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UX_HangFire_Hash_Key_Field]    Script Date: 7/11/2018 11:13:05 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UX_HangFire_Hash_Key_Field] ON [HangFire].[Hash]
(
	[Key] ASC,
	[Field] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_Job_ExpireAt]    Script Date: 7/11/2018 11:13:05 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Job_ExpireAt] ON [HangFire].[Job]
(
	[ExpireAt] ASC
)
INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_HangFire_Job_StateName]    Script Date: 7/11/2018 11:13:05 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Job_StateName] ON [HangFire].[Job]
(
	[StateName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_HangFire_JobParameter_JobIdAndName]    Script Date: 7/11/2018 11:13:05 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_JobParameter_JobIdAndName] ON [HangFire].[JobParameter]
(
	[JobId] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_HangFire_JobQueue_QueueAndFetchedAt]    Script Date: 7/11/2018 11:13:05 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_JobQueue_QueueAndFetchedAt] ON [HangFire].[JobQueue]
(
	[Queue] ASC,
	[FetchedAt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_List_ExpireAt]    Script Date: 7/11/2018 11:13:05 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_List_ExpireAt] ON [HangFire].[List]
(
	[ExpireAt] ASC
)
INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_HangFire_List_Key]    Script Date: 7/11/2018 11:13:05 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_List_Key] ON [HangFire].[List]
(
	[Key] ASC
)
INCLUDE ( 	[ExpireAt],
	[Value]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_Set_ExpireAt]    Script Date: 7/11/2018 11:13:05 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Set_ExpireAt] ON [HangFire].[Set]
(
	[ExpireAt] ASC
)
INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_HangFire_Set_Key]    Script Date: 7/11/2018 11:13:05 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Set_Key] ON [HangFire].[Set]
(
	[Key] ASC
)
INCLUDE ( 	[ExpireAt],
	[Value]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UX_HangFire_Set_KeyAndValue]    Script Date: 7/11/2018 11:13:05 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UX_HangFire_Set_KeyAndValue] ON [HangFire].[Set]
(
	[Key] ASC,
	[Value] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_State_JobId]    Script Date: 7/11/2018 11:13:05 AM ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_State_JobId] ON [HangFire].[State]
(
	[JobId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SettingValues] ADD  CONSTRAINT [DF_SettingValues_LanguageID]  DEFAULT ((1033)) FOR [LanguageID]
GO
ALTER TABLE [dbo].[Dialout_Strings]  WITH CHECK ADD  CONSTRAINT [FK_Dialout_Strings_Dialout_String_Types] FOREIGN KEY([StringTypeID])
REFERENCES [dbo].[Dialout_String_Types] ([StringTypeID])
GO
ALTER TABLE [dbo].[Dialout_Strings] CHECK CONSTRAINT [FK_Dialout_Strings_Dialout_String_Types]
GO
ALTER TABLE [dbo].[SettingValues]  WITH CHECK ADD  CONSTRAINT [FK_SettingValues_SettingTypes] FOREIGN KEY([Mnemonic])
REFERENCES [dbo].[SettingTypes] ([Mnemonic])
GO
ALTER TABLE [dbo].[SettingValues] CHECK CONSTRAINT [FK_SettingValues_SettingTypes]
GO
ALTER TABLE [HangFire].[JobParameter]  WITH CHECK ADD  CONSTRAINT [FK_HangFire_JobParameter_Job] FOREIGN KEY([JobId])
REFERENCES [HangFire].[Job] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [HangFire].[JobParameter] CHECK CONSTRAINT [FK_HangFire_JobParameter_Job]
GO
ALTER TABLE [HangFire].[State]  WITH CHECK ADD  CONSTRAINT [FK_HangFire_State_Job] FOREIGN KEY([JobId])
REFERENCES [HangFire].[Job] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [HangFire].[State] CHECK CONSTRAINT [FK_HangFire_State_Job]
GO
/****** Object:  StoredProcedure [dbo].[AddPatronNote]    Script Date: 7/11/2018 11:13:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AddPatronNote]
	@patronId int,
	@noteText varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if (exists (select 1 from Polaris.polaris.PatronNotes where patronid = @patronId))
	begin
		if ((select len(NonBlockingStatusNotes) from polaris.Polaris.PatronNotes where PatronID = @patronId) < 3900)
		begin
			update Polaris.Polaris.PatronNotes
			set NonBlockingStatusNotes = NonBlockingStatusNotes + @noteText
			where PatronID = @patronId
		end
	end
	else
	begin
		insert into Polaris.Polaris.PatronNotes(PatronID, NonBlockingStatusNotes) values (@patronId, @noteText)
	end
    
END
GO
/****** Object:  StoredProcedure [dbo].[GetCheckedOutItems]    Script Date: 7/11/2018 11:13:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetCheckedOutItems]
	@patronId int,
	@overdueOnly int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @dueDate datetime = case when @overdueOnly = 1 then current_timestamp else current_timestamp + 10000 end;

    select	ic.ItemRecordID,
			cir.Barcode,
			cir.MaterialTypeID,
			cir.ElectronicItem,
			br.BrowseTitle,
			ic.OrganizationID,
			ic.CheckOutDate,
			ic.DueDate
	from Polaris.Polaris.ItemCheckouts ic
	join Polaris.Polaris.CircItemRecords cir on
		ic.ItemRecordID = cir.ItemRecordID
	join Polaris.Polaris.BibliographicRecords br on
		cir.AssociatedBibRecordID = br.BibliographicRecordID
	where ic.PatronID = @patronId and ic.DueDate <= @dueDate
	order by ic.DueDate
END


GO
/****** Object:  StoredProcedure [dbo].[GetPatronsForEmail]    Script Date: 7/11/2018 11:13:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetPatronsForEmail]
	@emailAddress varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select pr.PatronID, p.Barcode, p.OrganizationID [BranchID], o.ParentOrganizationID [LibraryID]
	from Polaris.polaris.PatronRegistration pr
	join polaris.Polaris.patrons p
		on pr.PatronID = p.PatronID
	join polaris.Polaris.Organizations o
		on o.OrganizationID = p.OrganizationID
	where pr.EmailAddress = @emailAddress or pr.AltEmailAddress = @emailAddress
END
GO
/****** Object:  StoredProcedure [dbo].[GetPatronsForSmsNumber]    Script Date: 7/11/2018 11:13:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GetPatronsForSmsNumber]
	@pn varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select
		pr.patronid
		,p.OrganizationID [PatronBranchID]
		,o.ParentOrganizationID [PatronLibraryID]
		,p.Barcode
		,pr.PhoneVoice1
		,pr.Phone1CarrierID
		,pr.PhoneVoice2
		,pr.Phone2CarrierID
		,pr.PhoneVoice3
		,pr.Phone3CarrierID
		,pr.EmailAddress
		,pr.DeliveryOptionID
		,pr.TxtPhoneNumber
	from Polaris.Polaris.PatronRegistration pr
	join Polaris.polaris.Patrons p
		on pr.PatronID = p.PatronID
	join polaris.polaris.Organizations o
		on o.OrganizationID = p.OrganizationID
	where (pr.DeliveryOptionID = 8 or pr.EnableSMS = 1 or pr.TxtPhoneNumber is not null) and
	dbo.RemoveNonNumeric(case
		when pr.TxtPhoneNumber = 1 then pr.PhoneVoice1
		when pr.TxtPhoneNumber = 2 then pr.phonevoice2
		when pr.TxtPhoneNumber = 3 then pr.PhoneVoice3
	end) = @pn
END

GO
/****** Object:  StoredProcedure [dbo].[RemovePatronSmsSettings]    Script Date: 7/11/2018 11:13:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[RemovePatronSmsSettings]
	@patronid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    declare @currentCarrier int = ( select case when TxtPhoneNumber = 1 then Phone1CarrierID when TxtPhoneNumber = 2 then Phone2CarrierID when TxtPhoneNumber = 3 then Phone3CarrierID end from polaris.polaris.PatronRegistration where PatronID = @patronid )

	update Polaris.polaris.PatronRegistration
	set PhoneVoice1 = case when TxtPhoneNumber = 1 then '' else PhoneVoice1 end
		,Phone1CarrierID = case when TxtPhoneNumber = 1 then null else Phone1CarrierID end

		,PhoneVoice2 = case when TxtPhoneNumber = 2 then '' else PhoneVoice2 end
		,Phone2CarrierID = case when TxtPhoneNumber = 2 then null else Phone2CarrierID end

		,PhoneVoice3 = case when TxtPhoneNumber = 3 then '' else PhoneVoice3 end
		,Phone3CarrierID = case when TxtPhoneNumber = 3 then null else Phone3CarrierID end

		,TxtPhoneNumber = null
		,EnableSMS = 0
		,DeliveryOptionID = case when EmailAddress is not null and len(EmailAddress) > 0 then 2 else 1 end
		,eReceiptOptionID = case when eReceiptOptionID = 2 then 2 when eReceiptOptionID = 100 then 2 when eReceiptOptionID = 8 then null end
	where PatronID = @patronid

	select @currentCarrier
END
GO
USE [master]
GO
ALTER DATABASE [CLC_Notices] SET  READ_WRITE 
GO
