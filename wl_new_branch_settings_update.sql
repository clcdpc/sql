--Copy loan period from Old Worthington for Restricted 3 and Teacher/Educator patron codes
--Copy loan/hold limits from Old worthington for restricted 3 and Teacher/Educator if necessary
--Zero out hold/loan limits for all other patron codes

-- copy 34 to 100
begin tran

update lp2
set Units = lp.Units, TimeUnit = lp.TimeUnit
from Polaris.polaris.LoanPeriods lp
join polaris.Polaris.LoanPeriods lp2
	on lp.PatronCodeID = lp2.PatronCodeID and lp.LoanPeriodCodeID = lp2.LoanPeriodCodeID
where lp.OrganizationID = 34 and lp2.OrganizationID = 100 and lp.PatronCodeID in (9,15)

select *
from Polaris.polaris.LoanPeriods lp
where lp.OrganizationID in (34,100) and lp.PatronCodeID in (9,15)
order by lp.PatronCodeID, lp.LoanPeriodCodeID, lp.OrganizationID


update mll2
set MaxItems = mll.MaxItems, MaxRequestItems = mll.MaxRequestItems
from Polaris.Polaris.MaterialLoanLimits mll
join Polaris.Polaris.MaterialLoanLimits mll2
	on mll.PatronCodeID = mll2.PatronCodeID and mll.MaterialTypeID = mll2.MaterialTypeID
where mll.OrganizationID = 34 and mll2.OrganizationID = 100 and mll.PatronCodeID in (9,15)


update Polaris.Polaris.MaterialLoanLimits
set MaxItems = 0, MaxRequestItems = 0
where OrganizationID = 100 and PatronCodeID not in (9,15)

select *
from Polaris.Polaris.MaterialLoanLimits mll
where mll.OrganizationID in (34,100) and mll.PatronCodeID in (9,15)
order by mll.PatronCodeID, mll.MaterialTypeID, mll.OrganizationID


update pll2
set TotalItems = pll.TotalItems, TotalOverDue = pll.TotalOverDue, TotalHolds = pll.TotalHolds, TotalReserveItems = pll.TotalReserveItems, TotalILL = pll.TotalILL
from polaris.polaris.PatronLoanLimits pll
join polaris.polaris.PatronLoanLimits pll2
	on pll.PatronCodeID = pll2.PatronCodeID
where pll.OrganizationID = 34 and pll2.OrganizationID = 100 and pll.PatronCodeID in (9,15)

update Polaris.polaris.PatronLoanLimits
set TotalItems = 0, TotalOverDue = 0, TotalHolds = 0, TotalReserveItems = 0, TotalILL = 0
where OrganizationID = 100 and PatronCodeID not in (9,15)

select * 
from polaris.polaris.PatronLoanLimits pll
where pll.OrganizationID in (34,100) and pll.PatronCodeID in (9,15)
order by pll.PatronCodeID, pll.OrganizationID

--rollback
--commit