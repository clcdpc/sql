declare @recordsetid int = 530

declare @patronid int
declare cur CURSOR LOCAL for
    select top 10 prs.PatronID
	from Polaris.Polaris.PatronRecordSets prs 
	join Polaris.Polaris.Patrons p on
		prs.PatronID = p.PatronID
	where RecordSetID = @recordsetid and p.SystemBlocks = 1024

open cur

fetch next from cur into @patronid

while @@FETCH_STATUS = 0 BEGIN

    --execute your sproc on each row
    exec Polaris.Circ_RemoveCABlockByPatronID @patronid

    fetch next from cur into @patronid
END

close cur
deallocate cur


select top 10 prs.PatronID
from Polaris.Polaris.PatronRecordSets prs 
join Polaris.Polaris.Patrons p on
	prs.PatronID = p.PatronID
where RecordSetID = @recordsetid and p.SystemBlocks = 1024