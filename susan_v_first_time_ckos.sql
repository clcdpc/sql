/*
select  th.TransactionID
		,th.TranClientDate
		,th.OrganizationID
		,td_item.numValue [ItemId]
		,td_patron.numValue [PatronId]
		,td_mt.numValue [MaterialTypeId]
		,td_c.numValue [CollectionId]
		,td_sl.numValue [ShelfLocationId]
		,td_sc.numValue [StatClassId]
from PolarisTransactions.Polaris.TransactionHeaders th
join PolarisTransactions.Polaris.TransactionDetails td_item
	on td_item.TransactionID = th.TransactionID and td_item.TransactionSubTypeID = 38
join PolarisTransactions.Polaris.TransactionDetails td_patron
	on td_patron.TransactionID = th.TransactionID and td_patron.TransactionSubTypeID = 6
left join PolarisTransactions.polaris.TransactionDetails td_renewal
	on td_renewal.TransactionID = th.TransactionID and td_renewal.TransactionSubTypeID = 124
left join PolarisTransactions.polaris.TransactionDetails td_mt
	on td_mt.TransactionID = th.TransactionID and td_mt.TransactionSubTypeID = 4
left join PolarisTransactions.polaris.TransactionDetails td_c
	on td_c.TransactionID = th.TransactionID and td_c.TransactionSubTypeID = 61
left join PolarisTransactions.polaris.TransactionDetails td_sl
	on td_sl.TransactionID = th.TransactionID and td_sl.TransactionSubTypeID = 296
left join PolarisTransactions.polaris.TransactionDetails td_sc
	on td_sc.TransactionID = th.TransactionID and td_sc.TransactionSubTypeID = 60
where th.TransactionTypeID = 6001 and th.TranClientDate > '10/1/2019'

select * from PolarisTransactions.dbo.clc_custom_viewtransactiondetails vtd where vtd.TransactionID = 592678491

select td.TransactionID, count(*) 
from PolarisTransactions.polaris.TransactionHeaders th
join PolarisTransactions.polaris.TransactionDetails td 
	on td.TransactionID = th.TransactionID
where th.TransactionTypeID = 6001
group by td.TransactionID 
having count(*) != 20
*/

select count(*) from PolarisTransactions.Polaris.TransactionHeaders th where th.TransactionID between 382953379 and 382953379 + 100000000

select * from PolarisTransactions.polaris.TransactionHeaders th where th.TransactionID = 383359220

select *
from (
select th.TransactionID, rank() over(order by th.TransactionID) + 382953379 - 1 [rowrank]
from PolarisTransactions.Polaris.TransactionHeaders th 
where th.TransactionID between 382953379 and 382953379 + 100000000
) d
where d.TransactionID != d.rowrank

--99,962,096

declare @startTxnId int = ( select max(d.TxnId) from Polaris.dbo.CLC_Custom_First_Time_CKOs d )
declare @batchSize int = 100000000

insert into Polaris.dbo.CLC_Custom_First_Time_CKOs (TxnId,TxnDate,TxnOrgId,ItemRecordId,ItemAssignedBranchId,MaterialTypeId,AssignedCollectionCodeId,ShelfLocationId,ItemStatClassId,CheckoutTypeId,HoldRequestId,PatronId,PatronBranchId,PatronCodeId,PatronPostalCodeId,PatronStatClassId,ItemHomeBranchId,VendorAccountId,ElectronicItem)
select top 100000000 p.*
from (
	select td.TransactionID, th.TranClientDate, th.OrganizationID, tst.TransactionSubTypeDescription [SubType], coalesce(cast(td.numValue as varchar), cast(td.dateValue as varchar)) [thevalue]
	from PolarisTransactions.polaris.TransactionHeaders th
	join PolarisTransactions.Polaris.TransactionDetails td
		on td.TransactionID = th.TransactionID
	join PolarisTransactions.Polaris.TransactionSubTypes tst
		on tst.TransactionSubTypeID = td.TransactionSubTypeID
	where th.TransactionTypeID = 6001
		--and th.TranClientDate > '12/1/2019'
		and th.TransactionID between @startTxnId and @startTxnId + @batchSize
		and not exists ( select 1 from PolarisTransactions.Polaris.TransactionDetails td2 where td2.TransactionID = th.TransactionID and td2.TransactionSubTypeID = 124 and td2.numValue = 1 )
) x
pivot (
	max(x.thevalue)
	for x.SubType in (
		[ItemRecordID]	
		,[Items Assigned Branch ID]	
		,[MaterialType]
		,[Assigned Collection Code]
		,[Shelf location]
		,[Item Statistical Code]
		,[Checkout Type]
		,[HoldRequestID]
		,[PatronID]	
		,[Patrons Assigned Branch ID]	
		,[PatronCode]
		,[Patron Postal Code ID]
		,[PatronStatClassCode]
		,[Item Home BranchID]
		,[Vendor Account ID]
		,[Electronic Item]
	)
) p



--select * from PolarisTransactions.Polaris.TransactionSubTypes tst where tst.TransactionSubTypeID in (4,6,7,33,38,60,61,123,124,125,145,186,220,224,233,264,268,296,300,301,302)







/*
select count(*)
from (
	select td.TransactionID, th.TranClientDate, tst.TransactionSubTypeDescription [SubType], coalesce(cast(td.numValue as varchar), cast(td.dateValue as varchar)) [thevalue]
	from PolarisTransactions.polaris.TransactionHeaders th
	join PolarisTransactions.Polaris.TransactionDetails td
		on td.TransactionID = th.TransactionID
	join PolarisTransactions.Polaris.TransactionSubTypes tst
		on tst.TransactionSubTypeID = td.TransactionSubTypeID
	where th.TransactionTypeID = 6001
		and th.TranClientDate > '1/1/2019'
		--and (td.TransactionSubTypeID != 124 or td.TransactionSubTypeID = 124 and (td.numValue is null or td.numValue = 0))
) x
pivot (
	max(x.thevalue)
	for x.SubType in ([MaterialType],[PatronID],[PatronCode],[PatronStatClassCode],[ItemRecordID],[Item Statistical Code],[Assigned Collection Code]/*,[Renewal]*/,[Patrons Assigned Branch ID],[Items Assigned Branch ID],[Checkout Type],[Checkout Date],[CourseReserveID],[TermID],[HoldRequestID],[NextHoldRequestID],[Item Home BranchID],[Shelf location],[Vendor Account ID],[Electronic Item],[Patron Postal Code ID])
) p
*/

--truncate table polaris.dbo.CLC_Custom_First_Time_CKOs

-- 4:08 - 2022178
-- 3:28 - 12991039
--17:24 - 12929039
--------- 25132498
--------- 42035251

select count(*) from Polaris.dbo.CLC_Custom_First_Time_CKOs

select count(*) 
from PolarisTransactions.Polaris.TransactionHeaders th 
where th.TransactionTypeID = 6001
	and not exists ( select 1 from PolarisTransactions.Polaris.TransactionDetails td2 where td2.TransactionID = th.TransactionID and td2.TransactionSubTypeID = 124 and td2.numValue = 1 )


	select top 100 * from PolarisTransactions.Polaris.TransactionHeaders th where th.TranClientDate >= '1/1/2017'

select top 100 * 
from
(
select top 100 th.TransactionID, th.TranClientDate
from PolarisTransactions.Polaris.TransactionHeaders th
where th.TransactionID between 382953379 and 382953379 + 100000000
order by th.TranClientDate desc
) d
order by d.TranClientDate desc


--713082778
--100000000
--382953379

select max(d.TxnId) from Polaris.dbo.CLC_Custom_First_Time_CKOs d