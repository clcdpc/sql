;with groups as (
	select distinct pg.GroupID
	from Polaris.Polaris.PermissionGroups pg
	where pg.PermissionID = 482
)

insert into Polaris.polaris.PermissionGroups
select cr.ControlRecordID, 482, g.GroupID--, '' [ ], cr.*
from polaris.polaris.controlrecords cr
join polaris.polaris.ControlRecordDefs crd
	on crd.ControlRecordDefID = cr.ControlRecordDefID
join polaris.polaris.PermissionDefs pd
	on pd.ControlRecordDefID = crd.ControlRecordDefID
cross join groups g
where crd.ControlRecordDefID = 20 and pd.PermissionNameID = 71 and not exists ( select 1 from Polaris.polaris.PermissionGroups pg where pg.GroupID = g.GroupID and pg.ControlRecordID = cr.ControlRecordID and pg.PermissionID = 482 )
order by g.GroupID

--rollback
--commit