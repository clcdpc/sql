set nocount on

	declare @associations table (parent int, child int)
begin -- configure branch associations
	insert into @associations values (9  ,145) -- fcdl lockers -> fcdl main
	insert into @associations values (6  ,150) -- ghpl drive through -> ghpl main
	insert into @associations values (51 ,73)  -- new albany drive up -> new albany
	insert into @associations values (52 ,71)  -- nlt drive up -> nlt
	insert into @associations values (56 ,68)  -- parsons drive up -> parsons
	insert into @associations values (58 ,70)  -- shepard drive up -> shepard
	insert into @associations values (63 ,69)  -- whitehall drive up -> whitehall
	insert into @associations values (11 ,138) -- fcdl baltimore lockers -> fcdl baltimore
	insert into @associations values (11 ,126) -- fcdl millersport kiosk -> fcdl baltimore
	-- mpl misc -> mpl
	insert into @associations values (17,117)
	insert into @associations values (17,118)
	insert into @associations values (17,152)
	insert into @associations values (17,119)
	insert into @associations values (17,120)
	insert into @associations values (17,121)
	insert into @associations values (17,122)
	insert into @associations values (17,123)
	insert into @associations values (17,124)
	insert into @associations values (17,151)
	insert into @associations values (17,125)
	insert into @associations values (17,141)
	insert into @associations values (17,140)
	-- end mpl misc
	insert into @associations values (20 ,98)  -- pcl drive through -> pcl main
	insert into @associations values (24 ,154) -- ppl lockers -> ppl
	insert into @associations values (24 ,143) -- ppl drive up -> ppl
	insert into @associations values (103,104) -- sycamore lockers -> sycamore
	-- mpc schools -> mpc
	insert into @associations values (15,129)
	insert into @associations values (15,128)
	insert into @associations values (15,127)
	insert into @associations values (15,132)
	insert into @associations values (15,133)
	insert into @associations values (15,134)
	insert into @associations values (15,135)
	insert into @associations values (15,136)
	-- end mpc schools
	insert into @associations values (43,147) -- cml gahanna curbside -> cml gahanna
	insert into @associations values (44,148) -- cml hilliard curbside -> cml hilliard
	insert into @associations values (62,146) -- cml whetstone curbside -> cml whetstone
	insert into @associations values (107,116) -- dcdl orange window -> dcdl orange
	insert into @associations values (15,130) -- mpc homebound -> mpc
	insert into @associations values (15,131) -- mpc lockers -> mpc
	insert into @associations values (81,142) -- tremont curbside -> tremont
	insert into @associations values (26,144) -- wagnalls lockers -> wagnalls
	insert into @associations values (34, 89) -- wl vending lazelle -> wowl
	insert into @associations values (34, 96) -- wl vending rec -> wowl
	insert into @associations values (33, 38) -- wl northwest lockers -> wl northwest
	insert into @associations values (33, 97) -- wl northwest drive up -> wl northwest
	insert into @associations values (34, 37) -- wowl lockers -> wowl
	insert into @associations values (35, 94) -- worthington park lockers -> worthington park 
	insert into @associations values(153,155) -- liberty drive up -> liberty
end

begin -- Route setup
	declare @routes table ( orgid int, rte int)
	-- Route 1
	insert into @routes values (43, 1) -- CML Gahanna
	insert into @routes values (51, 1) -- CML New Albany
	insert into @routes values (3,  1) -- Alexandria
	insert into @routes values (113,1) -- Granville
	insert into @routes values (115,1) -- Pataskala
	insert into @routes values (63, 1) -- CML Whitehall
	insert into @routes values (50, 1) -- CML MLK
	insert into @routes values (58, 1) -- CML Shepard

	-- Route 2
	insert into @routes values (47, 2) -- CML Linden
	insert into @routes values (53, 2) -- CML Northside
	insert into @routes values (62, 2) -- CML Whetstone
	insert into @routes values (44, 2) -- CML Hilliard
	insert into @routes values (31, 2) -- SPL Westland
	insert into @routes values (29, 2) -- SPL Grove City
	insert into @routes values (45, 2) -- CML Hilltop
	insert into @routes values (42, 2) -- CML Franklinton

	-- Route 3
	insert into @routes values (49, 3) -- CML Main
	insert into @routes values (57, 3) -- CML Reynoldsburg
	insert into @routes values (103,3) -- PPL Sycamore
	insert into @routes values (24, 3) -- PPL Main
	insert into @routes values (11, 3) -- FCDL Baltimore
	insert into @routes values (12, 3) -- FCDL Bremen
	insert into @routes values (9,  3) -- FCDL Main
	insert into @routes values (13, 3) -- FCDL Northwest
	insert into @routes values (40, 3) -- CML Driving Park

	-- Route 4
	insert into @routes values (52, 4) -- CML Northern Lights
	insert into @routes values (46, 4) -- CML Karl Road
	insert into @routes values (34, 4) -- WL Old Worthington
	insert into @routes values (108, 4)-- Delaware Powell
	insert into @routes values (33, 4) -- WL Northwest
	insert into @routes values (41, 4) -- CML Dublin
	insert into @routes values (80, 4) -- UA Lane Road
	insert into @routes values (81, 4) -- UA Tremont
	insert into @routes values (79, 4) -- UA Miller Park

	-- Route 5
	insert into @routes values (48, 5) -- CML Barnett
	insert into @routes values (60, 5) -- CML Southeast
	insert into @routes values (99, 5) -- CML Canal Winchester
	insert into @routes values (26, 5) -- Wagnalls
	insert into @routes values (10, 5) -- FCDL Johns
	insert into @routes values (20,	5) -- Pickaway Main
	insert into @routes values (22, 5) -- Pickaway Younkin
	insert into @routes values (59, 5) -- CML South High
	insert into @routes values (77, 5) -- CML Marion Franklin
	insert into @routes values (56, 5) -- CML Parsons
	insert into @routes values (85, 5) -- Bexley

	-- Route 6
	insert into @routes values (35, 6) -- WL Worthington Park
	insert into @routes values (107,6) -- Delaware Orange
	insert into @routes values (106,6) -- Delaware Main
	insert into @routes values (109,6) -- Delaware Ostrander
	insert into @routes values (17, 6) -- Marysville Main
	insert into @routes values (18, 6) -- Marysville Raymond
	insert into @routes values (15, 6) -- Plain City
	insert into @routes values (87, 6) -- London
	insert into @routes values (7,  6) -- Grandview
	insert into @routes values (117,6) -- Marysville Early College High School
	insert into @routes values (118,6) -- Marysville High School
	insert into @routes values (119,6) -- MPL Bunsold Middle School
	insert into @routes values (120,6) -- MPL Creekview Intermediate
	insert into @routes values (121,6) -- MPL Edgewood Elementary
	insert into @routes values (122,6) -- MPL Mill Valley Elementary
	insert into @routes values (123,6) -- MPL Navin Elementary
	insert into @routes values (124,6) -- MPL Northwood Elementary
	insert into @routes values (125,6) -- MPL Raymond Elementary

	-- Special additions
	--insert into @routes values (95, (select rte from @routes where orgid = 24)) -- Add Pickerington Pickup Locker to the same route as Pickerington Library
	--insert into @routes values (104, (select rte from @routes where orgid = 103)) -- Add Pickerington Sycamore Pickup Locker to the same route as Pickerington Sycamore
	--insert into @routes values (116, (select rte from @routes where orgid = 107)) -- Add Delaware Orange Pickup Window to same route as Delaware Orange
	--insert into @routes values (126, (select rte from @routes where orgid = 11)) -- Add Millersport kiosk to same route as FCDL Baltimore
	--insert into @routes values (142, (select rte from @routes where orgid = 81)) -- Add UA-TD to same route as UA-T

	insert into @routes
	select a.child, r.rte
	from @associations a
	join @routes r
		on r.orgid = a.parent
	where not exists ( select 1 from @routes r2 where r2.orgid = a.child )

end

begin -- CML branch queue config
	declare @cml table ( orgid int, q int, foo int identity(1,1), seq int default(null) )
	insert into @cml (orgid,q) values (44,1) -- CML Hilliard Branch
	insert into @cml (orgid,q) values (41,1) -- CML Dublin Branch
	insert into @cml (orgid,q) values (40,1) -- CML Driving Park Branch
	insert into @cml (orgid,q) values (50,1) -- CML Martin Luther King Branch
	insert into @cml (orgid,q) values (46,1) -- CML Karl Road Branch
	insert into @cml (orgid,q) values (60,1) -- CML Southeast Branch
	insert into @cml (orgid,q) values (53,1) -- CML Northside Branch
	insert into @cml (orgid,q) values (49,1) -- CML Columbus Main Library
	insert into @cml (orgid,q) values (43,1) -- CML Gahanna Branch
	insert into @cml (orgid,q) values (99,1) -- CML Canal Winchester Branch
	insert into @cml (orgid,q) values (47,1) -- CML Linden Branch
	insert into @cml (orgid,q) values (56,1) -- CML Parsons Branch
	insert into @cml (orgid,q) values (51,1) -- CML New Albany Branch
	insert into @cml (orgid,q) values (52,1) -- CML Northern Lights Branch
	insert into @cml (orgid,q) values (63,1) -- CML Whitehall Branch
	insert into @cml (orgid,q) values (77,1) -- CML Marion-Franklin Branch
	insert into @cml (orgid,q) values (62,1) -- CML Whetstone Branch
	insert into @cml (orgid,q) values (59,1) -- CML South High Branch
	insert into @cml (orgid,q) values (48,1) -- CML Livingston Branch
	insert into @cml (orgid,q) values (58,1) -- CML Shepard Branch
	insert into @cml (orgid,q) values (42,1) -- CML Franklinton Branch
	insert into @cml (orgid,q) values (57,1) -- CML Reynoldsburg Branch
	insert into @cml (orgid,q) values (45,1) -- CML Hilltop Branch
	insert into @cml (orgid,q) values (54,1) -- CML Operations Center
	insert into @cml (orgid,q) values (64,1) -- CML ILL

	-- Queue 2
	insert into @cml (orgid,q) values (62,2) -- CML Whetstone Branch
	insert into @cml (orgid,q) values (59,2) -- CML South High Branch
	insert into @cml (orgid,q) values (48,2) -- CML Livingston Branch
	insert into @cml (orgid,q) values (58,2) -- CML Shepard Branch
	insert into @cml (orgid,q) values (42,2) -- CML Franklinton Branch
	insert into @cml (orgid,q) values (52,2) -- CML Northern Lights Branch
	insert into @cml (orgid,q) values (56,2) -- CML Parsons Branch
	insert into @cml (orgid,q) values (57,2) -- CML Reynoldsburg Branch
	insert into @cml (orgid,q) values (45,2) -- CML Hilltop Branch
	insert into @cml (orgid,q) values (44,2) -- CML Hilliard Branch
	insert into @cml (orgid,q) values (41,2) -- CML Dublin Branch
	insert into @cml (orgid,q) values (40,2) -- CML Driving Park Branch
	insert into @cml (orgid,q) values (50,2) -- CML Martin Luther King Branch
	insert into @cml (orgid,q) values (46,2) -- CML Karl Road Branch
	insert into @cml (orgid,q) values (60,2) -- CML Southeast Branch
	insert into @cml (orgid,q) values (53,2) -- CML Northside Branch
	insert into @cml (orgid,q) values (49,2) -- CML Columbus Main Library
	insert into @cml (orgid,q) values (43,2) -- CML Gahanna Branch
	insert into @cml (orgid,q) values (99,2) -- CML Canal Winchester Branch
	insert into @cml (orgid,q) values (47,2) -- CML Linden Branch
	insert into @cml (orgid,q) values (51,2) -- CML New Albany Branch
	insert into @cml (orgid,q) values (63,2) -- CML Whitehall Branch
	insert into @cml (orgid,q) values (77,2) -- CML Marion-Franklin Branch
	insert into @cml (orgid,q) values (65,2) -- CML Tech Services
	insert into @cml (orgid,q) values (54,2) -- CML Operations Center
	insert into @cml (orgid,q) values (64,2) -- CML ILL

	-- Queue 3
	insert into @cml (orgid,q) values (43,3) -- CML Gahanna Branch
	insert into @cml (orgid,q) values (99,3) -- CML Canal Winchester Branch
	insert into @cml (orgid,q) values (56,3) -- CML Parsons Branch
	insert into @cml (orgid,q) values (47,3) -- CML Linden Branch
	insert into @cml (orgid,q) values (51,3) -- CML New Albany Branch
	insert into @cml (orgid,q) values (52,3) -- CML Northern Lights Branch
	insert into @cml (orgid,q) values (63,3) -- CML Whitehall Branch
	insert into @cml (orgid,q) values (77,3) -- CML Marion-Franklin Branch
	insert into @cml (orgid,q) values (62,3) -- CML Whetstone Branch
	insert into @cml (orgid,q) values (59,3) -- CML South High Branch
	insert into @cml (orgid,q) values (48,3) -- CML Livingston Branch
	insert into @cml (orgid,q) values (58,3) -- CML Shepard Branch
	insert into @cml (orgid,q) values (42,3) -- CML Franklinton Branch
	insert into @cml (orgid,q) values (57,3) -- CML Reynoldsburg Branch
	insert into @cml (orgid,q) values (45,3) -- CML Hilltop Branch
	insert into @cml (orgid,q) values (44,3) -- CML Hilliard Branch
	insert into @cml (orgid,q) values (41,3) -- CML Dublin Branch
	insert into @cml (orgid,q) values (40,3) -- CML Driving Park Branch
	insert into @cml (orgid,q) values (50,3) -- CML Martin Luther King Branch
	insert into @cml (orgid,q) values (46,3) -- CML Karl Road Branch
	insert into @cml (orgid,q) values (60,3) -- CML Southeast Branch
	insert into @cml (orgid,q) values (53,3) -- CML Northside Branch
	insert into @cml (orgid,q) values (49,3) -- CML Columbus Main Library
	insert into @cml (orgid,q) values (54,3) -- CML Operations Center
	insert into @cml (orgid,q) values (64,3) -- CML ILL

	update c2
	set c2.seq = d.therank
	from @cml c2
	join (
		select c.orgid
				,c.q
				,rank() over(partition by c.q order by c.foo) [therank]
		from @cml c
	) d
		on d.orgid = c2.orgid and d.q = c2.q
end

begin -- CML branch queue assignment
	declare @queues table ( orgid int, q int )
	-- Queue1
	insert into @queues values (3,1)  -- Alexandria Public Library
	insert into @queues values (49,1) -- CML Columbus Main Library
	insert into @queues values (40,1) -- CML Driving Park Branch
	insert into @queues values (41,1) -- CML Dublin Branch
	insert into @queues values (44,1) -- CML Hilliard Branch
	insert into @queues values (46,1) -- CML Karl Road Branch
	insert into @queues values (50,1) -- CML Martin Luther King Branch
	insert into @queues values (53,1) -- CML Northside Branch
	insert into @queues values (54,1) -- CML Operations Center
	insert into @queues values (55,1) -- CML OSU Thompson
	insert into @queues values (88,1) -- CML School Delivery
	insert into @queues values (60,1) -- CML Southeast Branch
	insert into @queues values (65,1) -- CML Tech Services
	insert into @queues values (111,1)-- Delaware Columbus State Community College Annex
	insert into @queues values (109,1)-- Delaware County District Library Ostrander Branch
	insert into @queues values (10,1) -- Fairfield County Johns Branch
	insert into @queues values (113,1)-- Granville Public Library
	insert into @queues values (87,1) -- London Public Library
	insert into @queues values (17,1) -- Marysville Public Library
	insert into @queues values (21,1) -- Pickaway Outreach
	insert into @queues values (95,1) -- Pickerington 24 Hour Pickup Lockers
	insert into @queues values (24,1) -- Pickerington Public Library
	insert into @queues values (104,1)-- Pickerington Sycamore Plaza 24 Hour Pickup Lockers
	insert into @queues values (76,1) -- SPL Tech Services
	insert into @queues values (83,1) -- UA Technical Services
	insert into @queues values (81,1) -- UA Tremont Road
	insert into @queues values (38,1) -- WL Northwest After Hours Pickup Locker
	insert into @queues values (75,1) -- WL Technical Services
	insert into @queues values (94,1) -- WL Worthington Park After Hours Pickup Locker
	insert into @queues values (101,1)-- SPLGC Mobile
	insert into @queues values (102,1)-- SPLWL Mobile
	insert into @queues values (89,1) -- WL Materials Vending Lazelle

	insert into @queues values (5,2)  -- CLC Electronic Library
	insert into @queues values (42,2) -- CML Franklinton Branch
	insert into @queues values (45,2) -- CML Hilltop Branch
	insert into @queues values (48,2) -- CML Livingston Branch
	insert into @queues values (66,2) -- CML Outreach
	insert into @queues values (67,2) -- CML R2R
	insert into @queues values (57,2) -- CML Reynoldsburg Branch
	insert into @queues values (58,2) -- CML Shepard Branch
	insert into @queues values (70,2) -- CML Shepard Drive-up Window
	insert into @queues values (59,2) -- CML South High Branch
	insert into @queues values (62,2) -- CML Whetstone Branch
	insert into @queues values (108,2)-- Delaware County District Library Powell Branch
	insert into @queues values (12,2) -- Fairfield County Bremen Branch
	insert into @queues values (9,2)  -- Fairfield County Main Library
	insert into @queues values (7,2)  -- Grandview Heights Public Library
	insert into @queues values (18,2) -- MPL Raymond Branch
	insert into @queues values (20,2) -- Pickaway Main Library Circleville
	insert into @queues values (30,2) -- SPL Outreach
	insert into @queues values (31,2) -- SPL Westland
	insert into @queues values (82,2) -- UA Outreach
	insert into @queues values (97,2) -- WL Northwest Drive Up Window
	insert into @queues values (34,2) -- WL Old Worthington
	insert into @queues values (37,2) -- WL Old Worthington After Hours Pickup Locker
	insert into @queues values (35,2) -- WL Worthington Park
	insert into @queues values (100,2)-- WL Worthington Schools
	insert into @queues values (96,2) -- WL Materials Vending Worthington Rec
	insert into @queues values (91,2) -- WL Mobile
	insert into @queues values (36,2) -- WL Outreach

	-- Queue 3
	insert into @queues values (64,3) -- CML ILL
	insert into @queues values (99,3) -- CML Canal Winchester Branch
	insert into @queues values (43,3) -- CML Gahanna Branch
	insert into @queues values (47,3) -- CML Linden Branch
	insert into @queues values (77,3) -- CML Marion-Franklin Branch
	insert into @queues values (51,3) -- CML New Albany Branch
	insert into @queues values (73,3) -- CML New Albany Drive-up Window
	insert into @queues values (52,3) -- CML Northern Lights Branch
	insert into @queues values (71,3) -- CML Northern Lights Drive-up Window
	insert into @queues values (56,3) -- CML Parsons Branch
	insert into @queues values (68,3) -- CML Parsons Drive-up Window
	insert into @queues values (63,3) -- CML Whitehall Branch
	insert into @queues values (69,3) -- CML Whitehall Drive-up Window
	insert into @queues values (106,3)-- Delaware County District Library Main Branch
	insert into @queues values (107,3)-- Delaware County District Library Orange Branch
	insert into @queues values (110,3)-- Delaware County District Library Outreach Services
	insert into @queues values (116,3)-- Delaware County Library Orange Drive-Up Window
	insert into @queues values (11,3) -- Fairfield County Baltimore Branch
	insert into @queues values (13,3) -- Fairfield County Northwest Branch
	insert into @queues values (115,3)-- Pataskala Public Library
	insert into @queues values (22,3) -- Pickaway Younkin Branch Ashville
	insert into @queues values (103,3)-- Pickerington Sycamore Plaza Library
	insert into @queues values (15,3) -- Plain City Public Library
	insert into @queues values (29,3) -- SPL Grove City
	insert into @queues values (80,3) -- UA Lane Road
	insert into @queues values (79,3) -- UA Miller Park
	insert into @queues values (26,3) -- Wagnalls Memorial Library
	insert into @queues values (33,3) -- WL Northwest
	insert into @queues values (85,3) -- Bexley Public Library
	insert into @queues values (27,3) -- FCDL Outreach Services
	insert into @queues values (93,3) -- GHPL Book Cart
	insert into @queues values (98,3) -- PCL Circleville Middle School

	insert into @queues
	select a.child, q.q
	from @associations a
	join @queues q
		on q.orgid = a.parent
	where not exists ( select 1 from @queues q2 where q2.orgid = a.child )
end

begin -- Self responder only setup
	declare @self_responders table ( orgid int )
	insert into @self_responders values --(21),	-- PCL Outreach
										(27)	-- FCDL Outreach Services
										,(30)	-- SPL Outreach
										,(36)	-- WL Outreach
										,(66)	-- CML Outreach
										,(67)	-- CML R2R
										,(74)	-- CML R2R Bookmobile
										,(82)	-- UA Outreach
										,(83)	-- UA Technical Services
										,(93)	-- GHPL Book Cart
										,(100)  -- WL Worthington Kilbourne High School
										,(101)	-- SPLGC Mobile
										,(102)	-- SPLWL Mobile
										,(117)	-- Marysville Early College High School
										,(118)	-- Marysville High School
										,(119)	-- MPL Bunsold Middle School
										,(120)	-- MPL Creekview Intermediate
										,(121)	-- MPL Edgewood Elementary
										,(122)	-- MPL Mill Valley Elementary
										,(123)	-- MPL Navin Elementary
										,(124)	-- MPL Northwood Elementary
										,(125)	-- MPL Raymond Elementary
										,(126)  -- Fairfield Millersport Kiosk
										,(127),(128),(129),(130),(131),(132),(133),(134),(135),(136) -- MPC School Branches
										,(138) -- Fairfield County Baltimore 24 Hour Pickup Lockers
										,(140) -- MPL Trinity Lutheran School
										,(141) -- MPL St. John's Lutheran School
										,(142) -- UA Tremont Curbside Pickup
										,(143) -- Pickerington Main Drive-Up Window
										,(144) -- Wagnalls Pickup Locker
										,(145) -- FCDL Main Lockers
										,(146) -- CML Whetstone Curbside
										,(147) -- CML Gahanna Curbside
										,(148) -- CML Hilliard Curbside
										,(149) -- CML Dublin Curbside
										,(150) -- Grandview Hts Drive-Thru Pickup
										,(151) -- MPL Raymond Branch Pickup Lockers
										,(152) -- Marysville Public Library Pickup Lockers
										,(154) -- Pickerington Library On-The-Go
										,(155) -- Delaware County Library Liberty Drive-Up Window
										,(156) -- WL Pop Up Library
										,(115) -- Pataskala Temp
end

begin -- Excluded branches setup
	declare @exclude table ( orgid int )
	insert into @exclude values (5)    -- CLC Electronic Library
								,(37)  -- WL Old Worthington After Hours Pickup Locker
								,(38)  -- WL Northwest After Hours Pickup Locker
								,(55)  -- CML OSU Thompson
								,(61)  -- zzzdonotuseCML Weinland Branch
								,(68)  -- CML Parsons Drive-Up Window
								,(69)  -- CML Whitehall Drive-up Window
								,(70)  -- ZZZ CML Do Not Use Formerly R2R Northland
								,(71)  -- ZZZ CML Do Not Use Formerly R2R Hilltop
								,(72)  -- ZZZ CML Do Not Use Formerly R2R Whitehall
								,(73)  -- CML New Albany Drive-up Window
								,(88)  -- CML School Delivery
								,(89)  -- WL Materials Vending Lazelle
								,(91)  -- WL Mobile
								,(92)  -- CML Local History and Genealogy
								,(94)  -- WL Worthington Park After Hours Pickup Locker
								,(95)  -- Pickerington 24 Hour Pickup Lockers
								,(96)  -- WL Materials Vending Worthington Rec
								,(97)  -- WL Northwest Drive Up Window
								,(98)  -- PCL Circleville Middle School
								,(104) -- Pickerington Sycamore Plaza 24 Hour Pickup Lockers
								,(116) -- Delaware Orange Drive-Up Window
								,(137) -- Columbus City Schools
								,(139) -- CLC Test branch
								,(159), (160), (161), (162), (163), (164), (165), (166), (167), (168) -- Westerville temp
end

begin -- Special branch orders to make locations first for their alternative pickup locations
	declare @special_orders table ( orgid int, responder int, seq int )
	insert into @special_orders values ( 94, 35, -98 ) -- Worthington Park as #1 for their pickup Locker
	insert into @special_orders values ( 97, 33, -98 ) -- Northwest as #1 for their drive up window
	insert into @special_orders values ( 38, 33, -98 ) -- Northwest as #1 for their pickup locker
	insert into @special_orders values ( 37, 34, -98 ) -- Old Worthington as #1 for their pickup locker
	insert into @special_orders values ( 95, 24, -98 ) -- Pickerington as #1 for their pickup locker
	insert into @special_orders values (103,104, -98 ) -- Pickerington Sycamore as #1 for their pickup locker
	insert into @special_orders values (116,107, -98 ) -- Delaware Orange as #1 for their drive up window
	insert into @special_orders values ( 68, 56, -98 ) -- CML Parsons as #1 for their drive up window
	insert into @special_orders values ( 69, 63, -98 ) -- CML Whitehall as #1 for their drive up window
	insert into @special_orders values ( 70, 58, -98 ) -- CML Shepard as #1 for their drive up window
	insert into @special_orders values ( 71, 52, -98 ) -- CML Northern Lights as #1 for their drive up window
	insert into @special_orders values ( 73, 51, -98 ) -- CML New Albany as #1 for their drive up window
	insert into @special_orders values (155,153, -99 ) -- liberty as #1 for their drive up window
end

begin -- MaxDaysInRTF exceptions for small branches
	declare @maxdays table ( orgid int, maxdays int )
	insert into @maxdays values (3, 1)	-- Alexandria Public Library
	insert into @maxdays values (15, 1) -- Plain City Public Library
	insert into @maxdays values (44, 3) -- CML Hilliard
end

set nocount off

declare @table table ( requester int, requestername varchar(255), responder int, respondername varchar(255), seq int, maxdays int, flag int )
declare @THIS_BRANCH int = -99
declare @MAIN_LIB int = -98
declare @SAME_LIB int = -97
declare @DEFAULT_CML int = 97
declare @SAME_ROUTE int = 98


-- build primary queue
insert into @table
select	o.OrganizationID [Requester ID], 
		o.Name [Requester Name],
		o2.OrganizationID [Responder ID],
		o2.Name [Responder Name],
		case 
			when o.OrganizationID = o2.OrganizationID then @THIS_BRANCH
			when o2.ParentOrganizationID = 39 or o.ParentOrganizationID = 39 then COALESCE((select seq from @cml cml2 where cml2.orgid = data.orgid and cml2.q = (select q from @queues where [@queues].orgid = o.OrganizationID)),@DEFAULT_CML)
			when o.ParentOrganizationID = o2.ParentOrganizationID then 
				case 
					when o2.name like '%main%' or o2.name like '%tremont%' then @MAIN_LIB					
					else @SAME_LIB
				end	
			when (select _rte.rte from @routes _rte where _rte.orgid = o.OrganizationID) = (select _rte.rte from @routes _rte where _rte.orgid = o2.OrganizationID)	then @SAME_ROUTE					
			else 99 
		end [_sequence],
		case when o.OrganizationID = o2.OrganizationID then 5 else COALESCE(m.maxdays,5) end [MaxDaysInRTF],
		1 [PrimarySecondaryFlag]
from polaris.Polaris.Organizations o
cross join 
(
	select distinct COALESCE(r.orgid, c.orgid, o3.OrganizationID) [orgid], o3.ParentOrganizationID [library], r.rte, case when o3.ParentOrganizationID = 39 then 1 else 0 end [is_cml]
	from Polaris.Polaris.Organizations o3
	left join @routes r on
		o3.OrganizationID = r.orgid
	full outer join @cml c on
		r.orgid = c.orgid
	where o3.OrganizationCodeID = 3
) data
join Polaris.Polaris.Organizations o2 on
	data.orgid = o2.OrganizationID
left join @maxdays m on
	o2.OrganizationID = m.orgid
where (data.is_cml = 1 or (data.rte = (select rte from @routes r where r.orgid = o.OrganizationID)) or o2.ParentOrganizationID = o.ParentOrganizationID) and o.OrganizationCodeID = 3
order by o.OrganizationID, _sequence

-- build secondary queue
insert into @table
select	o.OrganizationID
		,o.Name
		,o2.OrganizationID
		,o2.Name
		,case when o2.ParentOrganizationID = 39 then o2.OrganizationID else o2.OrganizationID + 1000 end [seq]
		,case when o.OrganizationID = o2.OrganizationID then 5 else COALESCE(md.maxdays,5) end
		,2
from polaris.polaris.Organizations o
cross join polaris.polaris.organizations o2
left join @maxdays md
	on md.orgid = o2.OrganizationID
where o.OrganizationCodeID = 3 and o2.OrganizationCodeID = 3

-- handle special branch orders
update t
set t.seq = so.seq
from @table t
join @special_orders so
	on so.orgid = t.requester and so.responder = t.responder
where t.flag = 1

-- remove only-self-responders from other branches
delete t
from @table t
join @self_responders e on
	t.responder = e.orgid
where t.requester != e.orgid
	and not exists ( select 1 from @special_orders so where so.orgid = t.requester and so.responder = t.responder )

-- remove excluded branches
delete from @table
where responder in ( select * from @exclude )

-- Remove ALL Non-CML locations from CML primary
delete t
from @table t
join Polaris.polaris.organizations o
	on o.OrganizationID = t.requester
join polaris.polaris.organizations o2
	on o2.OrganizationID = t.responder
where o.ParentOrganizationID = 39 and o2.ParentOrganizationID != 39 and t.flag = 1

-- debug select
--select t.requester, t.requestername, t.responder, t.respondername, t.maxdays, row_number() over(partition by t.requester, t.flag order by t.seq, t.responder) [newseq]/*, t.seq */, t.flag from @table t order by t.requester, t.flag, newseq




--compare against current
/*
select shrs.RequesterBranchID
		,o_requester.Name [RequesterName]
		,shrs.ResponderBranchID
		,o_responder.Name [ResponderName]
		,shrs.MaxDaysInRTF
		,shrs.Sequence
		,shrs.PrimarySecondaryFlag
from polaris.polaris.SysHoldRoutingSequences shrs
join polaris.polaris.organizations o_requester
	on o_requester.OrganizationID = shrs.RequesterBranchID
join polaris.polaris.organizations o_responder
	on o_responder.OrganizationID = shrs.ResponderBranchID
order by shrs.RequesterBranchID, shrs.PrimarySecondaryFlag, shrs.Sequence
*/


/*
insert into clcdb.dbo.SysHoldRoutingSequencesDev
select	1,
		t.requester, 
		t.responder, 
		ROW_NUMBER() over(partition by t.requester, t.flag order by t.seq, t.responder), 
		t.maxdays, 
		t.flag
from @table t
*/


/* do the actual insert */
begin tran; delete from Polaris.Polaris.SysHoldRoutingSequences; insert into Polaris.Polaris.SysHoldRoutingSequences
select	t.requester, 
		t.responder, 
		ROW_NUMBER() over(partition by t.requester, t.flag order by t.seq, t.responder), 
		t.maxdays, 
		t.flag
from @table t
order by t.requester, t.flag, t.seq


--rollback
--commit
