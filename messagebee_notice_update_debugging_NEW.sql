/*
patronid, itemid
1270799, 27526837/27355862			- not all failure updates in polaris
2601337, 27266515/25370242			- nothing in messagebee nor polaris
2404406, 14032508/25139544/14020458 - nothing in messagebee nor polaris
2611834, 20653336					- nothing in messagebee nor polaris
2692972, 19386551					- nothing in messagebee nor polaris
2633437, 27336454					- nothing in messagebee nor polaris
*/

declare @patronid int = 2692972  

select nq.PatronID, nq.NotificationTypeID, nq.ItemRecordID, nq.CreationDate, pr.EmailAddress, br.BrowseTitle, o.Name
from Results.polaris.NotificationQueue nq
join polaris.polaris.PatronRegistration pr
	on pr.PatronID = nq.PatronID
left join polaris.polaris.CircItemRecords cir
	on cir.ItemRecordID = nq.ItemRecordID
left join polaris.polaris.BibliographicRecords br
	on br.BibliographicRecordID = cir.AssociatedBibRecordID
join polaris.polaris.Organizations o
	on o.OrganizationID = nq.ReportingOrgID
where nq.DeliveryOptionID = 2
	and nq.ReportingOrgID in (3,5,7,9,10,11,12,13,15,17,18,20,21,22,24,26,27,29,30,31,33,34,35,36,37,38,75,76,79,80,81,82,83,85,87,89,91,93,94,95,96,97,98,100,101,102,103,104,106,107,108,109,110,111,113,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,138,139,140,141,142,143,144,145,150,151,152,153,154)
	and not exists ( 
		select 1 
		from results.polaris.NotificationHistory nh
		where nh.PatronId = nq.PatronID
			and nh.NotificationTypeId = nq.NotificationTypeID
			and nh.DeliveryOptionId = nq.DeliveryOptionID
			and nh.ItemRecordId = nq.ItemRecordID
			and nh.NoticeDate >= dateadd(day, -1, nq.CreationDate)
			and nh.NotificationStatusId = 14
		)
	--and nq.CreationDate < (
	--	select msdb.dbo.agent_datetime(sjs.last_run_date,sjs.last_run_time)
	--	from msdb.dbo.sysjobs j
	--	join msdb.dbo.sysjobsteps sjs
	--		on sjs.job_id = j.job_id
	--	where j.job_id = '5C75D5B4-ED32-4281-88C7-AFC30FFCBA75'
	--		and sjs.step_id = 3
	--)
	and (@patronid = 0 or nq.PatronID = @patronid)
order by nq.CreationDate


if (@patronid > 0)
begin
	select *
	from results.polaris.NotificationHistory nh
	where nh.NoticeDate > cast((getdate() -49) as date)
		and nh.PatronId = @patronid
	order by nh.NoticeDate
end