

select pli.*
		,heldForPatron.PatronID [HeldForPatron]
from polaris.polaris.PatronLostItems pli
outer apply (
	select top 1 *
	from polaris.polaris.ItemRecordHistory irh
	where irh.ActionTakenID = 66
		and irh.ItemRecordID = pli.ItemRecordID
	order by irh.TransactionDate desc
) heldForPatron
where pli.PatronID = 1973636




select irh.ItemRecordHistoryID
		,irh.ItemRecordID
		,irh.TransactionDate
		,irh.ActionTakenID
		,irha.ActionTakenDesc
		,irh.OldItemStatusID
		,iso.Description [OldItemStatus]
		,irh.NewItemStatusID
		,isn.Description [NewItemStatus]
from polaris.polaris.ItemRecordHistoryDaily irh 
join polaris.polaris.ItemRecordHistoryActions irha
	on irha.ActionTakenID = irh.ActionTakenID
join polaris.polaris.ItemStatuses iso
	on iso.ItemStatusID = irh.OldItemStatusID
join polaris.polaris.ItemStatuses isn
	on isn.ItemStatusID = irh.NewItemStatusID
where irh.ItemRecordID in (962105,6797141,24147223) and irh.TransactionDate > '6/17/2024'


select *
from polaris.dbo.CLC_Custom_ViewItemRecordHistory irh
where irh.ItemRecordID in (962105,6797141,24147223)
	and irh.NewItemStatusID = 4
order by irh.ItemRecordID

select * from polaris.polaris.ItemStatuses

select top 1 *
from polaris.polaris.ItemRecordHistory irh
where irh.ActionTakenID = 66
	and irh.ItemRecordID = 962105
order by irh.TransactionDate desc

select *
from polaris.polaris.PatronLostItems pli
join polaris.polaris.PatronRegistration pr
	on pr.PatronID = pli.PatronID
where pr.NameFirst like '%missing%' or pr.NameLast like '%missing%'
order by pr.PatronID