declare @POLineItemID int = 1329871
declare @sTempTable varchar = null

-- create table with all output fields from stored proc
create table #tempwork
	(BibliographicRecordID int, POLineItemID int, BrowseTitle nvarchar(255), BrowseAuthor nvarchar(255), ReqName NVARCHAR(50), SupplierName NVARCHAR(50),
	NoteStaff NVARCHAR(255), NotePub NVARCHAR(255), ISBN_ISSN NVARCHAR(50), LCCN NVARCHAR(50), OtherCntrlNumber NVARCHAR(50), ListPrice money, StatusDate datetime, 
	FundID int, Name NVARCHAR(50), AlternativeName NVARCHAR(50), QuantRec int, TotalQuant int, SegID int, PONumber NVARCHAR(39), NumHolds int, Alert bit,
	BarCode NVARCHAR(20), ItemNumber NVARCHAR(255), CallNumber nvarchar(370), MaterialType NVARCHAR(80), Destination NVARCHAR(80), Location NVARCHAR(50),
	ItemCount int, SegmentNumber int, NormalTitle nvarchar(255), FundTotalQty int, InvNumber NVARCHAR(39), BaseCurrency int, CurrCode NCHAR(3), OCLCNumber nvarchar(50))

-- create our custom table with all old fields plus our custom ones
create table #workslip
	(BibliographicRecordID int, POLineItemID int, BrowseTitle nvarchar(255), BrowseAuthor nvarchar(255), ReqName NVARCHAR(50), SupplierName NVARCHAR(50),
	NoteStaff NVARCHAR(255), NotePub NVARCHAR(255), ISBN_ISSN NVARCHAR(50), LCCN NVARCHAR(50), OtherCntrlNumber NVARCHAR(50), ListPrice money, StatusDate datetime, 
	FundID int, Name NVARCHAR(50), AlternativeName NVARCHAR(50), QuantRec int, TotalQuant int, SegID int, PONumber NVARCHAR(39), NumHolds int, Alert bit,
	BarCode NVARCHAR(20), ItemNumber NVARCHAR(255), CallNumber nvarchar(370), MaterialType NVARCHAR(80), Destination NVARCHAR(80), Location NVARCHAR(50),
	ItemCount int, SegmentNumber int, NormalTitle nvarchar(255), FundTotalQty int, InvNumber NVARCHAR(39), BaseCurrency int, CurrCode NCHAR(3), OCLCNumber nvarchar(50)
	,ShelfLocation nvarchar(255),instructions1 nvarchar(255), instructions2 nvarchar(255), instructions3 nvarchar(255))

-- populate first temp table
insert #tempwork
exec polaris.polaris.Rpt_Workslip @POLineItemID, @sTempTable

-- populate working tables, insert blanks for custom values for now
insert into #workslip
select *, '', '', '', ''
from #tempwork

-- break note field into three
declare @noteSplit table ( polineid int, ind int, val varchar(255) )

insert into @noteSplit
select w.POLineItemID, s.ItemIndex, s.Item
from #tempwork w
cross apply Polaris.dbo.CLC_Custom_SplitString2(replace(w.NoteStaff, char(13) + char(10) + char(13) + char(10), nchar(9999)), nchar(9999)) s		

update w
set w.instructions1 = ns1.val
	,w.instructions2 = ns2.val
	,w.instructions3 = ns3.val
from #workslip w
left join @noteSplit ns1
	on ns1.polineid = w.POLineItemID and ns1.ind = 0
left join @noteSplit ns2
	on ns2.polineid = w.POLineItemID and ns2.ind = 1
left join @noteSplit ns3
	on ns3.polineid = w.POLineItemID and ns3.ind = 2

-- update shelf location
update w
set w.ShelfLocation = sl.Description
from #workslip w
join polaris.polaris.LineItemSegmentToItemRecord listir
	on listir.POLineItemSegmentID = w.SegID
join polaris.polaris.CircItemRecords cir
	on cir.ItemRecordID = listir.ItemRecordID
join polaris.polaris.ShelfLocations sl
	on sl.ShelfLocationID = cir.ShelfLocationID and sl.OrganizationID = cir.AssignedBranchID


select * from #workslip

drop table #tempwork
drop table #workslip