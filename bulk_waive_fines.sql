begin tran

-- adjust these as needed
declare @txnorgid int = 0
declare @txnworkstationid int = 0
declare @txnuserid int = 0
declare @note varchar(255) = ''

declare valueCursor cursor for
-- adjust this query to select the transactions you'd like to waive
select pa.PatronID, pa.TxnID, pa.OutstandingAmount, pa.ItemRecordID
from polaris.polaris.PatronAccount pa
where pa.TxnCodeID = 1
	and pa.OutstandingAmount > 0
	-- probably want to leave the two above generally as-is
	and pa.PatronID = 1752752

declare @patronid int
declare @txnid int
declare @txnamount money
declare @itemrecordid int

open valueCursor
fetch next from valueCursor into @patronid, @txnid, @txnamount, @itemrecordid
while @@FETCH_STATUS = 0
begin
	
	exec Polaris.Polaris.Circ_WritePatronAccountTxnEx @patronid
													 ,@txnid
													 ,NULL		-- fee reason, polaris client waives with this as null
													 ,6			-- txn code, 6 = waive
													 ,@txnamount
													 ,@itemrecordid
													 ,NULL		-- txn date, polaris client waives with this as null
													 ,NULL		-- due date, polaris client waives with this as null
													 ,NULL		-- payment method, polaris client waives with this as null
													 ,@txnorgid	
													 ,@txnworkstationid
													 ,@txnuserid
													 ,@note		-- note, polaris client waives with this as null
													 ,NULL		-- ils store txn id, polaris client waives with this as null
													 ,0			-- manual charge, polaris client waives with this as 0
													 ,null		-- new txn id output
	
	
	fetch next from valueCursor into @patronid, @txnid, @txnamount, @itemrecordid
end

close valueCursor
deallocate valueCursor

--rollback
--commit