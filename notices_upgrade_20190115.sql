USE [CLC_Notices]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[PolarisNotifications]
AS

with hoursofoperation(orgid, [hours]) as 
(
	select o.OrganizationID, isnull(opb.Value, opl.Value) [HoursOfOperation]
	from Polaris.Polaris.Organizations o
	left join Polaris.polaris.OrganizationsPPPP opb
		on opb.OrganizationID = o.OrganizationID and opb.AttrID = 96
	left join Polaris.polaris.OrganizationsPPPP opl
		on o.ParentOrganizationID = opl.OrganizationID and opl.AttrID = 96
	where o.OrganizationCodeID = 3
),
notices as (
	select nq.PatronID, nq.NotificationTypeID, nq.ItemRecordID, nq.DeliveryOptionID, nq.ReportingOrgID
	from results.Polaris.NotificationQueue nq
	union all
	select cr.PatronID, cr.NotificationTypeID, cr.ItemRecordID, cr.DeliveryOptionID, cr.ReportingOrgID
	from Results.polaris.CircReminders cr
)

select
	n.NotificationTypeID
	,p.PatronID
	,p.Barcode [PatronBarcode]
	,pr.NameFirst
	,pr.NameLast
	,a.StreetOne [PatronStreetOne]
	,a.StreetTwo [PatronStreetTwo]
	,pc.City [PatronCity]
	,pc.State [PatronState]
	,pc.PostalCode [PatronZip]
	,isnull(n.ItemRecordID, 0) [ItemRecordID]
	,p.OrganizationID [PatronBranch]
	,o_patron.Abbreviation [PatronBranchAbbr]
	,isnull(o_patron.ParentOrganizationID, 0) [PatronLibrary]
	,o_patron2.Abbreviation [PatronLibraryAbbr]
	,n.DeliveryOptionID
	,pr.EmailAddress
	,pr.AltEmailAddress
	,pr.EmailFormatID
	,case n.DeliveryOptionID
		when 3 then pr.PhoneVoice1
		when 4 then pr.PhoneVoice2
		when 5 then pr.PhoneVoice3
		when 8 then
			case pr.TxtPhoneNumber
				when 1 then pr.PhoneVoice1
				when 2 then pr.PhoneVoice2
				when 3 then pr.PhoneVoice3
				else ''
			end
		else ''
	end [DeliveryPhone]
	,isnull(n.ReportingOrgID,0) [ReportingBranchID]
	,o_notice.Abbreviation [ReportingBranchAbbr]	
	,o_notice.Name [ReportingBranchName]	
	,saa_notice.StreetOne [ReportingBranchStreetOne]
	,saa_notice.StreetTwo [ReportingBranchStreetTwo]
	,saa_notice.City [ReportingBranchCity]
	,saa_notice.State [ReportingBranchState]
	,saa_notice.PostalCode [ReportingBranchZip]
	,isnull(o_notice.ParentOrganizationID,0) [ReportingLibraryID]
	,coalesce(shr.HoldTillDate, ir.HoldTillDate) [HoldTillDate]
	,ic.DueDate [DueDate]
	,cir.Barcode [ItemBarcode]
	,br.BrowseAuthor [Author]
	,br.BrowseTitle [Title]
	,isnull(mt.Description,'') [MaterialType]
	,ic.OriginalCheckOutDate [CkoDate]
	,o_cko.Name [CkoLocation]
	,ird.CallNumber [ItemCallNumber]
	,hoo.hours
	,isnull(l.AdminLanguageID, 1033) [PatronLanguage]
	,case pr.TxtPhoneNumber
		when 1 then pr.PhoneVoice1
		when 2 then pr.PhoneVoice2
		when 3 then pr.PhoneVoice3
		else ''
	end [TxtPhoneNumber]
from notices n
join Polaris.Polaris.Patrons p
	on n.PatronID = p.PatronID
join Polaris.Polaris.PatronRegistration pr
	on p.PatronID = pr.PatronID
join Polaris.Polaris.Organizations o_patron
	on p.OrganizationID = o_patron.OrganizationID
join Polaris.Polaris.Organizations o_patron2
	on o_patron.ParentOrganizationID = o_patron2.OrganizationID
join Polaris.Polaris.Organizations o_notice
	on n.ReportingOrgID = o_notice.OrganizationID
join polaris.polaris.OrganizationsPPPP op_notice_addr
	on op_notice_addr.OrganizationID = n.ReportingOrgID and op_notice_addr.AttrID = 55
join polaris.Polaris.SA_Addresses saa_notice
	on saa_notice.SA_AddressID = op_notice_addr.Value
left join polaris.polaris.CircItemRecords cir
	on cir.ItemRecordID = n.ItemRecordID
left join Polaris.Polaris.SysHoldRequests shr
	on shr.PatronID = p.PatronID and n.ItemRecordID = shr.TrappingItemRecordID
left join Polaris.polaris.ILLRequests ir
	on ir.PatronID = n.PatronID and ir.ItemRecordID = n.ItemRecordID
left join Polaris.polaris.BibliographicRecords br
	on br.BibliographicRecordID = cir.AssociatedBibRecordID
left join hoursofoperation hoo
	on hoo.orgid = n.ReportingOrgID
left join polaris.polaris.PatronAddresses pa
	on pa.PatronID = p.PatronID and pa.AddressTypeID = 2
left join polaris.Polaris.Addresses a
	on a.AddressID = pa.AddressID
left join Polaris.polaris.PostalCodes pc
	on pc.PostalCodeID = a.PostalCodeID
left join Polaris.polaris.ItemCheckouts ic
	on ic.PatronID = n.PatronID and ic.ItemRecordID = n.ItemRecordID
left join polaris.Polaris.Languages l
	on l.LanguageID = pr.LanguageID
left join polaris.polaris.MaterialTypes mt
	on mt.MaterialTypeID = cir.MaterialTypeID
left join polaris.Polaris.Organizations o_cko
	on o_cko.OrganizationID = ic.OrganizationID
left join polaris.polaris.ItemRecordDetails ird
	on ird.ItemRecordID = cir.ItemRecordID
where n.DeliveryOptionID in (2, 3, 4, 5) 
and n.NotificationTypeID in (1, 2, 10, 11, 12, 13) 
and (n.DeliveryOptionID = 2 or 
		(
		not exists (select 1 from polaris.Polaris.DatesClosed dc where (dc.OrganizationID = p.OrganizationID or dc.OrganizationID = o_patron.ParentOrganizationID) 
		and 
		cast(dc.DateClosed as date) = cast(getdate() as date)) and isnull(hoo.hours,'') not like '%' + left(datename(dw, getdate()), 3) + 'closed%'
		)
	)
GO

CREATE TABLE [dbo].[Dialin_String_Types](
	[Mnemonic] [varchar](255) NOT NULL,
	[Notes] [varchar](max) NULL,
 CONSTRAINT [PK_Dialin_String_Types] PRIMARY KEY CLUSTERED 
(
	[Mnemonic] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Dialin_Strings]    Script Date: 1/14/2019 1:01:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Dialin_Strings](
	[OrganizationID] [int] NOT NULL,
	[Mnemonic] [varchar](255) NOT NULL,
	[Value] [varchar](5000) NOT NULL,
 CONSTRAINT [PK_Dialin_Strings] PRIMARY KEY CLUSTERED 
(
	[OrganizationID] ASC,
	[Mnemonic] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'email_titles_confirm', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'error_incorrect_login', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'error_no_gather_entry', N'Said when the user doesn''t enter anything at a prompt')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'holds_count_information', N'{0} is the count of holds. {1} is item or items depending on count')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'holds_next_hold_prompt', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'holds_no_holds', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'holds_no_more_holds', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'item_list_all_items_prompt', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'item_list_email_prompt', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'item_list_no_items', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'item_list_overdue_only_prompt', N'{0} is the count. {1} is item or items depending on count. {2} is this item or these items depending on count.')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'item_list_renew_barcode_prompt', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'list_items_no_items', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'list_items_no_more_items', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'list_items_no_overdues', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'login_account_lookup', N'Said after the user enters their pin')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'menu_call_library', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'menu_greeting', N'{0} is the patron''s firstname. {1} is the patron''s registered branch name.')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'menu_greeting2', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'menu_hold_prompt', N'{0} is the count of holds. {1} is item or items depending on item count')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'menu_list_items_prompt', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'menu_renew_all_prompt', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'prompt_barcode', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'prompt_pin', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'prompt_remembered_patron', N'{0} will be replaced with the patron''s first name and MUST be present')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'renew_all_no_items', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'renew_barcode_error', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'renew_barcode_invalid_entry', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'renew_barcode_prompt', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'renew_barcode_success', N'{0} is the title of the item. {1} is the new duedate.')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'renew_item_error', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'renew_item_error_confirm', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'renew_item_success', N'{0} is the new due date')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'renew_item_unknown_error', N'')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'email_titles_confirm', N'An email has been sent to the address on file and you should receive it in a few minutes. Returning to the main menu.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'error_incorrect_login', N'Incorrect barcode or pin')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'error_no_gather_entry', N'Sorry, I didn''t get that.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'holds_count_information', N'You currently have {0} {1} ready for pickup.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'holds_next_hold_prompt', N'To hear your next hold, press 1. To return to the main menu, press 7.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'holds_no_holds', N'You currently have no holds ready for pickup. Returning to main menu.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'holds_no_more_holds', N'You have no more holds to list. Returning to main menu.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'item_list_all_items_prompt', N'To hear a list of all items you currently have checked out press 2.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'item_list_email_prompt', N'To have a list of your checked out titles sent to your email address, press 4')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'item_list_no_items', N'You currently have no items checked out. Returning to main menu.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'item_list_overdue_only_prompt', N'You currently have {0} {1} overdue. To list only {2} press 1.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'item_list_renew_barcode_prompt', N'To look up an item by barcode press 3.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'list_items_no_items', N'You currently have no items checked out. Returning to main menu.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'list_items_no_more_items', N'You have no more items to list, returning to the main menu.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'list_items_no_overdues', N'You currently have no overdue items')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'login_account_lookup', N'One moment while I look up your account.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'menu_call_library', N'Connecting you to your library.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'menu_greeting', N'Hello {0}, thank you for calling the {1}.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'menu_greeting2', N'Press 0 at any time to be connected to your library or press 7 to be taken to this menu.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'menu_hold_prompt', N'You have {0} {1} ready for pickup. To hear hold information, press 4.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'menu_list_items_prompt', N'To list the items you currently have checked out and renew individual items, press 2.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'menu_renew_all_prompt', N'To renew all items, press 1.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'prompt_barcode', N'Hello, thank you for calling the library. Please enter your library account barcode, followed by the pound key')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'prompt_pin', N'Please enter your pin')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'prompt_remembered_patron', N'Hello. Are you calling about {0}''s library account? Press 1 to confirm or press 2 to enter your library account barcode.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'renew_all_no_items', N'You currently have no items to renew, returning to main menu.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'renew_barcode_error', N'Unable to renew {0} for the following reason: {1}')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'renew_barcode_invalid_entry', N'The barcode you entered is not valid. Please try again.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'renew_barcode_prompt', N'"Enter the barcode of the item you wish to renew, followed by the pound key. Or press 7 to go back to the main menu.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'renew_barcode_success', N'Successfully renewed {0}. The new due date is {1}.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'renew_item_error', N'Your item was unable to be renewed for the following reason, {0}')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'renew_item_error_confirm', N'Press 2 to confirm and return to your list of items out.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'renew_item_success', N'Your item was successfully renewed. This item is due back on {0}.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'renew_item_unknown_error', N'Your item was unable to be renewed due to an unknown error. If this problem persists please contact your library for assistance.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (5, N'menu_greeting', N'custom menu greeting')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (6, N'menu_greeting', N'parent inheritance check')
GO

delete from [dbo].[SettingTypes]
SET IDENTITY_INSERT [dbo].[SettingTypes] ON 
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (1, N'email_hold_template_html', N'Email notice hold template URL', N'/content/templates/default/hold.html')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (2, N'email_overdue_template_html', N'', N'/content/templates/default/overdue.html')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (4, N'email_bill_template_html', N' ', N'/content/templates/default/bill.html')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (5, N'email_almost_overdue_template_html', N' ', N'/content/templates/default/almostoverdue.html')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (6, N'email_second_hold_template_html', N' ', N'/content/templates/default/secondhold.html')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (7, N'emaiL_combined_template_html', N' ', N'/content/templates/default/combined.html')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (8, N'email_hold_item_row_format', N' ', N'<tr class="item-row">
	<td class="item-title">{item_title}</td>
	<td>{item_author}</td>
	<td>{item_material_type}</td>
	<td>{item_barcode}</td>
	<td>{held_date}</td>
</tr>')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (10, N'email_overdue_item_format', N' ', N'<tr class="item-row">
	<td class="item-title">{item_title}</td>
	<td>{item_author}</td>
	<td>{item_material_type}</td>
	<td>{item_barcode}</td>
	<td>{due_date}</td>
</tr>')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (11, N'email_bill_item_format', N' ', N'<tr class="item-row">
	<td class="item-title">{item_title}</td>
	<td>{item_author}</td>
	<td>{item_material_type}</td>
	<td>{item_barcode}</td>
	<td>{due_date}</td>
</tr>')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (12, N'email_reminder_item_format', N' ', N'<tr class="item-row">
	<td class="item-title">{item_title}</td>
	<td>{item_author}</td>
	<td>{item_material_type}</td>
	<td>{item_barcode}</td>
	<td>{due_date}</td>
</tr>')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (15, N'email_use_patron_branch', N'', N'true')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (16, N'email_expiration_subject', N'', N'Your library account will expire soon')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (17, N'email_bill_subject', N'', N'Your library account has been billed')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (18, N'email_hold_subject', N'', N'Held items available for pickup at the library')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (19, N'email_overdue_subject', N'', N'You have overdue library items')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (20, N'email_second_hold_subject', N'', N'Held items still available for pickup at your library')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (22, N'email_from_address', N'', N'clcdpc@clcohio.org')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (23, N'email_expiration_template_html', N'', N'/content/templates/default/expiration.html')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (24, N'email_expiration_template_plaintext', N'', N'/content/templates/default/expiration.txt')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (25, N'enable_email_notices', N'Enable sending of email notices', N'false')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (26, N'enable_expiration_reminders', N' ', N'false')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (27, N'expiration_reminder_send_hour', N'', N'8')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (28, N'email_hold_template_plaintext', N'', N'/content/templates/default/hold.txt')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (29, N'email_overdue_template_plaintext', N' ', N'/content/templates/default/overdue.txt')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (30, N'email_bill_template_plaintext', N' ', N'/content/templates/default/bill.txt')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (31, N'email_almost_overdue_template_plaintext', N'', N'/content/templates/default/almostoverdue.txt')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (32, N'email_second_hold_template_plaintext', N'', N'/content/templates/default/secondhold.txt')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (33, N'email_combined_template_plaintext', N'', N'/content/templates/default/combined.txt')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (34, N'email_hold_cancellation_template_html', N'', N'/content/templates/default/holdcancellation.html')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (35, N'email_hold_cancellation_template_plaintext', N'', N'/content/templates/default/holdcancellation.txt')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (36, N'enable_hold_cancellation_notices', N'', N'false')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (37, N'hold_cancellation_send_hour_1', N'', N'8')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (38, N'hold_cancellation_send_hour_2', N'', N'14')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (39, N'auth_string', N'Authentication query string value for webhooks, etc', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (41, N'papi_request_hostname', N'PAPI server hostname', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (42, N'papi_key', N'PAPI key', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (43, N'papi_user', N'PAPI user', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (44, N'staff_username', N'credentials used for PAPI protected methods', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (45, N'staff_password', N'credentials used for PAPI protected methods', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (47, N'staff_domain', N'credentials used for PAPI protected methods', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (48, N'twilio_account_sid', N'twilio account sid', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (49, N'twilio_auth_token', N'twilio auth token', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (50, N'mandrill_api_key', N'mandrill api key', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (51, N'enable_notifier', N'enable built in notifier, requires setting notify_email_addresses or notify_phone_numbers', N'false')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (52, N'notify_email_addresses', N'email addresses to notify via notifier', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (53, N'notify_phone_numbers', N'phone numbers to notify via notifier', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (54, N'enable_hangfire_msmq', N'legacy option, leave at false', N'false')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (55, N'admin_permission_group', N'Users in this group will have access to protected areas of app (hangfire, test method, etc)', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (56, N'dev', N'Puts app into dev mode for testing purposes', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (57, N'rebuild_hangfire_jobs', N'If true, will delete and re-add all hangfire jobs, useful for forcing job setting updates', N'false')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (58, N'enable_prtg_sensors', N'Enable prtg sensors within the app, requires external setup', N'false')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (59, N'twilio_errors_sensor_url', N'Twilio error prtg sensor url', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (61, N'enable_dialout', N'Enable dialout process', N'false')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (62, N'base_url', N'Base application hostname', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (63, N'dialout_from_phone', N'Phone number to use to place dialout calls', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (64, N'record_calls', N'Record dialout calls', N'true')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (65, N'debug_phone_number', N'Phone number called while application is in debug mode', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (66, N'dialout_debug', N'Puts dialout in debug mode, will place a call for the first patron to the debug phone number and update any notices with a ''call not answered'' status', N'false')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (67, N'dialout_weekday_start_hour', N'When to start placing calls on weekdays', N'9')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (68, N'dialout_weekday_stop_hour', N'When to stop placing calls on weekdays', N'20')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (69, N'dialout_weekend_start_hour', N'When to start placing calls on weekends', N'12')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (70, N'dialout_weekend_stop_hour', N'When to stop placing calls on weekends', N'17')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (71, N'dialout_libraries_to_call', N'Patron libraries to place calls for', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (72, N'call_recording_retention_days', N'How long to keep call recordings for', N'30')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (73, N'call_retention_days', N'How long to keep twilio call details for', N'90')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (75, N'test_patron_id', N'Used for testing, leave at 0', N'0')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (76, N'enable_dialin', N'Enable dialin application', N'true')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (77, N'dialin_timeout_limit', N'Number of times the application receives no input before hanging up', N'5')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (78, N'dialin_pin_digit_length', N'Maximum number of pin digits to accept', N'8')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (79, N'dialin_enable_itiva_transfer', N'Enable transferring certain libraries'' patrons to a different dialin system', N'false')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (80, N'itiva_number', N'Number to transfer itiva patrons to', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (81, N'dialin_email_template_name', N'Name of Mandrill template to use for emailing title list', N'Dial_In_Email_CheckedOutTitles')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (82, N'dialin_test_barcode', N'Used for internal dialin testing', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (83, N'dialin_test_pin', N'Used for internal dialin testing', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (84, N'enable_email_processing', N'Enable automatic handling of bounced/spam reported Mandrill emails', N'false')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (85, N'library_specific_spam', N'On spam reports only process patrons registered at the library that sent the notice. Requires setting up email domain table', N'false')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (86, N'enable_spam_rejection_removal', N'Automatically remove rejection list entry for spam reports, allows patrons to add back their email address without staff intervention', N'true')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (87, N'email_processing_excluded_words', N'Emails with these words in their title will be excluded from automated email processing', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (88, N'email_processing_excluded_senders', N'Emails sent by these addresses will be excluded from automated email processing', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (89, N'email_processing_excluded_receivers', N'Emails sent to these addresses will be excluded from automated email processing', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (90, N'mandrill_unsigned_rejections_sensor_url', N'Prtg sensor used for mandrill unsigned rejection errors', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (91, N'enable_sms_processing', N'Enable sms process', N'false')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (92, N'sms_retention_days', N'Number of days to keep SMS message details on Twilio', N'90')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (93, N'after_hours_sms_send_hour', N'Hour to send SMS messages queued after hours', N'10')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (94, N'after_hours_sms_send_minute', N'Minute to send SMS messages queued after hours', N'0')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (95, N'sms_start_hour', N'When to start sending SMS messages', N'8')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (96, N'sms_stop_hour', N'When to stop sending SMS messages', N'22')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (97, N'rabbitmq_hostname', N'RabbitMQ server hostname', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (98, N'rabbitmq_virtualhost', N'RabbitMQ virtual host', N'notices')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (99, N'rabbitmq_username', N'RabbitMQ username', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (100, N'rabbitmq_password', N'RabbitMQ password', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (101, N'sms_test_number', N'Phone number used for SMS testing', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (102, N'message_service_sid', N'Twilio message service sid', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (103, N'sms_stop_note_identifier', N'Additional text to include in SMS STOP patron notes for identification purposes', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (104, N'sms_stop_trigger_words', N'List of words that will trigger internal STOP processing', N'stop;unsubscribe all;stop all')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (105, N'sms_start_trigger_words', N'List of words that will trigger internal START message', N'start')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (106, N'sms_reply_text', N'Reply text for messages sent to application phone numbers', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (107, N'sms_stop_reply_text', N'Text to use for STOP replies', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (108, N'sms_stop_zero_accounts_reply_text', N'Text to use for STOP replies where the number isn''t found in the system', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (109, N'sms_start_reply_text', N'Text to use for START replies', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (110, N'twilio_carrier_violation_errors_sensor_url', N'Prtg sensor url', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (111, N'website_url', N'', N'')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (112, N'phone', N'', N'')
GO
SET IDENTITY_INSERT [dbo].[SettingTypes] OFF
GO
ALTER TABLE [dbo].[Dialin_Strings]  WITH CHECK ADD  CONSTRAINT [FK_Dialin_Strings_Dialin_String_Types] FOREIGN KEY([Mnemonic])
REFERENCES [dbo].[Dialin_String_Types] ([Mnemonic])
GO
ALTER TABLE [dbo].[Dialin_Strings] CHECK CONSTRAINT [FK_Dialin_Strings_Dialin_String_Types]
GO

CREATE TABLE [dbo].[EmailStatus](
	[StatusID] [int] IDENTITY(1,1) NOT NULL,
	[StatusDate] [date] NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_EmailStatus] PRIMARY KEY CLUSTERED 
(
	[StatusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO