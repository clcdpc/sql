/*
branches
closure date
repoen date
remove as pickup location | bool
suspend holds | bool
address change | bool
new address
name change | bool
new name

reactivation date = close date - 7
suspend hold job creation date = close date - 7, add stored proc code here
closed dates = sql to add closed dates
stop rtf = closed date - 7
address change date = closed date, include new address
re-add as pickup location date, reopen date
start rtf = reopen date + 21
*/



declare @branches int = 5
declare @closeDate date = '1/1/2024'
declare @reopenDate date = '3/1/2024'
declare @removeAsPickupLocation bit = 1
declare @suspendHolds bit = 0
declare @newAddress varchar(1024) = null
declare @newName varchar(255) = null

-- date offsets
declare @removePickupLocationDateOffset int = -7
declare @suspendHoldJobCreationOffset int = @removePickupLocationDateOffset
declare @suspendHoldJobReactivationOffset int = @removePickupLocationDateOffset
declare @rtfStopDateOffset int = @removePickupLocationDateOffset
declare @addressChangeDateOffset int = 0
declare @nameChangeDateOffset int = 0
declare @pickupLocationReaddDate int = 0
declare @rtfStartDateOffset int = 21

declare @forceEnabledSuspendJob bit = 0



declare @branchTable table ( OrganizationID int, Name varchar(255), idstring varchar(3) )

insert into @branchTable
select OrganizationID, Name, cast(o.OrganizationID as varchar(3))
from polaris.polaris.Organizations o
where o.OrganizationID in (@branches)

declare @cal table ( thedate date )

insert into @cal
SELECT  TOP 800
        Date = DATEADD(DAY, ROW_NUMBER() OVER(ORDER BY a.object_id) - 1, getdate())
FROM    sys.all_objects a
        CROSS JOIN sys.all_objects b;


select d.Branch, d2.*
from (
	select distinct
			cast(b.OrganizationID as varchar) [OrganizationID]
			,b.Name [Branch]
			,case @removeAsPickupLocation when 0 then '' else format(dateadd(day, @removePickupLocationDateOffset, @closeDate), 'MM/dd/yyyy') end [RemoveAsPickupLocationDate]
			,case @suspendHolds when 0 then '' else format(dateadd(day, @suspendHoldJobCreationOffset, @closeDate), 'MM/dd/yyyy') end [SuspendHoldJobCreationDate]
			,case @suspendHolds when 0 then '' else min('exec polaris.dbo.CLC_Custom_Create_Holds_Suspend_Job ' + b.idstring + ', ''' + format(dateadd(day, @suspendHoldJobReactivationOffset, @reopenDate), 'MM/dd/yyyy') + '''') + ', ' + cast(@forceEnabledSuspendJob as varchar) end [SuspendHoldJobCreationSQL]
			,string_agg('insert into polaris.polaris.datesclosed select ' + cast(b.OrganizationID as varchar(max)) + ', ''' + format(c.thedate, 'MM/dd/yyyy') + '''', CHAR(13) + CHAR(10)) [ClosedDateInsertionSQL]
			,format(dateadd(day, @rtfStopDateOffset, @closeDate), 'MM/dd/yyyy') [RTFStopDate]
			,case when @newAddress is null then '' else format(dateadd(day, @addressChangeDateOffset, @reopenDate), 'MM/dd/yyyy') end [AddressChangeDate]
			,isnull(@newAddress, '') [NewAddress]
			,case when @newName is null then '' else format(dateadd(day, @nameChangeDateOffset, @reopenDate), 'MM/dd/yyyy') end [NameChangeDate]
			,isnull(@newName, '') [NewName]
			,case @removeAsPickupLocation when 1 then format(dateadd(day, @pickupLocationReaddDate, @reopenDate), 'MM/dd/yyyy') else '' end [ReaddAsPickupLocationDate]
			,format(dateadd(day, @rtfStartDateOffset, @reopenDate), 'MM/dd/yyyy') [RTFStartDate]
	from @branchTable b
	cross join @cal c
	where c.thedate between @closeDate and dateadd(day, -1, @reopenDate)
		and not exists ( select 1 from polaris.polaris.DatesClosed dc where dc.DateClosed = c.thedate )
	group by b.OrganizationID, b.Name
) d
cross apply
(
  values
  ('Remove As Pickup Location Date', d.RemoveAsPickupLocationDate),
  ('Suspend Hold Job Creation Date', d.SuspendHoldJobCreationDate),
  ('Suspend Hold Job Creation SQL', d.SuspendHoldJobCreationSQL),
  ('Closed Date Insertion SQL', d.ClosedDateInsertionSQL),
  ('RTF Stop Date', d.RTFStopDate),  
  ('Address Change Date', d.AddressChangeDate),
  ('New Address', d.NewAddress),
  ('Name Change Date', d.NameChangeDate),
  ('New Name', d.NewName),
  ('Readd As Pickup Location Date', d.ReaddAsPickupLocationDate),
  ('RTF Start Date', d.RTFStartDate)
) d2 (fieldname, fieldvalue)