declare @excluded_libraries table (orgid int)
declare @controlRecordDefExclusions table ( crdid int )
declare @librarySpecificControlRecordDefExclusions table ( orgid int, crdid int, pnid int )
declare @groupSpecificControlRecordDefExclusions table ( groupid int, crdid int, pnid int )
declare @excludedGroups table ( groupid int )


insert into @excluded_libraries values (2),(86)

insert into @controlRecordDefExclusions values (18) -- exclude paton record sets
	,(274) -- Picklist application

insert into @librarySpecificControlRecordDefExclusions values (14,20,null) -- don't add mpc branches to hold permissions

insert into @groupSpecificControlRecordDefExclusions values 
	 (28,17,70),(29,17,70),(30,17,70)			-- don't add 'Patron Status' 'Modify reader services' to mpc groups
	,(28,220,null),(29,220,null),(30,220,null)	-- don't add 'Modify ORS patron ratings' permissions to mpc groups
	,(28,20,null),(29,20,null),(30,20,null)		-- don't add 'Hold Requests' permissions to mpc groups
	,(28,222,null),(29,222,null),(30,222,null)	-- don't add 'Reading history: Remove non-ORS patron history entries' permissions to mpc groups
	,(28,221,null),(29,221,null),(30,221,null)	-- don't add 'Reading history: Remove ORS patron history entries' permissions to mpc groups

insert into @excludedGroups values (330) -- cml test group

--select distinct d.GroupID, d.GroupName, pp.ControlRecordDefID, pp.ControlRecordName, pp.PermissionNameID, pp.PermissionName, pp.OrganizationID, o.Name [OrganizationName]
-- distinct d.GroupID, d.GroupName, d.ControlRecordDefID, d.PermissionNameID, d.OrganizationCodeID, '' [ ], pp.*

begin tran; insert into Polaris.Polaris.PermissionGroups
select distinct pp.ControlRecordID, pp.PermissionID, d.GroupID
from Polaris.dbo.CLC_Custom_PolarisPermissions pp
join (
	select   pg.GroupID
			,g.GroupName
			,pp.ControlRecordDefID
			,pp.PermissionNameID
			,pp.OrganizationID
			,o.OrganizationCodeID
	from Polaris.Polaris.PermissionGroups pg
	join Polaris.Polaris.Groups g
		on g.GroupID = pg.GroupID
	join Polaris.dbo.CLC_Custom_PolarisPermissions pp
		on pp.ControlRecordID = pg.ControlRecordID and pp.PermissionID = pg.PermissionID
	join polaris.polaris.Organizations o
		on o.OrganizationID = pp.OrganizationID
	where pp.OrganizationID > 1
		and g.GroupID >= 25
		and g.GroupName not like 'ALE%' 
		and g.GroupName not like 'LPL%' 
		and g.GroupName not like 'Scoville%' 
		and pp.subsystemid = 2
) d
	on d.ControlRecordDefID = pp.ControlRecordDefID and d.PermissionNameID = pp.PermissionNameID
join Polaris.Polaris.Organizations o
	on o.OrganizationID = pp.OrganizationID and o.OrganizationCodeID = d.OrganizationCodeID
where 
	-- exclude existing permissions
	not exists ( select 1 from Polaris.polaris.PermissionGroups pg where pg.GroupID = d.GroupID and pg.ControlRecordID = pp.ControlRecordID and pg.PermissionID = pp.PermissionID )

	-- exclude specific libraries
	and not exists ( select 1 from @excluded_libraries el where el.orgid = (case o.OrganizationCodeID when 2 then o.OrganizationID else o.ParentOrganizationID end) )

	-- exclude some control records
	and not exists ( select 1 from @controlRecordDefExclusions crde where crde.crdid = d.ControlRecordDefID )

	-- exclude more control records, based on library
	and not exists ( select 1 from @librarySpecificControlRecordDefExclusions lscrde where lscrde.orgid in (o.OrganizationID,o.ParentOrganizationID) and lscrde.crdid = d.ControlRecordDefID  )

	-- exclude more control records, based on group
	and not exists ( select 1 from @groupSpecificControlRecordDefExclusions gscrde where gscrde.groupid = d.GroupID and gscrde.crdid = d.ControlRecordDefID and (gscrde.pnid is null or gscrde.pnid = pp.PermissionNameID) )

	-- insert full permissions for fairfield, and only fairfield permissions for everyone else
	and (d.GroupName like 'fcd%' or 8 in (o.OrganizationID, o.ParentOrganizationID))

	-- **DEBUG**, exclude fairfield. hides "known good" entries to make problems easier to spot
	--and 8 not in (o.OrganizationID, o.ParentOrganizationID) 

	-- exclude test branch and any archived branches
	and o.OrganizationID != 139 and o.name not like 'zz%'
--order by d.GroupName, pp.ControlRecordName, pp.PermissionName


--rollback
--commit
