select pr.PatronID
		,pr.EntryDate
		,pr.AddrCheckDate
		,pr.ExpirationDate
		,cast(dateadd(year, 3, pr.EntryDate) as date) [ShouldBeAddrCheckDate]
		,cast(dateadd(year, 10, pr.EntryDate) as date) [ShouldBeExpirationDate]
from polaris.polaris.patrons p
join polaris.polaris.PatronRegistration pr
	on pr.PatronID = p.PatronID
join polaris.polaris.organizations o
	on o.OrganizationID = p.OrganizationID
where o.ParentOrganizationID = 78
	and pr.EntryDate > '1/1/2019'
	--and pr.ExpirationDate != cast(dateadd(year, 10, pr.EntryDate) as date)
	and p.PatronCodeID = 1
	and pr.ExpirationDate > getdate()
	and pr.AddrCheckDate > getdate()
order by pr.EntryDate