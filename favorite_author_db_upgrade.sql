use [AuthorSubscription]
GO
GRANT EXECUTE ON [dbo].[CreateAuthorRecordSet] TO [CLCDPC\author_subscription]
GRANT EXECUTE ON [dbo].[GetPatronAuthors] TO [CLCDPC\author_subscription]
GRANT EXECUTE ON [dbo].[GetSubscriberCount] TO [CLCDPC\author_subscription]
GO
