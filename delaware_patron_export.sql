--RECORD #(PATRON)	p10009999
--EMAIL ADDR	xxxxxxx@hotmail.com
--PATRN NAME	xxxxxx, xxxx x
--EXP DATE	12/1/2020
--BIRTH DATE	4/4/1957
--P TYPE	1
--HOME LIBR	main 
--CIRCACTIVE	12/30/2017
--CUR CHKOUT	0
--CUR ITEMA	0
--CUR ITEMB	0
--CUR ITEMC	0
--CUR ITEMD	0
--TOT CHKOUT	230
--TOT RENWAL	7
--CL RTRND	0
--MONEY OWED	$0.00 
--ILL CHKOUT	0
--PCODE1	d
--PCODE2	z
--PCODE3	0
--P BARCODE	2.24E+13
--SCHL DISTR	
--PMESSAGE	 
--MBLOCK	-
--NOTICE PREF	z
--ADDRESS	49 xxxxxxxxxxxx AVE$DELAWARE, OH 43015
--TELEPHONE	xxx-xxx-xxxx
--CREATED(PATRON)	2/20/1997


-- CIR ITEMA, CUR ITEMB, CUR ITEMC, CUR ITEMD, PCODE1, PCODE2, PCODE3, PMESSAGE, MBLOCK


select	p.PatronID [RECORD #(PATRON)]
		,coalesce(pr.EmailAddress,'')	[EMAIL ADDR]
		,pr.NameFirst					[PATRN FNAME]
		,pr.NameLast					[PATRN LNAME]
		,pr.ExpirationDate				[EXP DATE]
		,coalesce(pr.Birthdate,'')		[BIRTH DATE]
		,p.PatronCodeID					[P TYPE]
		,o.Name							[HOME LIBR]
		,coalesce(p.LastActivityDate,'')[CIRCACTIVE]
		,(select count(*) from Polaris.polaris.ItemCheckouts ic where ic.PatronID = p.patronid) [CUR CHKOUT]
		,0								[CUR ITEMA]
		,0								[CUR ITEMB]
		,0								[CUR ITEMC]
		,0								[CUR ITEMD]
		,p.LifetimeCircCount			[TOT CHKOUT]
		,p.ClaimCount					[CL RTRNN]
		,p.ChargesAmount				[MONEY OWED]
		,p.Barcode						[P BARCODE]
		,coalesce(pr.User3,'')			[SCHL DISTR]
		,pr.DeliveryOptionID			[NOTICE PREF]
		,a.StreetOne + ' ' + coalesce(a.StreetTwo, '') [STREET]
		,a.StreetOne					[STREET ONE]
		,coalesce(a.StreetTwo, '')		[STREET TWO]
from Polaris.polaris.Patrons p
join Polaris.Polaris.PatronRegistration pr
	on pr.PatronID = p.PatronID
join polaris.Polaris.PatronAddresses pa
	on pa.PatronID = p.PatronID and pa.AddressTypeID = 2
join Polaris.Polaris.Addresses a
	on a.AddressID = pa.AddressID
join Polaris.polaris.PostalCodes pc
	on pc.PostalCodeID = a.PostalCodeID
join polaris.polaris.Organizations o
	on o.OrganizationID = p.OrganizationID
where p.PatronCodeID = 1 and pr.EntryDate > '1/1/2018' and o.ParentOrganizationID = 105

