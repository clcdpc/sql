--select * from polaris.polaris.RecordSets rs where rs.Name like 'patpl %'
--select * from polaris.polaris.FindToolSQLSearches s where s.DisplayName	like '%patpl%'

begin tran dowork

set nocount on

declare @pairs table (recordsetid int, searchid int, rstype char)

insert into @pairs values (124303,257,'b') --PATPL Dashboard - New Large Print
insert into @pairs values (124304,258,'b') --PATPL Dashboard - New Audiobooks
insert into @pairs values (124305,259,'b') --PATPL Dashboard - New Books Just For Kids

insert into @pairs values (122675,252,'b') --PATPL Carousel - DVD
insert into @pairs values (122676,251,'b') --PATPL Carousel - Fiction
insert into @pairs values (122677,253,'b') --PATPL Carousel - Non-Fiction
insert into @pairs values (122678,231,'b') --PATPL Carousel - YA Fiction
insert into @pairs values (122680,232,'b') --PATPL Carousel - Music CD

declare rs_cursor cursor local fast_forward for
select recordsetid, searchid, rstype
from @pairs
order by recordsetid

declare @recordsetid int
declare @searchid int
declare @rstype char

declare @errors int = 0

open rs_cursor

set nocount off
while 1=1
begin
	fetch next from rs_cursor into @recordsetid, @searchid, @rstype

	if @@fetch_status <> 0
	begin
		break
	end

	begin tran doupdate

	begin try
	if (@rstype = 'b')
		begin
			delete from Polaris.polaris.BibRecordSets where RecordSetID = @recordsetid
			declare @sql nvarchar(max) = (select SQLStatement from polaris.polaris.FindToolSQLSearches where SearchID = @searchid)

			declare @tmp table (bibid int)

			insert into @tmp exec sys.sp_executesql @sql

			insert into Polaris.polaris.BibRecordSets
			select bibid, @recordsetid
			from @tmp

			delete from @tmp
		end
	end try
	begin catch
		select @errors = @errors + 1
		rollback tran doupdate
	end catch

	commit tran doupdate
end

close rs_cursor
deallocate rs_cursor

select @errors [ErrorCount]

select RecordSetID, count(*) from Polaris.polaris.BibRecordSets where RecordSetID in (124303,124304,124305,122675,122676,122677,122678,122680) group by RecordSetID
																					  
--rollback tran dowork																  
--commit tran dowork																  
																					  

--declare @current int = 0

--while (select count(*) from @pairs) > 0
--begin
--	select @current = min(recordsetid) from @pairs


--end


--select * from Polaris.polaris.BibRecordSets where RecordSetID in (124303,124304,124305)



--declare @sql2 nvarchar(max) = (select SQLStatement from polaris.polaris.FindToolSQLSearches where SearchID = 258)
----select * from OPENROWSET('SQLNCLI', 'Server=192.168.144.90;database=polaris;Trusted_Connection=yes;', '')
--declare @test table (bibid int)
--insert into @test exec sys.sp_executesql @sql2

--select 1234,* from @test



















