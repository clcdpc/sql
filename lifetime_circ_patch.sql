SELECT p.PatronID, p.LifetimeCircCount, p.YTDCircCount, pypc.YTDCircCount AS [PrevYTDCircCount] INTO tempPatrons
FROM Polaris.Polaris.Patrons AS p (NOLOCK)
INNER JOIN Polaris.Polaris.PrevYearPatronsCirc AS pypc (NOLOCK)
ON p.PatronID=pypc.PatronID
WHERE p.LifetimeCircCount < p.YTDCircCount
OR p.LifetimeCircCount < pypc.YTDCircCount

SELECT COUNT(DISTINCT th.TransactionID) AS [LifetimeCircCount], td.numValue AS [PatronID] INTO tempPatronCircCounts
FROM PolarisTransactions.Polaris.TransactionHeaders AS th (NOLOCK)
INNER JOIN PolarisTransactions.Polaris.TransactionDetails AS td (NOLOCK)
	ON th.TransactionID = td.TransactionID AND td.TransactionSubTypeID = 6
LEFT JOIN PolarisTransactions.Polaris.TransactionDetails AS td2 (NOLOCK)
	ON th.TransactionID = td2.TransactionID AND td2.TransactionSubTypeID=124
INNER JOIN tempPatrons AS tp (NOLOCK)
	ON td.numValue = tp.PatronID
WHERE th.TransactionTypeID = 6001
	AND td2.numValue is NULL
GROUP BY td.numvalue
ORDER BY td.numValue

SELECT cir.ItemRecordID, cir.LifetimeCircCount, cir.YTDCircCount, pyic.YTDCircCount AS [PrevYTDCircCount] INTO tempItems
FROM Polaris.Polaris.CircItemRecords AS cir (NOLOCK)
INNER JOIN Polaris.Polaris.PrevYearItemsCirc AS pyic (NOLOCK)
	ON cir.ItemRecordID=pyic.ItemRecordID
WHERE cir.LifetimeCircCount < cir.YTDCircCount
	OR cir.LifetimeCircCount < pyic.YTDCircCount

SELECT COUNT(DISTINCT th.TransactionID) AS [LifetimeCircCount], td.numValue AS [ItemRecordID] INTO tempItemCircCounts
FROM PolarisTransactions.Polaris.TransactionHeaders AS th (NOLOCK)
INNER JOIN PolarisTransactions.Polaris.TransactionDetails AS td (NOLOCK)
	ON th.TransactionID = td.TransactionID AND td.TransactionSubTypeID = 38
LEFT JOIN PolarisTransactions.Polaris.TransactionDetails AS td2 (NOLOCK)
	ON th.TransactionID = td2.TransactionID AND td2.TransactionSubTypeID=124
INNER JOIN tempItems AS ti (NOLOCK)
	ON td.numValue = ti.ItemRecordID
WHERE th.TransactionTypeID = 6001
	AND td2.numValue is NULL
GROUP BY td.numvalue
ORDER BY td.numValue


begin tran

ALTER TABLE Polaris.Patrons DISABLE TRIGGER Patrons_UPD

UPDATE Polaris.Polaris.Patrons
SET Patrons.LifetimeCircCount = tempPatronCircCounts.LifetimeCircCount
FROM tempPatronCircCounts
WHERE tempPatronCircCounts.PatronID = Patrons.PatronID
	AND Patrons.LifetimeCircCount < tempPatronCircCounts.LifetimeCircCount

ALTER TABLE Polaris.Patrons ENABLE TRIGGER Patrons_UPD


ALTER TABLE Polaris.CircItemRecords DISABLE TRIGGER CircItemRecords_UPD

UPDATE Polaris.Polaris.CircItemRecords
SET CircItemRecords.LifetimeCircCount = tempItemCircCounts.LifetimeCircCount
FROM tempItemCircCounts
WHERE tempItemCircCounts.ItemRecordID = CircItemRecords.ItemRecordID
AND CircItemRecords.LifetimeCircCount < tempItemCircCounts.LifetimeCircCount

ALTER TABLE Polaris.CircItemRecords ENABLE TRIGGER CircItemRecords_UPD

/*
DROP TABLE tempPatrons
DROP TABLE tempPatronCircCounts
DROP TABLE tempItems
DROP TABLE tempItemCircCounts
*/


--rollback
--commit