declare @library int = 84

--select * from polaris.polaris.organizations where OrganizationCodeID = 2

begin tran

update pr
set  pr.Phone1CarrierID = case when pr.Phone1CarrierID is not null then 26 end
	,pr.Phone2CarrierID = case when pr.Phone2CarrierID is not null then 26 end
	,pr.Phone3CarrierID = case when pr.Phone3CarrierID is not null then 26 end
from polaris.Polaris.Patrons p
join polaris.polaris.PatronRegistration pr
	on pr.PatronID = p.PatronID
join Polaris.polaris.organizations o
	on o.OrganizationID = p.OrganizationID
where o.ParentOrganizationID = @library
	and (pr.Phone1CarrierID is not null or pr.Phone2CarrierID is not null or pr.Phone3CarrierID is not null)

--rollback
--commit