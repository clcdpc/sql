select top 1000 * from PolarisTransactions.dbo.clc_custom_viewtransactiondetails th where th.PolarisUserID = 76 and th.TranClientDate > cast(getdate() as date) and th.TransactionTypeID = 7200


/*
2/8/2022 = 16:00
11:55
2/9/2022 = 12:00
New
11:55
2/26/2022 = 14:00
*/
select distinct 
		 vtd.TranClientDate
		,vtd.OrganizationID
		,o.Name
		,vtd.PolarisUserID
		,vtd.Username
		,vtd.WorkstationID
		,vtd.ComputerName
		,vtd.TransactionTypeID
		,vtd.TransactionTypeDescription
from PolarisTransactions.dbo.clc_custom_viewtransactiondetails vtd
join polaris.polaris.organizations o
	on o.OrganizationID = vtd.OrganizationID
where vtd.PolarisUserID = 2861
	and (
		(cast(vtd.TranClientDate as date) = '2/8/2022' and datepart(hour, vtd.TranClientDate) = 16)
		or 
		(cast(vtd.TranClientDate as date) = '2/9/2022' and datepart(hour, vtd.TranClientDate) = 12)
		or
		(cast(vtd.TranClientDate as date) = '2/26/2022' and datepart(hour, vtd.TranClientDate) = 14)
		)
	and vtd.TransactionTypeID in (7200,7201)
order by vtd.TranClientDate
