declare @table table (OrganizationID int, ItemAvailabilityLevelID int, SortOrder int, BranchLocationOrgID int)

declare @branches table ( orgid int )

insert into @branches 
select OrganizationID 
from Polaris.Polaris.Organizations 
where OrganizationCodeID = 3
--list of branches that shouldn't have their items displayed in the PAC 
and OrganizationID not in (  5   --CLC Electronic Library
							,37  --WL Old Worthington After Hours Pickup Locker
							,38  --WL Northwest After Hours Pickup Locker
							,61  --zzzdonotuseCMLTabletDispenser
							,64  --CML ILL
							,67  --CML R2R
							,68  --CML Parsons Drive-up Window
							,69  --CML Whitehall Drive-up Window
							,70  --CML Shepard Drive-up Window
							,71  --CML Northern Lights Drive-up Window
							,72  --CML-Check Out App
							,73  --CML New Albany Drive-up Window
							,74  --zzzdonotuse CML-Phone Renewals
							,82  --UA Outreach
							,88  --CML School Delivery
							,89  --WL Materials Vending Lazelle
							,91  --WL Mobile
							,92  --zzzdonotuse CML Local History and Genealogy
							,93  --GHPL Book Cart
							,94  --WL Worthington Park After Hours Pickup Locker
							,95  --Pickerington 24 Hour Pickup Lockers
							,96  --WL Materials Vending Worthington Rec
							,97  --WL Northwest Drive Up Window
							,98  --Pickaway Main Drive Through
							,100 --zzzdonotuse WL Worthington Schools
							,101 --zzzdonotuse SPLGC Mobile
							,102 --zzzdonotuse SPLWL Mobile
							,104 --Pickerington Sycamore Plaza 24 Hour Pickup Lockers
							,116 --Delaware County Library Orange Drive-Up Window
							,117 --Marysville Early College High School
							,118 --Marysville High School
							,119 --MPL Bunsold Middle School
							,120 --MPL Creekview Intermediate
							,121 --MPL Edgewood Elementary
							,122 --MPL Mill Valley Elementary
							,123 --MPL Navin Elementary
							,124 --MPL Northwood Elementary
							,125 --MPL Raymond Elementary
							,127 --Plain City JA Junior High
							,128 --Plain City JA High School
							,129 --Plain City JA Canaan Middle School
							,130 --Plain City Homebound
							,131 --Plain City Pickup Lockers
							,132 --Plain City JA Plain City Elementary
							,133 --Plain City JA Monroe Elementary
							,134 --zfuturePlain City JA High School6
							,135 --zfuturePlain City JA High School7
							,136 --zfuturePlain City JA High School8
							,137 --CML-Columbus City Schools
							,138 --Fairfield County Baltimore 24 Hour Pickup Lockers
							,139 --CLC Test branch
							,140 --MPL Trinity Lutheran School
							,141 --MPL St. John's Lutheran School
							,142 --UA Tremont Curbside Pickup
							,143 --Pickerington Main Drive-Up Window
							,144 --Wagnalls Pickup Locker
							,145 --Fairfield County Main Lockers
							,146 --CML Whetstone Curbside
							,147 --CML Gahanna Drive-up Window
							,148 --CML Hilliard Curbside
							,149 --CML Dublin Curbside
							,150 --Grandview Hts Drive-Thru Pickup
							,151 --MPL Raymond Branch Pickup Lockers
							,152 --Marysville Public Library Pickup Lockers
							,154 --Pickerington Library On-The-Go
							,155 --Delaware County Library Liberty Drive-Up Window
							--,156 --WL Pop Up Library
							,157 --Pataskala Drive-Thru Pickup
							,159 --Westerville Uptown
							,160 --Westerville Uptown Self-Serve
							,161 --Westerville Uptown Drive-Thru
							,162 --Westerville Uptown Lockers
							,163 --Westerville Blendon Hempstead Lockers
							,164 --Westerville Genoa Old 3C Kiosk
							,165 --Westerville Genoa Old 3C Lockers
							,166 --Westerville Outreach
							,167 --Westerville Future South Branch
							,168 --Westerville Materials Processing
							,169)--Westerville School Delivery

insert into @table
select o.OrganizationID [profile]
		,2
		,ROW_NUMBER () over ( 
			partition by o.OrganizationID 
			order by case when o.OrganizationID = o2.OrganizationID then 3 when o.ParentOrganizationID = o2.ParentOrganizationID and o2.Name like '%main%' then 2 when o.ParentOrganizationID = o2.ParentOrganizationID then 1 else 0 end desc, o2.Name 
		) [sortorder]
		,b.orgid [itembranch]
from polaris.polaris.Organizations o
cross join @branches b
join Polaris.Polaris.Organizations o2 on
	o2.OrganizationID = b.orgid
order by profile, sortorder


insert into @table
select	o.OrganizationID,
		1 [ItemAvailabilityLevelID],
		row_number() over(PARTITION by o.OrganizationID order by case when o.OrganizationID = o2.OrganizationID then 1 when o2.name like '%main%' or o2.name like '%tremont%' then 2 else o2.OrganizationID end) [SortOrder],
		o2.OrganizationID [BranchLocationOrgID]
from polaris.polaris.organizations o
join polaris.polaris.organizations o2 ON
	o.ParentOrganizationID = o2.ParentOrganizationID
where o.OrganizationCodeID = 3 
	and o2.OrganizationID in ( select * from @branches )
order by o.OrganizationID, SortOrder


declare @include_branches_in_local table ( libraryid int )
insert into @include_branches_in_local values (32) -- Worthington
											 ,(27) -- FCDL Outreach (web branch)
											 ,(95) -- Pickerington 24 Hour Pickup Lockers (web branch)
											 ,(98) -- Pickaway Main Drive Through
											 ,(105)-- Delaware

delete t
from @table t
join polaris.polaris.Organizations o
	on t.OrganizationID = o.OrganizationID
where t.OrganizationID != t.BranchLocationOrgID and t.ItemAvailabilityLevelID = 1 and (o.ParentOrganizationID not in (select * from @include_branches_in_local) and t.OrganizationID not in (select * from @include_branches_in_local) )

--update @table set Highlight = 1 where OrganizationID = BranchLocationOrgID



/* update */
--begin tran; delete from Polaris.Polaris.ItemAvailabilityDisplay; insert into polaris.polaris.ItemAvailabilityDisplay
--select *
--from @table t

--commit
--rollback




/* compare against current */
select t.OrganizationID, o.Name [Org], t.ItemAvailabilityLevelID, t.SortOrder, t.BranchLocationOrgID, o2.Name [Location]
from @table t
join polaris.polaris.Organizations o
	on o.OrganizationID = t.OrganizationID
join Polaris.polaris.organizations o2
	on o2.OrganizationID = t.BranchLocationOrgID
order by o.OrganizationID, t.ItemAvailabilityLevelID, t.SortOrder

select iad.OrganizationID, o.Name, iad.ItemAvailabilityLevelID, iad.SortOrder, iad.BranchLocationOrgID, o2.Name
from polaris.polaris.ItemAvailabilityDisplay iad
join polaris.polaris.Organizations o
	on o.OrganizationID = iad.OrganizationID
join Polaris.polaris.organizations o2
	on o2.OrganizationID = iad.BranchLocationOrgID
order by o.OrganizationID, iad.ItemAvailabilityLevelID, iad.SortOrder
