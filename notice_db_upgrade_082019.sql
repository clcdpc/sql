use CLC_Notices

begin tran

truncate table clc_notices.dbo.emailtemplates

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailNoticeFooters](
	[OrganizationId] [int] NOT NULL,
	[NotificationTypeId] [int] NOT NULL,
	[EmailFormatId] [int] NOT NULL,
	[LanguageId] [int] NOT NULL,
	[Value] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_EmailNoticeFooters] PRIMARY KEY CLUSTERED 
(
	[OrganizationId] ASC,
	[NotificationTypeId] ASC,
	[EmailFormatId] ASC,
	[LanguageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmailNoticeHeaders]    Script Date: 8/6/2019 10:46:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailNoticeHeaders](
	[OrganizationId] [int] NOT NULL,
	[NotificationTypeId] [int] NOT NULL,
	[EmailFormatId] [int] NOT NULL,
	[LanguageId] [int] NOT NULL,
	[Value] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_EmailNoticeHeaders] PRIMARY KEY CLUSTERED 
(
	[OrganizationId] ASC,
	[NotificationTypeId] ASC,
	[EmailFormatId] ASC,
	[LanguageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 0, 1, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 0, 2, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 1, 1, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 1, 2, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 2, 1, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 2, 2, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 3, 1, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 3, 2, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 4, 1, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 4, 2, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 5, 1, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 5, 2, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 6, 1, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 6, 2, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 7, 1, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 7, 2, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 8, 1, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 8, 2, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 9, 1, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 9, 2, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 10, 1, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 10, 2, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 11, 1, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 11, 2, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 12, 1, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 12, 2, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 13, 1, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 13, 2, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 14, 1, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 14, 2, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 15, 1, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 15, 2, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 16, 1, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 16, 2, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 17, 1, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 17, 2, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 18, 1, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 18, 2, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 19, 1, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 19, 2, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 20, 1, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeFooters] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 20, 2, 1033, N'For assistance or more information, please contact the library.')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 0, 1, 1033, N'')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 0, 2, 1033, N'')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 1, 1, 1033, N'According to our library records the following items checked out to your account are overdue. Please return these items as soon as possible.')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 1, 2, 1033, N'According to our library records the following items checked out to your account are overdue. Please return these items as soon as possible.')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 2, 1, 1033, N'The following items are being held for you at the library. Please pick the items up on or before the date indicated.')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 2, 2, 1033, N'The following items are being held for you at the library. Please pick the items up on or before the date indicated.')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 3, 1, 1033, N'Your request for:')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 3, 2, 1033, N'Your request for:')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 4, 1, 1033, N'')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 4, 2, 1033, N'')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 5, 1, 1033, N'')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 5, 2, 1033, N'')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 6, 1, 1033, N'')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 6, 2, 1033, N'')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 7, 1, 1033, N'')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 7, 2, 1033, N'')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 8, 1, 1033, N'')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 8, 2, 1033, N'')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 9, 1, 1033, N'')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 9, 2, 1033, N'')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 10, 1, 1033, N'Phone Number: {patron_phone}
E-mail Address: {patron_email_address}')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 10, 2, 1033, N'<table border="0">
            <tr>
                <td>Phone Number</td>
                <td>{patron_phone}</td>
            </tr>
            <tr>
                <td>E-mail Address</td>
                <td>{patron_email_address}</td>
            </tr>
        </table>')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 11, 1, 1033, N'')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 11, 2, 1033, N'')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 12, 1, 1033, N'According to our library records the following items checked out to your account are overdue. Please return these items as soon as possible.')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 12, 2, 1033, N'According to our library records the following items checked out to your account are overdue. Please return these items as soon as possible.')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 13, 1, 1033, N'According to our library records the following items checked out to your account are overdue. Please return these items as soon as possible.')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 13, 2, 1033, N'According to our library records the following items checked out to your account are overdue. Please return these items as soon as possible.')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 14, 1, 1033, N'')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 14, 2, 1033, N'')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 15, 1, 1033, N'')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 15, 2, 1033, N'')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 16, 1, 1033, N'')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 16, 2, 1033, N'')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 17, 1, 1033, N'')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 17, 2, 1033, N'')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 18, 1, 1033, N'The following items are being held for you at the library. Please pick the items up on or before the date indicated.')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 18, 2, 1033, N'The following items are being held for you at the library. Please pick the items up on or before the date indicated.')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 19, 1, 1033, N'')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 19, 2, 1033, N'')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 20, 1, 1033, N'')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (1, 20, 2, 1033, N'')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (86, 2, 1, 1033, N'The London Library has items on hold for you.  Call 740-852-9543 or log in to My Account at http://www.mylondonlibrary.org for more information. The following items will be held until the date shown below.')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (86, 2, 2, 1033, N'The London Library has items on hold for you.  Call 740-852-9543 or log in to My Account at <a href="http://www.mylondonlibrary.org">http://www.mylondonlibrary.org</a> for more information. The following items will be held until the date shown below.')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (86, 18, 1, 1033, N'The London Library has items on hold for you.  Call 740-852-9543 or log in to My Account at http://www.mylondonlibrary.org for more information. The following items will be held until the date shown below.')
GO
INSERT [dbo].[EmailNoticeHeaders] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Value]) VALUES (86, 18, 2, 1033, N'The London Library has items on hold for you.  Call 740-852-9543 or log in to My Account at <a href="http://www.mylondonlibrary.org">http://www.mylondonlibrary.org</a> for more information. The following items will be held until the date shown below.')
GO
INSERT [dbo].[EmailTemplates] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Url]) VALUES (1, 1, 1, 1033, N'/content/templates/default/generic.txt')
GO
INSERT [dbo].[EmailTemplates] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Url]) VALUES (1, 1, 2, 1033, N'/content/templates/default/generic.html')
GO
INSERT [dbo].[EmailTemplates] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Url]) VALUES (1, 2, 1, 1033, N'/content/templates/default/generic.txt')
GO
INSERT [dbo].[EmailTemplates] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Url]) VALUES (1, 2, 2, 1033, N'/content/templates/default/generic.html')
GO
INSERT [dbo].[EmailTemplates] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Url]) VALUES (1, 3, 1, 1033, N'/content/templates/default/generic.txt')
GO
INSERT [dbo].[EmailTemplates] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Url]) VALUES (1, 3, 2, 1033, N'/content/templates/default/generic.html')
GO
INSERT [dbo].[EmailTemplates] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Url]) VALUES (1, 7, 1, 1033, N'/content/templates/default/generic.txt')
GO
INSERT [dbo].[EmailTemplates] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Url]) VALUES (1, 7, 2, 1033, N'/content/templates/default/generic.html')
GO
INSERT [dbo].[EmailTemplates] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Url]) VALUES (1, 10, 1, 1033, N'/content/templates/default/generic.txt')
GO
INSERT [dbo].[EmailTemplates] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Url]) VALUES (1, 10, 2, 1033, N'/content/templates/default/generic.html')
GO
INSERT [dbo].[EmailTemplates] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Url]) VALUES (1, 12, 1, 1033, N'/content/templates/default/generic.txt')
GO
INSERT [dbo].[EmailTemplates] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Url]) VALUES (1, 12, 2, 1033, N'/content/templates/default/generic.html')
GO
INSERT [dbo].[EmailTemplates] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Url]) VALUES (1, 13, 1, 1033, N'/content/templates/default/generic.txt')
GO
INSERT [dbo].[EmailTemplates] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Url]) VALUES (1, 13, 2, 1033, N'/content/templates/default/generic.html')
GO
INSERT [dbo].[EmailTemplates] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Url]) VALUES (1, 18, 1, 1033, N'/content/templates/default/generic.txt')
GO
INSERT [dbo].[EmailTemplates] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Url]) VALUES (1, 18, 2, 1033, N'/content/templates/default/generic.html')
GO
INSERT [dbo].[EmailTemplates] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Url]) VALUES (2, 3, 1, 1033, N'/content/templates/cancel/alexandria.txt')
GO
INSERT [dbo].[EmailTemplates] ([OrganizationId], [NotificationTypeId], [EmailFormatId], [LanguageId], [Url]) VALUES (2, 3, 2, 1033, N'/content/templates/cancel/alexandria.html')
GO


update st
set mnemonic = replace(replace(st.Mnemonic, '_subject', ''), 'email', 'email_subject')
from CLC_Notices.dbo.SettingTypes st
where st.Mnemonic like '%subject%'

update st
set DefaultValue = replace(st.DefaultValue, '{item_format}', '{item_material_type}')
from CLC_Notices.dbo.SettingTypes st
where st.DefaultValue like '%{item_format}%'

delete from CLC_Notices.dbo.SettingTypes 
where SettingID in ( 25  -- enable_email_notices
					,140 -- hold_message_intro_plaintext
					,141 -- hold_message_intro_html
					,37  -- hold_cancellation_send_hour_1
					,38  -- hold_cancellation_send_hour_2
				   )
				   
delete from CLC_Notices.dbo.SettingValues
where Mnemonic in (
	 'email_first_overdue_header'
	,'email_second_overdue_header'
	,'email_third_overdue_header'
	)

delete from CLC_Notices.dbo.SettingTypes
where Mnemonic in (
	 'email_first_overdue_header'
	,'email_second_overdue_header'
	,'email_third_overdue_header'
	)	

-- wrong mnemonic on purpose, gets fixed by following step
update CLC_Notices.dbo.SettingTypes set Mnemonic ='enable_expiration_reminder_notices' where SettingID = 26

update st
set Mnemonic = replace(replace(st.Mnemonic, 'email_',''), 'enable', 'email_enable')
from CLC_Notices.dbo.SettingTypes st
where st.Mnemonic like '%enable%notices'
	or st.Mnemonic = 'enable_expiration_reminders'

update st
set st.Mnemonic = 'email_' + st.Mnemonic
from CLC_Notices.dbo.SettingTypes st
where st.SettingID in (113,118,137,139,166,168)
	or st.Mnemonic like 'reminder_%'

--rollback
--commit



