declare @patron int = 1752752
declare @option int = 2
declare @type int = 1
declare @reportingOrg int = 3
declare @autorenewDate date = '8/7/2019'
declare @duedate date = '8/3/2019'


declare @insert bit = 1

declare @deleteOnInsert bit = 1

declare @orgID int = ( select organizationid from polaris.polaris.patrons where patronid = @patron )
declare @szReportingOrgPhone varchar(255) = ( select result FROM Polaris.Polaris.fn_SA_GetValueEx(N'ORGPHONE1', N'Branch', 0, 0, 0, @orgID, 0, 0, 0, 0) )

/*
INSERT INTO Results.Polaris.HoldCancellationNotices ( RequestID, PatronID, PickupOrganizationID, Title, RequestDate, StatusID, Reason, BibliographicRecordID, AdminLanguageID, DeliveryOptionID )
	select * from #hcn
	INSERT INTO Results.Polaris.CircReminders (NotificationTypeID,ReportingOrgID,ReportingOrgName,ReportingOrgAddress,ReportingOrgPhone,PatronID,PatronName,EmailAddress,PatronAddress,EmailFormatID,AdminLanguageID,DeliveryOptionID,ItemRecordID,DueDate,BrowseTitle,ItemFormat,ItemAssignedBranch,Renewals,AutoRenewal)
	
	
*/

declare @hcn table ( RequestID int, PatronID int, PickupOrganizationID int, Title varchar(255), RequestDate datetime, StatusID int, Reason varchar(255), BiblioGraphicRecordID int, AdminLangugeID int, DeliveryOptionID int)
declare @cr table ( NotificationTypeID int, ReportingOrgID int, ReportingOrgName varchar(255), ReportingOrgAddress varchar(255), ReportingOrgPhone varchar(255), PatronID int, PatronName varchar(255), EmailAddress varchar(255), PatronAddress varchar(255), EmailFormatID int, AdminLanguageID int, DeliveryOptionID int, ItemRecordID int, DueDate datetime, BrowseTitle varchar(255), ItemFormat  varchar(255), ItemAssignedBranch  varchar(255), Renewals int, AutoRenewal bit)
declare @nq table ( ItemId int, ReportingOrgId int, NotificationTypeId int, PatronId int, DeliveryOptionId int, Processed int )

begin -- HoldCancellationNotices
	insert into @hcn
	select shr.SysHoldRequestID
			,shr.PatronID
			,shr.PickupBranchID
			,Polaris.Polaris.fn_Circ_GetSysHoldRequestDisplayTitle(shr.SysHoldRequestID, 100, 1) [Title]
			,shr.CreationDate
			,shr.SysHoldStatusID
			,CASE shr.SysHoldStatusID
									WHEN 7 THEN Polaris.Polaris.SA_GetMultiLingualStringValue(7, 1033, shr.PickupBranchID, N'TXT_CANCELREASON_UNFILLED')
									WHEN 9 THEN Polaris.Polaris.SA_GetMultiLingualStringValue(7, 1033, shr.PickupBranchID, N'TXT_CANCELREASON_EXPIRED')
									else Polaris.Polaris.SA_GetMultiLingualStringValue(7, 1033, shr.PickupBranchID, N'TXT_CANCELREASON_CANCELLED')
								end [Reason]
			,shr.BibliographicRecordID
			,1033 [AdminLanguageID]
			,@option [DeliveryOptionID]
	from Polaris.Polaris.SysHoldRequests shr
	where shr.PatronID = @patron
end

begin --CircReminders
	insert into @cr
	SELECT--INSERT INTO Results.Polaris.CircReminders (NotificationTypeID,ReportingOrgID,ReportingOrgName,ReportingOrgAddress,ReportingOrgPhone,PatronID,PatronName,EmailAddress,PatronAddress,EmailFormatID,AdminLanguageID,DeliveryOptionID,ItemRecordID,DueDate,BrowseTitle,ItemFormat,ItemAssignedBranch,Renewals,AutoRenewal)
	
		7 [NotificationTypeID],
		pr.OrganizationID [ReportingOrgId],
		o1.Name [ReportingOrgName],
		Polaris.Polaris.fn_OrganizationAddress(pr.OrganizationID) [ReportingOrgAddress],
		@szReportingOrgPhone [ReportingOrgPhone],
		pr.PatronID, 
		Polaris.Polaris.fn_PatronFullName(pr.PatronID) [PatronName], 
		pr.EmailAddress,
		polaris.Polaris.fn_PatronNoticeAddress (pr.PatronID) [PatronAddress],
		pr.EmailFormatID,
		polaris.Polaris.fn_Rpt_GetLanguageID(pr.OrganizationID, pr.AdminLanguageID) [AdminLanguageID],
		@option [DeliveryOptionId], -- e-mail
		ic.ItemRecordID, 
		ic.DueDate, 
		br.BrowseTitle, 
		mtm.Description, 
		o2.Name, 
		CASE
			WHEN IsNull(ir.RenewalLimit, 0) > IsNull(ic.Renewals, 0) THEN IsNull(ir.RenewalLimit, 0) - IsNull(ic.Renewals, 0)
			ELSE 0
		end [Renewals],
		case cast(ic.DueDate as date) when @autorenewDate then 1 else 0 end [AutoRenew]
	FROM 
		Polaris.Polaris.ItemCheckouts ic WITH (NOLOCK) 
		INNER JOIN Polaris.Polaris.ViewPatronRegistration pr WITH (NOLOCK) ON (pr.PatronID = ic.PatronID)
		INNER JOIN Polaris.Polaris.Organizations o1 WITH (NOLOCK) ON (o1.OrganizationID = pr.OrganizationId)
		INNER JOIN Polaris.Polaris.CircReserveItemRecords_View ir WITH (NOLOCK) ON (ic.ItemRecordID = ir.ItemRecordID)
		INNER JOIN Polaris.Polaris.CircItemRecords cir WITH (NOLOCK) ON (ic.ItemRecordID = cir.ItemRecordID)
		INNER JOIN Polaris.Polaris.BibliographicRecords br WITH (NOLOCK) ON (ir.AssociatedBibRecordID = br.BibliographicRecordID)
		INNER JOIN Polaris.Polaris.MARCTypeOfMaterial mtm WITH (NOLOCK) ON (br.PrimaryMARCTOMID = mtm.MARCTypeOfMaterialID)
		INNER JOIN Polaris.Polaris.Organizations o2 WITH (NOLOCK) ON (ir.AssignedBranchID = o2.OrganizationID)
	WHERE 
		pr.PatronID = @patron
end

/*** NotificationQueue ***/

-- Cancel
insert into @nq
select shr.SysHoldRequestID
		,coalesce(shr.NewPickupBranchID, shr.PickupBranchID) [ReportingOrgID]
		,3 [NotificationTypeID]
		,shr.PatronID
		,@option [DeliveryOptionID]
		,0 [Processed]
from Polaris.Polaris.SysHoldRequests shr
where shr.PatronID = @patron

-- Overdue
insert into @nq
select	ic.ItemRecordID	[ItemRecordID], 
		@reportingOrg	[ReportingOrgID],
		case
			when datepart(ms, ic.CheckOutDate) % 12 < 3 then 12
			when datepart(ms, ic.CheckOutDate) % 13 < 3 then 13
			else 1
		end				[NotificationTypeID], 
		ic.PatronID		[PatronID], 
		@option			[DeliveryOptionID], 
		0				[Processed]
from Polaris.polaris.ItemCheckouts ic
where ic.PatronID = @patron
	and cast(ic.DueDate as date) = @duedate


if (@insert = 1)
begin
	if (@deleteOnInsert = 1) 
	begin
		delete from Results.Polaris.HoldCancellationNotices where PatronId = @patron
		delete from Results.Polaris.CircReminders where PatronId = @patron
		delete from Results.Polaris.NotificationQueue where PatronId = @patron
	end

	INSERT INTO Results.Polaris.HoldCancellationNotices ( RequestID, PatronID, PickupOrganizationID, Title, RequestDate, StatusID, Reason, BibliographicRecordID, AdminLanguageID, DeliveryOptionID )
	select * from @hcn

	INSERT INTO Results.Polaris.CircReminders (NotificationTypeID,ReportingOrgID,ReportingOrgName,ReportingOrgAddress,ReportingOrgPhone,PatronID,PatronName,EmailAddress,PatronAddress,EmailFormatID,AdminLanguageID,DeliveryOptionID,ItemRecordID,DueDate,BrowseTitle,ItemFormat,ItemAssignedBranch,Renewals,AutoRenewal)
	select * from @cr

	INSERT INTO Results.Polaris.NotificationQueue ( ItemRecordID, ReportingOrgID, NotificationTypeID, PatronID, DeliveryOptionID, Processed )
	select * from @nq

	select * from Results.Polaris.HoldCancellationNotices where PatronId = @patron
	select * from Results.Polaris.CircReminders where PatronId = @patron
	select * from Results.Polaris.NotificationQueue where PatronId = @patron

end
else
begin
	select * from @nq
	select * from @hcn
	select * from @cr
end





--INSERT INTO Results.Polaris.CircReminders (NotificationTypeID,ReportingOrgID,ReportingOrgName,ReportingOrgAddress,ReportingOrgPhone,PatronID,PatronName,EmailAddress,PatronAddress,EmailFormatID,AdminLanguageID,DeliveryOptionID,ItemRecordID,DueDate,BrowseTitle,ItemFormat,ItemAssignedBranch,Renewals,AutoRenewal)
	
/*
	INSERT INTO Results.Polaris.NotificationQueue
			(
				ItemRecordID,
				ReportingOrgID,
				NotificationTypeID,
				PatronID,
				DeliveryOptionID,
				Processed	
			)
*/


/*
select * from CLC_Notices.dbo.PolarisNotifications pn where pn.PatronID = 1752752

select * from polaris.polaris.itemcheckouts where PatronID = 1752752

select * from results.polaris.NotificationQueue where patronid = 1752752

select * from Results.polaris.CircReminders cr where cr.PatronID = 1752752
*/