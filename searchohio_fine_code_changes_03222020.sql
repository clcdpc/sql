begin tran

-- update all searchohio fine codes
update f_dest
set f_dest.Amount = f_source.Amount
	,f_dest.MaximumFine = f_source.MaximumFine
from polaris.Polaris.Fines f_dest
join polaris.polaris.Organizations o
	on o.OrganizationID = f_dest.OrganizationID
join polaris.polaris.Fines f_source
	on f_source.OrganizationID = f_dest.OrganizationID and f_source.PatronCodeID = f_dest.PatronCodeID and f_source.FineCodeID = 8
where f_dest.FineCodeID = 52

-- set searchohio templates to $25 replacement cost
update it
set it.Price = 25
from polaris.polaris.ItemTemplates it 
where it.Name like 'innr%ohpir%'
	and it.name not like '%9plai'

-- plain city set templates to book
update it
set it.FineCodeID = 8
from polaris.polaris.ItemTemplates it 
where it.ItemTemplateID in (
	2990	-- INNR Item ohpir-230 mpc 9plai
	,3023	-- INNR Item ohpir-233 mpc 9plai
	,7329	-- INNR Item ohpir-232 mpc 9plai
)

-- plain city set template to video pg
update it
set it.FineCodeID = 3
from polaris.polaris.ItemTemplates it 
where it.ItemTemplateID in (3009) -- INNR Item ohpir-231 mpc 9plai

--rollback
--commit