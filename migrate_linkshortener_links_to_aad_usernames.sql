select distinct s.Item
from Links l
cross apply Polaris.dbo.CLC_Custom_SplitString(l.Creator, '\') s
where s.ItemIndex = 0

declare @windowsToEmailDomainMap table (wd varchar(50), ed varchar(50))

insert into @windowsToEmailDomainMap values ('clcdpc','clcohio.org'),('ghpdpc','ghpl.org'),('nt_wor','worthingtonlibraries.org'),('pcldpc','pickawaylib.org'),('uapl','ualibrary.org')

begin tran

update l
set l.Creator = s2.Item + '@' + m.ed
from LinkShortener.dbo.Links l
cross apply Polaris.dbo.CLC_Custom_SplitString(l.Creator, '\') s
cross apply Polaris.dbo.CLC_Custom_SplitString(l.Creator, '\') s2
join @windowsToEmailDomainMap m
	on m.wd = s.Item
where s.ItemIndex = 0
	and s2.ItemIndex = 1

--rollback
--commit