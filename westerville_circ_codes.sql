declare @branch int = 7

select mt.MaterialTypeID
		,mt.Description [MaterialType]
		,pc.PatronCodeID
		,pc.Description [PatronCode]
		,mll.MaxItems
		,mll.MaxRequestItems
from polaris.polaris.MaterialLoanLimits mll
join polaris.polaris.PatronCodes pc
	on pc.PatronCodeID = mll.PatronCodeID
join polaris.polaris.MaterialTypes mt
	on mt.MaterialTypeID = mll.MaterialTypeID
where mt.Description not like 'z%'
	and pc.Description not like 'z%'
	and mll.OrganizationID = @branch
order by pc.Description, mt.Description


select   pc.PatronCodeID
		,pc.Description [PatronCode]
		,pll.MinFine
		,pll.MaxFine
		,pll.TotalItems
		,pll.TotalOverDue
		,pll.TotalHolds
		from polaris.polaris.PatronLoanLimits pll
join polaris.polaris.PatronCodes pc
	on pc.PatronCodeID = pll.PatronCodeID
where pc.Description not like 'z%'
	and pll.OrganizationID = @branch
order by pc.Description


select   lpc.LoanPeriodCodeID
		,lpc.Description [LoanPeriod]
		,pc.PatronCodeID
		,pc.Description [PatronCode]
		,lp.Units [Days]
from polaris.polaris.LoanPeriods lp
join polaris.polaris.LoanPeriodCodes lpc
	on lpc.LoanPeriodCodeID = lp.LoanPeriodCodeID
join polaris.polaris.PatronCodes pc
	on pc.PatronCodeID = lp.PatronCodeID
where lpc.Description not like 'z%'
	and pc.Description not like 'z%'
	and lp.OrganizationID = @branch
order by pc.Description, lpc.Description