begin tran

update ic
set ic.DueDate = cast(coalesce(dr.ItemDueDate, dr.EndDate) as datetime) + .9999999
--output deleted.ItemRecordID
--		,deleted.DueDate
--		into Polaris.dbo.CLC_Custom_DueDateBackup2
from Polaris.Polaris.ItemCheckouts ic
join polaris.Polaris.Organizations o
	on o.OrganizationID = ic.OrganizationID
join polaris.Polaris.CircItemRecords cir
	on cir.ItemRecordID = ic.ItemRecordID
join Polaris.dbo.CLC_Custom_CV_Closed_Date_Ranges dr
	on dr.LibraryId = o.ParentOrganizationID
where ic.DueDate between '3/12/2020' and coalesce(dr.ItemDueDate, dr.EndDate)
	and cir.ElectronicItem = 0
	and coalesce(dr.ItemDueDate, dr.EndDate) != cast(ic.DueDate as date)
	-- no longer excludes plain city as of 5/5
	and o.ParentOrganizationID not in (2)

--select @@rowcount
--select count(*) from polaris.dbo.CLC_Custom_DueDateBackup2

--rollback
--commit

/*

USE [Polaris]
GO

CREATE TABLE [dbo].[CLC_Custom_DueDateBackup2](
	[ItemRecordID] [int] NOT NULL,
	[OldDueDate] [date] NOT NULL
) ON [PRIMARY]
GO

*/


/*

begin tran

update ic
set ic.DueDate = ddb.OldDueDate
from Polaris.polaris.ItemCheckouts ic
join polaris.dbo.CLC_Custom_DueDateBackup ddb
	on ddb.ItemRecordID = ic.ItemRecordID

--commit
--rollback

*/
-- 856
