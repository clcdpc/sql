begin tran

update pr
set pr.Phone1CarrierID = 23
from Polaris.Polaris.PatronRegistration pr
left join Polaris.Polaris.SA_MobilePhoneCarriers mpc
	on mpc.CarrierID = pr.Phone1CarrierID
where mpc.Display = 0

update pr
set pr.Phone2CarrierID = 23
from Polaris.Polaris.PatronRegistration pr
left join Polaris.Polaris.SA_MobilePhoneCarriers mpc
	on mpc.CarrierID = pr.Phone2CarrierID
where mpc.Display = 0

update pr
set pr.Phone3CarrierID = 23
from Polaris.Polaris.PatronRegistration pr
left join Polaris.Polaris.SA_MobilePhoneCarriers mpc
	on mpc.CarrierID = pr.Phone3CarrierID
where mpc.Display = 0

--rollback
--commit