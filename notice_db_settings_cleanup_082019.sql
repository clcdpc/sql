begin tran

delete from CLC_Notices.dbo.SettingValues
where Mnemonic in (
	 'email_first_overdue_header'
	,'email_second_overdue_header'
	,'email_third_overdue_header'
	)

delete from CLC_Notices.dbo.SettingTypes
where Mnemonic in (
	 'email_first_overdue_header'
	,'email_second_overdue_header'
	,'email_third_overdue_header'
	)

--rollback
--commit