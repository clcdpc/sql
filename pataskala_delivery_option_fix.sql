begin tran

update pr
set pr.TxtPhoneNumber = pd.NewTxtPhone
	,pr.DeliveryOptionID = case pd.LegacyDeliveryOption
			when 'S' then 8
			when 'E' then 2
			when 'P' then case 
							when pd.PhoneVoice1 is not null then 3 
							when pd.PhoneVoice2 is not null then 4 
							when pd.PhoneVoice3 is not null then 5 
							else 1 end
		end

		,pr.Phone1CarrierID = case when pd.NewTxtPhone = 1 then 23 else null end 
		,pr.Phone2CarrierID = case when pd.NewTxtPhone = 2 then 23 else null end
		,pr.Phone3CarrierID = case when pd.NewTxtPhone = 3 then 23 else null end			
from (
	select	pd.BARCODE
			,coalesce(case when pd.ARRIVEDHOLDNOTICESCONFIG is null or pd.ARRIVEDHOLDNOTICESCONFIG = '' then null else pd.ARRIVEDHOLDNOTICESCONFIG end
					,case when pd.FRIENDLYWARNINGNOTICECONFIG is null or pd.FRIENDLYWARNINGNOTICECONFIG = '' then null else pd.FRIENDLYWARNINGNOTICECONFIG end
					,case when pd.OVERDUENOTICESCONFIG is null or pd.OVERDUENOTICESCONFIG = '' then null else pd.OVERDUENOTICESCONFIG end
					,case when pd.CANCELLEDHOLDNOTICESCONFIG is null or pd.CANCELLEDHOLDNOTICESCONFIG = '' then null else pd.CANCELLEDHOLDNOTICESCONFIG end
					,'P') [LegacyDeliveryOption]
			,pr.PhoneVoice1
			,pr.PhoneVoice2
			,pr.PhoneVoice3
			,pd.SMSPHONE
			,pd.ADDRESS1PHONE1
			,pd.ADDRESS1PHONE2
			,case polaris.dbo.CLC_Custom_StripCharacters(pd.SMSPHONE)
				when Polaris.dbo.CLC_Custom_StripCharacters(pr.PhoneVoice1) then 1
				when Polaris.dbo.CLC_Custom_StripCharacters(pr.PhoneVoice2) then 2
				when Polaris.dbo.CLC_Custom_StripCharacters(pr.PhoneVoice3) then 3
			end [NewTxtPhone]
			,pr.Phone1CarrierID
			,pr.Phone2CarrierID
	from polaris.polaris.Cnv_Tmp_CLC_Pataskala_PatronData pd
	join polaris.polaris.Patrons p
		on p.Barcode = pd.BARCODE
	join Polaris.Polaris.PatronRegistration pr
		on pr.PatronID = p.PatronID
) pd
join polaris.Polaris.Patrons p
	on p.Barcode = pd.BARCODE
join polaris.polaris.PatronRegistration pr
	on pr.PatronID = p.PatronID



--rollback
--commit
