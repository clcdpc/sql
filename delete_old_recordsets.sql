begin tran

declare @rsids table ( rsid int )

insert into @rsids
select rs.RecordSetID
from Polaris.Polaris.RecordSets rs
where coalesce(rs.ModificationDate, rs.CreationDate) < dateadd(month, -18, getdate()) and rs.Name + coalesce(rs.Note, '') not like '%keep%'

select count(*) from Polaris.Polaris.RecordSets

delete from Polaris.Polaris.BibRecordSets where RecordSetID in ( select * from @rsids )
delete from Polaris.Polaris.ItemRecordSets where RecordSetID in ( select * from @rsids )
delete from Polaris.Polaris.PatronRecordSets where RecordSetID in ( select * from @rsids )
delete from Polaris.Polaris.AuthorityRecordSets where RecordSetID in ( select * from @rsids )

delete from Polaris.Polaris.RecordSets where RecordSetID in ( select * from @rsids )

select count(*) from Polaris.Polaris.RecordSets

--rollback
--commit