declare @oldBibId int = 3737190
declare @newBidId int = 3771519
declare @libraryId int = 39

begin tran

update shr
set shr.BibliographicRecordID = @newBidId
from polaris.polaris.SysHoldRequests shr
join polaris.polaris.organizations o
	on o.OrganizationID = isnull(shr.NewPickupBranchID, shr.PickupBranchID)
where shr.SysHoldStatusID in (1,3,4)
	and shr.BibliographicRecordID = @oldBibId
	and o.ParentOrganizationID = @libraryId

--rollback
--commit