begin tran

delete lb from Polaris.polaris.SA_PACLimitBy lb
where lb.OrganizationID = 76
	and lb.DisplayOrder > 1

declare @newlb table (thename varchar(255), ccl varchar(255))
insert into @newlb values('-- Available Now at Grove City', 'AVAILABILITY>0 AND AB=29')
insert into @newlb values('---- Grove City Juvenile', 'AVAILABILITY>0 AND AB=29 AND MAT={list}13,15,16,17,19,18,42,20{/list}')
insert into @newlb values('-- Available Now at Westland', 'AVAILABILITY>0 AND AB=31')
insert into @newlb values('---- Westland Juvenile', 'AVAILABILITY>0 AND AB=31 AND MAT={list}13,15,16,17,19,18,42,20{/list}')
insert into @newlb values('All Books', 'TOM=bks')
insert into @newlb values('-- Books available now at GCL', 'AVAILABILITY>0 AND TOM=bks AND AB=29')
insert into @newlb values('-- Books available now at WAL', 'AVAILABILITY>0 AND TOM=bks AND AB=31')
insert into @newlb values('-- Large Print', 'TOM=lpt')
insert into @newlb values('---- LP available now at GCL', 'AVAILABILITY>0 AND TOM=lpt AND AB=29')
insert into @newlb values('---- LP available now at WAL', 'AVAILABILITY>0 AND TOM=lpt AND AB=31')
insert into @newlb values('All Movies', 'TOM=vid')
insert into @newlb values('-- Movies available now at GCL', 'AVAILABILITY>0 AND TOM=vid AND AB=29')
insert into @newlb values('-- Movies available now at WAL', 'AVAILABILITY>0 AND TOM=vid AND AB=31')
insert into @newlb values('-- DVDs', 'TOM=dvd')
insert into @newlb values('-- Blu-rays', 'TOM=brd')
insert into @newlb values('-- Juvenile Movies', 'TOM=vid AND MAT={list}18,42,49{/list}')
insert into @newlb values('All Audio Books', 'TOM=abk')
insert into @newlb values('-- Audio Books available now at GCL', 'AVAILABILITY>0 AND TOM=abk AND AB=29')
insert into @newlb values('-- Audio Books available now at WAL', 'AVAILABILITY>0 AND TOM=abk AND AB=31')
insert into @newlb values('All Music CDs', 'TOM=mus')
insert into @newlb values('-- CDs available now at GCL', 'AVAILABILITY>0 AND TOM=mus AND AB=29')
insert into @newlb values('-- CDs available now at WAL', 'AVAILABILITY>0 AND TOM=mus AND AB=31')

insert into Polaris.Polaris.SA_PACLimitBy
select 76, lb.thename, lb.ccl, 1, row_number() over(order by (select null)) + 1,0
from @newlb lb


select *
from Polaris.polaris.SA_PACLimitBy lb
where lb.OrganizationID = 76
order by lb.DisplayOrder


--rollback
--commit


--select * from polaris.polaris.materialtypes order by Description

