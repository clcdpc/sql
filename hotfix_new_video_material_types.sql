begin tran

update lp
set lp.Units = lp_source.Units
from polaris.polaris.LoanPeriods lp
join polaris.Polaris.LoanPeriods lp_source
	on lp_source.PatronCodeID = lp.PatronCodeID and lp_source.OrganizationID = lp.OrganizationID
where lp.LoanPeriodCodeID in (39,46 ) and lp_source.LoanPeriodCodeID = 2 and lp.Units = 0 and lp.OrganizationID < 105


update lp
set lp.Units = lp_source.Units
from polaris.polaris.LoanPeriods lp
join polaris.Polaris.LoanPeriods lp_source
	on lp_source.PatronCodeID = lp.PatronCodeID and lp_source.OrganizationID = lp.OrganizationID
where lp.LoanPeriodCodeID in ( 49 ) and lp_source.LoanPeriodCodeID = 5 and lp.Units = 0 and lp.OrganizationID < 105

update lp
set lp.Units = lp_source.Units
from polaris.polaris.LoanPeriods lp
join polaris.Polaris.LoanPeriods lp_source
	on lp_source.PatronCodeID = lp.PatronCodeID and lp_source.OrganizationID = lp.OrganizationID
where lp.LoanPeriodCodeID in ( 48 ) and lp_source.LoanPeriodCodeID = 4 and lp.Units = 0 and lp.OrganizationID < 105

update lp
set lp.Units = lp_source.Units
from polaris.polaris.LoanPeriods lp
join polaris.Polaris.LoanPeriods lp_source
	on lp_source.PatronCodeID = lp.PatronCodeID and lp_source.OrganizationID = lp.OrganizationID
where lp.LoanPeriodCodeID in ( 50 ) and lp_source.LoanPeriodCodeID = 7 and lp.Units = 0 and lp.OrganizationID < 105

update lp
set lp.Units = lp_source.Units
from polaris.polaris.LoanPeriods lp
join polaris.Polaris.LoanPeriods lp_source
	on lp_source.PatronCodeID = lp.PatronCodeID and lp_source.OrganizationID = lp.OrganizationID
where lp.LoanPeriodCodeID in ( 47 ) and lp_source.LoanPeriodCodeID = 6 and lp.Units = 0 and lp.OrganizationID < 105

--rollback
--commit