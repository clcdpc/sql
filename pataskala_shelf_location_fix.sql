--PA7 = New Fiction
--PA8 = New Large Print
--PB7 = New Nonfiction
--PE1 = New Teen Fiction
-- PC = New Periodicals
begin tran

-- New Fiction
update cir
set ShelfLocationID = 4
from polaris.Polaris.Cnv_Tmp_CLC_Pataskala_ItemStatisticsData isd
join polaris.polaris.CircItemRecords cir
	on cir.Barcode = isd.ITEMBARCODE
where isd.CIRCEDBYHOLDINGSCODE = 'pa7'

-- New Large Print
update cir
set ShelfLocationID = 3
from polaris.Polaris.Cnv_Tmp_CLC_Pataskala_ItemStatisticsData isd
join polaris.polaris.CircItemRecords cir
	on cir.Barcode = isd.ITEMBARCODE
where isd.CIRCEDBYHOLDINGSCODE = 'pa8'

-- New Nonfiction
update cir
set ShelfLocationID = 5
from polaris.Polaris.Cnv_Tmp_CLC_Pataskala_ItemStatisticsData isd
join polaris.polaris.CircItemRecords cir
	on cir.Barcode = isd.ITEMBARCODE
where isd.CIRCEDBYHOLDINGSCODE = 'pb7'

-- New Teen Fiction
update cir
set ShelfLocationID = 1
from polaris.Polaris.Cnv_Tmp_CLC_Pataskala_ItemStatisticsData isd
join polaris.polaris.CircItemRecords cir
	on cir.Barcode = isd.ITEMBARCODE
where isd.CIRCEDBYHOLDINGSCODE = 'pe1'

-- New Periodicals
update cir
set ShelfLocationID = 6
from polaris.Polaris.Cnv_Tmp_CLC_Pataskala_ItemStatisticsData isd
join polaris.polaris.CircItemRecords cir
	on cir.Barcode = isd.ITEMBARCODE
where isd.CIRCEDBYHOLDINGSCODE = 'pc'

select sl.Description, count(*)
from polaris.polaris.CircItemRecords cir
join polaris.polaris.ShelfLocations sl
	on sl.OrganizationID = cir.AssignedBranchID and sl.ShelfLocationID = cir.ShelfLocationID
where cir.AssignedBranchID = 115
group by sl.Description


--rollback
--commit


select cir.itemrecordid
from polaris.Polaris.Cnv_Tmp_CLC_Pataskala_ItemStatisticsData isd
join polaris.polaris.CircItemRecords cir
	on cir.Barcode = isd.ITEMBARCODE
where isd.CIRCEDBYHOLDINGSCODE = 'pc'