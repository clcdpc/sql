USE [CLC_Notices]
GO
/****** Object:  Table [dbo].[Dialin_String_Types]    Script Date: 8/12/2016 12:31:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Dialin_String_Types](
	[Mnemonic] [varchar](255) NOT NULL,
	[Notes] [varchar](max) NULL,
 CONSTRAINT [PK_Dialin_String_Types] PRIMARY KEY CLUSTERED 
(
	[Mnemonic] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Dialin_Strings]    Script Date: 8/12/2016 12:31:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Dialin_Strings](
	[OrganizationID] [int] NOT NULL,
	[Mnemonic] [varchar](255) NOT NULL,
	[Value] [varchar](5000) NOT NULL,
 CONSTRAINT [PK_Dialin_Strings] PRIMARY KEY CLUSTERED 
(
	[OrganizationID] ASC,
	[Mnemonic] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'email_titles_confirm', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'error_incorrect_login', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'error_no_gather_entry', N'Said when the user doesn''t enter anything at a prompt')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'holds_count_information', N'{0} is the count of holds. {1} is item or items depending on count')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'holds_next_hold_prompt', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'holds_no_holds', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'holds_no_more_holds', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'item_list_all_items_prompt', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'item_list_email_prompt', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'item_list_no_items', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'item_list_overdue_only_prompt', N'{0} is the count. {1} is item or items depending on count. {2} is this item or these items depending on count.')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'item_list_renew_barcode_prompt', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'list_items_no_items', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'list_items_no_more_items', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'list_items_no_overdues', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'login_account_lookup', N'Said after the user enters their pin')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'menu_call_library', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'menu_greeting', N'{0} is the patron''s firstname. {1} is the patron''s registered branch name.')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'menu_greeting2', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'menu_hold_prompt', N'{0} is the count of holds. {1} is item or items depending on item count')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'menu_list_items_prompt', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'menu_renew_all_prompt', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'prompt_barcode', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'prompt_pin', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'prompt_remembered_patron', N'{0} will be replaced with the patron''s first name and MUST be present')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'renew_all_no_items', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'renew_barcode_error', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'renew_barcode_invalid_entry', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'renew_barcode_prompt', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'renew_barcode_success', N'{0} is the title of the item. {1} is the new duedate.')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'renew_item_error', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'renew_item_error_confirm', N'')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'renew_item_success', N'{0} is the new due date')
GO
INSERT [dbo].[Dialin_String_Types] ([Mnemonic], [Notes]) VALUES (N'renew_item_unknown_error', N'')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'email_titles_confirm', N'An email has been sent to the address on file and you should receive it in a few minutes. Returning to the main menu.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'error_incorrect_login', N'Incorrect barcode or pin')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'error_no_gather_entry', N'Sorry, I didn''t get that.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'holds_count_information', N'You currently have {0} {1} ready for pickup.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'holds_next_hold_prompt', N'To hear your next hold, press 1. To return to the main menu, press 7.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'holds_no_holds', N'You currently have no holds ready for pickup. Returning to main menu.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'holds_no_more_holds', N'You have no more holds to list. Returning to main menu.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'item_list_all_items_prompt', N'To hear a list of all items you currently have checked out press 2.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'item_list_email_prompt', N'To have a list of your checked out titles sent to your email address, press 4')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'item_list_no_items', N'You currently have no items checked out. Returning to main menu.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'item_list_overdue_only_prompt', N'You currently have {0} {1} overdue. To list only {2} press 1.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'item_list_renew_barcode_prompt', N'To look up an item by barcode press 3.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'list_items_no_items', N'You currently have no items checked out. Returning to main menu.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'list_items_no_more_items', N'You have no more items to list, returning to the main menu.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'list_items_no_overdues', N'You currently have no overdue items')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'login_account_lookup', N'One moment while I look up your account.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'menu_call_library', N'Connecting you to your library.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'menu_greeting', N'Hello {0}, thank you for calling the {1}.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'menu_greeting2', N'Press 0 at any time to be connected to your library or press 7 to be taken to this menu.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'menu_hold_prompt', N'You have {0} {1} ready for pickup. To hear hold information, press 4.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'menu_list_items_prompt', N'To list the items you currently have checked out and renew individual items, press 2.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'menu_renew_all_prompt', N'To renew all items, press 1.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'prompt_barcode', N'Hello, thank you for calling the library. Please enter your library account barcode, followed by the pound key')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'prompt_pin', N'Please enter your pin')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'prompt_remembered_patron', N'Hello. Are you calling about {0}''s library account? Press 1 to confirm or press 2 to enter your library account barcode.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'renew_all_no_items', N'You currently have no items to renew, returning to main menu.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'renew_barcode_error', N'Unable to renew {0} for the following reason: {1}')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'renew_barcode_invalid_entry', N'The barcode you entered is not valid. Please try again.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'renew_barcode_prompt', N'"Enter the barcode of the item you wish to renew, followed by the pound key. Or press 7 to go back to the main menu.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'renew_barcode_success', N'Successfully renewed {0}. The new due date is {1}.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'renew_item_error', N'Your item was unable to be renewed for the following reason, {0}')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'renew_item_error_confirm', N'Press 2 to confirm and return to your list of items out.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'renew_item_success', N'Your item was successfully renewed. This item is due back on {0}.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (1, N'renew_item_unknown_error', N'Your item was unable to be renewed due to an unknown error. If this problem persists please contact your library for assistance.')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (5, N'menu_greeting', N'custom menu greeting')
GO
INSERT [dbo].[Dialin_Strings] ([OrganizationID], [Mnemonic], [Value]) VALUES (6, N'menu_greeting', N'parent inheritance check')
GO
ALTER TABLE [dbo].[Dialin_Strings]  WITH CHECK ADD  CONSTRAINT [FK_Dialin_Strings_Dialin_String_Types] FOREIGN KEY([Mnemonic])
REFERENCES [dbo].[Dialin_String_Types] ([Mnemonic])
GO
ALTER TABLE [dbo].[Dialin_Strings] CHECK CONSTRAINT [FK_Dialin_Strings_Dialin_String_Types]
GO
