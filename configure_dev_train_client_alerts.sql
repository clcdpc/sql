-- if dev, update the last sync date for the logon message
declare @lastSyncDate varchar(25) = '4/7/2020'

declare @env char = (select case ComputerName when 'devdb' then 'd' when 'traindb' then 't' end from polaris.Polaris.servers where ServerID = 1)

if (@env not in ('d','t'))
begin
	select 'invalid environment'
	return
end

declare @msgDev varchar(max) = 'You are logging into the Polaris Dev system. Data in this system was copied on ' + @lastSyncDate + ', changes are NOT backed up. Please document or backup any settings that you are testing in this environment.'
declare @msgTrain varchar(max) = 'You are logging into the TRAINING environment.  The training system is refreshed each night from Production, so any changes you make will be overwritten the next day.'

declare @activeTitlebarColorDev varchar(10) = 'ff0080'
declare @inactiveTitlebarColorDev varchar(10) = 'ff5780'
declare @activeTitlebarColorTrain varchar(10) = 'ff00ff'
declare @inactiveTitlebarColorTrain varchar(10) = 'ff00ff'

begin tran

-- delete any existing values
delete from polaris.polaris.OrganizationsPPPP where OrganizationID = 1 and AttrID in (2949,2950,2951,2952,2953)

-- logon message alert
insert into Polaris.polaris.OrganizationsPPPP values (1, 2949, 'Yes') -- Enable alert
insert into polaris.polaris.OrganizationsPPPP values (1, 2950, case @env when 'd' then @msgDev when 't' then @msgTrain end) -- Change logon message

-- title bar alert color
insert into Polaris.polaris.OrganizationsPPPP values (1, 2951, 'Yes') -- Enable title bar colors
insert into Polaris.polaris.OrganizationsPPPP values (1, 2952, case @env when 'd' then @activeTitlebarColorDev when 't' then @activeTitlebarColorTrain end) -- Set active title bar color
insert into Polaris.polaris.OrganizationsPPPP values (1, 2953, case @env when 'd' then @inactiveTitlebarColorDev when 't' then @inactiveTitlebarColorTrain end) -- Set inactive title bar color


--rollback
--commit