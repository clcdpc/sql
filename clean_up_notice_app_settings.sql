begin tran

delete sv
from CLC_Notices.dbo.SettingValues sv
join CLC_Notices.dbo.SettingTypes st
	on st.Mnemonic = sv.Mnemonic
where st.SettingID in (1,2,4,5,6,7,23,24,28,29,30,31,32,33,34,35,115,119)

delete from CLC_Notices.dbo.SettingTypes where SettingID in (1,2,4,5,6,7,23,24,28,29,30,31,32,33,34,35,115,119)

--rollback
--commit