declare @categories table ( description varchar(255), callno varchar(255), rsprefix varchar(10) )


insert into @categories values
	('Animals', 'ANIMALS j', 'JNF'),
	('Arts and Crafts', 'ARTS j', 'JNF'),
	('Biographies', 'BIO j', 'JNF'),
	('Food and Cooking', 'FOOD j', 'JNF'),
	('Fun and Games', 'FUN j', 'JNF'),
	('Government and Law', 'GOV j', 'JNF'),
	('Health and Wellness', 'HEALTH j', 'JNF'),
	('History', 'HISTORY j', 'JNF'),
	('Holidays and Traditions', 'HOL j', 'JNF'),
	('Language and Writing', 'LANG j', 'JNF'),
	('Literature', 'LIT j', 'JNF'),
	('Math', 'MATH j', 'JNF'),
	('Military', 'MILITARY j', 'JNF'),
	('Mysterious and Unexplained', 'MYST j', 'JNF'),
	('Mythology', 'MYTH j', 'JNF'),
	('People and Places', 'PLACE j', 'JNF'),
	('Religion', 'REL j', 'JNF'),
	('Science and Nature', 'SCIENCE j', 'JNF'),
	('Social Topics', 'SOCIAL j', 'JNF'),
	('Space', 'SPACE j', 'JNF'),
	('Sports', 'SPORTS j', 'JNF'),
	('Technology and Computers', 'TECH j', 'JNF'),
	('Transportation', 'TRANS j', 'JNF'),
	('First Chapter ', 'jF FIRST CHAPTER', 'JF'),
	('Adventure ', 'jF ADV', 'JF'),
	('Animals ', 'jF ANIMALS', 'JF'),
	('Choose Your Story ', 'jF CHOOSE', 'JF'),
	('Fantasy ', 'jF FANTASY', 'JF'),
	('Fiction ', 'jF FIC', 'JF'),
	('Funny ', 'jF FUNNY', 'JF'),
	('Historical Fiction ', 'jF HIST', 'JF'),
	('Mystery ', 'jF MYSTERY', 'JF'),
	('Royal Reads ', 'jF ROYAL', 'JF'),
	('Scary ', 'jF SCARY', 'JF'),
	('Science Fiction ', 'jF SciFi', 'JF'),
	('Sports ', 'jF SPORTS', 'JF'),
	('Superhero ', 'jF SUPER', 'JF'),
	('Animals', 'ANIMALS E', 'PB'),
	('Author', 'AUTHOR E', 'PB'),
	('Bedtime', 'BEDTIME E', 'PB'),
	('Community Helper', 'COMMUNITY HELPER E', 'PB'),
	('Concepts', 'CONCEPTS E', 'PB'),
	('Dinosaurs', 'DINOSAURS E', 'PB'),
	('Disney', 'DISNEY E', 'PB'),
	('Fairy Tales/Folk Tales', 'FAIRY TALES/ FOLKTALES E', 'PB'),
	('Favorite Character', 'FAVORIT CHARACTER E', 'PB'),
	('Food', 'FOOD E', 'PB'),
	('Growing Up', 'GROWING UP E', 'PB'),
	('Imagination', 'IMAGINATION E', 'PB'),
	('Monster', 'MONSTERS E', 'PB'),
	('Nature', 'NATURE E', 'PB'),
	('Potty Training', 'POTTY TRAINING E', 'PB'),
	('Royal Reads', 'ROYAL READS E', 'PB'),
	('School', 'SCHOOL E', 'PB'),
	('Seek and Find', 'SEEK & FIND E', 'PB'),
	('Superheroes', 'SUPERHEROES E', 'PB'),
	('Things That Go', 'THINGS THAT GO E', 'PB')

/*

select cir.ItemRecordID, ird.CallNumber, c.*
from Polaris.polaris.CircItemRecords cir
join Polaris.Polaris.ItemRecordDetails ird
	on ird.ItemRecordID = cir.ItemRecordID
join @nonFictionCategories c
	on patindex(c.callno + '%', ird.CallNumber) = 1
where cir.AssignedBranchID = 24

select cir.ItemRecordID, ird.CallNumber, c.*
from Polaris.polaris.CircItemRecords cir
join Polaris.Polaris.ItemRecordDetails ird
	on ird.ItemRecordID = cir.ItemRecordID
join @fictionCategories c
	on patindex(c.callno + '%', ird.CallNumber) = 1
where cir.AssignedBranchID = 24

select cir.ItemRecordID, ird.CallNumber, c.*
from Polaris.polaris.CircItemRecords cir
join Polaris.Polaris.ItemRecordDetails ird
	on ird.ItemRecordID = cir.ItemRecordID
join @pictureBookCategories c
	on patindex(c.callno + '%', ird.CallNumber) = 1
where cir.AssignedBranchID = 24

*/

declare @searchTemplate varchar(max) = '
select distinct cir.AssociatedBibRecordID
from Polaris.polaris.CircItemRecords cir
join Polaris.Polaris.ItemRecordDetails ird
	on ird.ItemRecordID = cir.ItemRecordID
join polaris.polaris.Organizations o
	on o.OrganizationID = cir.AssignedBranchID
where o.ParenoanizationID = 23
	and ird.CallNumber like ''QQQQ%''
'

begin tran

declare @description varchar(255)
declare @callno varchar(255)
declare @rsprefix varchar(10)

declare @sql nvarchar(max)

declare categoryCursor cursor for
select description, callno, rsprefix from @categories

open categoryCursor
fetch next from categoryCursor into @description, @callno, @rsprefix
while @@FETCH_STATUS = 0
begin
	
	set @sql = replace(@searchTemplate, 'QQQQ', @callno)

	insert into Polaris.polaris.RecordSets (Name, ObjectTypeID, CreatorID, OrganizationOwnerID, CreationDate, Note)
	select	'PPL - ' + @rsprefix + ' ' + @description + ' - ' + @callno -- Name
			,2			-- Type
			,425		-- Creator
			,1	-- Owner
			,getdate()	-- Creation Date
			,'PPL - ' + @rsprefix + ' ' + @description + ' - ' + @callno -- Note
	
	insert into Polaris.dbo.CLC_Custom_Record_Set_SQL_Search_Mapping values (@@identity, 0, @sql, 'b', 'PPL - ' + @rsprefix + ' ' + @description + ' - ' + @callno)	
	fetch next from categoryCursor into @description, @callno, @rsprefix
end

close categoryCursor; deallocate categoryCursor


--rollback
--commit


select * from Polaris.Polaris.RecordSets where CreatorID = 425 order by CreationDate desc
select * from Polaris.dbo.CLC_Custom_Record_Set_SQL_Search_Mapping


select m.RecordSetID from Polaris.dbo.CLC_Custom_Record_Set_SQL_Search_Mapping m where m.Note like 'ppl -%'

select * from Polaris.polaris.ObjectTypes


begin tran

update rs
set rs.ObjectTypeID = 2
from polaris.polaris.RecordSets rs
join polaris.dbo.CLC_Custom_Record_Set_SQL_Search_Mapping m
	on m.RecordSetID = rs.RecordSetID
where m.Note like 'ppl - %'

--rollback
--commit


/*

select '"' + replace(replace(replace(split.Item, ' jnf ', ''), ' jf ', ''), ' pb ', '') + '" = ' + cast(m.RecordSetID as varchar) [category], case split2.Item when 'jnf' then 'Juvenile Non-Fiction' when 'jf' then 'Juvenile Fiction' when 'pb' then 'Picture Books' end [type]
from Polaris.dbo.CLC_Custom_Record_Set_SQL_Search_Mapping m
cross apply Polaris.dbo.CLC_Custom_SplitString(m.Note, '-') split
cross apply Polaris.dbo.CLC_Custom_SplitString(m.Note, ' ') split2
where m.Note like 'ppl%'
	and split.ItemIndex = 1
	and split2.ItemIndex = 2

*/