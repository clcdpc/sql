use [master]
if not exists (select * from sys.server_principals p where p.name = 'CML\ReadWriteAccessToClcPatronDatabase') 
begin
	CREATE LOGIN [CML\ReadWriteAccessToClcPatronDatabase] FROM WINDOWS WITH DEFAULT_DATABASE=[Polaris]
end

if not exists (select * from sys.server_principals p where p.name = 'CML\AllowAccessToClcPatronDatabase') 
begin
	CREATE LOGIN [CML\AllowAccessToClcPatronDatabase] FROM WINDOWS WITH DEFAULT_DATABASE=[Polaris]
end

/*************************
Polaris
**************************/

USE [Polaris]

if not exists (select * from polaris.sys.database_principals p where p.name = 'CLC_Custom_Manage_Stored_Procedures') 
begin
	create role [CLC_Custom_Manage_Stored_Procedures]
	grant view definition on schema::dbo TO [CLC_Custom_Manage_Stored_Procedures]
	grant view definition on schema::polaris TO [CLC_Custom_Manage_Stored_Procedures]
	grant create procedure to [CLC_Custom_Manage_Stored_Procedures]
	grant execute on schema::dbo to [CLC_Custom_Manage_Stored_Procedures]
	grant alter on schema::dbo to [CLC_Custom_Manage_Stored_Procedures]
	grant execute on schema::polaris to [CLC_Custom_Manage_Stored_Procedures]
end
--CREATE LOGIN [CML\ReadWriteAccessToClcPatronDatabase] FROM WINDOWS WITH DEFAULT_DATABASE=[master], DEFAULT_LANGUAGE=[us_english]

if not exists (select * from polaris.sys.database_principals p where p.name = 'CML\AllowAccessToClcPatronDatabase') 
begin
	CREATE LOGIN [CML\AllowAccessToClcPatronDatabase] FROM WINDOWS WITH DEFAULT_DATABASE=[Polaris]
end

if not exists (select * from polaris.sys.database_principals p where p.name = 'CML\ReadWriteAccessToClcPatronDatabase') 
begin
	CREATE USER [CML\ReadWriteAccessToClcPatronDatabase] FOR LOGIN [CML\ReadWriteAccessToClcPatronDatabase]
end

if not exists (select * from polaris.sys.database_principals p where p.name = 'CML\AllowAccessToClcPatronDatabase') 
begin
	CREATE USER [CML\AllowAccessToClcPatronDatabase] FOR LOGIN [CML\AllowAccessToClcPatronDatabase]
end

-- add readonly group
ALTER ROLE [db_datareader] ADD MEMBER [CML\AllowAccessToClcPatronDatabase]
-- add read/write group
ALTER ROLE [CLC_Custom_Manage_Stored_Procedures] ADD MEMBER [CML\ReadWriteAccessToClcPatronDatabase]
ALTER ROLE [db_datareader] ADD MEMBER [CML\ReadWriteAccessToClcPatronDatabase]
ALTER ROLE [db_datawriter] ADD MEMBER [CML\ReadWriteAccessToClcPatronDatabase]

/*************************
PolarisTransactions
**************************/

USE [PolarisTransactions]
if not exists (select * from PolarisTransactions.sys.database_principals p where p.name = 'CML\ReadWriteAccessToClcPatronDatabase') 
begin
	CREATE USER [CML\ReadWriteAccessToClcPatronDatabase] FOR LOGIN [CML\ReadWriteAccessToClcPatronDatabase]
end

if not exists (select * from PolarisTransactions.sys.database_principals p where p.name = 'CML\AllowAccessToClcPatronDatabase') 
begin
	CREATE USER [CML\AllowAccessToClcPatronDatabase] FOR LOGIN [CML\AllowAccessToClcPatronDatabase]
end

ALTER ROLE [db_datareader] ADD MEMBER [CML\AllowAccessToClcPatronDatabase]
ALTER ROLE [db_datareader] ADD MEMBER [CML\ReadWriteAccessToClcPatronDatabase]



--select * from polaris.sys.database_principals order by name