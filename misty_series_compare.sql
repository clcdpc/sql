/*

If a bib has a 490 field, we'd like a few pieces of information:

    Does the bib have any 800 or 830 fields? If so, output what is in those fields.
    How many holds does that title have
    How many other bibs have the same 490/800/830


What is tricky here is that we would like the 490/800/830 to be standardized. Meaning, some might say "The Boxcar Children", some might say "Boxcar Children", 
we want the initial articles to be stripped out as part of the comparisons for the number of bibs and holds for the series. But... we want to know if you had to strip out an initial article to make them match.
*/


create table #seriesData ( BibliographicRecordID int, hasia490 bit, val490 nvarchar(4000), hasia800 bit, val800 nvarchar(4000), hasia830 bit, val830 nvarchar(4000) )

insert into #seriesData
select	 bt490.BibliographicRecordID
		--,bs490.Data [val490]
		--,bs800.Data [val800]
		--,bs830.Data [val830]
		,case when ia490.InitialArticleWord is not null then 1 else 0 end [hasia490]
		,case when ia490.InitialArticleWord is not null then substring(trim(bs490.Data), len(ia490.InitialArticleWord) + 2, len(bs490.Data) - len(ia490.InitialArticleWord) - 1) else trim(bs490.Data) end [noia490]
		,case when ia800.InitialArticleWord is not null then 1 else 0 end [hasia800]
		,case when ia800.InitialArticleWord is not null then substring(trim(bs800.Data), len(ia800.InitialArticleWord) + 2, len(bs800.Data) - len(ia800.InitialArticleWord) - 1) else trim(bs800.Data) end [noia800]
		,case when ia830.InitialArticleWord is not null then 1 else 0 end [hasia830]
		,case when ia830.InitialArticleWord is not null then substring(trim(bs830.Data), len(ia830.InitialArticleWord) + 2, len(bs830.Data) - len(ia830.InitialArticleWord) - 1) else trim(bs830.Data) end [noia830]
from polaris.polaris.BibliographicTags bt490
join polaris.polaris.BibliographicSubfields bs490
	on bs490.BibliographicTagID = bt490.BibliographicTagID and bs490.Subfield = 'a'
left join polaris.polaris.BibliographicTags bt800
	on bt800.BibliographicRecordID = bt490.BibliographicRecordID and bt800.TagNumber = 800
left join polaris.polaris.BibliographicSubfields bs800
	on bs800.BibliographicTagID = bt800.BibliographicTagID and bs800.Subfield = 't'
left join polaris.polaris.BibliographicTags bt830
	on bt830.BibliographicRecordID = bt490.BibliographicRecordID and bt830.TagNumber = 830
left join polaris.polaris.BibliographicSubfields bs830
	on bs830.BibliographicTagID = bt830.BibliographicTagID and bs830.Subfield = 'a'
left join polaris.polaris.InitialArticle ia490
	on trim(bs490.Data) like ia490.InitialArticleWord + ' %'
left join polaris.polaris.InitialArticle ia800
	on trim(bs800.Data) like ia800.InitialArticleWord + ' %'
left join polaris.polaris.InitialArticle ia830
	on trim(bs830.Data) like ia830.InitialArticleWord + ' %'
where bt490.TagNumber = 490	


/*
select bt490.BibliographicRecordID
		,bs490.Data [val490]
		,bs800.Data [val800]
		,bs830.Data [val830]
from polaris.polaris.BibliographicTags bt490
join polaris.polaris.BibliographicSubfields bs490
	on bs490.BibliographicTagID = bt490.BibliographicTagID and bs490.Subfield = 'a'
left join polaris.polaris.BibliographicTags bt800
	on bt800.BibliographicRecordID = bt490.BibliographicRecordID and bt800.TagNumber = 800
left join polaris.polaris.BibliographicSubfields bs800
	on bs800.BibliographicTagID = bt800.BibliographicTagID and bs800.Subfield = 't'
left join polaris.polaris.BibliographicTags bt830
	on bt830.BibliographicRecordID = bt490.BibliographicRecordID and bt830.TagNumber = 830
left join polaris.polaris.BibliographicSubfields bs830
	on bs830.BibliographicTagID = bt830.BibliographicTagID and bs830.Subfield = 'a'
where bt490.TagNumber = 490	
--order by bt490.BibliographicRecordID
*/








select d.*
		,other490.OtherBibsWith490
		,other800.OtherBibsWith800
		,other830.OtherBibsWith830
from #seriesData d
left join (
	select d2.val490
			,count(distinct d2.BibliographicRecordID) - 1 [OtherBibsWith490]
	from #seriesData d2
	group by d2.val490
) other490
	on other490.val490 = d.val490
left join (
	select d2.val800
			,count(distinct d2.BibliographicRecordID) - 1 [OtherBibsWith800]
	from #seriesData d2
	group by d2.val800
) other800
	on other800.val800 = d.val800
left join (
	select d2.val830
			,count(distinct d2.BibliographicRecordID) - 1 [OtherBibsWith830]
	from #seriesData d2
	group by d2.val830
) other830
	on other830.val830 = d.val830
join polaris.polaris.BibRecordSets brs
	on brs.BibliographicRecordID = d.BibliographicRecordID and brs.RecordSetID = @rsid
order by d.hasia490, d.val490, d.BibliographicRecordID


select * from #seriesData sd where sd.val800 = '"Nameless Detective" mysteries ;'

select d.val800
		,count(distinct d.BibliographicRecordID) - 1 [OtherBibsWith800]
from #seriesData d
where d.val800 = '"Nameless Detective" mysteries ;'
group by d.val800


--drop table #seriesData
	


select	 bt490.BibliographicRecordID
		--,bs490.Data [val490]
		--,bs800.Data [val800]
		--,bs830.Data [val830]
		,case when ia490.InitialArticleWord is not null then 1 else 0 end [hasia490]
		,case when ia490.InitialArticleWord is not null then substring(trim(bs490.Data), len(ia490.InitialArticleWord) + 2, len(bs490.Data) - len(ia490.InitialArticleWord) - 1) else bs490.Data end [noia490]
		,case when ia800.InitialArticleWord is not null then 1 else 0 end [hasia800]
		,case when ia800.InitialArticleWord is not null then substring(trim(bs800.Data), len(ia800.InitialArticleWord) + 2, len(bs800.Data) - len(ia800.InitialArticleWord) - 1) else bs800.Data end [noia800]
		,case when ia830.InitialArticleWord is not null then 1 else 0 end [hasia830]
		,case when ia830.InitialArticleWord is not null then substring(trim(bs830.Data), len(ia830.InitialArticleWord) + 2, len(bs830.Data) - len(ia830.InitialArticleWord) - 1) else bs830.Data end [noia830]
from polaris.polaris.BibliographicTags bt490
join polaris.polaris.BibliographicSubfields bs490
	on bs490.BibliographicTagID = bt490.BibliographicTagID and bs490.Subfield = 'a'
left join polaris.polaris.BibliographicTags bt800
	on bt800.BibliographicRecordID = bt490.BibliographicRecordID and bt800.TagNumber = 800
left join polaris.polaris.BibliographicSubfields bs800
	on bs800.BibliographicTagID = bt800.BibliographicTagID and bs800.Subfield = 't'
left join polaris.polaris.BibliographicTags bt830
	on bt830.BibliographicRecordID = bt490.BibliographicRecordID and bt830.TagNumber = 830
left join polaris.polaris.BibliographicSubfields bs830
	on bs830.BibliographicTagID = bt830.BibliographicTagID and bs830.Subfield = 'a'
left join polaris.polaris.InitialArticle ia490
	on trim(bs490.Data) like ia490.InitialArticleWord + ' %'
left join polaris.polaris.InitialArticle ia800
	on trim(bs800.Data) like ia800.InitialArticleWord + ' %'
left join polaris.polaris.InitialArticle ia830
	on trim(bs830.Data) like ia830.InitialArticleWord + ' %'
where bt490.TagNumber = 490	
	and bt490.BibliographicRecordID = 1941654
