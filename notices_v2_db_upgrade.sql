USE [CLC_Notices]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetPatronsForSmsNumber]
	@pn varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select
		pr.patronid
		,p.OrganizationID [PatronBranchID]
		,o.ParentOrganizationID [PatronLibraryID]
		,p.Barcode
		,pr.PhoneVoice1
		,pr.Phone1CarrierID
		,pr.PhoneVoice2
		,pr.Phone2CarrierID
		,pr.PhoneVoice3
		,pr.Phone3CarrierID
		,pr.EmailAddress
		,pr.DeliveryOptionID
		,pr.TxtPhoneNumber
	from Polaris.Polaris.PatronRegistration pr
	join Polaris.polaris.Patrons p
		on pr.PatronID = p.PatronID
	join polaris.polaris.Organizations o
		on o.OrganizationID = p.OrganizationID
	where (pr.DeliveryOptionID = 8 or pr.EnableSMS = 1) and
	dbo.RemoveNonNumeric(case
		when pr.TxtPhoneNumber = 1 then pr.PhoneVoice1
		when pr.TxtPhoneNumber = 2 then pr.phonevoice2
		when pr.TxtPhoneNumber = 3 then pr.PhoneVoice3
	end) = @pn
END

GO


CREATE PROCEDURE [dbo].[RemovePatronSmsSettings]
	@patronid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    declare @currentCarrier int = ( select case when TxtPhoneNumber = 1 then Phone1CarrierID when TxtPhoneNumber = 2 then Phone2CarrierID when TxtPhoneNumber = 3 then Phone3CarrierID end from polaris.polaris.PatronRegistration where PatronID = @patronid )

	update Polaris.polaris.PatronRegistration
	set PhoneVoice1 = case when TxtPhoneNumber = 1 then '' else PhoneVoice1 end
		,Phone1CarrierID = case when TxtPhoneNumber = 1 then null else Phone1CarrierID end

		,PhoneVoice2 = case when TxtPhoneNumber = 2 then '' else PhoneVoice2 end
		,Phone2CarrierID = case when TxtPhoneNumber = 2 then null else Phone2CarrierID end

		,PhoneVoice3 = case when TxtPhoneNumber = 3 then '' else PhoneVoice3 end
		,Phone3CarrierID = case when TxtPhoneNumber = 3 then null else Phone3CarrierID end

		,TxtPhoneNumber = null
		,EnableSMS = 0
		,DeliveryOptionID = case when EmailAddress is not null and len(EmailAddress) > 0 then 2 else 1 end
	where PatronID = @patronid

	select @currentCarrier
END
GO


DROP VIEW [dbo].[PolarisNotifications]
GO


CREATE VIEW [dbo].[PolarisNotifications]
AS

with hoursofoperation(orgid, [hours]) as 
(
	select o.OrganizationID, isnull(opb.Value, opl.Value) [HoursOfOperation]
	from Polaris.Polaris.Organizations o
	left join Polaris.polaris.OrganizationsPPPP opb
		on opb.OrganizationID = o.OrganizationID and opb.AttrID = 96
	left join Polaris.polaris.OrganizationsPPPP opl
		on o.ParentOrganizationID = opl.OrganizationID and opl.AttrID = 96
	where o.OrganizationCodeID = 3
),
notices as (
	select nq.PatronID, nq.NotificationTypeID, nq.ItemRecordID, nq.DeliveryOptionID, nq.ReportingOrgID
	from results.Polaris.NotificationQueue nq
	union all
	select cr.PatronID, cr.NotificationTypeID, cr.ItemRecordID, cr.DeliveryOptionID, cr.ReportingOrgID
	from Results.polaris.CircReminders cr
)

select
	n.NotificationTypeID
	,p.PatronID
	,p.Barcode [PatronBarcode]
	,pr.NameFirst
	,pr.NameLast
	,a.StreetOne [PatronStreetOne]
	,a.StreetTwo [PatronStreetTwo]
	,pc.City [PatronCity]
	,pc.State [PatronState]
	,pc.PostalCode [PatronZip]
	,isnull(n.ItemRecordID, 0) [ItemRecordID]
	,p.OrganizationID [PatronBranch]
	,o_patron.Abbreviation [PatronBranchAbbr]
	,isnull(o_patron.ParentOrganizationID, 0) [PatronLibrary]
	,o_patron2.Abbreviation [PatronLibraryAbbr]
	,n.DeliveryOptionID
	,pr.EmailAddress
	,pr.AltEmailAddress
	,pr.EmailFormatID
	,case n.DeliveryOptionID
		when 3 then pr.PhoneVoice1
		when 4 then pr.PhoneVoice2
		when 5 then pr.PhoneVoice3
		when 8 then
			case pr.TxtPhoneNumber
				when 1 then pr.PhoneVoice1
				when 2 then pr.PhoneVoice2
				when 3 then pr.PhoneVoice3
				else ''
			end
		else ''
	end [DeliveryPhone]
	,isnull(n.ReportingOrgID,0) [ReportingBranchID]
	,o_notice.Abbreviation [ReportingBranchAbbr]	
	,o_notice.Name [ReportingBranchName]	
	,saa_notice.StreetOne [ReportingBranchStreetOne]
	,saa_notice.StreetTwo [ReportingBranchStreetTwo]
	,saa_notice.City [ReportingBranchCity]
	,saa_notice.State [ReportingBranchState]
	,saa_notice.PostalCode [ReportingBranchZip]
	,isnull(o_notice.ParentOrganizationID,0) [ReportingLibraryID]
	,coalesce(shr.HoldTillDate, ir.HoldTillDate) [HoldTillDate]
	,ic.DueDate [DueDate]
	,br.BrowseTitle [Title]
	,hoo.hours
	,isnull(l.AdminLanguageID, 1033) [PatronLanguage]
	,case pr.TxtPhoneNumber
		when 1 then pr.PhoneVoice1
		when 2 then pr.PhoneVoice2
		when 3 then pr.PhoneVoice3
		else ''
	end [TxtPhoneNumber]
from notices n
join Polaris.Polaris.Patrons p
	on n.PatronID = p.PatronID
join Polaris.Polaris.PatronRegistration pr
	on p.PatronID = pr.PatronID
join Polaris.Polaris.Organizations o_patron
	on p.OrganizationID = o_patron.OrganizationID
join Polaris.Polaris.Organizations o_patron2
	on o_patron.ParentOrganizationID = o_patron2.OrganizationID
join Polaris.Polaris.Organizations o_notice
	on n.ReportingOrgID = o_notice.OrganizationID
join polaris.polaris.OrganizationsPPPP op_notice_addr
	on op_notice_addr.OrganizationID = n.ReportingOrgID and op_notice_addr.AttrID = 55
join polaris.Polaris.SA_Addresses saa_notice
	on saa_notice.SA_AddressID = op_notice_addr.Value
left join polaris.polaris.CircItemRecords cir
	on cir.ItemRecordID = n.ItemRecordID
left join Polaris.Polaris.SysHoldRequests shr
	on shr.PatronID = p.PatronID and n.ItemRecordID = shr.TrappingItemRecordID
left join Polaris.polaris.ILLRequests ir
	on ir.PatronID = n.PatronID and ir.ItemRecordID = n.ItemRecordID
left join Polaris.polaris.BibliographicRecords br
	on br.BibliographicRecordID = cir.AssociatedBibRecordID
left join hoursofoperation hoo
	on hoo.orgid = n.ReportingOrgID
left join polaris.polaris.PatronAddresses pa
	on pa.PatronID = p.PatronID and pa.AddressTypeID = 2
left join polaris.Polaris.Addresses a
	on a.AddressID = pa.AddressID
left join Polaris.polaris.PostalCodes pc
	on pc.PostalCodeID = a.PostalCodeID
left join Polaris.polaris.ItemCheckouts ic
	on ic.PatronID = n.PatronID and ic.ItemRecordID = n.ItemRecordID
left join polaris.Polaris.Languages l
	on l.LanguageID = pr.LanguageID
where n.DeliveryOptionID in (2, 3, 4, 5) 
and n.NotificationTypeID in (1, 2, 10, 11, 12, 13) 
and not exists (select 1 from polaris.Polaris.DatesClosed dc where (dc.OrganizationID = p.OrganizationID or dc.OrganizationID = o_patron.ParentOrganizationID) and cast(dc.DateClosed as date) = cast(getdate() as date))
and isnull(hoo.hours,'') not like '%' + left(datename(dw, getdate()), 3) + 'closed%'

GO


CREATE TABLE [dbo].[SettingTypes](
	[SettingID] [int] IDENTITY(1,1) NOT NULL,
	[Mnemonic] [varchar](255) NOT NULL,
	[Description] [varchar](max) NULL,
	[DefaultValue] [varchar](max) NOT NULL,
 CONSTRAINT [PK_SettingTypes] PRIMARY KEY CLUSTERED 
(
	[SettingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[SettingTypes] ON 
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (1, N'email_hold_template_html', N'Email notice hold template URL', N'/content/templates/default/hold.html')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (2, N'email_overdue_template_html', N'', N'/content/templates/default/overdue.html')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (4, N'email_bill_template_html', N' ', N'/content/templates/default/bill.html')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (5, N'email_almost_overdue_template_html', N' ', N'/content/templates/default/almostoverdue.html')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (6, N'email_second_hold_template_html', N' ', N'/content/templates/default/secondhold.html')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (7, N'emaiL_combined_template_html', N' ', N'/content/templates/default/combined.html')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (8, N'email_hold_item_row_format', N' ', N'<tr class="item-row">
	<td class="item-title">{item_title}</td>
	<td>{item_author}</td>
	<td>{item_material_type}</td>
	<td>{item_barcode}</td>
	<td>{held_date}</td>
</tr>')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (10, N'email_overdue_item_format', N' ', N'<tr class="item-row">
	<td class="item-title">{item_title}</td>
	<td>{item_author}</td>
	<td>{item_material_type}</td>
	<td>{item_barcode}</td>
	<td>{due_date}</td>
</tr>')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (11, N'email_bill_item_format', N' ', N'<tr class="item-row">
	<td class="item-title">{item_title}</td>
	<td>{item_author}</td>
	<td>{item_material_type}</td>
	<td>{item_barcode}</td>
	<td>{due_date}</td>
</tr>')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (12, N'email_reminder_item_format', N' ', N'<tr class="item-row">
	<td class="item-title">{item_title}</td>
	<td>{item_author}</td>
	<td>{item_material_type}</td>
	<td>{item_barcode}</td>
	<td>{due_date}</td>
</tr>')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (15, N'email_use_patron_branch', N'', N'true')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (16, N'email_expiration_subject', N'', N'Your library account will expire soon')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (17, N'email_bill_subject', N'', N'Your library account has been billed')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (18, N'email_hold_subject', N'', N'Held items available for pickup at the library')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (19, N'email_overdue_subject', N'', N'You have overdue library items')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (20, N'email_second_hold_subject', N'', N'Held items still available for pickup at your library')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (22, N'email_from_address', N'', N'clcdpc@clcohio.org')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (23, N'email_expiration_template_html', N'', N'/content/templates/default/expiration.html')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (24, N'email_expiration_template_plaintext', N'', N'/content/templates/default/expiration.txt')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (25, N'enable_email_notices', N'Enable sending of email notices', N'false')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (26, N'enable_expiration_reminders', N' ', N'false')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (27, N'expiration_reminder_send_hour', N'', N'8')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (28, N'email_hold_template_plaintext', N'', N'/content/templates/default/hold.txt')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (29, N'email_overdue_template_plaintext', N' ', N'/content/templates/default/overdue.txt')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (30, N'email_bill_template_plaintext', N' ', N'/content/templates/default/bill.txt')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (31, N'email_almost_overdue_template_plaintext', N'', N'/content/templates/default/almostoverdue.txt')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (32, N'email_second_hold_template_plaintext', N'', N'/content/templates/default/secondhold.txt')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (33, N'email_combined_template_plaintext', N'', N'/content/templates/default/combined.txt')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (34, N'email_hold_cancellation_template_html', N'', N'/content/templates/default/holdcancellation.html')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (35, N'email_hold_cancellation_template_plaintext', N'', N'/content/templates/default/holdcancellation.txt')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (36, N'enable_hold_cancellation_notices', N'', N'false')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (37, N'hold_cancellation_send_hour_1', N'', N'8')
GO
INSERT [dbo].[SettingTypes] ([SettingID], [Mnemonic], [Description], [DefaultValue]) VALUES (38, N'hold_cancellation_send_hour_2', N'', N'14')
GO
SET IDENTITY_INSERT [dbo].[SettingTypes] OFF
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_SettingTypes_Mnemonic]    Script Date: 6/13/2018 2:30:43 PM ******/
ALTER TABLE [dbo].[SettingTypes] ADD  CONSTRAINT [IX_SettingTypes_Mnemonic] UNIQUE NONCLUSTERED 
(
	[Mnemonic] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE TABLE [dbo].[SettingValues](
	[OrganizationID] [int] NOT NULL,
	[Mnemonic] [varchar](255) NOT NULL,
	[LanguageID] [int] NOT NULL,
	[Value] [varchar](max) NOT NULL,
 CONSTRAINT [PK_SettingValues] PRIMARY KEY CLUSTERED 
(
	[OrganizationID] ASC,
	[Mnemonic] ASC,
	[LanguageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[SettingValues] ADD  CONSTRAINT [DF_SettingValues_LanguageID]  DEFAULT ((1033)) FOR [LanguageID]
GO

ALTER TABLE [dbo].[SettingValues]  WITH CHECK ADD  CONSTRAINT [FK_SettingValues_SettingTypes] FOREIGN KEY([Mnemonic])
REFERENCES [dbo].[SettingTypes] ([Mnemonic])
GO

ALTER TABLE [dbo].[SettingValues] CHECK CONSTRAINT [FK_SettingValues_SettingTypes]
GO


CREATE TABLE [dbo].[EmailHistorySearchLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Username] [varchar](255) NOT NULL,
	[Query] [varchar](max) NOT NULL,
 CONSTRAINT [PK_EmailHistorySearchLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO




DROP PROCEDURE [dbo].[GetClosedDates]
GO
DROP TABLE [dbo].[SMS_Queue]
GO