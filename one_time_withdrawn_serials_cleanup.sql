declare @userid int = 425
declare @workstationid int = 2563

declare @items table (itemid int)

-- compare using start of day to make sure list doesn't change between runs
declare @compareDate date = DATEADD(day, DATEDIFF(day, 0, GETDATE()), 0)

insert into @items
select cir.ItemRecordID
from Polaris.Polaris.CircItemRecords cir
where cir.MaterialTypeID in (25,19) and cir.ItemStatusID = 11 and cir.ItemStatusDate < dateadd(month, -3, @compareDate)

set nocount on
declare @numdeleted INT = 0;
declare @currentitem INT = 0;
while ((select COUNT(*) from @items) > 0)
begin 
	set @currentitem = ( select MIN(itemid) from @items )
	exec Polaris.Polaris.Cat_DeleteItemRecordProcessing @currentitem,5,@userid,@workstationid,1,1,1,N'<ROOT></ROOT>'
	delete from @items where itemid = @currentitem

	set @numdeleted = @numdeleted + 1
	if (@numdeleted % 100 = 0) 
	begin
		declare @msg VARCHAR(50) = 'deleted ' + cast(@numdeleted as VARCHAR) + ' items'
		RAISERROR (@msg , 0, 1) WITH NOWAIT
	end
end
set nocount off