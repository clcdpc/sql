begin tran

declare @oldBranch int = 15
declare @newBranch int = 131


--select * from Polaris.Polaris.Organizations where ParentOrganizationID = 14

begin -- Copy Collections 
	delete oc from Polaris.Polaris.OrganizationsCollections oc where oc.OrganizationID = @newBranch and not exists ( select 1 from Polaris.polaris.OrganizationsCollections _oc where _oc.OrganizationID = @oldBranch and _oc.CollectionID = oc.CollectionID)

	insert into Polaris.polaris.OrganizationsCollections
	select @newBranch, oc.CollectionID
	from Polaris.polaris.OrganizationsCollections oc
	where oc.OrganizationID = @oldBranch
		and not exists ( select 1 from Polaris.polaris.OrganizationsCollections oc2 where oc2.OrganizationID = @newBranch and oc2.CollectionID = oc.CollectionID )
end
--commit
--rollback
-----

begin -- Copy Patron Loan Limits
	update pll_dest
	set pll_dest.MaxFine = pll_src.MaxFine
		,pll_dest.MinFine = pll_src.MinFine
		,pll_dest.TotalItems = pll_src.TotalItems
		,pll_dest.TotalOverDue = pll_src.TotalOverDue
		,pll_dest.TotalHolds = pll_src.TotalHolds
		,pll_dest.TotalILL = pll_src.TotalILL
		,pll_dest.TotalReserveItems = pll_src.TotalReserveItems
	from polaris.polaris.PatronLoanLimits pll_dest
	join polaris.polaris.PatronLoanLimits pll_src
		on pll_src.PatronCodeID = pll_dest.PatronCodeID
	where pll_dest.OrganizationID = @newBranch
		and pll_src.OrganizationID = @oldBranch

	--delete from Polaris.Polaris.PatronLoanLimits where OrganizationID = @newBranch
	--delete from Polaris.Polaris.MaterialLoanLimits where OrganizationID = @newBranch

	--select * into #temp_pll
	--from Polaris.Polaris.PatronLoanLimits pll
	--where pll.OrganizationID = @oldBranch

	--update #temp_pll set organizationid = @newBranch

	--insert into Polaris.Polaris.PatronLoanLimits select * from #temp_pll

	--drop table #temp_pll
end

-----

begin -- Copy Material Loan Limits
	update mll_dest
	set mll_dest.MaxItems = mll_src.MaxItems
		,mll_dest.MaxRequestItems = mll_src.MaxRequestItems 
	from polaris.polaris.MaterialLoanLimits mll_dest
	join Polaris.polaris.MaterialLoanLimits mll_src
		on mll_src.PatronCodeID = mll_dest.PatronCodeID and mll_src.MaterialTypeID = mll_dest.MaterialTypeID
	where mll_dest.OrganizationID = @newBranch
		and mll_src.OrganizationID = @oldBranch

	--delete from Polaris.Polaris.MaterialLoanLimits where OrganizationID = @newBranch

	--select * into #temp_mll
	--from Polaris.Polaris.MaterialLoanLimits mll
	--where mll.OrganizationID = @oldBranch

	--update #temp_mll set organizationid = @newBranch

	--insert into Polaris.Polaris.MaterialLoanLimits select * from #temp_mll

	--drop table #temp_mll
end

-----

begin -- Copy Loan Periods
	update lp2
	set lp2.TimeUnit = lp.TimeUnit, lp2.Units = lp.Units
	from Polaris.polaris.loanperiods lp 
	join polaris.Polaris.LoanPeriods lp2
		on lp.PatronCodeID = lp2.PatronCodeID and lp.LoanPeriodCodeID = lp2.LoanPeriodCodeID
	where lp.OrganizationID = @oldBranch and lp2.OrganizationID = @newBranch
end

-----

begin -- Copy Patron Blocks
	delete from polaris.polaris.PatronStopsByBranch
	where OrganizationID = @newBranch

	insert into Polaris.Polaris.PatronStopsByBranch
	select @newBranch, pssb.PatronStopID, pssb.SequenceID, pssb.DisplayInPAC, pssb.Selected
	from polaris.Polaris.PatronStopsByBranch pssb
	where pssb.OrganizationID = @oldBranch
end

-----

begin -- Copy Fines
	update f_dest
	set f_dest.Amount = f_src.Amount
		,f_dest.MaximumFine = f_src.MaximumFine
		,f_dest.GraceUnits = f_src.GraceUnits
	from polaris.polaris.Fines f_dest
	join polaris.polaris.Fines f_src
		on f_src.PatronCodeID = f_dest.PatronCodeID and f_src.FineCodeID = f_dest.FineCodeID
	where f_dest.OrganizationID = @newBranch
		and f_src.OrganizationID = @oldBranch

	--delete from Polaris.Polaris.Fines where OrganizationID = @newBranch

	--select * into #temp_f
	--from Polaris.Polaris.Fines f
	--where f.OrganizationID = @oldBranch

	--alter table #temp_f drop column FinesID

	--update #temp_f set organizationid = @newBranch

	--insert into Polaris.Polaris.Fines select * from #temp_f

	--drop table #temp_f
end

-----

begin -- Copy ShelfLocations
	delete from Polaris.Polaris.ShelfLocations where OrganizationID = @newBranch

	select * into #temp_sl
	from Polaris.Polaris.ShelfLocations sl
	where sl.OrganizationID = @oldBranch

	update #temp_sl set organizationid = @newBranch

	insert into Polaris.Polaris.ShelfLocations select * from #temp_sl

	drop table #temp_sl
end

-----

begin -- Copy Dates Closed
	delete from Polaris.Polaris.DatesClosed where OrganizationID = @newBranch

	select * into #temp_dc
	from Polaris.Polaris.DatesClosed dc
	where dc.OrganizationID = @oldBranch

	update #temp_dc set organizationid = @newBranch

	insert into Polaris.Polaris.DatesClosed select * from #temp_dc

	drop table #temp_dc
end

-----

begin -- Copy Search Databases
	delete from Polaris.Polaris.SearchDatabases where OrganizationID = @newBranch

	select * into #temp_sd
	from Polaris.Polaris.SearchDatabases sd
	where sd.OrganizationID = @oldBranch

	alter table #temp_sd drop column SearchDBID

	update #temp_sd set organizationid = @newBranch

	insert into Polaris.Polaris.SearchDatabases select * from #temp_sd

	drop table #temp_sd
end

-----

begin -- Copy Item Statistical Codes
	delete from Polaris.Polaris.StatisticalCodes where OrganizationID = @newBranch

	select * into #temp_sc
	from Polaris.Polaris.StatisticalCodes sc
	where sc.OrganizationID = @oldBranch

	update #temp_sc set organizationid = @newBranch

	insert into Polaris.Polaris.StatisticalCodes select * from #temp_sc

	drop table #temp_sc
end

-----

begin -- Copy Patron Statistical Class Codes
	delete from Polaris.Polaris.PatronStatClassCodes where OrganizationID = @newBranch

	insert into Polaris.polaris.PatronStatClassCodes (OrganizationID, Description)
	select @newBranch, Description
	from polaris.polaris.PatronStatClassCodes
	where OrganizationID = @oldBranch
end

-----

begin -- Copy Bib Overlay Settings
	-- delete any old settings
	delete from Polaris.Polaris.BibOverlayRetentionIndOne where RetentionID in ( select RetentionID from Polaris.Polaris.BibOverlayRetentionTags where OrganizationID = @newBranch )
	delete from Polaris.Polaris.BibOverlayRetentionIndTwo where RetentionID in ( select RetentionID from Polaris.Polaris.BibOverlayRetentionTags where OrganizationID = @newBranch )
	delete from Polaris.Polaris.BibOverlayRetentionTags where OrganizationID = @newBranch

	-- copy source data from old branch
	select * into #temp_bort from Polaris.polaris.BibOverlayRetentionTags where OrganizationID = @oldBranch

	-- update temp table with new org id
	update #temp_bort set OrganizationID = @newBranch

	-- table to hold data to map old retention ids to new ones
	declare @temp_mapping table ( id int, oldid int, tag int, org int, importprofile int, retain int )

	-- insert the retention settings for the new branch, outputting the new data plus the OLD retention id into temp mapping table
	merge into polaris.polaris.BibOverlayRetentionTags using #temp_bort as temp on 1 = 0
	when not matched then
		insert ( RetainTagNumber, OrganizationID, ImportProfileID, Retain )
		values ( temp.RetainTagNumber, temp.OrganizationID, temp.ImportProfileID, temp.Retain )
		output inserted.IdentityCol, temp.RetentionID, temp.RetainTagNumber, temp.OrganizationID, temp.ImportProfileID, temp.Retain
		into @temp_mapping;

	-- copy retention ind one source data
	select borio.* into #temp_borio
	from Polaris.Polaris.BibOverlayRetentionIndOne borio 
	where borio.RetentionID in ( select RetentionID from Polaris.Polaris.BibOverlayRetentionTags where OrganizationID = @oldBranch )

	-- insert retention ind one data using the preserved old retention id to map to the new retention id
	insert into Polaris.Polaris.BibOverlayRetentionIndOne
	select t.id, tb.RetainIndicator
	from #temp_borio tb
	join @temp_mapping t on
		tb.RetentionID = t.oldid

	-- copy retention ind two source data
	select borit.* into #temp_borit
	from Polaris.Polaris.BibOverlayRetentionIndTwo borit
	where borit.RetentionID in ( select RetentionID from Polaris.Polaris.BibOverlayRetentionTags where OrganizationID = @oldBranch )

	-- insert retention ind two data using the preserved old retention id to map to the new retention id
	insert into Polaris.Polaris.BibOverlayRetentionIndTwo
	select t.id, tb.RetainIndicator
	from #temp_borit tb
	join @temp_mapping t on
		tb.RetentionID = t.oldid

	drop table #temp_bort
	drop table #temp_borio
	drop table #temp_borit
end

-----

begin -- Copy Call Number Hierarchies
	delete from Polaris.Polaris.CallNumberHierarchies where OrganizationID = @newBranch

	select * into #temp_cnh
	from Polaris.Polaris.CallNumberHierarchies cnh
	where cnh.OrganizationID = @oldBranch

	update #temp_cnh set organizationid = @newBranch

	insert into Polaris.Polaris.CallNumberHierarchies select * from #temp_cnh

	drop table #temp_cnh
end

-----

begin -- Copy Sys Hold Routing Sequences
	delete from Polaris.Polaris.SysHoldRoutingSequences where RequesterBranchID = @newBranch

	select * into #temp_shrs
	from Polaris.Polaris.SysHoldRoutingSequences shrs
	where shrs.RequesterBranchID = @oldBranch

	update #temp_shrs set RequesterBranchID = @newBranch

	insert into Polaris.Polaris.SysHoldRoutingSequences select * from #temp_shrs

	drop table #temp_shrs
end

-----

begin -- Copy SA Floating Matrix
	delete from Polaris.Polaris.SA_FloatingMatrix where ItemHomeBranchID = @newBranch

	select * into #temp_safm
	from Polaris.Polaris.SA_FloatingMatrix safm
	where safm.ItemHomeBranchID = @oldBranch

	update #temp_safm set ItemHomeBranchID = @newBranch

	insert into Polaris.Polaris.SA_FloatingMatrix select * from #temp_safm

	drop table #temp_safm
end

-----

begin -- Copy Organizations Floating Collections
	delete from Polaris.Polaris.OrganizationsFloatingCollections where HomeBranchID = @newBranch

	select * into #temp_ofc
	from Polaris.Polaris.OrganizationsFloatingCollections ofc
	where ofc.HomeBranchID = @oldBranch

	update #temp_ofc set HomeBranchID = @newBranch

	insert into Polaris.Polaris.OrganizationsFloatingCollections select * from #temp_ofc

	drop table #temp_ofc
end

-----

begin -- Copy SA Floating Matrix Collection Limits
	delete from Polaris.Polaris.SA_FloatingMatrixCollectionLimits where ItemCheckInBranchID = @newBranch

	select * into #temp_safmcl
	from Polaris.Polaris.SA_FloatingMatrixCollectionLimits safmcl
	where safmcl.ItemCheckInBranchID = @oldBranch

	update #temp_safmcl set ItemCheckInBranchID = @newBranch

	insert into Polaris.Polaris.SA_FloatingMatrixCollectionLimits select * from #temp_safmcl

	drop table #temp_safmcl
end

-----

begin -- Copy SA Floating Matrix Material Limits
	delete from Polaris.Polaris.SA_FloatingMatrixMaterialLimits where ItemCheckInBranchID = @newBranch

	select * into #temp_safmml
	from Polaris.Polaris.SA_FloatingMatrixMaterialLimits safmml
	where safmml.ItemCheckInBranchID = @oldBranch

	update #temp_safmml set ItemCheckInBranchID = @newBranch

	insert into Polaris.Polaris.SA_FloatingMatrixMaterialLimits select * from #temp_safmml

	drop table #temp_safmml
end

-----

begin -- Copy SA Floating Matrix Material Types
	delete from Polaris.Polaris.SA_FloatingMatrixMaterialTypes where ItemHomeBranchID = @newBranch

	select * into #temp_safmmt
	from Polaris.Polaris.SA_FloatingMatrixMaterialTypes safmmt
	where safmmt.ItemHomeBranchID = @oldBranch

	update #temp_safmmt set ItemHomeBranchID = @newBranch

	insert into Polaris.Polaris.SA_FloatingMatrixMaterialTypes select * from #temp_safmmt

	drop table #temp_safmmt
end

-----

begin -- Copy SA Payment Methods
	delete from
	Polaris.Polaris.SA_PaymentMethods
	where OrganizationID = @newBranch

	insert into polaris.Polaris.SA_PaymentMethods
	select @newBranch, pm.PaymentMethodID, pm.DisplayOrder, pm.Selected
	from polaris.polaris.SA_PaymentMethods pm
	where pm.OrganizationID = @oldBranch
end

-----

begin -- Copy PAC Limit By
	delete from
	Polaris.Polaris.SA_PACLimitBy
	where OrganizationID = @newBranch

	insert into polaris.Polaris.SA_PACLimitBy
	select @newBranch, lb.LimitDescription, lb.CCLFilter, lb.Enabled, lb.DisplayOrder, lb.Protected
	from Polaris.Polaris.SA_PACLimitBy lb
	where lb.OrganizationID = @oldBranch
end

--commit
--rollback

