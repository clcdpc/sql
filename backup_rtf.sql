declare @table varchar(255) = 'CLC_Custom_SysHoldRoutingSequences_BACKUP_' + convert(varchar,getDate(),112);

declare @sql varchar(max) =
'use Polaris;
CREATE TABLE dbo.[' + @table + '](
[RequesterBranchID] [int] NOT NULL,
[ResponderBranchID] [int] NOT NULL,
[Sequence] [int] NOT NULL,
[MaxDaysInRTF] [int] NOT NULL,
[PrimarySecondaryFlag] [tinyint] NOT NULL);'
	
exec(@sql)
--copy the data from the production table into the backup table
set @sql = 'insert into polaris.dbo.' + @table + '
			select * from polaris.polaris.SysHoldRoutingSequences'
exec(@sql)
	
select * from polaris.polaris.SysHoldRoutingSequences
	
--verify the backup table has entries in it
set @sql = 'select * from polaris.dbo.' + @table
exec(@sql)