--select mlc.Language
--		,isnull(c.Name, 'None') [Collection]
--		,count(distinct cko.TxnId) [CKOs]
--from polaris.dbo.CLC_Custom_First_Time_CKOs cko
--join polaris.polaris.CircItemRecords cir
--	on cir.ItemRecordID = cko.ItemRecordId
--join polaris.polaris.BibliographicRecords br
--	on br.BibliographicRecordID = cir.AssociatedBibRecordID
--join polaris.polaris.MARCLanguageCodes mlc
--	on mlc.Code = br.MARCLanguage
--left join polaris.polaris.Collections c
--	on c.CollectionID = cir.AssignedCollectionID
--where mlc.MARCLanguageCodeID not in (1,2,451,510)
--	and cko.TxnDate between @beginDate and @endDate
--	and cko.TxnOrgId = @branchId
--group by mlc.Language, isnull(c.Name, 'None')
--order by mlc.Language, isnull(c.Name, 'None')

select mlc.Language
		,mlc.Code
		,count(distinct br.BibliographicRecordID) [bibs]
from polaris.polaris.MARCLanguageCodes mlc
join polaris.polaris.BibliographicRecords br
	on br.MARCLanguage = mlc.Code
where mlc.MARCLanguageCodeID not in (1,2,451,510)
	and br.RecordStatusID = 1
group by mlc.Language, mlc.code
--having count(*) > 10
order by mlc.Language


select mlc.Language
		,o2.Name [Library]
		,o.Name [Branch]
		,count(distinct cir.AssociatedBibRecordID) [NumTitles]
		,count(distinct cir.ItemRecordID) [NumItems]
from polaris.polaris.BibliographicRecords br
join polaris.polaris.CircItemRecords cir
	on cir.AssociatedBibRecordID = br.BibliographicRecordID
join polaris.polaris.organizations o
	on o.OrganizationID = cir.AssignedBranchID
join polaris.polaris.Organizations o2
	on o2.OrganizationID = o.ParentOrganizationID
join polaris.polaris.MARCLanguageCodes mlc
	on mlc.Code = br.MARCLanguage
where br.MARCLanguage in ( @languages )
group by o2.Name, o.Name, mlc.Language
order by mlc.Language