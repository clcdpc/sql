declare @newnames table ( rsid int, newname varchar(255) )

insert into @newnames
select rs.RecordSetID, rs.Name + ' ' + cast(rs.RecordSetID as varchar) [newname]
from polaris.polaris.recordsets rs
where RecordStatusID = 4
	and rs.Name not like '%' + cast(rs.RecordSetID as varchar(15))

begin tran

update rs
set rs.Name = nn.newname
from polaris.polaris.RecordSets rs
join @newnames nn
	on nn.rsid = rs.RecordSetID
where rs.RecordStatusID = 4
	and len(nn.newname) <= 70

commit