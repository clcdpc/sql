--Missing Transferred (hold request) items
--Shows items sent TO or FROM this location
--	i.      Order by collection, call number

--	ii.      Shows the patron who is on hold for the item

--Defaults to showing items that were sent more than 7 days ago
--	i.      These should be looked for by both the sending and receiving library

--Has a section that after 13 days, instructs the ITEM OWING library to CKI items, deny hold filling the hold, maintain customer request
--	i.      Then the owning library would these items missing


--select * from Polaris.polaris.ItemStatuses

declare @branch int = 7


select	cir.Barcode [ItemBarcode]
		,c.Name [Collection]
		,ird.CallNumber
		,br.BrowseTitle [Title]
		,coalesce(sl.Description, '') [ShelfLocation]
		,p.Barcode [PatronBarcode]
		,cir.ItemStatusDate
from polaris.Polaris.CircItemRecords cir
join polaris.polaris.BibliographicRecords br
	on br.BibliographicRecordID = cir.AssociatedBibRecordID
left join polaris.polaris.ShelfLocations sl
	on sl.ShelfLocationID = cir.ShelfLocationID and sl.OrganizationID = cir.AssignedBranchID
join Polaris.polaris.SysHoldRequests shr
	on shr.TrappingItemRecordID = cir.ItemRecordID
join polaris.Polaris.patrons p
	on p.PatronID = shr.PatronID
join Polaris.polaris.Collections c
	on c.CollectionID = cir.AssignedCollectionID
join Polaris.polaris.ItemRecordDetails ird
	on ird.ItemRecordID = cir.ItemRecordID
where cir.ItemStatusID = 5
	and datediff(day, cir.InTransitSentDate, getdate()) between 7 and 12
	and @branch in ( cir.InTransitSentBranchID, cir.InTransitRecvdBranchID )
order by c.Name, ird.CallNumber

select	cir.Barcode [ItemBarcode]
		,c.Name [Collection]
		,ird.CallNumber
		,br.BrowseTitle [Title]
		,coalesce(sl.Description, '') [ShelfLocation]
		,p.Barcode [PatronBarcode]
		,cir.ItemStatusDate
from polaris.Polaris.CircItemRecords cir
join polaris.polaris.BibliographicRecords br
	on br.BibliographicRecordID = cir.AssociatedBibRecordID
left join polaris.polaris.ShelfLocations sl
	on sl.ShelfLocationID = cir.ShelfLocationID and sl.OrganizationID = cir.AssignedBranchID
join Polaris.polaris.SysHoldRequests shr
	on shr.TrappingItemRecordID = cir.ItemRecordID
join polaris.Polaris.patrons p
	on p.PatronID = shr.PatronID
join Polaris.polaris.Collections c
	on c.CollectionID = cir.AssignedCollectionID
join Polaris.polaris.ItemRecordDetails ird
	on ird.ItemRecordID = cir.ItemRecordID
where cir.ItemStatusID = 5
	and datediff(day, cir.InTransitSentDate, getdate()) >= 13
	and @branch in ( cir.InTransitSentBranchID, cir.InTransitRecvdBranchID )
order by c.Name, ird.CallNumber