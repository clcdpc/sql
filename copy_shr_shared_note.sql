begin tran


insert into polaris.polaris.SHRSharedPublicNotes
select 31, pn.BibliographicRecordID, 1, pn.Value
from polaris.polaris.SHRSharedPublicNotes pn
where pn.OrganizationID in (29,30) and pn.Value = 'Current issue does not circulate.'
and not exists (select 1
				from polaris.polaris.SHRSharedPublicNotes pn2
				where pn.BibliographicRecordID = pn2.BibliographicRecordID and pn2.OrganizationID = 31 and pn2.Value = 'Current issue does not circulate.')

--rollback
--commit

select *
from polaris.polaris.SHRSharedPublicNotes pn
where pn.BibliographicRecordID = 1203099