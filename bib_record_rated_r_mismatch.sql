select bibid
from (
	select cir.AssociatedBibRecordID [bibid]
			,cast(count(case when cir.MaterialTypeID = 52 then 1 end)/cast((select count(cir2.ItemRecordID) from polaris.polaris.circitemrecords cir2 where cir2.AssociatedBibRecordID = cir.AssociatedBibRecordID and cir2.MaterialTypeID in (49,50,51,52,53)) as decimal(18,2)) as decimal(18,2)) [r_ratio]
	from polaris.polaris.CircItemRecords cir
	join polaris.polaris.BibliographicTags bt
		on bt.BibliographicRecordID = cir.AssociatedBibRecordID
	join polaris.polaris.BibliographicSubfields bs
		on bs.BibliographicTagID = bt.BibliographicTagID
	where cir.MaterialTypeID in (49,51,52) and bt.TagNumber = 521 and bs.Subfield = 'a' and bs.Data not like '%not rated%'
	group by cir.AssociatedBibRecordID
	having count(distinct cir.MaterialTypeID) > 1
) d
where d.r_ratio > 0.33