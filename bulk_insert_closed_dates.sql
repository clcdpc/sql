declare @library int = 6

-- =CONCAT("insert into @dates values ('", TEXT(A1, "m/d/yyyy"), "')")
declare @dates table ( closeddate date )

-- PASTE EXCEL DATA BELOW
insert into @dates values ('4/8/2024')
insert into @dates values ('4/9/2024')
insert into @dates values ('4/10/2024')
insert into @dates values ('4/11/2024')
insert into @dates values ('4/12/2024')
insert into @dates values ('4/13/2024')
insert into @dates values ('4/14/2024')
-- PASTE EXCEL DATA ABOVE

begin tran;insert into Polaris.Polaris.DatesClosed
select distinct o.OrganizationID
		,d.closeddate
from @dates d
cross join Polaris.polaris.Organizations o
where o.ParentOrganizationID = @library
	and not exists ( select 1 from Polaris.polaris.DatesClosed _dc where _dc.OrganizationID = o.OrganizationID and _dc.DateClosed = d.closeddate )

--rollback
--commit

--select * from polaris.polaris.organizations