declare @groupid int = 310

;with pg
as
(
	select cr.ControlRecordID, pd.PermissionID, pss.Description [Subsystem], crd.ControlRecordName, pn.PermissionName, cr.OrganizationID, crd.SystemFlag, crd.LibraryFlag, crd.BranchFlag
	from Polaris.Polaris.ControlRecords cr
	join Polaris.Polaris.ControlRecordDefs crd on
		cr.ControlRecordDefID = crd.ControlRecordDefID
	join Polaris.Polaris.PermissionDefs pd on
		pd.ControlRecordDefID = crd.ControlRecordDefID
	join Polaris.Polaris.PermissionNames pn on
		pd.PermissionNameID = pn.PermissionNameID
	join Polaris.Polaris.SA_PermissionSubsystems pss on
		pss.SubsystemID = crd.SubSystemID
	join Polaris.Polaris.PermissionGroups pg on
		pg.permissionid = pd.permissionid and pg.controlrecordid = cr.controlrecordid
	where pg.groupid = @groupid and crd.subsystemid = 2
)

--delete from polaris.polaris.permissiongroups where groupid = @groupid

select cr.ControlRecordID, pd.PermissionID, pss.Description [Subsystem], crd.ControlRecordName, pn.PermissionName, cr.OrganizationID, crd.SystemFlag, crd.LibraryFlag, crd.BranchFlag
from Polaris.Polaris.ControlRecords cr
join Polaris.Polaris.ControlRecordDefs crd on
	cr.ControlRecordDefID = crd.ControlRecordDefID
join Polaris.Polaris.PermissionDefs pd on
	pd.ControlRecordDefID = crd.ControlRecordDefID
join Polaris.Polaris.PermissionNames pn on
	pd.PermissionNameID = pn.PermissionNameID
join Polaris.Polaris.SA_PermissionSubsystems pss on
	pss.SubsystemID = crd.SubSystemID
where crd.ControlRecordName not like '%Patron record sets%' and pd.permissionid in ( select permissionid from pg )
order by crd.ControlRecordName, pn.PermissionName, cr.organizationid


--select * from Polaris.Polaris.groups








select cr.ControlRecordID, pd.PermissionID, pss.Description [Subsystem], crd.ControlRecordName, pn.PermissionName, cr.OrganizationID, crd.SystemFlag, crd.LibraryFlag, crd.BranchFlag
from Polaris.Polaris.ControlRecords cr
join Polaris.Polaris.ControlRecordDefs crd on
	cr.ControlRecordDefID = crd.ControlRecordDefID
join Polaris.Polaris.PermissionDefs pd on
	pd.ControlRecordDefID = crd.ControlRecordDefID
join Polaris.Polaris.PermissionNames pn on
	pd.PermissionNameID = pn.PermissionNameID
join Polaris.Polaris.SA_PermissionSubsystems pss on
	pss.SubsystemID = crd.SubSystemID
join Polaris.Polaris.PermissionGroups pg on
	pg.permissionid = pd.permissionid and pg.controlrecordid = cr.controlrecordid
where pg.groupid = 310 and crd.subsystemid = 2