declare @yearsToAdd int = 2

declare @datesToAdd table ( DateClosed date )

insert into @datesToAdd
select d.TheDate
from (
	select row_number() over (partition by TheDayName, TheMonth, TheYear order by TheDate) as WeekdayOrdinal,
    count(*) over (partition by TheDayName, TheMonth, TheYear ) as MaxOrdinal, *  
    from clcdb.dbo.DateDim d
	where d.TheYear between year(getdate()) and year(getdate()) + @yearsToAdd
		and d.TheDate > getdate()
) d
cross apply (select e7.EasterDay
	from (values (year(TheDate) % 19, year(TheDate) % 4, year(TheDate) % 7, year(TheDate) / 100)) as e1 (a, b, c, k)
	cross apply (values ((13 + 8 * k) / 25, k / 4)) as e2 (p, q)
	cross apply (values ((15 - p + k - q) % 30, (4 + k - q) % 7)) as e3 (M, N)
	cross apply (values ((19 * a + M) % 30)) as e4 (d)
	cross apply (values ((2 * b + 4 * c + 6 * d + N) % 7)) as e5 (e)
	cross apply (values (iif(e = 6 and (d = 29 or (d = 28 and a > 10)), 7, 0))) as e6 (o)
	cross apply (values (dateadd(day, d + e - o, datefromparts(year(TheDate), 3, 22)))) as e7 (EasterDay)
) as easter
where   (TheMonthName = 'January'   and TheDay  = 1)											  /* New Years Day */
    or ((TheMonthName = 'May'       and TheDayName = 'Monday')   and WeekdayOrdinal = MaxOrdinal) /* Memorial Day  */
    or  (TheMonthName = 'July'      and TheDay = 4)												  /* July 4th      */
    or ((TheMonthName = 'September' and TheDayName = 'Monday')   and WeekdayOrdinal = 1)		  /* Labor Day     */
    or ((TheMonthName = 'November'  and TheDayName = 'Thursday') and WeekdayOrdinal = 4)		  /* Thanksgiving  */
    or  (TheMonthName = 'December'  and TheDay  = 24 )											  /* Christmas Eve */
    or  (TheMonthName = 'December'  and TheDay  = 25 )											  /* Christmas     */
    or  (TheMonthName = 'December'  and TheDay  = 31 )											  /* New Years Eve */
	or  (TheDate = easter.EasterDay)															  /* Easter        */



begin tran

-- add future dates
insert into polaris.polaris.DatesClosed
select o.OrganizationID
		,dta.DateClosed
from @datesToAdd dta
cross join polaris.polaris.Organizations o
where o.OrganizationCodeID in (2,3)
	and not exists ( select 1 from polaris.polaris.DatesClosed dc where dc.OrganizationID = o.OrganizationID and dc.DateClosed = dta.DateClosed )

-- delete old dates
delete dc
from polaris.polaris.DatesClosed dc
where dc.DateClosed < DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) - 2, 0)

commit