

select * 
from polaris.polaris.ServersPPPP spppp
join polaris.polaris.AdminAttributes aa
	on aa.AttrID = spppp.AttrID
join polaris.polaris.Servers s
	on s.ServerID = spppp.ServerID


update DbSyncConfig.polaris.ServersPPPP
set value = 'traindb'
where value = 'proddb'

update DbSyncConfig.polaris.ServersPPPP
set value = replace(value, 'catalog.clcohio.org', 'trainpac.clcohio.org')
where value like '%catalog.clcohio.org%'

SELECT TOP (1000) [ServerID]
      ,[AttrID]
      ,[Value]
  FROM [DBSyncConfig].[Polaris].[ServersPPPP]


select spppp.*, aa.AttrDesc, s.ComputerName
from polaris.polaris.ServersPPPP spppp
join polaris.polaris.AdminAttributes aa
	on aa.AttrID = spppp.AttrID
join polaris.polaris.Servers s
	on s.ServerID = spppp.ServerID