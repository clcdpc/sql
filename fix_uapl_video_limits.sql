declare @map table ( pcid int, mtid int )
insert into @map values (25, 49)
insert into @map values (25, 29)

insert into @map values (26, 49)
insert into @map values (26, 3)
insert into @map values (26, 51)
insert into @map values (26, 29)
insert into @map values (26, 30)

insert into @map values (26, 3)
insert into @map values (26, 49)
insert into @map values (26, 29)


select mll.*
from polaris.polaris.MaterialLoanLimits mll
join polaris.polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
join @map m
	on m.pcid = mll.PatronCodeID and m.mtid = mll.MaterialTypeID
where o.ParentOrganizationID = 78