declare @items table ( id int )

declare @patron int = 1752752
declare @option int = 3

insert into @items
select top 2 cir.ItemRecordID
from Polaris.Polaris.CircItemRecords cir
join polaris.polaris.ItemRecordDetails ird
	on ird.ItemRecordID = cir.ItemRecordID
where cir.ItemStatusID = 1 and ird.CallNumberPrefix != 'overdrive'
order by NEWID()

insert into Results.Polaris.NotificationQueue
select 	i.id, 
		1			[NotificationTypeID], 
		@patron		[PatronID], 
		@option		[DeliveryOptionID], 
		0			[Processed], 
		null		[MinorPatronID], 
		7			[ReportingOrgID], 
		null		[Amount], 
		GETDATE()	[CreationDate], 
		0			[IsAdditionalTxt]
from @items i


insert into Polaris.Polaris.ItemCheckouts 
select	i.id		[ItemRecordID],
		@patron		[PatronID],
		1			[OrganizationID],
		1			[CreatorID],
		GETDATE()	[CheckOutDate],
		GETDATE()+1	[DueDate],
		0			[Renewals],
		0			[OVDNoticeCount],
		null		[RecallFlag],
		null		[RecallDate],
		1			[LoanUnits],
		null		[OVDNoticeDate],
		null		[BillingNoticeSent],
		GETDATE()	[OriginalCheckOutDate],
		GETDATE()	[OriginalDueDate],
		null
from @items i


select * from Results.Polaris.NotificationQueue where DeliveryOptionID = 3 and PatronID = 1752752

select * from Results.Polaris.NotificationHistory nh where nh.PatronId = 1752752 and nh.DeliveryOptionId = 3

select * from PolarisTransactions.Polaris.NotificationLog nl where nl.PatronID = 1752752 order by nl.NotificationDateTime desc

select * from Results.Polaris.CircReminders

select * from PolarisTransactions.polaris.NotificationLog where PatronID = 1423468 order by NotificationDateTime desc


select * from CLC_Notices.dbo.PolarisNotifications where PatronID = 1752752
