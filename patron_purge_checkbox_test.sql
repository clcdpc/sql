declare @library int = 6
declare @yearsInactive int = 3
declare @moneyOwed money = 10
declare @patronCodes int = 1

/*
Linked to items with a status of Out, Lost or Claimed
Linked to hold requests with a status of Held or Shipped
Linked to ILL requests with a status of Received
System assigned block
Collection agency block
Do not delete option is checked
An item is currently routed to the patron through Serials routing
*/

declare @checkAccountBalance int =	0
declare @includeLostItems int =		1
declare @includeOutItems int =		1
declare @includeClaimedItems int =	1
declare @includeHoldRequests int =	1
declare @includeIllRequests int =	1
declare @includeCollections int =	1
declare @includeDoNotDelete int =	1
declare @includeSerials int =	1
declare @includeClean int = 0


select *
from (
select p.PatronID
		,p.Barcode
		,pr.PatronFullName
		,p.ChargesAmount
		,p.LastActivityDate
		,case when p.ChargesAmount > @moneyOwed then 0 else 1 end [UnderFineCutoff]
		,case when exists ( select 1 from polaris.polaris.PatronLostItems pli where pli.PatronID = p.PatronID ) then 1 else 0 end [HasLostItems]
		,case when exists ( select 1 from polaris.polaris.ItemCheckouts ic where ic.PatronID = p.PatronID ) then 1 else 0 end [HasOutItems]
		,case when exists ( select 1 from polaris.Polaris.PatronClaims pc where pc.PatronID = p.PatronID ) then 1 else 0 end [HasClaims]
		,case when exists ( select 1 from polaris.polaris.SysHoldRequests shr where shr.PatronID = p.PatronID and shr.SysHoldStatusID in (5,6) ) then 1 else 0 end [HasHolds]
		,case when exists ( select 1 from polaris.polaris.ILLRequests ir where ir.PatronID = p.PatronID and ir.ILLStatusID in (10,11,12,13,14) ) then 1 else 0 end [HasILLs]
		,case when p.SystemBlocks % 1024 = 1024 then 1 else 0 end [InCollections]
		,isnull(pr.DeletionExempt,0) [IsDeletionExempt]
		--,@includeClaimedItems + @includeCollections + @includeDoNotDelete + @includeHoldRequests + @includeIllRequests + @includeLostItems + @includeOutItems [foo]
from Polaris.Polaris.Patrons p
join Polaris.polaris.PatronRegistration pr
	on pr.PatronID = p.PatronID
join polaris.polaris.Organizations o
	on o.OrganizationID = p.OrganizationID
where not exists ( select 1 from Polaris.polaris.ItemCheckouts ic where ic.PatronID = p.PatronID )
	and not exists ( select 1 from Polaris.polaris.SysHoldRequests shr where shr.PatronID = p.PatronID and shr.SysHoldStatusID in (5,6) )
	and not exists ( select 1 from polaris.Polaris.ILLRequests r where r.PatronID = p.PatronID and r.ILLStatusID in (10,11,12,13,14) )
	and isnull(pr.DeletionExempt,0) = 0
	and o.ParentOrganizationID = @library
	and p.LastActivityDate < dateadd(year, @yearsInactive * -1, getdate())
	--and p.ChargesAmount <= @moneyOwed
	and p.PatronCodeID in (@patronCodes)
) d
where (@checkAccountBalance = 0 or d.UnderFineCutoff = 1) and 
	((@includeLostItems = 1 and d.HasLostItems = 1)
	or @includeClaimedItems = 1 and d.HasClaims = 1)
	or (@includeOutItems = 1 and d.HasOutItems = 1)
	or (@includeClaimedItems = 1 and d.HasClaims = 1)
	or (@includeHoldRequests = 1 and d.HasHolds = 1)
	or (@includeIllRequests = 1 and d.HasILLs = 1)
	or (@includeCollections = 1 and d.InCollections = 1)
	or (@includeDoNotDelete = 1 and d.IsDeletionExempt = 1)
	or (@includeClean = 1 and d.HasLostItems + d.HasOutItems + d.HasClaims + d.HasLostItems + d.HasILLs + d.InCollections + d.IsDeletionExempt = 0))
/*
(@checkAccountBalance = 0 or d.UnderFineCutoff = 1)
	or (@includeLostItems = 1 or d.HasLostItems = 0)
	or (@includeOutItems = 1 or d.HasOutItems = 0)
	or (@includeClaimedItems = 1 or d.HasClaims = 0)
	or (@includeHoldRequests = 1 or d.HasHolds = 0)
	or (@includeIllRequests = 1 or d.HasILLs = 0)
	or (@includeCollections = 1 or d.InCollections = 0)
	or (@includeDoNotDelete = 1 or d.IsDeletionExempt = 0)
	
*/