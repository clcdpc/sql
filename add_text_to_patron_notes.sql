begin tran

declare @text varchar(max) = '---Only remove/change lines below if you are the registered library (DNR)---
'

delete from Polaris.Polaris.PatronNotes

insert into Polaris.Polaris.PatronNotes
select	tpn.PatronID,
		case when tpn.NonBlockingStatusNotes is null then null when LEN(tpn.NonBlockingStatusNotes) > 3900 then tpn.NonBlockingStatusNotes else @text + tpn.NonBlockingStatusNotes end, 
		case when tpn.BlockingStatusNotes is null then null when LEN(tpn.BlockingStatusNotes) > 3900 then tpn.BlockingStatusNotes else @text + tpn.BlockingStatusNotes end,
		tpn.NonBlockingStatusNoteDate,
		tpn.BlockingStatusNoteDate,
		tpn.NonBlockingBranchID,
		tpn.NonBlockingUserID,
		tpn.NonBlockingWorkstationID,
		tpn.BlockingBranchID,
		tpn.BlockingUserID,
		tpn.BlockingWorkstationID
from Polaris.dbo.CLC_Custom_Temp_PatronNotes tpn

select * from Polaris.Polaris.PatronNotes where patronid = 9227
select COUNT(*) from Polaris.Polaris.PatronNotes
select COUNT(*) from Polaris.dbo.CLC_Custom_Temp_PatronNotes

--commit
--rollback


--begin tran

--update Polaris.Polaris.PatronFreeTextBlocks
--set FreeTextBlock = 'DNR - ' + FreeTextBlock
--where LEN(FreeTextBlock) < 249

----rollback
----commit

--select top 1000 * from Polaris.Polaris.PatronFreeTextBlocks
