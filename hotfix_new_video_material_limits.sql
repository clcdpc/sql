declare @mapping table ( sourcemtid int, destmtid int )

insert into @mapping values (7,50)

begin tran

update mll
	set	mll.maxitems = case when mll.MaxItems = 0 then mll2.MaxItems else mll.MaxItems end
		,mll.maxrequestitems = case when mll.MaxRequestItems = 0 then mll2.MaxRequestItems else mll.MaxRequestItems end
from Polaris.Polaris.MaterialLoanLimits mll
join @mapping m
	on m.destmtid = mll.MaterialTypeID
join Polaris.polaris.MaterialLoanLimits mll2
	on mll2.MaterialTypeID = m.sourcemtid and mll2.OrganizationID = mll.OrganizationID and mll2.PatronCodeID = mll.PatronCodeID
join polaris.Polaris.PatronCodes pc
	on pc.PatronCodeID = mll.PatronCodeID
join polaris.polaris.MaterialTypes mt
	on mt.MaterialTypeID = mll.MaterialTypeID


select mll.OrganizationID
		,mll.PatronCodeID
		,pc.Description
		,mll.MaterialTypeID
		,mt.Description
		,mll.MaxItems
		,mll.MaxRequestItems
		,'' [ ]
		,case when mll.MaxItems = 0 then mll2.MaxItems else mll.MaxItems end [NewMaxItems]
		,case when mll.MaxRequestItems = 0 then mll2.MaxRequestItems else mll.MaxRequestItems end [NewMaxRequestItems]
from Polaris.Polaris.MaterialLoanLimits mll
join @mapping m
	on m.destmtid = mll.MaterialTypeID
join Polaris.polaris.MaterialLoanLimits mll2
	on mll2.MaterialTypeID = m.sourcemtid and mll2.OrganizationID = mll.OrganizationID and mll2.PatronCodeID = mll.PatronCodeID
join polaris.Polaris.PatronCodes pc
	on pc.PatronCodeID = mll.PatronCodeID
join polaris.polaris.MaterialTypes mt
	on mt.MaterialTypeID = mll.MaterialTypeID

--rollback
--commit

