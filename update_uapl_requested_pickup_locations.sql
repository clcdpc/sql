/*

exec Polaris.Circ_UpdatePickupBranchID 30562469,81,2691,2915,81,33,@p7 output

CREATE PROCEDURE [Polaris].[Circ_UpdatePickupBranchID]
	@nSysHoldRequestID int,
	@nNewPickupBranchID int,
	@nUserID int,
	@nWorkstationID int,
	@nTxnBranchID int,
	@nSubSystemID int,	

*/


declare @statuses table ( id int )
declare @oldPickupLocations table ( orgid int )


begin tran


insert into @statuses values (6)
insert into @oldPickupLocations values (79), (80)

declare @logonBranchId int = 81
declare @newPickupBranchId int = 81

declare @polarisUserId int = 2691 -- a.mfields
declare @workstationId int = 2915 -- clcdevvm
declare @subSystemId int = 33 -- don't change



declare @requestId int
 
DECLARE HoldNotices CURSOR FOR
select shr.SysHoldRequestID
from polaris.polaris.SysHoldRequests shr 
where isnull(shr.NewPickupBranchID, shr.PickupBranchID) in ( select * from @oldPickupLocations )
	and shr.SysHoldStatusID in ( select * from @statuses )
	and shr.Origin != 3
 
OPEN HoldNotices
 
FETCH NEXT FROM HoldNotices INTO @requestId
 
WHILE @@FETCH_STATUS = 0
BEGIN
       exec Polaris.Polaris.Circ_UpdatePickupBranchID @requestId,@logonBranchId,@polarisUserId,@workstationId,@newPickupBranchId,@subSystemId,null
       FETCH NEXT FROM HoldNotices INTO @requestId
END
CLOSE HoldNotices
DEALLOCATE HoldNotices

--rollback
--commit