begin tran

delete d
from polaris.Polaris.SA_MaterialTypeGroups g
join polaris.polaris.SA_MaterialTypeGroups_Definitions d
	on d.GroupID = g.GroupID
where g.OrganizationID in (15,48,113,115,128)

--rollback
--commit