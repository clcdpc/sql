begin tran


declare @webparts table (WebPartID int, WebPartTypeID int, Title varchar(30))
declare @webpartelements table (WebPartElementID int, WebPartID int, WebPartUDTypeID int, DisplayOrder int)
declare @webpartelementdetails table (WebPartUDElementID int, WebPartUDSubTypeID int, Value varchar(255))


-- populate temp tables
insert into @webparts
select wp.WebPartID, wp.WebPartTypeID, wp.Title
from devdb.Polaris.Polaris.WebParts wp
where wp.Title like 'bexley%'

insert into @webpartelements
select e.*
from devdb.polaris.polaris.WebPartUDElements e
where e.WebPartID in ( select WebPartID from @webparts )

insert into @webpartelementdetails
select d.*
from devdb.polaris.polaris.WebPartUDElements e
join devdb.polaris.polaris.WebPartUDDetails d
	on d.WebPartUDElementID = e.WebPartUDElementID
where e.WebPartID in ( select WebPartID from @webparts )

/*
update @webparts
set WebPartID = WebPartID + 10000
	,Title = Title + ' mftest'

update @webpartelements
set WebPartID = WebPartID + 10000
*/




-- delete current data
delete from Polaris.polaris.WebPageParts
where WebPartID in ( select WebPartID from @webparts )

delete from polaris.Polaris.WebPartUDElements
where WebPartID in ( select WebPartID from @webparts )

delete from polaris.Polaris.WebParts
where WebPartID in ( select webpartid from @webparts )


-- insert new data
declare @elementmap table (oldid int, id int)

insert into Polaris.polaris.WebParts (WebPartID, WebPartTypeID, Title)
select * from @webparts

--insert into Polaris.polaris.WebPartUDElements (WebPartID, WebPartUDTypeID, DisplayOrder)
--select * from @webpartelements e

merge into Polaris.polaris.WebPartUDElements dest using @webpartelements s
on 1=0
when not matched by target
then insert (WebPartID, WebPartUDTypeID, DisplayOrder)
values (s.WebPartID, s.WebPartUDTypeID, s.DisplayOrder)

output
	s.WebPartElementID, inserted.WebPartUDElementID into @elementmap
;

insert into Polaris.polaris.WebPartUDDetails
select m.id,  e.WebPartUDSubTypeID, e.Value
from @webpartelementdetails e
join @elementmap m
	on m.oldid = e.WebPartUDElementID


select * from @elementmap


/*
select * from @webparts
select * from @webpartelements
select * from @webpartelementdetails
*/

--rollback
--commit


select wp.*, '' [ ], e.*, '' [  ], d.*, '' [   ]
from Polaris.Polaris.WebParts wp
join polaris.polaris.WebPartUDElements e
	on e.WebPartID = wp.WebPartID
join polaris.polaris.WebPartUDDetails d
	on d.WebPartUDElementID = e.WebPartUDElementID
where wp.Title like 'bexley%'

select wp.*
from Polaris.Polaris.WebParts wp
where wp.Title like 'bexley%'
