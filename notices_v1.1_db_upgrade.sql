USE [CLC_Notices]
GO

/******************************************
	Stored Procedures
******************************************/

IF OBJECT_ID('dbo.AddPatronNote', 'P') IS NOT NULL DROP PROCEDURE [dbo].[AddPatronNote]
IF OBJECT_ID('dbo.GetPatronsForEmail', 'P') IS NOT NULL DROP PROCEDURE [dbo].[GetPatronsForEmail]
--Remove unused stored procedure
IF OBJECT_ID('dbo.CLC_Custom_Refresh_DatesClosed_For_Dialout', 'P') IS NOT NULL DROP PROCEDURE [dbo].[CLC_Custom_Refresh_DatesClosed_For_Dialout]
GO

CREATE PROCEDURE [dbo].[AddPatronNote]
	@patronId int,
	@noteText varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if (exists (select 1 from Polaris.polaris.PatronNotes where patronid = @patronId))
	begin
		if ((select len(NonBlockingStatusNotes) from polaris.Polaris.PatronNotes where PatronID = @patronId) < 3900)
		begin
			update Polaris.Polaris.PatronNotes
			set NonBlockingStatusNotes = NonBlockingStatusNotes + @noteText
			where PatronID = @patronId
		end
	end
	else
	begin
		insert into Polaris.Polaris.PatronNotes(PatronID, NonBlockingStatusNotes) values (@patronId, @noteText)
	end
    
END
GO

CREATE PROCEDURE [dbo].[GetPatronsForEmail]
	@emailAddress varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select pr.PatronID, p.Barcode, p.OrganizationID [BranchID], o.ParentOrganizationID [LibraryID]
	from Polaris.polaris.PatronRegistration pr
	join polaris.Polaris.patrons p
		on pr.PatronID = p.PatronID
	join polaris.Polaris.Organizations o
		on o.OrganizationID = p.OrganizationID
	where pr.EmailAddress = @emailAddress or pr.AltEmailAddress = @emailAddress
END
GO


/******************************************
	Views
******************************************/

IF OBJECT_ID('dbo.PolarisNotifications', 'V') IS NOT NULL DROP VIEW [dbo].[PolarisNotifications]
IF OBJECT_ID('dbo.TodaysHoldCalls', 'V') IS NOT NULL DROP VIEW [dbo].[TodaysHoldCalls]
GO

CREATE VIEW [dbo].[PolarisNotifications]
AS

with hoursofoperation(orgid, [hours]) as 
(
	select o.OrganizationID, isnull(opb.Value, opl.Value) [HoursOfOperation]
	from Polaris.Polaris.Organizations o
	left join Polaris.polaris.OrganizationsPPPP opb
		on opb.OrganizationID = o.OrganizationID and opb.AttrID = 96
	left join Polaris.polaris.OrganizationsPPPP opl
		on o.OrganizationID = opl.OrganizationID and opl.AttrID = 96
	where o.OrganizationCodeID = 3
)

select
	nq.NotificationTypeID
	,p.PatronID
	,p.Barcode [PatronBarcode]
	,pr.NameFirst
	,pr.NameLast
	,isnull(nq.ItemRecordID, 0) [ItemRecordID]
	,p.OrganizationID [PatronBranch]
	,o_patron.Abbreviation [PatronBranchAbbr]
	,isnull(o_patron.ParentOrganizationID, 0) [PatronLibrary]
	,o_patron2.Abbreviation [PatronLibraryAbbr]
	,nq.DeliveryOptionID
	,case nq.DeliveryOptionID
		when 3 then pr.PhoneVoice1
		when 4 then pr.PhoneVoice2
		when 5 then pr.PhoneVoice3
	end [DeliveryString]
	,nq.ReportingOrgID [ReportingBranchID]
	,o_notice.Abbreviation [ReportingBranchAbbr]
	,o_notice.ParentOrganizationID [ReportingLibraryID]
	,shr.HoldTillDate
	,hoo.hours
from Results.Polaris.NotificationQueue nq
join Polaris.Polaris.Patrons p
	on nq.PatronID = p.PatronID
join Polaris.Polaris.PatronRegistration pr
	on p.PatronID = pr.PatronID
join Polaris.Polaris.Organizations o_patron
	on p.OrganizationID = o_patron.OrganizationID
join Polaris.Polaris.Organizations o_patron2
	on o_patron.ParentOrganizationID = o_patron2.OrganizationID
join Polaris.Polaris.Organizations o_notice
	on nq.ReportingOrgID = o_notice.OrganizationID
left join Polaris.Polaris.SysHoldRequests shr
	on shr.PatronID = p.PatronID and nq.ItemRecordID = shr.TrappingItemRecordID
left join hoursofoperation hoo
	on hoo.orgid = nq.ReportingOrgID
where nq.DeliveryOptionID in (3, 4, 5) 
and nq.NotificationTypeID in (1, 2, 12, 13) 
and not exists (select 1 from polaris.Polaris.DatesClosed dc where (dc.OrganizationID = p.OrganizationID or dc.OrganizationID = o_patron.ParentOrganizationID) and cast(dc.DateClosed as date) = cast(getdate() as date))
and isnull(hoo.hours,'') not like '%' + left(datename(dw, getdate()), 3) + 'closed%'

GO

CREATE VIEW [dbo].[TodaysHoldCalls]
AS
select distinct
	p.PatronID
	,nh.ReportingOrgId
	,isnull(o.ParentOrganizationID, 0) as ReportingLibraryID
from Results.Polaris.NotificationHistory nh
join Polaris.Polaris.Patrons p
	on nh.PatronId = p.PatronID
join Polaris.Polaris.Organizations o
	on o.OrganizationID = nh.ReportingOrgId
where cast(getdate() as date) = cast(nh.NoticeDate as date)
and nh.DeliveryOptionId in (3, 4, 5)
and nh.NotificationTypeId = 2
and exists (select 1 from Polaris.Polaris.SysHoldRequests shr where shr.PatronID = nh.PatronId and shr.TrappingItemRecordID = nh.ItemRecordId)

GO