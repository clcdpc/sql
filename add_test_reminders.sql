declare @patronid int = 1752752

declare @autorenewDate date = '4/20/2019'

declare @orgID int = ( select organizationid from polaris.polaris.patrons where patronid = @patronid )

declare @szReportingOrgPhone varchar(255) = ( select result FROM Polaris.Polaris.fn_SA_GetValueEx(N'ORGPHONE1', N'Branch', 0, 0, 0, @orgID, 0, 0, 0, 0) )

INSERT INTO Results.Polaris.CircReminders (NotificationTypeID,ReportingOrgID,ReportingOrgName,ReportingOrgAddress,ReportingOrgPhone,PatronID,PatronName,EmailAddress,PatronAddress,EmailFormatID,AdminLanguageID,DeliveryOptionID,ItemRecordID,DueDate,BrowseTitle,ItemFormat,ItemAssignedBranch,Renewals,AutoRenewal)
SELECT
	7,
	pr.OrganizationID,
	o1.Name,
	Polaris.Polaris.fn_OrganizationAddress(pr.OrganizationID),
	@szReportingOrgPhone,
	pr.PatronID, 
	Polaris.Polaris.fn_PatronFullName(pr.PatronID), 
	pr.EmailAddress,
	polaris.Polaris.fn_PatronNoticeAddress (pr.PatronID),
	pr.EmailFormatID,
	polaris.Polaris.fn_Rpt_GetLanguageID(pr.OrganizationID, pr.AdminLanguageID),
	2, -- e-mail

	ic.ItemRecordID, 
	ic.DueDate, 
	br.BrowseTitle, 
	mtm.Description, 
	o2.Name, 
	CASE
		WHEN IsNull(ir.RenewalLimit, 0) > IsNull(ic.Renewals, 0) THEN IsNull(ir.RenewalLimit, 0) - IsNull(ic.Renewals, 0)
		ELSE 0
	END,
	case cast(ic.DueDate as date) when @autorenewDate then 1 else 0 end
FROM 
	Polaris.Polaris.ItemCheckouts ic WITH (NOLOCK) 
	INNER JOIN Polaris.Polaris.ViewPatronRegistration pr WITH (NOLOCK) ON (pr.PatronID = ic.PatronID)
	INNER JOIN Polaris.Polaris.Organizations o1 WITH (NOLOCK) ON (o1.OrganizationID = pr.OrganizationId)
	INNER JOIN Polaris.Polaris.CircReserveItemRecords_View ir WITH (NOLOCK) ON (ic.ItemRecordID = ir.ItemRecordID)
	INNER JOIN Polaris.Polaris.CircItemRecords cir WITH (NOLOCK) ON (ic.ItemRecordID = cir.ItemRecordID)
	INNER JOIN Polaris.Polaris.BibliographicRecords br WITH (NOLOCK) ON (ir.AssociatedBibRecordID = br.BibliographicRecordID)
	INNER JOIN Polaris.Polaris.MARCTypeOfMaterial mtm WITH (NOLOCK) ON (br.PrimaryMARCTOMID = mtm.MARCTypeOfMaterialID)
	INNER JOIN Polaris.Polaris.Organizations o2 WITH (NOLOCK) ON (ir.AssignedBranchID = o2.OrganizationID)
WHERE 
	pr.PatronID = 1752752
	--AND cast(ic.DueDate as date) = '4/15/2019'
	--AND ic.DueDate <= '2/1/2019' 

--select top 100 * from Results.Polaris.CircReminders


select * from CLC_Notices.dbo.PolarisNotifications where PatronID = @patronid