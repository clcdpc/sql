begin tran

declare @rsid int = 90295

update ird
set ird.PhysicalCondition = cir.FreeTextBlock
from Polaris.Polaris.ItemRecordDetails ird
join Polaris.Polaris.CircItemRecords cir on
	cir.ItemRecordID = ird.ItemRecordID
where ird.ItemRecordID in ( select ItemRecordID from Polaris.Polaris.ItemRecordSets where RecordSetID = @rsid )
and cir.FreeTextBlock is not null

update cir
set cir.FreeTextBlock = null
from Polaris.Polaris.CircItemRecords cir
where cir.ItemRecordID in ( select ItemRecordID from Polaris.Polaris.ItemRecordSets where RecordSetID = @rsid )

select ird.ItemRecordID, cir.FreeTextBlock, ird.PhysicalCondition
from Polaris.Polaris.ItemRecordDetails ird
join Polaris.Polaris.CircItemRecords cir on
	cir.ItemRecordID = ird.ItemRecordID
where ird.ItemRecordID in ( select ItemRecordID from Polaris.Polaris.ItemRecordSets where RecordSetID = @rsid )

--commit
--rollback