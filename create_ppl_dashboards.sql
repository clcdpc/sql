/*
select * from Polaris.Polaris.WebParts
select * from Polaris.Polaris.WebPartUDElements order by WebPartUDElementID
select * from Polaris.Polaris.WebPartUDDetails order by WebPartUDElementID
select * from Polaris.Polaris.WebPartUDTypes
select * from Polaris.Polaris.WebPartUDSubTypes
*/

declare @categories table ( description varchar(255), callno varchar(255), rsprefix varchar(10) )


insert into @categories values
	('Animals', 'ANIMALS j', 'JNF'),
	('Arts and Crafts', 'ARTS j', 'JNF'),
	('Biographies', 'BIO j', 'JNF'),
	('Food and Cooking', 'FOOD j', 'JNF'),
	('Fun and Games', 'FUN j', 'JNF'),
	('Government and Law', 'GOV j', 'JNF'),
	('Health and Wellness', 'HEALTH j', 'JNF'),
	('History', 'HISTORY j', 'JNF'),
	('Holidays and Traditions', 'HOL j', 'JNF'),
	('Language and Writing', 'LANG j', 'JNF'),
	('Literature', 'LIT j', 'JNF'),
	('Math', 'MATH j', 'JNF'),
	('Military', 'MILITARY j', 'JNF'),
	('Mysterious and Unexplained', 'MYST j', 'JNF'),
	('Mythology', 'MYTH j', 'JNF'),
	('People and Places', 'PLACE j', 'JNF'),
	('Religion', 'REL j', 'JNF'),
	('Science and Nature', 'SCIENCE j', 'JNF'),
	('Social Topics', 'SOCIAL j', 'JNF'),
	('Space', 'SPACE j', 'JNF'),
	('Sports', 'SPORTS j', 'JNF'),
	('Technology and Computers', 'TECH j', 'JNF'),
	('Transportation', 'TRANS j', 'JNF'),
	('First Chapter ', 'jF FIRST CHAPTER', 'JF'),
	('Adventure ', 'jF ADV', 'JF'),
	('Animals ', 'jF ANIMALS', 'JF'),
	('Choose Your Story ', 'jF CHOOSE', 'JF'),
	('Fantasy ', 'jF FANTASY', 'JF'),
	('Fiction ', 'jF FIC', 'JF'),
	('Funny ', 'jF FUNNY', 'JF'),
	('Historical Fiction ', 'jF HIST', 'JF'),
	('Mystery ', 'jF MYSTERY', 'JF'),
	('Royal Reads ', 'jF ROYAL', 'JF'),
	('Scary ', 'jF SCARY', 'JF'),
	('Science Fiction ', 'jF SciFi', 'JF'),
	('Sports ', 'jF SPORTS', 'JF'),
	('Superhero ', 'jF SUPER', 'JF'),
	('Animals', 'ANIMALS E', 'PB'),
	('Author', 'AUTHOR E', 'PB'),
	('Bedtime', 'BEDTIME E', 'PB'),
	('Community Helper', 'COMMUNITY HELPER E', 'PB'),
	('Concepts', 'CONCEPTS E', 'PB'),
	('Dinosaurs', 'DINOSAURS E', 'PB'),
	('Disney', 'DISNEY E', 'PB'),
	('Fairy Tales/Folk Tales', 'FAIRY TALES/ FOLKTALES E', 'PB'),
	('Favorite Character', 'FAVORIT CHARACTER E', 'PB'),
	('Food', 'FOOD E', 'PB'),
	('Growing Up', 'GROWING UP E', 'PB'),
	('Imagination', 'IMAGINATION E', 'PB'),
	('Monster', 'MONSTERS E', 'PB'),
	('Nature', 'NATURE E', 'PB'),
	('Potty Training', 'POTTY TRAINING E', 'PB'),
	('Royal Reads', 'ROYAL READS E', 'PB'),
	('School', 'SCHOOL E', 'PB'),
	('Seek and Find', 'SEEK & FIND E', 'PB'),
	('Superheroes', 'SUPERHEROES E', 'PB'),
	('Things That Go', 'THINGS THAT GO E', 'PB')

declare @headings table ( heading varchar(255), prefix varchar(15) )

insert into @headings values ('Juvenile NonFiction Categories', 'JNF'), ('Juvenile Fiction Categories', 'JF'), ('Picture Book Categories', 'PB')


declare @webpartId int = (select max(WebPartID) + 1 from Polaris.polaris.WebParts)

begin tran

declare headingCursor cursor for
select heading from @headings

declare @heading varchar(255)

open headingCursor
fetch next from headingCursor into @heading

while @@FETCH_STATUS = 0
begin
	insert into Polaris.polaris.WebParts
	select @webpartId, 3, null, @heading, 0, null

	set @webpartId = @webpartId + 1

	fetch next from headingCursor into @heading
end

close headingCursor
deallocate headingCursor


declare categoryCursor cursor for
select c.description, wp.WebPartID, m.RecordSetID
from polaris.Polaris.WebParts wp
join @headings h
	on h.heading = wp.Title
join @categories c
	on c.rsprefix = h.prefix
join Polaris.dbo.CLC_Custom_Record_Set_SQL_Search_Mapping m
	on m.Note like '%' + c.callno

declare @category varchar(255)
declare @callno varchar(255)
declare @prefix varchar(255)
declare @rsid int


open categoryCursor
fetch next from categoryCursor into @category, @webpartid, @rsid

while @@FETCH_STATUS = 0
begin
	
	declare @displayOrder int = (select isnull(max(e.DisplayOrder) + 1, 1) from Polaris.polaris.WebPartUDElements e where e.WebPartID = @webpartId)

	insert into polaris.polaris.WebPartUDElements (WebPartID, WebPartUDTypeID, DisplayOrder)
	select @webpartId, 2, @displayOrder

	declare @subelementid int = ( select @@identity )

	insert into Polaris.polaris.WebPartUDDetails
	select @subelementId, 1, @category

	insert into polaris.polaris.WebPartUDDetails
	select @subelementId, 2, '/polaris/view.aspx?brs=' + cast(@rsid as varchar)


	 
	fetch next from categoryCursor into @category, @webpartid, @rsid
end

close categoryCursor
deallocate categoryCursor


--rollback
--commit


select *
from polaris.Polaris.WebParts wp
join polaris.polaris.WebPartUDElements e
	on e.WebPartID = wp.WebPartID
join polaris.polaris.WebPartUDDetails d
	on d.WebPartUDElementID = e.WebPartUDElementID
where wp.WebPartID > 1097
	and d.WebPartUDSubTypeID = 1
order by wp.WebPartID, e.DisplayOrder, d.WebPartUDSubTypeID