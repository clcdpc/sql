declare @mt_mapping table (old int, new int)
declare @fc_mapping table (old int, new int)
declare @lpc_mapping table (old int, new int)

begin -- mapping table setup

insert into @mt_mapping values (9,29)
insert into @mt_mapping values (11,41)
insert into @mt_mapping values (12,41)
insert into @mt_mapping values (14,15)
insert into @mt_mapping values (23,25)
insert into @mt_mapping values (26,43)
insert into @mt_mapping values (27,39)
insert into @mt_mapping values (28,45)
insert into @mt_mapping values (2,49)
insert into @mt_mapping values (6,50)
insert into @mt_mapping values (7,18)
insert into @mt_mapping values (4,51)
insert into @mt_mapping values (5,52)
insert into @mt_mapping values (54,18)

insert into @fc_mapping values (9,29)
insert into @fc_mapping values (11,38)
insert into @fc_mapping values (12,38)
insert into @fc_mapping values (14,15)
insert into @fc_mapping values (23,25)
insert into @fc_mapping values (26,40)
insert into @fc_mapping values (27,36)
insert into @fc_mapping values (28,42)
insert into @fc_mapping values (2,46)
insert into @fc_mapping values (6,47)
insert into @fc_mapping values (7,18)
insert into @fc_mapping values (4,48)
insert into @fc_mapping values (5,49)
insert into @fc_mapping values (51,18)

insert into @lpc_mapping values (9,29)
insert into @lpc_mapping values (11,38)
insert into @lpc_mapping values (12,38)
insert into @lpc_mapping values (14,15)
insert into @lpc_mapping values (23,25)
insert into @lpc_mapping values (26,40)
insert into @lpc_mapping values (27,36)
insert into @lpc_mapping values (28,42)
insert into @lpc_mapping values (2,46)
insert into @lpc_mapping values (6,47)
insert into @lpc_mapping values (7,18)
insert into @lpc_mapping values (4,48)
insert into @lpc_mapping values (5,49)
insert into @lpc_mapping values (51,18)

end


begin tran

update mll
set mll.MaxItems = mll2.MaxItems, mll.MaxRequestItems = mll2.MaxRequestItems
from polaris.polaris.MaterialLoanLimits mll
join @mt_mapping mt
	on mt.old = mll.MaterialTypeID
join polaris.polaris.MaterialLoanLimits mll2
	on mll2.MaterialTypeID = mt.new and mll2.OrganizationID = mll.OrganizationID and mll2.PatronCodeID = mll.PatronCodeID
where mll.OrganizationID > 104

update f
set f.Amount = f2.Amount, f.MaximumFine = f2.MaximumFine, f.GraceUnits = f2.GraceUnits
from polaris.polaris.fines f
join @fc_mapping ft
	on ft.old = f.FineCodeID
join polaris.polaris.Fines f2
	on f2.FineCodeID = ft.new and f2.OrganizationID = f.OrganizationID and f2.PatronCodeID = f.PatronCodeID
where f.OrganizationID > 104

update lp
set lp.TimeUnit = lp2.TimeUnit, lp.Units = lp2.units
from polaris.polaris.loanperiods lp
join @lpc_mapping lpcm
	on lpcm.old = lp.LoanPeriodCodeID
join polaris.polaris.LoanPeriods lp2
	on lp2.LoanPeriodCodeID = lpcm.new and lp2.OrganizationID = lp.OrganizationID and lp2.PatronCodeID = lp.PatronCodeID
where lp.OrganizationID > 104


--rollback
--commit


select * from polaris.polaris.materialloanlimits mll where mll.materialtypeid in (9,29) and mll.OrganizationID > 104
select * from polaris.polaris.fines f where f.FineCodeID in (9,29) and f.OrganizationID > 104
select * from polaris.polaris.loanperiods lpc where lpc.LoanPeriodCodeID in (9,29) and lpc.OrganizationID > 104
