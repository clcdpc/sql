declare @library int = 6
declare @yearsInactive int = 3
declare @moneyOwed money = 1000
declare @patronCodes int = 1

/*
Linked to items with a status of Out, Lost or Claimed
Linked to hold requests with a status of Held or Shipped
Linked to ILL requests with a status of Received
System assigned block
Collection agency block
Do not delete option is checked
An item is currently routed to the patron through Serials routing
*/



/*
0 = deleteable
1 = has unbreakable links
2 = either
*/
declare @accountStatus int = 1


select *
from (
select p.PatronID
		,p.Barcode
		,pr.PatronFullName
		,p.ChargesAmount
		,p.LastActivityDate
		,cast(case when exists ( select 1 from polaris.polaris.PatronLostItems pli where pli.PatronID = p.PatronID ) then 1 else 0 end as bit) [HasLostItems]
		,cast(case when exists ( select 1 from polaris.polaris.ItemCheckouts ic where ic.PatronID = p.PatronID ) then 1 else 0 end as bit) [HasOutItems]
		,cast(case when exists ( select 1 from polaris.Polaris.PatronClaims pc where pc.PatronID = p.PatronID ) then 1 else 0 end as bit) [HasClaims]
		,cast(case when exists ( select 1 from polaris.polaris.SysHoldRequests shr where shr.PatronID = p.PatronID and shr.SysHoldStatusID in (5,6) ) then 1 else 0 end as bit) [HasHolds]
		,cast(case when exists ( select 1 from polaris.polaris.ILLRequests ir where ir.PatronID = p.PatronID and ir.ILLStatusID in (10,11,12,13,14) ) then 1 else 0 end as bit) [HasILLs]
		,cast(case when p.SystemBlocks % 1024 = 1024 then 1 else 0 end as bit) [InCollections]
		,isnull(pr.DeletionExempt,0) [IsDeletionExempt]
		--,@hasClaimedItems + @hasCollections + @hasDoNotDelete + @hasHoldRequests + @hasIllRequests + @hasLostItems + @hasOutItems [foo]
from Polaris.Polaris.Patrons p
join Polaris.polaris.PatronRegistration pr
	on pr.PatronID = p.PatronID
join polaris.polaris.Organizations o
	on o.OrganizationID = p.OrganizationID
where not exists ( select 1 from Polaris.polaris.ItemCheckouts ic where ic.PatronID = p.PatronID )
	and not exists ( select 1 from Polaris.polaris.SysHoldRequests shr where shr.PatronID = p.PatronID and shr.SysHoldStatusID in (5,6) )
	and not exists ( select 1 from polaris.Polaris.ILLRequests r where r.PatronID = p.PatronID and r.ILLStatusID in (10,11,12,13,14) )
	and isnull(pr.DeletionExempt,0) = 0
	and o.ParentOrganizationID = @library
	and p.LastActivityDate < dateadd(year, @yearsInactive * -1, getdate())
	and (@moneyOwed is null or p.ChargesAmount <= @moneyOwed)
	and p.PatronCodeID in (@patronCodes)
) d
where (@accountStatus = 0 and d.HasLostItems | d.HasOutItems | d.HasClaims | d.HasLostItems | d.HasILLs | d.InCollections | d.IsDeletionExempt = 0)
	or (@accountStatus = 1 and d.HasLostItems | d.HasOutItems | d.HasClaims | d.HasLostItems | d.HasILLs | d.InCollections | d.IsDeletionExempt = 1)
	or @accountStatus = 2





/*
(@checkAccountBalance = 0 or d.UnderFineCutoff = 1)
	or (@hasLostItems = 1 or d.HasLostItems = 0)
	or (@hasOutItems = 1 or d.HasOutItems = 0)
	or (@hasClaimedItems = 1 or d.HasClaims = 0)
	or (@hasHoldRequests = 1 or d.HasHolds = 0)
	or (@hasIllRequests = 1 or d.HasILLs = 0)
	or (@hasCollections = 1 or d.InCollections = 0)
	or (@hasDoNotDelete = 1 or d.IsDeletionExempt = 0)
	
*/



select 0 [Id], 'Can be deleted' [Description]
union all
select 1, 'Has unbreakable links'
union all
select 2, 'Show all'