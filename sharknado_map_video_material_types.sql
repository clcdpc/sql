-- maps sharknado libraries' videos to most popular code currently in use for that bib

if (OBJECT_ID('tempdb..#existing') is null) 
begin
	create table #existing ( bibid int, mtid int, mt varchar(255), itemcount int, itemrank int )

	insert into #existing
	select	cir.AssociatedBibRecordID [bibid]
			,cir.MaterialTypeID [mtid]
			,mt.Description [mt]
			,count(distinct cir.ItemRecordID) [itemcount]
			,rank() over(partition by cir.AssociatedBibRecordID order by count(distinct cir.ItemRecordID) desc, mt.Description desc) [itemrank]
	from Polaris.polaris.CircItemRecords cir
	join polaris.Polaris.MaterialTypes mt
		on mt.MaterialTypeID = cir.MaterialTypeID
	where cir.MaterialTypeID in (2,3,4,5,6,7,49,50,51,52,53) 
		and cir.AssignedBranchID < 104
	group by cir.AssociatedBibRecordID, cir.MaterialTypeID, mt.Description
end



begin tran

update cir
set cir.MaterialTypeID = existing.mtid
from polaris.polaris.CircItemRecords cir
join polaris.polaris.MaterialTypes mt
	on mt.MaterialTypeID = cir.MaterialTypeID
join #existing existing
	on cir.AssociatedBibRecordID = existing.bibid
where cir.AssignedBranchID > 104 and cir.MaterialTypeID in (2,3,4,5,6,7,42,49,50,51,52,53) and existing.itemrank = 1

select cir.ItemRecordID
		,cir.AssociatedBibRecordID
		,mt.Description [old_mt]
		,existing.mt [new_mt]
from polaris.polaris.CircItemRecords cir
join polaris.polaris.MaterialTypes mt
	on mt.MaterialTypeID = cir.MaterialTypeID
join #existing existing
	on cir.AssociatedBibRecordID = existing.bibid
where cir.AssignedBranchID > 104 and cir.MaterialTypeID in (2,3,4,5,6,7,42,49,50,51,52,53) and existing.itemrank = 1
order by cir.AssociatedBibRecordID

--rollback
--commit

--drop table #existing
