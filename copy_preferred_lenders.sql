declare @sourceBranch int = 43

begin tran

delete hpli
from Polaris.Polaris.HoldsPreferredLendersInclude hpli
where hpli.OrganizationID = 39

insert into Polaris.Polaris.HoldsPreferredLendersInclude
select 39,hpli.IncludedBranchID,hpli.Include
from Polaris.Polaris.HoldsPreferredLendersInclude hpli
where hpli.Include = 0 
	and hpli.OrganizationID = @sourceBranch

delete hpli
from Polaris.Polaris.HoldsPreferredLendersInclude hpli
join Polaris.polaris.Organizations o
	on o.OrganizationID = hpli.OrganizationID
where o.ParentOrganizationID = 39 --and o.OrganizationID != @sourceBranch

--rollback
--commit


select hpli.*, o2.Name
from Polaris.Polaris.HoldsPreferredLendersInclude hpli
join Polaris.polaris.Organizations o
	on o.OrganizationID = hpli.OrganizationID
join polaris.polaris.Organizations o2
	on o2.OrganizationID = hpli.IncludedBranchID
where 39 in (o.OrganizationID,o.ParentOrganizationID)
order by hpli.OrganizationID, o2.Name
