declare @map table ( oldsc varchar(255), newsc varchar(255) )
declare @map2 table ( orgid int, oldscid int, newscid int )

insert into @map values ('afdm - Adult Fiction DVD Musicals', 'afdmus - Adult Fiction - DVD Musical')
insert into @map values ('afdtel - Adult Dvd Televison', 'afdtel - Adult Fiction - DVD Television')
insert into @map values ('affa - Adult Fiction - Fantasy', 'affan - Adult Fiction - Fantasy')
insert into @map values ('afhf - Adult Fiction - Historical Fiction', 'afhist - Adult Fiction - Historical')
insert into @map values ('AFIC - Adult Fiction', 'af - Adult Fiction')
insert into @map values ('afpbfan - Adult Fiction - Paperback Fantasy Fiction', 'afpbfan - Adult Fiction - Paperback Fantasy')
insert into @map values ('afpbi = Adult Fiction Paperback - Inspirational', 'afpbins - Adult Fiction - Paperback Inspirational')
insert into @map values ('afpbkp - Adult Fiction - Paperback Paranormal', 'afpbp - Adult Fiction - Paperback Paranormal')
insert into @map values ('anfdtel - Adult Non-Fiction DVD and Television', 'anfdtel - Adult Non-Fiction - DVD Television')
insert into @map values ('anfdtel - Adult Non-Fiction DVD Television', 'anfdtel - Adult Non-Fiction - DVD Television')
insert into @map values ('anfgn - Adult NonFiction Graphic Novels', 'anfgn - Adult Nonfiction - Graphic Novels')
insert into @map values ('aroy - Adult Reference - Ohio Collection', 'aro - Adult Reference - Ohio Collection')
insert into @map values ('hspot - Hotspot', 'hotspot - Hotspot')
insert into @map values ('jbr - Juvenile Bluray', 'jfdblu - Juvenile Fiction - DVD Blu-ray')
insert into @map values ('jfba -  juvenile fiction books with audio', 'jfba - Juvenile Fiction - Book with Audio')
insert into @map values ('jfc - Juvenile Fiction - Comics', 'jfcbs - Juvenile Fiction - Comic Books')
insert into @map values ('jfcb - Juvenile Fiction - Comic Books', 'jfcbs - Juvenile Fiction - Comic Books')
insert into @map values ('jfcdpicb - Picture Book with CD', 'jfpiccdab - Juvenile Fiction - Picture Book and CD')
insert into @map values ('jfdblu -  Juvenile Fiction DVD Blu-ray', 'jfdblu - Juvenile Fiction - DVD Blu-ray')
insert into @map values ('jfdblu - Juvenile Fiction DVD Blu-ray', 'jfdblu - Juvenile Fiction - DVD Blu-ray')
insert into @map values ('jfdhol - Juvenile DVD Holiday', 'jfdhol - Juvenile Fiction -  DVD Holiday')
insert into @map values ('jfebh  Juvenile Fiction Easy Book-Holiday', 'jfebh  Juvenile Fiction - Easy Book Holiday')
insert into @map values ('Jfer - Juvenile Fiction Easy Reader', 'jfer - Juvenile Fiction - Easy Reader')
insert into @map values ('jfgn - Juvenile Fiction Graphic Novel', 'jfgn - Juvenile Fiction - Graphic Novels')
insert into @map values ('jfi - Intermediate', 'jfint - Juvenile Fiction - Intermediate')
insert into @map values ('jfi - Juvenile Fiction - Inspirational', 'jfinsp - Juvenile Fiction - Inspirational')
insert into @map values ('jfipb - Intermediate Paperback', 'jfintpb - Juvenile Fiction - Intermediate Paperback')
insert into @map values ('jfnc - Juvenile Fiction - Newberry Collection', 'jfnc - Juvenile Fiction - Newbery Collection')
insert into @map values ('jfncpic - Juvenile Fiction Newbery Collection Picture Book', 'jfnc - Juvenile Fiction - Newbery Collection Picture Book')
insert into @map values ('jfpicb - Juvenile Fiction - Picture Books', 'jfpicb - Juvenile Fiction - Picture Book')
insert into @map values ('jfpicbcmas - Juvenile Picture Book Christmas', 'jfpicbcmas - Juvenile Fiction - Picture Book Christmas')
insert into @map values ('jfpicbn - Juvenile Fiction - Picture Books New', 'jfpicbn - Juvenile Fiction - Picture Book New')
insert into @map values ('jfpicbo - over-sized picture books', 'jfpicbo - Juvenile Fiction - Picture Book Oversize')
insert into @map values ('jfpicbpb - Juvenile Fiction - Picture Books Paperback', 'jfpicbpb - Juvenile Fiction - Picture Book Paperback')
insert into @map values ('jfpiccdab - Juvenile Fiction Picture Book and CD', 'jfpiccdab - Juvenile Fiction - Picture Book and CD')
insert into @map values ('jfrf - Juvenile Fiction - Realistic Fiction', 'jfrf - Juvenile Fiction - Realistic')
insert into @map values ('jnfbcc - Juvenile Nonfiction Biography Caldecott Collection', 'jnfbcc - Juvenile Nonfiction - Biography Caldecott Collection')
insert into @map values ('jnfbg - Board Games', 'jnfbg - Juvenile Nonfiction - Board Game')
insert into @map values ('jnfbnc - Juvenile Nonfiction Biopgraphy Newbery Collection', 'jnfbnc - Juvenile Nonfiction - Biography Newbery Collection')
insert into @map values ('jnfdblu - Juvenile Non-fiction DVD Blu-ray', 'jnfdblu - Juvenile Nonfiction - DVD Blu-ray')
insert into @map values ('jrbb - Juvenile Reference Board Book', 'jrbb - Juvenile Reference - Board Book')
--insert into @map values ('PER - Periodicals', 'per - periodicals')
insert into @map values ('yalm = Young Adult - Lease Material', 'ylm - Young Adult - Lease Material')
insert into @map values ('yfgn - Young Adult Fiction - Graphic Novels (comic Books)', 'yfgn - Young Adult Fiction - Graphic Novels')
insert into @map values ('ynlm - Young Adult New Lease Material', 'ylmn - Young Adult - Lease Material New')

declare @statcodestats table ( AssignedBranchID int, StatisticalCodeID int, numitems int)


insert into @statcodestats
select cir.AssignedBranchID, cir.StatisticalCodeID, count(*) [numitems]
from polaris.Polaris.CircItemRecords cir
where cir.StatisticalCodeID is not null
group by cir.AssignedBranchID, cir.StatisticalCodeID


--select sc.*, scs.numitems [numitems old], '' [ ], sc2.StatisticalCodeID [NewScId], sc2.Description [NewSc], scs2.numitems [numitems new], '' [  ], m.newsc, *
insert into @map2
select 	 sc.OrganizationID
		,sc.StatisticalCodeID [oldsc]
		,sc2.StatisticalCodeID [newsc]
from polaris.polaris.StatisticalCodes sc
join @map m
	on m.oldsc = sc.Description
left join polaris.polaris.StatisticalCodes sc2
	on sc2.OrganizationID = sc.OrganizationID and sc2.Description = m.newsc and sc2.StatisticalCodeID != sc.StatisticalCodeID
left join @statcodestats scs
	on scs.AssignedBranchID = sc.OrganizationID and scs.StatisticalCodeID = sc.StatisticalCodeID
left join @statcodestats scs2
	on scs2.AssignedBranchID = sc2.OrganizationID and scs2.StatisticalCodeID = sc2.StatisticalCodeID
where exists ( select 1 from Polaris.polaris.StatisticalCodes sc2 where sc2.OrganizationID = sc.OrganizationID and sc2.Description = m.newsc ) 
	--and ( scs.numitems is not null or scs2.numitems is not null )
--order by sc2.StatisticalCodeID, scs.numitems desc


begin tran

-- fix codes with conflicts

-- rename old conflicting code
update sc
set sc.Description = 'zArchived - ' + sc.Description
from Polaris.polaris.StatisticalCodes sc
join @map2 m
	on m.orgid = sc.OrganizationID and m.oldscid = sc.StatisticalCodeID

-- update items to use new code id
update cir
set cir.StatisticalCodeID = m.newscid
from Polaris.polaris.CircItemRecords cir
join @map2 m
	on m.orgid = cir.AssignedBranchID and m.oldscid = cir.StatisticalCodeID


-- rename codes without conflicts

update sc
set sc.Description = m.newsc
from polaris.polaris.StatisticalCodes sc
join @map m
	on m.oldsc = sc.Description

update sc
set sc.Description = m.newsc
from polaris.polaris.StatisticalCodes sc
join @map m
	on m.oldsc = sc.Description
where  (sc.OrganizationID = 98 and sc.StatisticalCodeID = 207)
	or (sc.OrganizationID = 20 and sc.StatisticalCodeID = 394)
	or (sc.OrganizationID = 22 and sc.StatisticalCodeID = 394)
	or (sc.OrganizationID = 21 and sc.StatisticalCodeID = 395)

--rollback
--commit


select *
from polaris.polaris.StatisticalCodes sc
join @map m
	on m.oldsc = sc.Description



--jfpicb - Juvenile Fiction - Picture Books

/*
select * from polaris.polaris.Organizations where OrganizationCodeID = 3
select * from polaris.polaris.PatronCodes



select *
from Polaris.polaris.StatisticalCodes sc
where sc.OrganizationID = 115
	and sc.StatisticalCodeID in (11)



select *
from Polaris.Polaris.StatisticalCodes sc
where sc.OrganizationID = 9
	and sc.StatisticalCodeID in (97,388)

select cir.ItemRecordID
		,cir.AssignedBranchID
		,cir.StatisticalCodeID
from Polaris.Polaris.CircItemRecords cir 
where cir.AssignedBranchID = 20 and cir.StatisticalCodeID in (385,134)



*/