


--declare @tcounts table (OrganizationID int, TransactionTypeID int, Total int)

--insert into @tcounts 
--select OrganizationID, TransactionTypeID, count(*) as total    
--from PolarisTransactions.Polaris.transactionheaders TH WITH (NOLOCK)
--inner join PolarisTransactions.Polaris.TransactionDetails TD WITH (NOLOCK)
--ON (TH.TransactionID = TD.TransactionID) AND TD.TransactionSubTypeID = 128 AND TD.numValue NOT IN (3, 8)
--where TranClientDate between '8/1/2017' and '11/1/2017' and TransactionTypeID IN (6002) and
--organizationid IN (17)
--group by OrganizationID, TransactionTypeID

--select TH.OrganizationID, P.OrganizationID as OrganizationID, TransactionTypeID, count(*) as Total
--FROM PolarisTransactions.Polaris.TransactionHeaders TH WITH (NOLOCK)
--INNER JOIN PolarisTransactions.Polaris.TransactionDetails TD WITH (NOLOCK)
--ON (TH.TransactionID = TD.TransactionID)
--INNER JOIN polaris.Polaris.Patrons P
--ON (TD.NumValue = P.OrganizationID)
--where TranClientDate between '8/1/2017' and '1/1/2018' and TD.TransactionSubTypeID = 123 and TH.TransactionTypeID IN (2001, 2002) and
--TH.OrganizationID IN (17) and P.OrganizationID NOT IN (17) 
--group by P.OrganizationID, TH.OrganizationID, TransactionTypeID

--declare @tcountscentral2 table (TransactingOrgID int, TransactionTypeID int, Total int)
--insert into @tcountscentral2
--select TH.OrganizationID, TransactionTypeID, count(*) as Total
--FROM PolarisTransactions.Polaris.TransactionHeaders TH WITH (NOLOCK)
--where TranClientDate between '8/1/2017' and '11/1/2017' and TH.TransactionTypeID IN (3002)
--group by TH.OrganizationID, TransactionTypeID

--select (SELECT sum(Total) from @tCountsCentral2 tc2 where tc2.TransactionTypeID = 3002 and tc2.TransactingOrgID != tc.OrganizationID)
--from @tcounts tc

--select * from PolarisTransactions.Polaris.TransactionTypes where TransactionTypeID = 3002


--select *
--from PolarisTransactions.dbo.clc_custom_viewtransactiondetails vtd
--where vtd.TransactionID in (
--select top 10 TH.TransactionID
--FROM PolarisTransactions.Polaris.TransactionHeaders TH WITH (NOLOCK)
--where TranClientDate between '8/1/2017' and '1/1/2017' and TH.TransactionTypeID IN (3002) and TH.OrganizationID != 17
--)
--order by vtd.TransactionID, vtd.transactionsubtypeid


--select br.PrimaryMARCTOMID, count(*)
--FROM PolarisTransactions.Polaris.TransactionHeaders TH WITH (NOLOCK)
--join PolarisTransactions.Polaris.TransactionDetails td
--	on TH.TransactionID = td.TransactionID and td.TransactionSubTypeID = 36
--left join Polaris.polaris.BibliographicRecords br
--	on br.BibliographicRecordID = td.numValue
--where TranClientDate between '8/1/2017' and '1/1/2018' and TH.TransactionTypeID IN (3002) and TH.OrganizationID != 17
--group by br.PrimaryMARCTOMID


--select * from polaris.polaris.ItemAvailabilityDisplay iad where iad.BranchLocationOrgID = 103 and iad.ItemAvailabilityLevelID = 1

--select * from polaris.Polaris.PatronRegistration where PatronID = 1752752


select 
    TH.OrganizationID, IR.AssignedBranchID as AssignedOrgID, TransactionTypeID, count(*) as Total
FROM 
    PolarisTransactions.Polaris.TransactionHeaders TH WITH (NOLOCK)
    INNER JOIN PolarisTransactions.Polaris.TransactionDetails TD WITH (NOLOCK)
        ON (TH.TransactionID = TD.TransactionID)
    INNER JOIN 
        polaris.Polaris.CircItemRecords IR 
            ON (TD.NumValue = IR.ItemRecordID)
where 
    TranClientDate between '8/1/2017' and '1/1/2018' and TD.TransactionSubTypeID = 38 and TH.TransactionTypeID = 3008
    and IR.AssignedBranchID IN (29) and TH.OrganizationID != IR.AssignedBranchID 
group by 
    IR.AssignedBranchID, TH.OrganizationID, TransactionTypeID

	select * from Polaris.polaris.organizations