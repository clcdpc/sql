

declare @nNotificationTypeID int = 10
declare @orgID int = 87


declare @saVal nvarchar(255)
declare @bUseAltEmail bit
declare @szReportingOrgAddress nvarchar(255)
declare @szReportingOrgPhone nvarchar(255)

declare @days int
declare @start datetime
declare @end datetime
declare @now datetime = getdate()


declare @tmpPatrons table (patronid int)
-- get all patrons that already been reminded
DELETE @tmpPatrons -- cleanup
INSERT INTO 
	@tmpPatrons (PatronID)
SELECT 
	Distinct PatronID 
FROM 
	PolarisTransactions.Polaris.NotificationLog (NOLOCK)
WHERE 
	--NotificationStatusID = @nNotificationStatusID
	NotificationStatusID in (12, 13)
	AND NotificationTypeID = @nNotificationTypeID 
	AND Year(NotificationDateTime) = Year(@now)
	AND Month(NotificationDateTime) = Month(@now)
	AND Day(NotificationDateTime) = Day(@now)


exec Polaris.Polaris.SA_GetValueByOrg N'NSPARMRMDAYSPATEXP', @orgID, 1, @saVal output
if IsNumeric(@saVal) != 1 SET @saVal = N'1'

SET @days = CAST(@saVal AS int)	
SET @start = DateAdd(Day, @days, @now)
SET @start = Polaris.Polaris.SA_SetDateTime (YEAR(@start), MONTH(@start), DAY(@start), 0, 0, 0)
SET @end = DateAdd(dd, 1, @start)
SET @end = DateAdd(ms, -3, @end)

EXECUTE Polaris.Polaris.SA_GetValueByOrg N'NSPARAM_EMAIL_ADDRTOUSE', @orgID, 1, @saVal OUT
IF (IsNumeric(@saVal) = 1 AND @saVal = N'2') 
	SELECT @bUseAltEmail = 1
ELSE
	SELECT @bUseAltEmail = 0

SELECT @szReportingOrgAddress = Polaris.Polaris.fn_OrganizationAddress (@orgID)

SELECT @szReportingOrgPhone = result FROM Polaris.Polaris.fn_SA_GetValueEx(N'ORGPHONE1', N'Branch', 0, 0, 0, @orgID, 0, 0, 0, 0)

INSERT INTO Results.Polaris.CircReminders
(
	NotificationTypeID ,
	ReportingOrgID ,
	ReportingOrgName ,
	ReportingOrgAddress ,
	ReportingOrgPhone, 

	PatronID ,
	PatronName ,
	EmailAddress ,
	PatronAddress ,
	PhoneVoice,
	EmailFormatID,
	AdminLanguageID,
	DeliveryOptionID
)
SELECT 
	@nNotificationTypeID,
	@orgID,
	o.Name,
	@szReportingOrgAddress,
	@szReportingOrgPhone,

	pr.PatronID,
	Polaris.Polaris.fn_PatronFullName (pr.PatronID),
	pr.EmailAddress +
		CASE @bUseAltEmail
			WHEN 1 THEN 
				CASE 
					WHEN pr.AltEmailAddress IS NULL THEN N''
					WHEN pr.AltEmailAddress like N'%@%' THEN N';' + pr.AltEmailAddress
					ELSE N''
				END
			ELSE N''
		END,
	Polaris.Polaris.fn_PatronNoticeAddress (pr.PatronID),
	COALESCE(pr.PhoneVoice1, pr.PhoneVoice2, pr.PhoneVoice3),
	pr.EmailFormatID,
	Polaris.Polaris.fn_Rpt_GetLanguageID(@orgId, pr.AdminLanguageID),
	2 -- e-mail
FROM 
	Polaris.Polaris.ViewPatronRegistration pr WITH (NOLOCK)
	INNER JOIN Polaris.Polaris.Organizations o WITH (NOLOCK) ON (pr.OrganizationID = o.OrganizationID)
	LEFT OUTER JOIN @tmpPatrons tmp ON (pr.PatronID = tmp.PatronID)
WHERE 
	pr.DeliveryOptionID is not null
	AND pr.OrganizationID = @orgID
	AND pr.EmailAddress LIKE N'%@%'
	--AND pr.DeliveryOptionID = 2	-- choose to receive e-mails
	AND ExpirationDate >= @start 
	AND ExpirationDate <= @end 
	AND tmp.PatronID IS NULL	-- to avoid duplicate reminder in the same day
	AND pr.DeliveryOptionID != 8	-- choose to receive TXT message only
	AND pr.ExcludeFromPatronRecExpiration = 0;