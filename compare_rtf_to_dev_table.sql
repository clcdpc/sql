-- match on sequence number
select d.RequesterBranchID [RequesterBranchID]
		,o_d_req.Name [RequesterBranch]
		,d.ResponderBranchID [DevResponderBranchID]
		,o_d_res.Name [DevResponderBranch]
		,d.Sequence [DevSequence]
		,d.MaxDaysInRTF [DevMaxDaysInRTF]
		,d.PrimarySecondaryFlag [DevPrimarySecondaryFlag]
		,'' [ ]
		,p.ResponderBranchID [ProdResponderBranchID]
		,o_p_res.Name [ProdResponderBranch]
		,p.Sequence [ProdSequence]
		,p.MaxDaysInRTF [ProdMaxDaysInRTF]
		,p.PrimarySecondaryFlag [ProdPrimarySecondaryFlag]
from clcdb.dbo.SysHoldRoutingSequencesDev d
full outer join Polaris.polaris.SysHoldRoutingSequences p
	on p.RequesterBranchID = d.RequesterBranchID and p.PrimarySecondaryFlag = d.PrimarySecondaryFlag and p.Sequence = d.Sequence
left join Polaris.Polaris.Organizations o_d_req	
	on o_d_req.OrganizationID = d.RequesterBranchID
left join Polaris.Polaris.Organizations o_d_res
	on o_d_res.OrganizationID = d.ResponderBranchID
left join polaris.Polaris.Organizations o_p_req
	on o_p_req.OrganizationID = p.RequesterBranchID
left join Polaris.Polaris.Organizations o_p_res
	on o_p_res.OrganizationID = p.ResponderBranchID
where (d.InstanceID is null or d.InstanceID = 1)
order by isnull(d.RequesterBranchID, p.RequesterBranchID), isnull(d.PrimarySecondaryFlag, p.PrimarySecondaryFlag), isnull(d.Sequence, p.Sequence)

-- match on responder branch id
select d.RequesterBranchID [RequesterBranchID]
		,o_d_req.Name [RequesterBranch]
		,d.ResponderBranchID [DevResponderBranchID]
		,o_d_res.Name [DevResponderBranch]
		,d.Sequence [DevSequence]
		,d.MaxDaysInRTF [DevMaxDaysInRTF]
		,d.PrimarySecondaryFlag [DevPrimarySecondaryFlag]
		,'' [ ]
		,p.ResponderBranchID [ProdResponderBranchID]
		,o_p_res.Name [ProdResponderBranch]
		,p.Sequence [ProdSequence]
		,p.MaxDaysInRTF [ProdMaxDaysInRTF]
		,p.PrimarySecondaryFlag [ProdPrimarySecondaryFlag]
from clcdb.dbo.SysHoldRoutingSequencesDev d
full outer join Polaris.polaris.SysHoldRoutingSequences p
	on p.RequesterBranchID = d.RequesterBranchID and p.PrimarySecondaryFlag = d.PrimarySecondaryFlag and p.ResponderBranchID = d.ResponderBranchID
left join Polaris.Polaris.Organizations o_d_req	
	on o_d_req.OrganizationID = d.RequesterBranchID
left join Polaris.Polaris.Organizations o_d_res
	on o_d_res.OrganizationID = d.ResponderBranchID
left join polaris.Polaris.Organizations o_p_req
	on o_p_req.OrganizationID = p.RequesterBranchID
left join Polaris.Polaris.Organizations o_p_res
	on o_p_res.OrganizationID = p.ResponderBranchID
where (d.InstanceID is null or d.InstanceID = 1)
order by isnull(d.RequesterBranchID, p.RequesterBranchID), isnull(d.PrimarySecondaryFlag, p.PrimarySecondaryFlag), isnull(d.Sequence, p.Sequence)