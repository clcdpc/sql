insert into clcdb.dbo.RegistrationFormSettingTypes
select *
from devdb.clcdb.dbo.RegistrationFormSettingTypes st
where not exists ( select 1 from clcdb.dbo.RegistrationFormSettingTypes st2 where st2.Setting = st.setting )


insert into clcdb.dbo.RegistrationFormSettings
select * 
from devdb.clcdb.dbo.RegistrationFormSettings s 
where not exists ( select 1 from clcdb.dbo.RegistrationFormSettings s2 where s2.Setting = s.Setting and s2.OrganizationID = s.organizationid)
	and s.organizationid in (1)