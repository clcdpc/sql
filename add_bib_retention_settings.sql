begin tran

declare @tags table (tag int)
declare @retentionids table (id int)
declare @indicators table (value varchar(1))

-- the tags to add settings for
insert into @tags values (49), (92), (99)
-- include all indicators
insert into @indicators values (' '), ('0'), ('1'), ('2'), ('3'), ('4'), ('5'), ('6'), ('7'), ('8'), ('9')

-- insert the "main" entries for each tag and organization 
-- outputs the new RetentionIDs into a temp table so we can add the indicator settings
insert into Polaris.Polaris.BibOverlayRetentionTags
output INSERTED.RetentionID into @retentionids
select tag.tag, o.OrganizationID, 1, 0
from Polaris.Polaris.Organizations o
cross join @tags tag
where o.OrganizationCodeID = 3

-- add indicator one settings
insert into Polaris.Polaris.BibOverlayRetentionIndOne
select id.id, indicator.value
from @retentionids id
cross join @indicators indicator

-- add indicator two settings
insert into Polaris.Polaris.BibOverlayRetentionIndTwo
select id.id, indicator.value
from @retentionids id
cross join @indicators indicator

-- select statements to check the work
select * 
from Polaris.Polaris.BibOverlayRetentionTags bort
where bort.RetainTagNumber in ( 49,92,99 )
order by bort.OrganizationID, bort.RetainTagNumber

select * from Polaris.Polaris.BibOverlayRetentionIndOne where RetentionID in ( select * from @retentionids )
select * from Polaris.Polaris.BibOverlayRetentionIndTwo where RetentionID in ( select * from @retentionids )


--rollback
--commit
