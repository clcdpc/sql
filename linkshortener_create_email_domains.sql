select *
from EmailDomains ed
where ed.EmailDomain = 'clcohio.org'
	and (ed.ApplicationName is null or ed.ApplicationName = 'link-shortener')
order by ed.ApplicationName


declare @ed table ( ed varchar(255), orgid int )

insert into @ed values ('alexandria.lib.oh.us',2)
insert into @ed values ('bexleylibrary.org',84)
insert into @ed values ('clcohio.org',-1)
insert into @ed values ('columbuslibrary.org',39)
insert into @ed values ('delawarelibrary.org',105)
insert into @ed values ('fcdlibrary.org',8)
insert into @ed values ('ghpl.org',6)
insert into @ed values ('granvillelibrary.org',112)
insert into @ed values ('marysvillelib.org',17)
insert into @ed values ('mylondonlibrary.org',86)
insert into @ed values ('pataskalalibrary.org',114)
insert into @ed values ('pickawaylib.org',19)
insert into @ed values ('pickeringtonlibrary.org',23)
insert into @ed values ('plaincitylib.org',14)
insert into @ed values ('swpl.org',28)
insert into @ed values ('ualibrary.org',78)
insert into @ed values ('wagnalls.org',24)
insert into @ed values ('wagnallslibrary.org',24)
insert into @ed values ('worthingtonlibraries.org',32)

insert into EmailDomains (EmailDomain, ApplicationName, LibraryId)
select ed.ed, null, ed.orgid
from @ed ed

select * from polaris.Polaris.organizations where OrganizationCodeID = 2

select * from emaildomains