select *
from polaris.dbo.CLC_Custom_First_Time_CKOs c
where c.TxnDate < '1/1/2022'


select * from polaris.dbo.CLC_Custom_OrganizationsPPPP op where op.AttrDesc like '%patron%id%'


select top 1000 *
from polaris.dbo.CLC_Custom_First_Time_CKOs c
where c.TxnDate < dateadd(year, (select op.Value from polaris.polaris.OrganizationsPPPP op where op.OrganizationID = 1 and op.AttrID = 2682) * -1, getdate())
order by txnid desc


select top 1000 *
from polaris.dbo.CLC_Custom_First_Time_CKOs c
where c.TxnDate < '1/1/2020'
	and c.PatronId > 0



begin tran

update c
set c.PatronId = 0
from polaris.dbo.CLC_Custom_First_Time_CKOs c
where c.TxnDate < dateadd(year, (select op.Value from polaris.polaris.OrganizationsPPPP op where op.OrganizationID = 1 and op.AttrID = 2682) * -1, getdate())
	and c.PatronId > 0

update c
set c.PatronId = 0
from polaris.dbo.CLC_Custom_First_Time_CKOs c
where c.TxnDate < dateadd(year, -6, getdate())
	and c.PatronId > 0

update c
set c.PatronId = 0
from polaris.dbo.CLC_Custom_First_Time_CKOs c
where c.TxnDate < dateadd(year, -5, getdate())
	and c.PatronId > 0

update c
set c.PatronId = 0
from polaris.dbo.CLC_Custom_First_Time_CKOs c
where c.TxnDate < dateadd(year, -4, getdate())
	and c.PatronId > 0


--rollback
--commit


select datepart(year, cko.TxnDate)
		,count(*)
from polaris.dbo.CLC_Custom_First_Time_CKOs cko
where patronid > 0
group by datepart(year, cko.TxnDate)