begin tran

declare @collectionId int
declare @collectionName varchar(1000)
declare cur CURSOR LOCAL for
    select CollectionID, Name from Polaris.Polaris.Collections

open cur

fetch next from cur into @collectionId, @collectionName

while @@FETCH_STATUS = 0 BEGIN
	declare @rs table ( recordSetID int )
    insert into Polaris.Polaris.RecordSets 
	output INSERTED.RecordSetID into @rs 
	values ('Old v2 ' + cast(@collectionId as varchar) + ' ' + case when len(@collectionName) >= 40 then left(@collectionName, len(@collectionname) - 12) else @collectionName end, 3, 425, null, 4, GETDATE(), null, 'pre-cleanup collection code backup v2')

	insert into Polaris.Polaris.ItemRecordSets
	select cir.ItemRecordID, (select MIN(RecordSetID) from @rs)
	from Polaris.Polaris.CircItemRecords cir
	where cir.AssignedCollectionID = @collectionId

	delete from @rs

    fetch next from cur into @collectionId, @collectionName
END

close cur
deallocate cur

--rollback
--commit


select rs.RecordSetID, rs.Name, COUNT(irs.ItemRecordID)
from Polaris.Polaris.RecordSets rs
join Polaris.Polaris.ItemRecordSets irs on
	rs.RecordSetID = irs.RecordSetID
where CreatorID = 425 and rs.Note = 'pre-cleanup collection code backup v2'
group by rs.recordsetid, rs.Name
