-- We need SQL scripts to lower the total cko, total holds, individual CKOs, individual holds and the group checkout limits by half for Fairfield.


declare @library int = 78

begin tran

-- double time to go unclaimed
--update op
--set op.Value = op.Value / 2
--from (
--	select *
--	from polaris.Polaris.OrganizationsPPPP op
--	where op.AttrID = 793
--) d
--join polaris.polaris.Organizationspppp op
--	on op.OrganizationID = d.OrganizationID and op.AttrID = d.AttrID
--where op.OrganizationID = @library


-- un-double material limits
update mll
set mll.MaxItems = mll.MaxItems / 2
	,mll.MaxRequestItems = mll.MaxRequestItems / 2
from polaris.polaris.MaterialLoanLimits mll
join Polaris.polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
where mll.MaterialTypeID not in (43,44,33,40)
	and mll.MaxItems < 999
	and o.ParentOrganizationID = @library

-- un-double patron limits
update pll
set pll.TotalItems = pll.TotalItems / 2
	,pll.TotalOverDue = pll.TotalOverDue / 2
	,pll.TotalHolds = pll.TotalHolds / 2
from Polaris.polaris.PatronLoanLimits pll
join Polaris.polaris.Organizations o
	on o.OrganizationID = pll.OrganizationID
where pll.TotalItems < 999
	and o.ParentOrganizationID = @library

-- un-double material group limits
update l
set l.Limit = l.Limit / 2
from Polaris.polaris.SA_MaterialTypeGroups_Limits l
join polaris.polaris.SA_MaterialTypeGroups g
	on g.GroupID = l.GroupID
where l.limit < 999
	and g.OrganizationID = @library

--rollback
--commit

/*

declare @library int = 78

select mt.Description, mll.*
from polaris.polaris.MaterialLoanLimits mll
join Polaris.polaris.Organizations o
	on o.OrganizationID = mll.OrganizationID
join Polaris.polaris.MaterialTypes mt
	on mt.MaterialTypeID = mll.MaterialTypeID
where mll.MaterialTypeID not in (43,44,33,40)
	and mll.MaxItems < 999
	and o.ParentOrganizationID = @library

select pll.*
from Polaris.polaris.PatronLoanLimits pll
join Polaris.polaris.Organizations o
	on o.OrganizationID = pll.OrganizationID
where pll.TotalItems < 999
	and o.ParentOrganizationID = @library


select *
from Polaris.polaris.SA_MaterialTypeGroups_Limits l
join polaris.polaris.SA_MaterialTypeGroups g
	on g.GroupID = l.GroupID
where l.limit < 999
	and g.OrganizationID = @library


*/