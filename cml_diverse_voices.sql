--IF OBJECT_ID('tempdb..#authorIndex') IS NULL 
--begin
--	create table #authorIndex ( BibliographicRecordID int, Author nvarchar(255) )

--	insert into #authorIndex
--	select distinct bt.BibliographicRecordID, polaris.dbo.CLC_Custom_StripNonAlphaNumericCharacters(bs.Data, '')
--	from polaris.polaris.BibliographicTags bt
--	join polaris.polaris.BibliographicSubfields bs
--		on bs.BibliographicTagID = bt.BibliographicTagID and bs.Subfield = 'a'
--	where bt.TagNumber = 100
--end

--select distinct brs.RecordSetID
--		,rs.Name [RecordSetName]
--		,ai2.Author [AuthorName]
--		,ai2.BibliographicRecordID [NewBibRecordID]
--		,br.BrowseTitle [NewBibTitle]
--from polaris.polaris.RecordSets rs
--join polaris.polaris.BibRecordSets brs
--	on brs.RecordSetID = rs.RecordSetID
--join #authorIndex ai
--	on ai.BibliographicRecordID = brs.BibliographicRecordID
--join #authorIndex ai2
--	on ai2.Author = ai.Author and ai2.BibliographicRecordID != brs.BibliographicRecordID
--join polaris.polaris.BibliographicRecords br
--	on br.BibliographicRecordID = ai2.BibliographicRecordID
--where rs.Name like 'cml dv%done%'
--order by rs.Name, ai2.Author, ai2.BibliographicRecordID

----drop table #authorIndex


--with rsets as (
--	select *
--	from polaris.polaris.RecordSets rs
--	where rs.Name like 'cml dv%'
--)

--select rs.Name, rs2.Name
--from rsets rs
--full outer join rsets rs2
--	on rs2.Name = replace(rs.Name, 'done ', '') and rs2.RecordSetID != rs.RecordSetID
--where rs.Name like '%done%'



--declare @rsmap table ( SourceRsId int, UserDestRsId int, SystemDestRsId int )

--insert into @rsmap
--select rs.RecordSetID
--		,rs2.RecordSetID
--		,0
--from polaris.polaris.RecordSets rs
--left join polaris.polaris.RecordSets rs2
--	on rs2.Name like replace(rs.Name, ' done', '') + '%' and rs2.RecordSetID != rs.RecordSetID
--where rs.Name like 'cml%dv%done%'

--select *
--from @rsmap

/*

select   distinct rs_source.Name [SourceRecordSetName]
		,mah.Author [AuthorName]
		,isnull(d.DestRsId, '9999999') [NewBidRecordSetID]
		,isnull(rs.Name, replace(rs_source.Name, ' done', '') + ' | EX') [NewBibRecordSetName]
		,bai.BibliographicRecordID [NewBibRecordID]
		,br.BrowseTitle [NewBibTitle]
from polaris.polaris.BibAuthorIndices bai
join polaris.polaris.BibliographicRecords br
	on br.BibliographicRecordID = bai.BibliographicRecordID
join (
	select distinct m.SourceRsId
			,m.DestRsId
			,bai.AuthorityRecordID
			,brs.BibliographicRecordID
			,ar.CreatorID [AuthorityCreatorID]
	from polaris.polaris.RecordSets rs
	join polaris.polaris.BibRecordSets brs
		on brs.RecordSetID = rs.RecordSetID
	join @rsmap m
		on m.SourceRsId = rs.RecordSetID
	join polaris.polaris.BibAuthorIndices bai
		on bai.BibliographicRecordID = brs.BibliographicRecordID and bai.AuthorityRecordID is not null
	join polaris.polaris.BibliographicTags bt
		on bt.BibliographicTagID = bai.BibliographicTagID and bt.TagNumber = 100
	join polaris.polaris.AuthorityRecords ar
		on ar.AuthorityRecordID = bai.AuthorityRecordID
) d
	on d.AuthorityRecordID = bai.AuthorityRecordID and d.BibliographicRecordID != bai.BibliographicRecordID
join polaris.polaris.MainAuthorHeadings mah
	on mah.MainAuthorHeadingID = bai.MainAuthorHeadingID
join polaris.polaris.RecordSets rs_source
	on rs_source.RecordSetID = d.SourceRsId
left join polaris.polaris.RecordSets rs
	on rs.RecordSetID = d.DestRsId
left join polaris.polaris.BibRecordSets brs
	on brs.RecordSetID = d.DestRsId and brs.RecordSetID != bai.BibliographicRecordID
	*/



/*
select rs.RecordSetID
		,rs.Name
		,'' [ ]
		,rs2.RecordSetID
		,rs2.Name
		,'' [  ]
		,rs3.RecordSetID
		,rs3.Name
from polaris.polaris.RecordSets rs
left join polaris.polaris.RecordSets rs2
	on rs2.Name = replace(rs.Name, ' done', '') + ' - cbu' and rs2.RecordSetID != rs.RecordSetID
left join polaris.polaris.RecordSets rs3
	on rs3.Name = replace(rs.Name, ' done', '') + ' - cbs' and rs3.RecordSetID != rs.RecordSetID
where rs.Name like 'cml%dv% done%'
*/



declare @rsmap table ( SourceRsId int, UserDestRsId int, SystemDestRsId int )

insert into @rsmap
select rs.RecordSetID
		,rs2.RecordSetID
		,rs3.RecordSetID
from polaris.polaris.RecordSets rs
left join polaris.polaris.RecordSets rs2
	on rs2.Name = replace(rs.Name, ' done', '') + ' - cbu' and rs2.RecordSetID != rs.RecordSetID
left join polaris.polaris.RecordSets rs3
	on rs3.Name = replace(rs.Name, ' done', '') + ' - cbs' and rs3.RecordSetID != rs.RecordSetID
where rs.Name like 'cml%dv% done%'

;with sourceAuthorities as (
	select distinct m.SourceRsId
			,m.UserDestRsId
			,m.SystemDestRsId
			,bai.AuthorityRecordID
			,brs.BibliographicRecordID
	from polaris.polaris.RecordSets rs
	join polaris.polaris.BibRecordSets brs
		on brs.RecordSetID = rs.RecordSetID
	join @rsmap m
		on m.SourceRsId = rs.RecordSetID
	join polaris.polaris.BibAuthorIndices bai
		on bai.BibliographicRecordID = brs.BibliographicRecordID and bai.AuthorityRecordID is not null
	join polaris.polaris.BibliographicTags bt
		on bt.BibliographicTagID = bai.BibliographicTagID and bt.TagNumber = 100
	join polaris.polaris.AuthorityRecords ar
		on ar.AuthorityRecordID = bai.AuthorityRecordID
)

-- insert
insert into polaris.polaris.BibRecordSets
select  distinct bai.BibliographicRecordID [NewBibRecordID]
		,case when polarisAuthorities.AuthorityRecordID is not null then d.SystemDestRsId else d.UserDestRsId end [NewRecordSetId]
from polaris.polaris.BibAuthorIndices bai
join polaris.polaris.BibliographicRecords br
	on br.BibliographicRecordID = bai.BibliographicRecordID
join sourceAuthorities d
	on d.AuthorityRecordID = bai.AuthorityRecordID and d.BibliographicRecordID != bai.BibliographicRecordID
join polaris.polaris.MainAuthorHeadings mah
	on mah.MainAuthorHeadingID = bai.MainAuthorHeadingID
left join polaris.polaris.BibRecordSets brs_source
	on brs_source.RecordSetID = d.SourceRsId and brs_source.BibliographicRecordID = bai.BibliographicRecordID
left join polaris.polaris.BibRecordSets brs_system
	on brs_system.RecordSetID = d.SystemDestRsId and brs_system.BibliographicRecordID = bai.BibliographicRecordID
left join polaris.polaris.BibRecordSets brs_user
	on brs_user.RecordSetID = d.UserDestRsId and brs_user.BibliographicRecordID = bai.BibliographicRecordID
left join (
	select distinct ats.AuthorityRecordID
	from polaris.polaris.AuthorityTags ats
	join polaris.polaris.AuthoritySubfields ass
		on ass.AuthorityTagID = ats.AuthorityTagID and ass.Subfield = 'a'
	where ats.TagNumber = 667
		and ass.Data = 'Polaris system generated authority record'
) polarisAuthorities
	on polarisAuthorities.AuthorityRecordID = bai.AuthorityRecordID
where brs_source.RecordSetID is null and brs_system.RecordSetID is null and brs_user.RecordSetID is null






/*
select brs.*
		,bai.AuthorityRecordID
		,ass.Data
from polaris.polaris.BibRecordSets brs
join polaris.polaris.BibAuthorIndices bai
	on bai.BibliographicRecordID = brs.BibliographicRecordID
join polaris.polaris.AuthorityTags ats
	on ats.AuthorityRecordID = bai.AuthorityRecordID and ats.TagNumber = 667
join polaris.polaris.AuthoritySubfields ass
	on ass.AuthorityTagID = ats.AuthorityTagID and ass.Subfield = 'a'
where brs.RecordSetID = 255987
order by bai.AuthorityRecordID, brs.BibliographicRecordID
*/



/*

select   distinct rs_source.Name [SourceRecordSetName]
		,mah.Author [AuthorName]
		,case when polarisAuthorities.AuthorityRecordID is not null then d.SystemDestRsId else d.UserDestRsId end [NewRecordSetId]
		,case when polarisAuthorities.AuthorityRecordID is not null then rs_system.Name else rs_user.Name end [NewRecordSetName]
		,bai.BibliographicRecordID [NewBibRecordID]
		,br.BrowseTitle [NewBibTitle]
		,d.AuthorityRecordID
		,bai.AuthorityRecordID
		,polarisAuthorities.AuthorityRecordID
from polaris.polaris.BibAuthorIndices bai
join polaris.polaris.BibliographicRecords br
	on br.BibliographicRecordID = bai.BibliographicRecordID
join (
	select distinct m.SourceRsId
			,m.UserDestRsId
			,m.SystemDestRsId
			,bai.AuthorityRecordID
			,brs.BibliographicRecordID
	from polaris.polaris.RecordSets rs
	join polaris.polaris.BibRecordSets brs
		on brs.RecordSetID = rs.RecordSetID
	join @rsmap m
		on m.SourceRsId = rs.RecordSetID
	join polaris.polaris.BibAuthorIndices bai
		on bai.BibliographicRecordID = brs.BibliographicRecordID and bai.AuthorityRecordID is not null
	join polaris.polaris.BibliographicTags bt
		on bt.BibliographicTagID = bai.BibliographicTagID and bt.TagNumber = 100
	join polaris.polaris.AuthorityRecords ar
		on ar.AuthorityRecordID = bai.AuthorityRecordID
) d
	on d.AuthorityRecordID = bai.AuthorityRecordID and d.BibliographicRecordID != bai.BibliographicRecordID
join polaris.polaris.MainAuthorHeadings mah
	on mah.MainAuthorHeadingID = bai.MainAuthorHeadingID
join polaris.polaris.RecordSets rs_source
	on rs_source.RecordSetID = d.SourceRsId
left join polaris.polaris.RecordSets rs_system
	on rs_system.RecordSetID = d.SystemDestRsId
left join polaris.polaris.BibRecordSets brs_system
	on brs_system.RecordSetID = d.SystemDestRsId and brs_system.BibliographicRecordID != bai.BibliographicRecordID
left join polaris.polaris.RecordSets rs_user
	on rs_user.RecordSetID = d.UserDestRsId
left join polaris.polaris.BibRecordSets brs_user
	on brs_user.RecordSetID = d.UserDestRsId and brs_user.BibliographicRecordID != bai.BibliographicRecordID
left join (
	select distinct ats.AuthorityRecordID
	from polaris.polaris.AuthorityTags ats
	join polaris.polaris.AuthoritySubfields ass
		on ass.AuthorityTagID = ats.AuthorityTagID and ass.Subfield = 'a'
	where ats.TagNumber = 667
		and ass.Data = 'Polaris system generated authority record'
) polarisAuthorities
	on polarisAuthorities.AuthorityRecordID = bai.AuthorityRecordID
--where brs_system.RecordSetID is null and brs_user.RecordSetID is null
order by bai.AuthorityRecordID, bai.BibliographicRecordID
*/

-- 8684
/*
;with sourceAuthorities as (
	select distinct m.SourceRsId
			,m.UserDestRsId
			,m.SystemDestRsId
			,bai.AuthorityRecordID
			,brs.BibliographicRecordID
	from polaris.polaris.RecordSets rs
	join polaris.polaris.BibRecordSets brs
		on brs.RecordSetID = rs.RecordSetID
	join @rsmap m
		on m.SourceRsId = rs.RecordSetID
	join polaris.polaris.BibAuthorIndices bai
		on bai.BibliographicRecordID = brs.BibliographicRecordID and bai.AuthorityRecordID is not null
	join polaris.polaris.BibliographicTags bt
		on bt.BibliographicTagID = bai.BibliographicTagID and bt.TagNumber = 100
	join polaris.polaris.AuthorityRecords ar
		on ar.AuthorityRecordID = bai.AuthorityRecordID
)

select  distinct bai.BibliographicRecordID [NewBibRecordID]
		,case when polarisAuthorities.AuthorityRecordID is not null then d.SystemDestRsId else d.UserDestRsId end [NewRecordSetId]
from sourceAuthorities d
join polaris.polaris.BibAuthorIndices bai
	on d.AuthorityRecordID = bai.AuthorityRecordID and d.BibliographicRecordID != bai.BibliographicRecordID
left join (
	select distinct ats.AuthorityRecordID
	from polaris.polaris.AuthorityTags ats
	join polaris.polaris.AuthoritySubfields ass
		on ass.AuthorityTagID = ats.AuthorityTagID and ass.Subfield = 'a'
	where ats.TagNumber = 667
		and ass.Data = 'Polaris system generated authority record'
) polarisAuthorities
	on polarisAuthorities.AuthorityRecordID = bai.AuthorityRecordID
outer apply (
	select top 1 brs.RecordSetID
	from polaris.polaris.BibRecordSets brs
	join @rsmap m
		on brs.BibliographicRecordID = bai.BibliographicRecordID and brs.RecordSetID in ( m.SourceRsId , m.UserDestRsId, m.SystemDestRsId )
) dvRecordSets
where dvRecordSets.RecordSetID is null
*/


/*
select   distinct rs_source.Name [SourceRecordSetName]
		,mah.Author [AuthorName]
		,case when polarisAuthorities.AuthorityRecordID is not null then d.SystemDestRsId else d.UserDestRsId end [NewRecordSetId]
		,case when polarisAuthorities.AuthorityRecordID is not null then rs_system.Name else rs_user.Name end [NewRecordSetName]
		,bai.BibliographicRecordID [NewBibRecordID]
		,br.BrowseTitle [NewBibTitle]
		,bai.AuthorityRecordID
from polaris.polaris.BibAuthorIndices bai
join polaris.polaris.BibliographicRecords br
	on br.BibliographicRecordID = bai.BibliographicRecordID
join (
	select distinct m.SourceRsId
			,m.UserDestRsId
			,m.SystemDestRsId
			,bai.AuthorityRecordID
			,brs.BibliographicRecordID
	from polaris.polaris.RecordSets rs
	join polaris.polaris.BibRecordSets brs
		on brs.RecordSetID = rs.RecordSetID
	join @rsmap m
		on m.SourceRsId = rs.RecordSetID
	join polaris.polaris.BibAuthorIndices bai
		on bai.BibliographicRecordID = brs.BibliographicRecordID and bai.AuthorityRecordID is not null
	join polaris.polaris.BibliographicTags bt
		on bt.BibliographicTagID = bai.BibliographicTagID and bt.TagNumber = 100
	join polaris.polaris.AuthorityRecords ar
		on ar.AuthorityRecordID = bai.AuthorityRecordID
) d
	on d.AuthorityRecordID = bai.AuthorityRecordID and d.BibliographicRecordID != bai.BibliographicRecordID
join polaris.polaris.MainAuthorHeadings mah
	on mah.MainAuthorHeadingID = bai.MainAuthorHeadingID
join polaris.polaris.RecordSets rs_source
	on rs_source.RecordSetID = d.SourceRsId
join polaris.polaris.RecordSets rs_system
	on rs_system.RecordSetID = d.SystemDestRsId
join polaris.polaris.RecordSets rs_user
	on rs_user.RecordSetID = d.UserDestRsId
left join polaris.polaris.BibRecordSets brs_source
	on brs_source.RecordSetID = rs_source.RecordSetID and brs_source.BibliographicRecordID = bai.BibliographicRecordID
left join polaris.polaris.BibRecordSets brs_system
	on brs_system.RecordSetID = d.SystemDestRsId and brs_system.BibliographicRecordID = bai.BibliographicRecordID
left join polaris.polaris.BibRecordSets brs_user
	on brs_user.RecordSetID = d.UserDestRsId and brs_user.BibliographicRecordID = bai.BibliographicRecordID
left join (
	select distinct ats.AuthorityRecordID
	from polaris.polaris.AuthorityTags ats
	join polaris.polaris.AuthoritySubfields ass
		on ass.AuthorityTagID = ats.AuthorityTagID and ass.Subfield = 'a'
	where ats.TagNumber = 667
		and ass.Data = 'Polaris system generated authority record'
) polarisAuthorities
	on polarisAuthorities.AuthorityRecordID = bai.AuthorityRecordID
where brs_source.RecordSetID is null and brs_system.RecordSetID is null and brs_user.RecordSetID is null
order by bai.AuthorityRecordID, bai.BibliographicRecordID
*/






/*
select * from polaris.polaris.BibAuthorIndices bai where bai.BibliographicRecordID = 1405734
select * from polaris.polaris.BibAuthorIndices bai where bai.AuthorityRecordID = 1156126

select *
from polaris.polaris.AuthorityTags ats
join polaris.polaris.AuthoritySubfields ass
	on ass.AuthorityTagID = ats.AuthorityTagID
where ats.TagNumber = 100
	and ass.Subfield = 'a'
	and ats.AuthorityRecordID = 1156126
*/


/*
declare @rsmap table ( SourceRsId int, UserDestRsId int, SystemDestRsId int )

insert into @rsmap
select rs.RecordSetID
		,rs2.RecordSetID
		,rs3.RecordSetID
from polaris.polaris.RecordSets rs
left join polaris.polaris.RecordSets rs2
	on rs2.Name = replace(rs.Name, ' done', '') + ' - cbu' and rs2.RecordSetID != rs.RecordSetID
left join polaris.polaris.RecordSets rs3
	on rs3.Name = replace(rs.Name, ' done', '') + ' - cbs' and rs3.RecordSetID != rs.RecordSetID
where rs.Name like 'cml%dv% done%'

delete brs
from polaris.polaris.BibRecordSets brs
join @rsmap map
	on brs.RecordSetID in (map.UserDestRsId, map.SystemDestRsId)
*/


--select * from polaris.polaris.AuthorityRecords ar where ar.AuthorityRecordID = 1435478 



/*
-- CREATE SYSTEM/USER RECORD SETS

begin tran

declare valueCursor cursor for
select rs.RecordSetID
		,replace(rs.Name, ' done', '') [BaseName]
from polaris.polaris.RecordSets rs
where rs.Name like 'cml%dv% done%'

declare @sourceRsId int
declare @sourceRsName nvarchar(255)

open valueCursor
fetch next from valueCursor into @sourceRsId, @sourceRsName
while @@FETCH_STATUS = 0
begin
	insert into Polaris.polaris.RecordSets (Name, ObjectTypeID, CreatorID, OrganizationOwnerID, CreationDate, Note)
	select	@sourceRsName + ' - cbs' -- Name
			,2			-- Type, bib
			,425		-- Creator
			,39			-- Owner, cml
			,getdate()	-- Creation Date
			,@sourceRsName + ' where authority created by system'

	insert into Polaris.polaris.RecordSets (Name, ObjectTypeID, CreatorID, OrganizationOwnerID, CreationDate, Note)
	select	@sourceRsName + ' - cbu' -- Name
			,2			-- Type, bib
			,425		-- Creator
			,39			-- Owner, cml
			,getdate()	-- Creation Date
			,@sourceRsName + ' where authority created by user'
	
	--update Polaris.dbo.CLC_Custom_Old_To_New_Material_Type_Mapping
	--set RecordSetID = @@identity
	--where LibraryID = @libraryid and OldMaterialTypeID = @oldmtid and OldCollectionID = @oldcid and NewMaterialTypeID = @newmtid and LibraryID = @libraryid
	

	fetch next from valueCursor into @sourceRsId, @sourceRsName
end

close valueCursor
deallocate valueCursor


select rs.RecordSetID
		,rs.Name
from polaris.polaris.RecordSets rs
where rs.Name like 'cml%dv%'


--rollback
--commit

*/

