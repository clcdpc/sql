begin tran

declare @patronid int = 1278151

declare @lpEnabled int = (select case ps.Value when 'True' then 1 else 0 end from AuthorSubscription.dbo.PatronSettings ps where ps.Mnemonic = 'enable_large_print' and ps.PatronId = @patronid)
set @lpEnabled = isnull(@lpEnabled,0)

insert into Polaris.polaris.PatronRecordSets
select ars2.RecordSetId, @patronid
from Polaris.polaris.PatronRecordSets prs 
join polaris.polaris.Patrons p
	on p.PatronID = prs.PatronID
join polaris.polaris.Organizations o
	on o.OrganizationID = p.OrganizationID
join polaris.polaris.RecordSets rs
	on rs.RecordSetID = prs.RecordSetID
join AuthorSubscription.dbo.AuthorRecordSets ars
	on ars.RecordSetId = prs.RecordSetID
join AuthorSubscription.dbo.AuthorRecordSets ars2
	on ars2.AuthorId = ars.AuthorId and ars2.OrganizationId = ars.OrganizationId and ars2.LargePrint = @lpEnabled
where prs.PatronID = @patronid 
	and not exists ( select 1 from polaris.polaris.PatronRecordSets prs2 where prs2.PatronID = @patronid and prs2.RecordSetID = ars2.RecordSetId )

delete prs
from Polaris.polaris.PatronRecordSets prs 
join polaris.polaris.RecordSets rs
	on rs.RecordSetID = prs.RecordSetID
join AuthorSubscription.dbo.AuthorRecordSets ars
	on ars.RecordSetId = prs.RecordSetID
where prs.PatronID = @patronid 
	and ars.LargePrint != @lpEnabled

--rollback
--commit