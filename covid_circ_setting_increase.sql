select * from polaris.dbo.CLC_Custom_OrganizationsPPPP op where op.OrganizationID = 3 order by op.AttrDesc--or op.AttrID in (1199,1299)



begin tran

-- double time to go unclaimed
update op
set op.Value = op.Value * 2
from (
	select *
	from polaris.Polaris.OrganizationsPPPP op
	where op.AttrID = 793
) d
join polaris.polaris.Organizationspppp op
	on op.OrganizationID = d.OrganizationID and op.AttrID = d.AttrID

-- double material limits
update mll
set mll.MaxItems = mll.MaxItems * 2
	,mll.MaxRequestItems = mll.MaxRequestItems * 2
from polaris.polaris.MaterialLoanLimits mll
where mll.MaterialTypeID not in (43,44,33,40)
	and mll.MaxItems < 999

-- double patron limits
update pll
set pll.TotalItems = pll.TotalItems * 2
	,pll.TotalOverDue = pll.TotalOverDue * 2
	,pll.TotalHolds = pll.TotalHolds * 2
from Polaris.polaris.PatronLoanLimits pll
where pll.TotalItems < 999

-- double material group limits
update l
set l.Limit = l.Limit * 2
from Polaris.polaris.SA_MaterialTypeGroups_Limits l
where l.limit < 999

--rollback
--commit

/*
Loanable Equipment 1
Loanable Equipment 2
In-House Equipment
Hotspots
*/

select * from polaris.polaris.MaterialTypes order by Description



select * from polaris.Polaris.MaterialLoanLimits mll where mll.OrganizationID = 24



select * from polaris.Polaris.AdminAttributes aa where aa.AttrID = 793