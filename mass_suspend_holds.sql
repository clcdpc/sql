/*********************************
Suspends all holds to one date
*********************************/

declare @dateAdditions table ( libraryid int, increase int )

--insert into @dateAdditions values ( 19, 6 )

begin tran

update
 	shr 
set
 	activationdate = coalesce(dr.HoldActivationDate, cast(dr.EndDate as datetime) + 1)
	,Suspended = 1
	,SysHoldStatusID = 1
	,LastStatusTransitionDate = current_timestamp
	,TrappingItemRecordID = null
output
	deleted.SysHoldRequestID
	,deleted.ActivationDate
	into Polaris.dbo.CLC_Custom_ActivationDateBackup
from polaris.polaris.sysholdrequests as shr
join polaris.Polaris.Organizations o
	on o.OrganizationID = shr.PickupBranchID
join Polaris.dbo.CLC_Custom_CV_Closed_Date_Ranges dr
	on dr.LibraryId = o.ParentOrganizationID	
where shr.ActivationDate < coalesce(dr.HoldActivationDate, dr.EndDate)
	--only modify requests that currently have a status of requested or suspended
	and SysHoldStatusID in ( 1,3,4 )
	--DON'T modify SearchOhio requests
	and shr.Origin != 3
	-- DON'T modify Plain City Requests as of 3/18/2020	
	and o.ParentOrganizationID not in (3,14)


begin tran
update shr
set shr.HoldTillDate = cast(coalesce(dr.HoldUnclaimedDate, '6/1/2020') as datetime) + .9999999
	,shr.SysHoldStatusID = 6
from Polaris.polaris.SysHoldRequests shr
join Polaris.Polaris.Organizations o
	on o.OrganizationID = coalesce(shr.NewPickupBranchID, shr.PickupBranchID)
join Polaris.dbo.CLC_Custom_CV_Closed_Date_Ranges dr
	on dr.LibraryId = o.ParentOrganizationID
where shr.HoldTillDate between '3/15/2020' 
	and coalesce(dr.HoldUnclaimedDate, '6/1/2020')
	and shr.TrappingItemRecordID is not null
	and o.ParentOrganizationID not in (2,14)





--select @@rowcount

--select count(*) from polaris.dbo.CLC_Custom_ActivationDateBackup

/*
select shr.ActivationDate, coalesce(dr.HoldUnclaimedDate, '4/13/2020')
from Polaris.polaris.SysHoldRequests shr
join Polaris.Polaris.Organizations o
	on o.OrganizationID = coalesce(shr.NewPickupBranchID, shr.PickupBranchID)
join Polaris.dbo.CLC_Custom_CV_Closed_Date_Ranges dr
	on dr.LibraryId = o.ParentOrganizationID
where shr.HoldTillDate between '3/15/2020' and coalesce(dr.HoldUnclaimedDate, '4/13/2020')
*/

--rollback
--commit




/*

*********************************
Script to create table
*********************************

USE [Polaris]
GO

CREATE TABLE [dbo].[CLC_Custom_ActivationDateBackup](
	[RequestID] [int] NOT NULL,
	[OldActivationDate] [date] NOT NULL
) ON [PRIMARY]
GO

*/


/*

*********************************
Suspends holds based on closed dates
*********************************

declare @suspendUntilDate datetime = '4/7/2020'

begin tran

update
 	shr 
set
 	activationdate = @SuspendUntilDate
	,Suspended = 1
	,SysHoldStatusID = 1
	,LastStatusTransitionDate = current_timestamp
	,TrappingItemRecordID = null
output
	deleted.SysHoldRequestID
	,deleted.ActivationDate
	into Polaris.dbo.CLC_Custom_ActivationDateBackup
from polaris.polaris.sysholdrequests as shr
join polaris.Polaris.Organizations o
	on o.OrganizationID = shr.PickupBranchID		
where ActivationDate < @SuspendUntilDate
	--only modify requests that currently have a status of requested or suspended
	and SysHoldStatusID in ( 1,3,4 )
	--DON'T modify SearchOhio requests
	and shr.Origin != 3

select * from polaris.dbo.CLC_Custom_ActivationDateBackup

--rollback
--commit

*/