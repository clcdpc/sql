--select * from SummerReading.dbo.Organizations


begin tran

declare @orgid int = 2

declare @programs table ( id int )
declare @patrons table ( id int )

insert into @programs
select id
from SummerReading.dbo.Programs p
where p.OrganizationId = @orgid

delete from SummerReading.dbo.AwardedPrizes where PrizeId in ( select id from SummerReading.dbo.Prizes p where p.ProgramId in ( select * from @programs ) )
delete from SummerReading.dbo.Prizes where ProgramId in ( select * from @programs )

delete from SummerReading.dbo.Patrons where Id in ( select patronid from SummerReading.dbo.ProgramRegistrations where ProgramId in ( select * from @programs ) )
delete from SummerReading.dbo.Programs where Id in ( select * from @programs )


--rollback
--commit