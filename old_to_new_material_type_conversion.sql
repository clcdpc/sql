begin tran

declare @libraryid int = 6

declare valueCursor cursor for-- select oldmtid, oldcid, newmtid from @values
select m.OldMaterialTypeID, m.OldCollectionID, m.NewMaterialTypeID
from Polaris.dbo.CLC_Custom_Old_To_New_Material_Type_Mapping m
where m.LibraryID = @libraryid

declare @oldmtid int
declare @oldcid int
declare @newmtid int

open valueCursor
fetch next from valueCursor into @oldmtid, @oldcid, @newmtid
while @@FETCH_STATUS = 0
begin
	
	declare @oldmt varchar(255) = ( select Description from polaris.Polaris.MaterialTypes where MaterialTypeID = @oldmtid )
	declare @oldc varchar(255) = ( select Name from polaris.Polaris.Collections where CollectionID = @oldcid )
	declare @newmt varchar(255) = ( select Description from polaris.Polaris.MaterialTypes where MaterialTypeID = @newmtid )

	insert into Polaris.polaris.RecordSets (Name, ObjectTypeID, CreatorID, CreationDate, Note)
	select left(@oldmt + ' + ' + @oldc + ' = ' + @newmt, 80), 3, 425, getdate(), @oldmt + ' + ' + @oldc + ' = ' + @newmt + ' | created automatically as part of the material type cleanup project'
	
	update Polaris.dbo.CLC_Custom_Old_To_New_Material_Type_Mapping
	set RecordSetID = @@identity
	where OldMaterialTypeID = @oldmtid and OldCollectionID = @oldcid and NewMaterialTypeID = @newmtid and LibraryID = @libraryid
	

	fetch next from valueCursor into @oldmtid, @oldcid, @newmtid
end

close valueCursor
deallocate valueCursor

select distinct m.LibraryID, o.Name
from polaris.dbo.CLC_Custom_Old_To_New_Material_Type_Mapping m
join polaris.polaris.Organizations o
	on o.OrganizationID = m.LibraryID







select o2.Name, oldmt.Description [Old MaterialType], oldc.Name [Old Collection], newmt.Description [New MaterialType], count(*) [Items]
from polaris.Polaris.CircItemRecords cir
join Polaris.Polaris.Organizations o
	on o.OrganizationID = cir.AssignedBranchID
join polaris.Polaris.Organizations o2
	on o2.OrganizationID = o.ParentOrganizationID
join Polaris.dbo.CLC_Custom_Old_To_New_Material_Type_Mapping m
	on m.LibraryID = o.ParentOrganizationID and m.OldMaterialTypeID = cir.MaterialTypeID and (m.OldCollectionID = cir.AssignedCollectionID or m.OldCollectionID = -1)
join polaris.polaris.MaterialTypes oldmt
	on oldmt.MaterialTypeID = cir.MaterialTypeID
left join polaris.Polaris.Collections oldc
	on oldc.CollectionID = cir.AssignedCollectionID
join polaris.Polaris.MaterialTypes newmt
	on newmt.MaterialTypeID = m.NewMaterialTypeID
where oldmt.Description != newmt.Description
group by o2.Name, oldmt.Description, oldc.Name, newmt.Description
order by o2.Name, oldmt.Description, oldc.Name, newmt.Description

--select oldmt.Description [Old MaterialType], oldc.Name [Old Collection], newmt.Description [New MaterialType], v.rsid
--from polaris.Polaris.CircItemRecords cir
--join Polaris.Polaris.Organizations o
--	on o.OrganizationID = cir.AssignedBranchID
--join @values v
--	on v.libraryid = o.ParentOrganizationID and v.oldmtid = cir.MaterialTypeID and v.oldcid = cir.AssignedCollectionID
--join polaris.polaris.MaterialTypes oldmt
--	on oldmt.MaterialTypeID = cir.MaterialTypeID
--join polaris.Polaris.Collections oldc
--	on oldc.CollectionID = cir.AssignedCollectionID
--join polaris.Polaris.MaterialTypes newmt
--	on newmt.MaterialTypeID = v.newmtid
--where oldmt.Description != newmt.Description
--order by v.rsid

--rollback
--commit

select * from Polaris.polaris.RecordSets where RecordSetID = 128010