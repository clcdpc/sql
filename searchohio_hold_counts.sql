declare @beginDate date = '1/1/2018'
declare @endDate date = '2/1/2018'

-- Incoming requests
select tt.TransactionTypeDescription, count(*) [Count]
from PolarisTransactions.Polaris.TransactionHeaders th
join PolarisTransactions.polaris.TransactionTypes tt
	on tt.TransactionTypeID = th.TransactionTypeID
where tt.TransactionTypeDescription like '%ill%request%' and th.TranClientDate between @beginDate and @endDate
group by tt.TransactionTypeDescription

select count(*) [Outgoing Requests]
from PolarisTransactions.Polaris.TransactionHeaders th
join PolarisTransactions.polaris.TransactionDetails td
	on td.TransactionID = th.TransactionID and td.TransactionSubTypeID = 235 -- Subsystem type
where th.TransactionTypeID = 6005 and th.TranClientDate between @beginDate and @endDate and td.numValue = 39 -- InnReach


select max(ILLRequestID) from polaris.polaris.ILLRequests