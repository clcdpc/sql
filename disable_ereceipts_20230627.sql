/*
All SMS e-receipt patrons will be switched to email-only e-receipts.

If they don�t have an email address on file, we�ll disable e-receipts.
If a patron has both email & sms, switch to email only.
The text-based receipt option in the Polaris SA system will be disabled.
*/

begin tran

update pr
set pr.eReceiptOptionID = case when pr.EmailAddress is null then 0 else 2 end
from polaris.polaris.PatronRegistration pr
where pr.eReceiptOptionID in (8,100)

delete op
from polaris.polaris.OrganizationsPPPP op
where op.AttrID in (2883, 2889)

--rollback
--commit
