USE [CLC_Notices]
GO
/****** Object:  StoredProcedure [dbo].[CLC_Custom_Refresh_DatesClosed_For_Dialout]    Script Date: 8/15/2016 1:34:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Michael Fields
-- Create date: 4/2/2013
-- Description:	Clears the dates closed for CLC Electronic Library and then reinserts
--				dates that the specified number of libraries have branches closed.
-- =============================================
ALTER PROCEDURE [dbo].[CLC_Custom_Refresh_DatesClosed_For_Dialout]
	@orgid int, @threshold float
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--declare @libraries decimal = ( select count(*) from polaris.polaris.Organizations where OrganizationCodeID = 2 and OrganizationID != 4 )
	
 --   delete from polaris.polaris.DatesClosed where OrganizationID = @orgid

	--insert into polaris.polaris.DatesClosed
	--select	@orgid,
	--		DateClosed
	--from 
	--(
	--	select	DateClosed,
	--			COUNT(distinct o.ParentOrganizationID) as NumClosed
	--	from polaris.polaris.DatesClosed dc
	--	join polaris.polaris.Organizations o
	--		on dc.OrganizationID = o.OrganizationID
	--	where dc.DateClosed >= DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()), 0) -- first day of current year
	--	group by DateClosed
	--) as sub
	--where (NumClosed / @libraries) >= @threshold
	--order by DateClosed

END