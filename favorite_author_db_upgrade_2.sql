USE [AuthorSubscription]
GO

IF OBJECT_ID('dbo.AddPatronNote', 'P') IS NOT NULL DROP PROCEDURE [dbo].[CreateAuthorRecordSet]
IF OBJECT_ID('dbo.GetPatronAuthors', 'P') IS NOT NULL DROP PROCEDURE [dbo].[GetPatronAuthors]
IF OBJECT_ID('dbo.GetSubscriberCount', 'P') IS NOT NULL DROP PROCEDURE [dbo].[GetSubscriberCount]
GO

CREATE PROCEDURE [dbo].[CreateAuthorRecordSet]
	@orgId int,
	@authorId int
AS
BEGIN
	SET NOCOUNT ON;

	declare @rsname varchar(100);

	select @rsname = o.Abbreviation + ' - ' + a.FirstName + ' ' + a.LastName
	from polaris.polaris.Organizations o
	join AuthorSubscription.dbo.Authors a
		on a.Id = @authorId
	where o.OrganizationID = @orgId

	insert into polaris.polaris.RecordSets(Name, ObjectTypeID, CreatorID, OrganizationOwnerID, CreationDate, Note) values (@rsname, 27, 1, 1, getdate(), 'used for clc favorite author service')

	select cast(@@identity as int) [RecordSetID]
END
GO

CREATE PROCEDURE [dbo].[GetPatronAuthors]
	@barcode varchar(255)
AS
BEGIN
	SET NOCOUNT ON;

	declare @patronId int = ( select PatronID from polaris.polaris.patrons where Barcode = @barcode )

    select a.*
	from polaris.polaris.PatronRecordSets prs
	join polaris.polaris.patrons p
		on p.PatronID = @patronId
	join polaris.polaris.organizations o
		on o.OrganizationID = p.OrganizationID
	join AuthorSubscription.dbo.AuthorRecordSets ars
		on prs.RecordSetID = ars.RecordSetId and ars.OrganizationId = o.ParentOrganizationID
	join AuthorSubscription.dbo.Authors a 
		on a.Id = ars.AuthorId
	where prs.PatronID = @patronId
END
GO

CREATE PROCEDURE [dbo].[GetSubscriberCount]
	@authorId int
AS
BEGIN
	SET NOCOUNT ON;

	select count(*)
	from polaris.polaris.PatronRecordSets prs
	join AuthorSubscription.dbo.AuthorRecordSets ars
		on prs.RecordSetID = ars.RecordSetId
	where ars.AuthorId = @authorId
END
GO
